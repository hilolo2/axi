﻿namespace Axiobat.Presentation.Controllers.Configuration
{
    using Application.Services.AccountManagement;
    using Application.Services.Localization;
    using Application.Services.Configuration;
    using Domain.Entities;
    using Microsoft.Extensions.Logging;
    using Microsoft.AspNetCore.Mvc;
    using System.Collections.Generic;
    using System.Net.Http.Headers;
    using System.Threading.Tasks;
    using Axiobat.Application.Models;
    using App.Common;
    using Axiobat.Application.Enums;
    using System.IO;
    using Axiobat.Application.Services.Maintenance;
    using System.Linq;

    [Route("api/[controller]")]
    public partial class PublishingController
    {
        /// <summary>
        /// Upload document
        /// </summary>
        /// <returns>return IEnumrable of tags</returns>
        [Route("ExtractTagsFromDocument")]
        [HttpPost, DisableRequestSizeLimit]
        [Consumes("multipart/form-data")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(500)]
        public ActionResult<ListResult<string>> ExtractTagsFromDocument()
        {
            if(Request.Form is null || Request.Form.Files.Count == 0)
                return BadRequest(Result.Failed("you must supply file"));

            var file = Request.Form.Files[0];
            if (file.Length <= 0)
                return BadRequest(Result.Failed("you must supply file"));

            var orginalFileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
            _service.ValidateFileName(orginalFileName);

            using (var stream = file.OpenReadStream())
            using (var memoryStrem = new MemoryStream())
            {
                stream.CopyTo(memoryStrem);
                var tags = _service.ExtractTagsFromDocument(memoryStrem);
                return Result.ListSuccess(tags);
            }
        }

        /// <summary>
        /// Upload contract
        /// </summary>
        /// <returns>return filename</returns>
        [Route("Upload")]
        [HttpPost, DisableRequestSizeLimit]
        [Consumes("multipart/form-data")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<Result<string>>> Upload()
        {
            // the file we want to upload it
            var file = Request.Form.Files[0];
            if (file.Length <= 0)
                return BadRequest(Result.Failed("you must supply file"));

            // original file name
            var orginalFileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
            _service.ValidateFileName(orginalFileName);

            using (var stream = file.OpenReadStream())
            {
                var uploadResult = await _service.SaveContractAsync(stream, orginalFileName);
                return Result.Success<string>(uploadResult);
            }
        }

        /// <summary>
        /// get the list of publishing contract as paged Result
        /// </summary>
        /// <param name="filterModel">the filter options</param>
        /// <returns>a paged result</returns>
        [HttpPost]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<PagedResult<PublishingContractModel>>> Get(
            [FromBody] FilterOptions filterModel)
            => ActionResultForAsync(_service.GetAsPagedResultAsync(filterModel));

        /// <summary>
        /// create a new publishing contract record
        /// </summary>
        /// <returns>the newly created publishing contract</returns>
        [HttpPost("create")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result<PublishingContractModel>>> Create(
            [FromBody] PublishingContractCreateModel model)
            => ActionResultForAsync(_service.CreateAsync(model));

        /// <summary>
        /// update the publishing contract with the given model
        /// </summary>
        /// <param name="contractId">the id of the type task to be updated</param>
        /// <param name="model">the update model</param>
        /// <returns>the updated type task</returns>
        [HttpPut("{id:int}/Update")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result<PublishingContractModel>>> Update(
            [FromRoute(Name = "id")] int contractId,
            [FromBody] PublishingContractUpdateModel model)
            => ActionResultForAsync(_service.UpdateAsync(contractId, model));

        /// <summary>
        /// delete the publishing contract with the given id
        /// </summary>
        /// <param name="id">the id of the publishing contract to be deleted</param>
        /// <returns>an operation result object</returns>
        [HttpDelete("{id:int}/delete")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result>> Delete(
            [FromRoute(Name = "id")] int contractId)
            => ActionResultForAsync(_service.DeleteAsync(contractId));

        /// <summary>
        /// download document
        /// </summary>
        /// <param name="model">the download body</param>
        /// <returns></returns>
        [HttpGet("Download/{id:int}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> Download(
            [FromRoute(Name = "id")] int contractId)
        {
            var result = await _service.DownloadContractAsync(contractId);
            return File(result.Data, result.ContentType, result.Filename);
        }

        /// <summary>
        /// download document
        /// </summary>
        /// <param name="model">the download body</param>
        /// <returns></returns>
        [HttpGet("Download/{publishingId:int}/MaintenanceContract/{maintenanceContractId}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> Download(
            [FromRoute(Name = "maintenanceContractId")] string MaintenanceContractId,
            [FromRoute(Name = "publishingId")] int publishingId,
            [FromServices] IMaintenanceContractService maintenanceContractService)
        {
            var contract = await maintenanceContractService.GetByIdAsync<MaintenanceContract>(MaintenanceContractId);
            var publishing = contract.Value.PublishingContracts.FirstOrDefault(e => e.Id == publishingId);
            if (publishing is null)
            {
                return BadRequest(Result.Failed("$there is no publishing with id [{contractId}]"));
            }

            var result = await _service.DownloadContractAsync(publishing);
            return File(result.Data, result.ContentType, result.Filename);
        }
    }

    public partial class PublishingController : BaseController<PublishingContract>
    {
        private readonly IPublishingService _service;

        public PublishingController(
            IPublishingService publishingService,
            ILoggedInUserService loggedInUserService,
            ITranslationService translationService,
            ILoggerFactory loggerFactory)
            : base(loggedInUserService, translationService, loggerFactory)
        {
            _service = publishingService;
        }
    }
}