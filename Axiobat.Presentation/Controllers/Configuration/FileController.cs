﻿namespace Axiobat.Presentation.Controllers.Configuration
{
    using Application.Models;
    using Application.Services.AccountManagement;
    using Application.Services.FileService;
    using Application.Services.Localization;
    using Application.Exceptions;
    using Domain.Entities;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    /// <summary>
    /// the file management API controller
    /// </summary>
    [Route("api/[controller]")]
    public partial class FileController
    {
        /// <summary>
        /// use this action to save the given file to server, the given id is required and should be unique or he will overrider any existing
        /// </summary>
        /// <param name="fileManagerModels">the file model</param>
        /// <returns>the operation result</returns>
        [HttpPost("Save")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<Result>> CreateAsync([FromBody] List<FileModel> fileManagerModels)
            => ActionResultFor(await _service.SaveAsync(fileManagerModels));

        /// <summary>
        /// use this action to return the file with the given name
        /// </summary>
        /// <param name="fileId">the id of the file to retrieve</param>
        /// <returns>the file</returns>
        [HttpGet("{fileId}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<Result<string>>> GetAsync([FromRoute] string fileId)
            => ActionResultFor(await _service.GetAsync(fileId));

        /// <summary>
        /// delete the file with the given name
        /// </summary>
        /// <param name="fileId">the id of the file to retrieve</param>
        /// <returns>an operation result</returns>
        [HttpDelete("{fileId}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<Result>> DeleteAsync([FromRoute] string fileId)
            => ActionResultFor(await _service.DeleteAsync(fileId));

        /// <summary>
        /// save the given memo to the given entity
        /// </summary>
        /// <param name="model">the memo to be saved</param>
        /// <returns>the operation result</returns>
        [HttpPost("{id}/Create/Memo")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result<Memo>>> SaveMemoAsync(
            [FromRoute(Name = "id")] string entityId,
            [FromBody] MemoModel model)
        {
            var _service = GetMemoService(entityId);
            return ActionResultForAsync(_service.SaveMemoAsync(entityId, model));
        }

        /// <summary>
        /// delete the memo from the entity using the given model
        /// </summary>
        /// <param name="entityId">the id of the client</param>
        /// <param name="model">the <see cref="DeleteMemoModel"/> model</param>
        /// <returns>the operation result</returns>
        [HttpPost("{id}/Delete/Memo")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result>> DeleteMemoAsync(
            [FromRoute(Name = "id")] string entityId,
            [FromBody] DeleteMemoModel model)
        {
            var _service = GetMemoService(entityId);
            return ActionResultForAsync(_service.DeleteMemosAsync(entityId, model));
        }

        /// <summary>
        /// update the memo from the entity using the given model
        /// </summary>
        /// <param name="entityId">the id of the client</param>
        /// <param name="memoId">the id of the memo</param>
        /// <param name="model">the <see cref="MemoModel"/> model</param>
        /// <returns>the operation result</returns>
        [HttpPost("{id}/Update/Memo/{memoId}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result<Memo>>> UpdateMemoAsync(
            [FromRoute(Name = "id")] string entityId,
            [FromRoute(Name = "memoId")] string memoId,
            [FromBody] MemoModel model)
        {
            var _service = GetMemoService(entityId);
            return ActionResultForAsync(_service.UpdateMemoAsync(entityId, memoId, model));
        }
    }

    /// <summary>
    /// partial part <see cref="FileController"/>
    /// </summary>
    public partial class FileController : BaseController
    {
        private readonly IFileService _service;

        public FileController(
            IFileService fileService,
            ILoggedInUserService loggedInUserService,
            ITranslationService translationService,
            ILoggerFactory loggerFactory)
            : base(loggedInUserService, translationService, loggerFactory)
        {
            _service = fileService;
        }

        private IMemoService GetMemoService(string entityId)
        {
            var documentType = EntityId.Get(entityId).DocType;
            var entityType = Helper.GetEntityType(documentType);
            if (entityType is null)
                throw new ValidationException($"there is no type defined for this document type [{documentType}]", MessageCode.InternalError);

            var serviceType = entityType.GetEntityService();
            if (serviceType is null)
                throw new ValidationException($"there is no service defined for doctype [{documentType}]", MessageCode.InternalError);

            if (HttpContext.RequestServices.GetService(serviceType) is IMemoService service)
                return service;

            throw new ValidationException($"the [{nameof(serviceType)}] service doesn't have a memo service associated with it", MessageCode.InternalError);
        }
    }
}