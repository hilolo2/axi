﻿namespace Axiobat.Presentation.Controllers.Configuration
{
    using Application.Models;
    using Application.Services.AccountManagement;
    using Application.Services.Localization;
    using Application.Services.Configuration;
    using Domain.Entities;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;
    using System.Threading.Tasks;
    using Axiobat.Application.Services.Contacts;
    using Axiobat.Application.Services.FileService;
    using Microsoft.AspNetCore.Http;
    using System.Net.Http.Headers;
    using System.IO;
    using Axiobat.Application.Models.DataImport;
    using System.Collections.Generic;
    using Axiobat.Domain.Entities.Configuration;
    using Axiobat.Domain.Constants;
    using System.Linq;
    using Axiobat.Domain.Enums;
    using App.Common;
    using Axiobat.Application.Data;

    /// <summary>
    /// the controller for importing data
    /// </summary>
    [Route("api/[controller]")]
    public partial class ImportController
    {
        /// <summary>
        /// import list of clients
        /// </summary>
        /// <returns>the import result</returns>
        [HttpPost("Clients"), DisableRequestSizeLimit]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(500)]
        [Consumes("multipart/form-data")]
        public async Task<ActionResult<Result>> Import(
            [FromServices] IClientDataAccess clientService,
            [FromServices] IApplicationConfigurationService configuration,
            [FromServices] IChartOfAccountsService chartOfAccountsService)
        {
            if (Request.Form is null || Request.Form.Files.Count == 0)
                return BadRequest(Result.Failed("you must supply file"));

            var file = Request.Form.Files[0];

            if (file.Length <= 0)
                return BadRequest(Result.Failed("you must supply file"));

            var orginalFileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');

            using (var stream = file.OpenReadStream())
            using (var memoryStrem = new MemoryStream())
            {
                stream.CopyTo(memoryStrem);

                var charts = await chartOfAccountsService.GetChartOfAccountsAsync();
                var defaultClientCode = charts[ChartOfAccounts.General, ChartAccountItem.Clients].Code;

                var numerators = await configuration.GetAsync<IEnumerable<Numerator>>(ApplicationConfigurationType.Counter);
                var clientNumerator = numerators.First(e => e.DocumentType == DocumentType.SupplierOrder);

                var clientColumnsDefinitions = new ClientColumnsDefinitions
                {
                    Reference = null,
                    AccountingCode = null,
                    ClientCode = 1,
                    FirstName = 2,
                    LastName = null,
                    PhoneNumber = 6,
                    Email = null,
                    Website = null,
                    Siret = null,
                    IntraCommunityVAT = null,
                    ClientType = null,
                    Address_Designation = 3,
                    Address_Street = 3,
                    Address_City = 5,
                    Address_PostalCode = 4,
                    Address_Country = null,
                };

                var clients = _service.GetClientsFromFile(
                    memoryStrem, 2,
                    clientNumerator,
                    defaultClientCode,
                    clientColumnsDefinitions,
                    "A");

                await clientService.AddRangeAsync(clients, false);

                // save the numerator
                await configuration.UpdateAsync(ApplicationConfigurationType.Counter, numerators.ToJson());

                return Result.Success();
            }
        }

        /// import list of Products
        /// </summary>
        /// <returns>the import result</returns>
        [HttpPost("Products"), DisableRequestSizeLimit]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(500)]
        [Consumes("multipart/form-data")]
        public async Task<ActionResult<Result>> Import(
            [FromServices] IProductDataAccess productService,
            [FromServices] IUsersManagementService usersManagementService,
            [FromServices] ISupplierService supplierService)
        {
            if (Request.Form is null || Request.Form.Files.Count == 0)
                return BadRequest(Result.Failed("you must supply file"));

            var file = Request.Form.Files[0];

            if (file.Length <= 0)
                return BadRequest(Result.Failed("you must supply file"));

            var orginalFileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');

            using (var stream = file.OpenReadStream())
            using (var memoryStrem = new MemoryStream())
            {
                stream.CopyTo(memoryStrem);

                var productColumnsDefinitions = new ProductColumnsDefinitions
                {
                    Reference = 1,
                    Designation = 2,
                    MaterialCost = 4,
                    VAT = 6,
                    Unite = 7,
                    TotalHours = 8,
                    HourlyCost = 9,
                    Description = null,                    
                    Classification = 11,
                    ProductCategoryTypeId = 12,
                    RefSupplier = 3,
                    PriceSupplier = 5,

                };
                var suppliers = await supplierService.GetSuppliersForImportAsync();
                var products = _service.GetProductsFromFile(
                    memoryStrem, 2,
                    await usersManagementService.GetAdminUserAsync(),
                    productColumnsDefinitions,
                    suppliers,
                    "A");

                await productService.AddRangeAsync(products, false);

                return Result.Success();
            }
        }
    }

    /// <summary>
    /// partial part for <see cref="ImportController"/>
    /// </summary>
    public partial class ImportController : BaseController
    {
        private readonly IDataImportService _service;

        public ImportController(
            IDataImportService dataImportService,
            ILoggedInUserService loggedInUserService,
            ITranslationService translationService,
            ILoggerFactory loggerFactory)
            : base(loggedInUserService, translationService, loggerFactory)
        {
            _service = dataImportService;
        }
    }
}