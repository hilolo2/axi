﻿namespace Axiobat.Presentation.Controllers
{
    using Application.Models;
    using Application.Services.AccountManagement;
    using Application.Services.Localization;
    using Domain.Entities;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;
    using Presentation.AuthManagement;
    using System;
    using System.Threading.Tasks;

    /// <summary>
    /// the account controller
    /// </summary>
    [Route("api/[controller]")]
    public partial class AccountController : BaseController<User>
    {
        #region Users management

        /// <summary>
        /// get the list of users as paged Result
        /// </summary>
        /// <param name="service">the user management service</param>
        /// <param name="filterModel">the filter options</param>
        /// <returns>a paged result</returns>
        [HttpPost]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<PagedResult<UserModel>>> Get([FromBody]UserFilterOptions filterModel)
            => ActionResultForAsync(_service.GetAsPagedResultAsync<UserModel, UserFilterOptions>(filterModel));

        /// <summary>
        /// get the user with the given Id
        /// </summary>
        /// <param name="service">the user management service</param>
        /// <param name="id">the id of the user</param>
        /// <returns>return the user</returns>
        [HttpGet("{id:Guid}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result<UserModel>>> Get([FromRoute] Guid id)
            => ActionResultForAsync(_service.GetByIdAsync<UserModel>(id));

        /// <summary>
        /// create a new user from the given module
        /// </summary>
        /// <param name="service">the user management service</param>
        /// <param name="userModel">the user model</param>
        /// <returns>the newly created user</returns>
        [AllowAnonymous]
        [HttpPost("Create")]
        [ProducesResponseType(201)]
        [ProducesResponseType(400)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result<UserModel>>> Create([FromBody] UserCreateModel userModel)
            => ActionResultForAsync(_service.CreateAsync<UserModel, UserCreateModel>(userModel));

        /// <summary>
        /// delete the user with the given Id
        /// </summary>
        /// <param name="service">the user management service</param>
        /// <param name="userId">the id of the user to be deleted</param>
        /// <returns>an operation result</returns>
        [HttpDelete("{id:Guid}/Delete")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result>> Delete([FromRoute(Name = "id")] Guid userId)
            => ActionResultForAsync(_service.DeleteAsync(userId));

        /// <summary>
        /// update the user with the given id
        /// </summary>
        /// <param name="service">the user management service</param>
        /// <param name="userId">the id of the user to be updated</param>
        /// <param name="userModel">the user model</param>
        /// <returns>the updated version of the user</returns>
        [HttpPut("{id:Guid}/Update")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result<UserModel>>> UpdateUser(
            [FromRoute(Name = "id")] Guid userId,
            [FromBody] UserUpdateModel userModel)
            => ActionResultForAsync(_service.UpdateAsync<UserModel, UserUpdateModel>(userId, userModel));

        [HttpPut("Update/Password")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result>> UpdatePasswordUser(
            [FromBody] UpdateUserPasswordModel userModel)
            => ActionResultForAsync(_service.UpdateUserPassword(userModel));

        /// <summary>
        /// check if the given user name is unique
        /// </summary>
        /// <param name="service">the user management service</param>
        /// <param name="userName">the user name to be checked</param>
        /// <returns></returns>
        [HttpGet("check/userName/{userName}")]
        [ProducesResponseType(200)]
        public async Task<ActionResult<bool>> IsUserNameUnique(
            [FromRoute] string userName) => !await _service.IsUserNameExistAsync(userName);

        /// <summary>
        /// check if the given reference is unique, returns true if unique, false if not
        /// </summary>
        /// <param name="reference">the reference to be checked</param>
        /// <returns>true if unique, false if not</returns>
        [HttpGet("Check/Reference/{reference}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<bool>> CheckReferenceIfUnique([FromRoute(Name = "reference")] string reference)
            => await _service.IsRefrenceUniqueAsync(reference);

        #endregion

        #region Authentication

        /// <summary>
        /// use this to send an email confirmation URL
        /// </summary>
        /// <param name="email">the email to send the confirmation to it</param>
        /// <returns>the operation result</returns>
        [AllowAnonymous]
        [HttpGet("ConfirmEmail/{email}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<Result>> SendEmailConfirmationLink([FromRoute] string email)
        {
            // send the confirmation email to the user
            await _service.SendEmailConfirmationAsync(email);
            return Ok(Result.Success());
        }

        /// <summary>
        /// use this to confirm email
        /// </summary>
        /// <param name="model">the email confirmation model</param>
        /// <returns>the operation result</returns>
        [AllowAnonymous]
        [HttpPost("ConfirmEmail")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<Result>> ConfirmEmail([FromBody] UserConfirmEmailModel model)
        {
            // complete user email confirmation
            await _service.ConfirmUserEmailAsync(model);
            return Ok(Result.Success());
        }

        /// <summary>
        /// log the user in using the given login model
        /// </summary>
        /// <param name="loginModel">the login model</param>
        /// <returns>the result</returns>
        [AllowAnonymous]
        [HttpPost("Login")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(401)]
        [ProducesResponseType(403)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<Result<LoginModel>>> Login(
            [FromBody]UserLoginModel loginModel)
        {
            // validate the user logins
            var user = await _service.ValidateUserLoginAsync(loginModel);

            // sign the user in
            var token = await HttpContext.SignUserInAsync(user);

            // if the token is null or empty, something wrong happened
            if (string.IsNullOrEmpty(token))
                return StatusCode(500);

            // update the last time the user logged in
            await _service.UpdateLastTimeLoggedInAsync(user.Id);

            // build token model and return the right action result
            return ActionResultFor(Result.Success(new LoginModel { Token = token }));
        }

        /// <summary>
        /// update the user password
        /// </summary>
        /// <param name="UpdatePasswordModel">the password model</param>
        [HttpPut("UpdatePassword")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<Result>> UpdatePassword(UserUpdatePasswordModel UpdatePasswordModel)
        {
            await _service.UpdateUserPassword(UpdatePasswordModel);
            return Ok(Result.Success());
        }

        /// <summary>
        /// this action is used to implement forget password operation
        /// </summary>
        /// <param name="forgetPasswordModel">the forget password model</param>
        [AllowAnonymous]
        [HttpPut("ForgetPassword")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<Result>> ForgetPassword(UserForgetPasswordModel forgetPasswordModel)
        {
            await _service.HandleForgetPasswordAsync(forgetPasswordModel);
            return Ok(Result.Success());
        }

        /// <summary>
        /// this action is used to update the password using the token from forget password operation
        /// </summary>
        /// <param name="RestPasswordModel">>the rest password model</param>
        [AllowAnonymous]
        [HttpPut("RestPassword")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<Result>> RestPassword(UserRestPasswordModel RestPasswordModel)
        {
            // try to update the user password and return the proper action result
            await _service.RestPasswordAsync(RestPasswordModel);
            return Ok(Result.Success());
        }

        #endregion
    }

    /// <summary>
    /// partial part for <see cref="AccountController"/>
    /// </summary>
    public partial class AccountController
    {
        private readonly IUsersManagementService _service;

        /// <summary>
        /// create an instant of <see cref="ClientController"/>
        /// </summary>
        /// <param name="service">the <see cref="IAccountService"/> instant</param>
        /// <param name="loggerFactory">the <see cref="ILoggerFactory"/> instant</param>
        /// <param name="loggedInUserService">the <see cref="ILoggedInUserService"/> instant</param>
        /// <param name="translationService">the <see cref="ITranslationService"/> instant</param>
        public AccountController(
            IUsersManagementService usersManagementService,
            ILoggedInUserService loggedInUserService,
            ITranslationService translationService,
            ILoggerFactory loggerFactory)
            : base(loggedInUserService, translationService, loggerFactory)
        {
            _service = usersManagementService;
        }
    }
}