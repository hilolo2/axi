﻿namespace Axiobat.Presentation.Controllers
{
    using Application.Enums;
    using Application.Models;
    using Application.Services.AccountManagement;
    using Application.Services.Localization;
    using AuthManagement;
    using Domain.Entities;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;
    using System.Threading.Tasks;

    /// <summary>
    /// the base controller for common code reuse
    /// </summary>
    [ApiController]
    [Authorize]
    public class BaseController<TEntity> : BaseController
    {
        /// <summary>
        /// create an instant of <see cref="BaseController"/>
        /// </summary>
        /// <param name="loggerFactory">the logger factory instant</param>
        /// <param name="loggedInUserService">the <see cref="ILoggedInUserService"/> instant</param>
        /// <param name="translationService">the <see cref="ITranslationService"/> instant</param>
        public BaseController(
            ILoggedInUserService loggedInUserService,
            ITranslationService translationService,
            ILoggerFactory loggerFactory)
            : base(loggedInUserService, translationService, loggerFactory)
        {
            _logger = loggerFactory.CreateLogger($"{nameof(TEntity)}.controller");
        }
    }

    /// <summary>
    /// the base controller for common code reuse
    /// </summary>
    [ApiController]
    [Authorize]
    public class BaseController : ControllerBase
    {
        /// <summary>
        /// the service for obtaining the current logged in user informations
        /// </summary>
        protected readonly ILoggedInUserService _loggedInUserService;

        /// <summary>
        /// the translation service
        /// </summary>
        protected readonly ITranslationService _translationService;

        /// <summary>
        /// the logger service
        /// </summary>
        protected ILogger _logger;

        /// <summary>
        /// create an instant of <see cref="BaseController"/>
        /// </summary>
        /// <param name="loggerFactory">the logger factory instant</param>
        /// <param name="loggedInUserService">the <see cref="ILoggedInUserService"/> instant</param>
        /// <param name="translationService">the <see cref="ITranslationService"/> instant</param>
        public BaseController(
            ILoggedInUserService loggedInUserService,
            ITranslationService translationService,
            ILoggerFactory loggerFactory)
        {
            _logger = loggerFactory.CreateLogger<BaseController>();
            _loggedInUserService = loggedInUserService;
            _translationService = translationService;
        }

        /// <summary>
        /// this method is used to return the proper action result type
        /// </summary>
        /// <typeparam name="TResult">the type of the Result being processed</typeparam>
        /// <param name="result">the result to process</param>
        /// <returns>the proper Action Result base on the passed in Result</returns>
        public async Task<ActionResult<TResult>> ActionResultForAsync<TResult>(Task<TResult> taskResult)
            where TResult : Result => ActionResultFor(await taskResult);

        /// <summary>
        /// this method is used to return the proper action result type
        /// </summary>
        /// <typeparam name="TResult">the type of the Result being processed</typeparam>
        /// <param name="result">the result to process</param>
        /// <returns>the proper Action Result base on the passed in Result</returns>
        public ActionResult<TResult> ActionResultFor<TResult>(TResult result)
            where TResult : Result
        {
            // the operation has failed
            if (result.Status == ResultStatus.Failed)
            {
                // something went wrong (exception)
                if (result.HasError)
                    return StatusCode(500, result);

                // result not found
                if (!result.HasValue || result.MessageCode == MessageCode.NotFound)
                    return NotFound(result);

                // user is not authorized
                if (result.MessageCode.Equals(MessageCode.Unauthorized))
                    return StatusCode(StatusCodes.Status403Forbidden, result);

                //if nothing bad request
                return BadRequest(result);
            }

            // all set, return the operation result
            return Ok(result);
        }

        /// <summary>
        /// get the base URL, ex: "http://localhost:5000"
        /// </summary>
        /// <returns>the base URL</returns>
        protected string GetBaseUrl()
            => $"{Request.Scheme}://{Request.Host.ToUriComponent()}{Request.PathBase.ToUriComponent()}";

        /// <summary>
        /// this method will log the user in by generating a token and building the login model
        /// </summary>
        /// <param name="service">the account management service</param>
        /// <param name="result"></param>
        /// <returns>the <see cref="LoginModel"/></returns>
        protected async Task<Result<LoginModel>> LogUserInAsync(IUsersManagementService service, Result<(LoginModel loginModel,User user)> result)
        {
            if (!result.IsSuccess)
                return Result.From<LoginModel>(result);

            // sign the user in
            var token = await HttpContext.SignUserInAsync(result.Value.user);

            // if the token is null or empty, something wrong happened
            if (string.IsNullOrEmpty(token))
                return Result.Failed<LoginModel>("Failed to log user in");

            result.Value.loginModel.Token = token;
            return result.Value.loginModel;
        }

        /// <summary>
        /// retrieve the translation version of the text if not found the text it self will be returned
        /// </summary>
        /// <param name="text">the text to translate</param>
        /// <returns>the translation version of the text</returns>
        protected string T(string text)
            => _translationService.Get(text);
    }
}