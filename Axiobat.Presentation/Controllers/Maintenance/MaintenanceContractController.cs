﻿namespace Axiobat.Presentation.Controllers
{
    using Application.Enums;
    using Application.Models;
    using Application.Services.AccountManagement;
    using Application.Services.Localization;
    using Application.Services.Maintenance;
    using Domain.Entities;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    /// <summary>
    /// controller for managing <see cref="MaintenanceContract"/>
    /// </summary>
    [Route("api/[controller]")]
    public partial class MaintenanceContractController
    {
        /// <summary>
        /// get list of all Credit Notes
        /// </summary>
        /// <returns>list of all Credits</returns>
        [HttpGet]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<ListResult<MaintenanceContractListModel>>> GetAll()
            => ActionResultForAsync(_service.GetAllAsync<MaintenanceContractListModel>());

        /// <summary>
        /// get a paged result of the MaintenanceContracts list using the given <see cref="FilterOptions"/>
        /// </summary>
        /// <param name="filter">the filter options model</param>
        /// <returns>list of MaintenanceContracts as paged result</returns>
        [HttpPost]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<PagedResult<MaintenanceContractListModel>>> Get([FromBody] MaintenanceContractFilterOptions filter)
            => ActionResultForAsync(_service.GetAsPagedResultAsync<MaintenanceContractListModel, MaintenanceContractFilterOptions>(filter));

        /// <summary>
        /// retrieve MaintenanceContract with the given id
        /// </summary>
        /// <param name="MaintenanceContractId">the id of the MaintenanceContract to be retrieved</param>
        /// <returns>the MaintenanceContract</returns>
        [HttpGet("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result<MaintenanceContractModel>>> Get([FromRoute(Name = "id")] string MaintenanceContractId)
            => ActionResultForAsync(_service.GetByIdAsync<MaintenanceContractModel>(MaintenanceContractId));

        /// <summary>
        /// create a new MaintenanceContract record
        /// </summary>
        /// <param name="MaintenanceContractModel">the model to create the MaintenanceContract from it</param>
        /// <returns>the newly created MaintenanceContract</returns>
        [HttpPost("Create")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<Result<MaintenanceContractModel>>> Create(
            [FromBody] MaintenanceContractPutModel MaintenanceContractModel)
        {
            var result = await _service.CreateAsync<MaintenanceContractModel, MaintenanceContractPutModel>(MaintenanceContractModel);
            if (result.Status == ResultStatus.Failed)
            {
                // something went wrong (exception)
                if (result.HasError)
                    return StatusCode(500, result);

                // result not found
                if (!result.HasValue || result.MessageCode == MessageCode.NotFound)
                    return NotFound(result);

                // user is not authorized
                if (result.MessageCode.Equals(MessageCode.Unauthorized))
                    return StatusCode(StatusCodes.Status403Forbidden, result);

                //if nothing bad request
                return BadRequest(result);
            }

            return CreatedAtAction(nameof(Get), new { result.Value.Id }, result);
        }

        /// <summary>
        /// update the MaintenanceContract informations
        /// </summary>
        /// <param name="MaintenanceContractModel">the model to use for updating the MaintenanceContract</param>
        /// <param name="MaintenanceContractId">the id of the MaintenanceContract to be updated</param>
        /// <returns>the updated MaintenanceContract</returns>
        [HttpPut("{id}/Update")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result<MaintenanceContractModel>>> Update(
            [FromBody] MaintenanceContractPutModel MaintenanceContractModel, [FromRoute(Name = "id")] string MaintenanceContractId)
            => ActionResultForAsync(_service.UpdateAsync<MaintenanceContractModel, MaintenanceContractPutModel>(MaintenanceContractId, MaintenanceContractModel));

        /// <summary>
        /// update the MaintenanceContract informations
        /// </summary>
        /// <param name="MaintenanceContractId">the id of the MaintenanceContract to be updated</param>
        /// <param name="model">list of publishing contract to be update MaintenanceContract with it</param>
        /// <returns>the updated MaintenanceContract</returns>
        [HttpPut("{id}/Update/PublishingDocument")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(500)]
        public Task<ActionResult<ListResult<PublishingContractMinimal>>> Update(
            [FromRoute(Name = "id")] string MaintenanceContractId,
            [FromBody] ICollection<PublishingContractMinimal> model)
            => ActionResultForAsync(_service.UpdateContractPublishingDocumentAsync(MaintenanceContractId, model));

        /// <summary>
        /// update the Operation Sheet status
        /// </summary>
        /// <param name="model">the model to use for updating the Operation Sheet status</param>
        /// <param name="operationSheetId">the id of the Operation Sheet to be updated</param>
        /// <returns>the updated Operation Sheet</returns>
        [HttpPut("{id}/Update/Status")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result>> UpdateStatus([FromBody] DocumentUpdateStatusModel model,
            [FromRoute(Name = "id")] string contratMainId)
            => ActionResultForAsync(_service.UpdateStatusAsync(contratMainId, model));

        /// <summary>
        /// delete the Equipment Maintenance with the given id
        /// </summary>
        /// <param name="MaintenanceContractId">the id of the MaintenanceContract to be deleted</param>
        /// <returns>the operation result</returns>
        [HttpDelete("{id}/Delete")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result>> Delete([FromRoute(Name = "id")] string MaintenanceContractId)
            => ActionResultForAsync(_service.DeleteAsync(MaintenanceContractId));

        /// <summary>
        /// check if the given reference is unique, returns true if unique, false if not
        /// </summary>
        /// <param name="reference">the reference to be checked</param>
        /// <returns>true if unique, false if not</returns>
        [HttpGet("Check/Reference/{reference}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<bool>> CheckReferenceIfUnique([FromRoute(Name = "reference")] string reference)
            => await _service.IsRefrenceUniqueAsync(reference);

        /// <summary>
        /// export the Journal as an excel file in form of a byte array
        /// </summary>
        /// <param name="filterModel">the filter options</param>
        /// <returns>the exported file as a byte array</returns>
        [HttpGet("Export/{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result<byte[]>>> ExportJournal([FromRoute(Name = "id")] string MaintenanceContractI)
            => ActionResultForAsync(_service.ExportContratAsync(MaintenanceContractI));

        [HttpPost("{id}/Export")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result<byte[]>>> Export([FromRoute(Name = "id")] string MaintenanceContractI, [FromBody] DataExportOptions exportOptions)
             => ActionResultForAsync(_service.ExportDataAsync(MaintenanceContractI, exportOptions));

        /// <summary>
        /// duplicate the MaintenanceContract with the given id
        /// </summary>
        /// <param name="QuoteId">the id of the MaintenanceContract to be duplicated</param>
        /// <returns>the generated MaintenanceContract</returns>
        [HttpGet("Duplicate/{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result<MaintenanceContractModel>>> EmailDocument([FromRoute(Name = "id")] string MaintenanceContractId)
            => ActionResultForAsync(_service.DuplicateAsync<MaintenanceContractModel>(MaintenanceContractId));
    }

    /// <summary>
    /// partial part for <see cref="MaintenanceContractController"/>
    /// </summary>
    public partial class MaintenanceContractController : BaseController<MaintenanceContract>
    {
        private readonly IMaintenanceContractService _service;

        public MaintenanceContractController(
            IMaintenanceContractService service,
            ILoggedInUserService loggedInUserService,
            ITranslationService translationService,
            ILoggerFactory loggerFactory)
            : base(loggedInUserService, translationService, loggerFactory)
        {
            _service = service;
        }
    }
}