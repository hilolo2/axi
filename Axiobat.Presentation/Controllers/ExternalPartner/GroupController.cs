﻿namespace Axiobat.Presentation.Controllers.ExternalPartner
{
    using Application.Enums;
    using Application.Models;
    using Application.Services.AccountManagement;
    using Application.Services.Contacts;
    using Application.Services.Localization;
    using Domain.Entities;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;
    using System.Threading.Tasks;

    /// <summary>
    /// the group management API controller
    /// </summary>
    [Route("api/[controller]")]
    public partial class GroupController : BaseController<Group>
    {
        /// <summary>
        /// get list of all Groups
        /// </summary>
        /// <returns>list of all Groups</returns>
        [HttpGet]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<ListResult<GroupModel>>> GetAll()
            => ActionResultForAsync(_service.GetAllAsync<GroupModel>());

        /// <summary>
        /// get a paged result of the Groups list using the given <see cref="FilterOptions"/>
        /// </summary>
        /// <param name="filter">the filter options model</param>
        /// <returns>list of Groups as paged result</returns>
        [HttpPost]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<PagedResult<GroupModel>>> Get([FromBody] FilterOptions filter)
        {
            var result = await _service.GetAsPagedResultAsync<GroupModel, FilterOptions>(filter);

            return Ok(result);
        }

        /// <summary>
        /// retrieve Group with the given id
        /// </summary>
        /// <param name="GroupId">the id of the Group to be retrieved</param>
        /// <returns>the Group</returns>
        [HttpGet("{id:int}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<Result<GroupModel>>> Get(
            [FromRoute(Name = "id")] int GroupId)
        {
            return ActionResultFor(await _service.GetByIdAsync<GroupModel>(GroupId));
        }

        /// <summary>
        /// create a new Group record
        /// </summary>
        /// <param name="GroupModel">the model to create the Group from it</param>
        /// <returns>the newly created Group</returns>
        [HttpPost("Create")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<Result<GroupModel>>> Create(
            [FromBody] GroupPutModel GroupModel)
        {
            var result = await _service.CreateAsync<GroupModel, GroupPutModel>(GroupModel);
            if (result.Status == ResultStatus.Failed)
            {
                // something went wrong (exception)
                if (result.HasError)
                    return StatusCode(500, result);

                // result not found
                if (!result.HasValue || result.MessageCode == MessageCode.NotFound)
                    return NotFound(result);

                // user is not authorized
                if (result.MessageCode.Equals(MessageCode.Unauthorized))
                    return StatusCode(StatusCodes.Status403Forbidden, result);

                //if nothing bad request
                return BadRequest(result);
            }

            return CreatedAtAction(nameof(Get), new { result.Value.Id }, result);
        }

        /// <summary>
        /// update the Group informations
        /// </summary>
        /// <param name="GroupModel">the model to use for updating the Group</param>
        /// <param name="GroupId">the id of the Group to be updated</param>
        /// <returns>the updated Group</returns>
        [HttpPut("{id:int}/Update")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<Result<GroupModel>>> Update(
            [FromBody] GroupPutModel GroupModel,
            [FromRoute(Name = "id")] int GroupId)
        {
            // try update the Group, and return the right action result
            return ActionResultFor(await _service.UpdateAsync<GroupModel, GroupPutModel>(GroupId, GroupModel));
        }

        /// <summary>
        /// delete the Group with the given id
        /// </summary>
        /// <param name="GroupId">the id of the Group to be deleted</param>
        /// <returns>the operation result</returns>
        [HttpDelete("{id:int}/Delete")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<Result>> Delete([FromRoute(Name = "id")] int GroupId)
        {
            // try to delete the Group, and return the right action result
            return ActionResultFor(await _service.DeleteAsync(GroupId));
        }

        /// <summary>
        /// check if the given group name is unique or not
        /// </summary>
        /// <param name="groupName">the name of the group</param>
        /// <returns>the operation result</returns>
        [HttpGet("IsNameUnique/{GroupName}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<bool>> IsGroupNameUnique([FromRoute(Name = "GroupName")] string groupName)
        {
            // try to delete the Group, and return the right action result
            return Ok(await _service.IsUniqueGroupNameAsync(groupName));
        }

    }

    public partial class GroupController : BaseController<Group>
    {
        private readonly IGroupService _service;

        public GroupController(
            IGroupService groupService,
            ILoggedInUserService loggedInUserService,
            ITranslationService translationService,
            ILoggerFactory loggerFactory)
            : base(loggedInUserService, translationService, loggerFactory)
        {
            _service = groupService;
        }
    }
}