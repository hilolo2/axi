﻿namespace Axiobat.Presentation.Controllers.ExternalPartner
{
    using Application.Models;
    using Application.Services.AccountManagement;
    using Application.Services.Contacts;
    using Application.Services.Localization;
    using Application.Enums;
    using Domain.Entities;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;
    using System.Threading.Tasks;

    /// <summary>
    /// the Supplier management API controller
    /// </summary>
    [Route("api/[controller]")]
    public partial class SupplierController : BaseController<Supplier>
    {
        /// <summary>
        /// get list of all Suppliers
        /// </summary>
        /// <returns>list of all Suppliers</returns>
        [HttpGet]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<ListResult<SupplierModel>>> GetAll()
            => ActionResultForAsync(_service.GetAllAsync<SupplierModel>());

        /// <summary>
        /// get a paged result of the Suppliers list using the given <see cref="FilterOptions"/>
        /// </summary>
        /// <param name="filter">the filter options model</param>
        /// <returns>list of Suppliers as paged result</returns>
        [HttpPost]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<PagedResult<SupplierModel>>> Get([FromBody] FilterOptions filter)
            => ActionResultForAsync(_service.GetAsPagedResultAsync<SupplierModel, FilterOptions>(filter));

        /// <summary>
        /// retrieve Supplier with the given id
        /// </summary>
        /// <param name="SupplierId">the id of the Supplier to be retrieved</param>
        /// <returns>the Supplier</returns>
        [HttpGet("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result<SupplierModel>>> Get([FromRoute(Name = "id")] string SupplierId)
            => ActionResultForAsync(_service.GetByIdAsync<SupplierModel>(SupplierId));

        /// <summary>
        /// retrieve Supplier products
        /// </summary>
        /// <param name="SupplierId">the id of the Supplier to retrieve his products</param>
        /// <returns>the Supplier products</returns>
        [HttpGet("Products/{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<ListResult<ProductModel>>> GetSupplierProducts([FromRoute(Name = "id")] string SupplierId)
            => ActionResultForAsync(_service.GetSupplierProductsAsync<ProductModel>(SupplierId));

        /// <summary>
        /// create a new Supplier record
        /// </summary>
        /// <param name="SupplierModel">the model to create the Supplier from it</param>
        /// <returns>the newly created Supplier</returns>
        [HttpPost("Create")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<Result<SupplierModel>>> Create(
            [FromBody] SupplierPutModel SupplierModel)
        {
            var result = await _service.CreateAsync<SupplierModel, SupplierPutModel>(SupplierModel);
            if (result.Status == ResultStatus.Failed)
            {
                // something went wrong (exception)
                if (result.HasError)
                    return StatusCode(500, result);

                // result not found
                if (!result.HasValue || result.MessageCode == MessageCode.NotFound)
                    return NotFound(result);

                // user is not authorized
                if (result.MessageCode.Equals(MessageCode.Unauthorized))
                    return StatusCode(StatusCodes.Status403Forbidden, result);

                //if nothing bad request
                return BadRequest(result);
            }

            return CreatedAtAction(nameof(Get), new { result.Value.Id }, result);
        }

        /// <summary>
        /// update the Supplier informations
        /// </summary>
        /// <param name="SupplierModel">the model to use for updating the Supplier</param>
        /// <param name="SupplierId">the id of the Supplier to be updated</param>
        /// <returns>the updated Supplier</returns>
        [HttpPut("{id}/Update")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result<SupplierModel>>> Update([FromRoute(Name = "id")] string SupplierId, [FromBody] SupplierPutModel SupplierModel) =>
            ActionResultForAsync(_service.UpdateAsync<SupplierModel, SupplierPutModel>(SupplierId, SupplierModel));

        /// <summary>
        /// delete the Supplier with the given id
        /// </summary>
        /// <param name="SupplierId">the id of the Supplier to be deleted</param>
        /// <returns>the operation result</returns>
        [HttpDelete("{id}/Delete")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result>> Delete([FromRoute(Name = "id")] string SupplierId)
            => ActionResultForAsync(_service.DeleteAsync(SupplierId));

        /// <summary>
        /// check if the given reference is unique, returns true if unique, false if not
        /// </summary>
        /// <param name="reference">the reference to be checked</param>
        /// <returns>true if unique, false if not</returns>
        [HttpGet("Check/Reference/{reference}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<bool>> CheckReferenceIfUnique([FromRoute(Name = "reference")] string reference)
            => await _service.IsRefrenceUniqueAsync(reference);
    }

    /// <summary>
    /// partial part for <see cref="SupplierController"/>
    /// </summary>
    public partial class SupplierController
    {
        private readonly ISupplierService _service;

        /// <summary>
        /// create an instant of <see cref="SupplierController"/>
        /// </summary>
        /// <param name="service">the <see cref="ISupplierService"/> instant</param>
        /// <param name="loggedInUserService">the <see cref="ILoggedInUserService"/> instant</param>
        /// <param name="translationService">the <see cref="ITranslationService"/> instant</param>
        /// <param name="loggerFactory">the <see cref="ILoggerFactory"/> instant</param>
        public SupplierController(ISupplierService service, ILoggedInUserService loggedInUserService, ITranslationService translationService, ILoggerFactory loggerFactory) : base(loggedInUserService, translationService, loggerFactory)
        {
            _service = service;
        }
    }
}