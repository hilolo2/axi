﻿namespace Axiobat.Presentation.Controllers
{
    using Application.Enums;
    using Application.Models;
    using Application.Services.AccountManagement;
    using Application.Services.Documents;
    using Application.Services.Localization;
    using Domain.Entities;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;
    using System.Threading.Tasks;

    /// <summary>
    /// the CreditNote API Controller
    /// </summary>
    [Route("api/[controller]")]
    public partial class CreditNoteController : BaseController<CreditNote>
    {
        /// <summary>
        /// get list of all Credit Notes
        /// </summary>
        /// <returns>list of all Credits</returns>
        [HttpGet]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<ListResult<CreditNoteListModel>>> GetAll()
            => ActionResultForAsync(_service.GetAllAsync<CreditNoteListModel>());

        /// <summary>
        /// get a paged result of the CreditNotes list using the given <see cref="FilterOptions"/>
        /// </summary>
        /// <param name="filter">the filter options model</param>
        /// <returns>list of CreditNotes as paged result</returns>
        [HttpPost]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<PagedResult<CreditNoteListModel>>> Get([FromBody] DocumentFilterOptions filter)
            => ActionResultForAsync(_service.GetAsPagedResultAsync<CreditNoteListModel, DocumentFilterOptions>(filter));

        /// <summary>
        /// retrieve CreditNote with the given id
        /// </summary>
        /// <param name="CreditNoteId">the id of the CreditNote to be retrieved</param>
        /// <returns>the CreditNote</returns>
        [HttpGet("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result<CreditNoteModel>>> Get([FromRoute(Name = "id")] string CreditNoteId)
            => ActionResultForAsync(_service.GetByIdAsync<CreditNoteModel>(CreditNoteId));


        /// <summary>
        /// retrieve Invoice with the given id
        /// </summary>
        /// <param name="CreditNoteId">the id of the CreditNote to be retrieved</param>
        /// <returns>the Quote</returns>
        [HttpPost("{id}/Email")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result>> EmailDocument([FromRoute(Name = "id")] string CreditNoteId, [FromBody] SendEmailOptions emailOptions)
            => ActionResultForAsync(_service.SendAsync(CreditNoteId, emailOptions));

        /// <summary>
        /// create a new CreditNote record
        /// </summary>
        /// <param name="CreditNoteModel">the model to create the CreditNote from it</param>
        /// <returns>the newly created CreditNote</returns>
        [HttpPost("Create")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<Result<CreditNoteModel>>> Create(
            [FromBody] CreditNotePutModel CreditNoteModel)
        {
            var result = await _service.CreateAsync<CreditNoteModel, CreditNotePutModel>(CreditNoteModel);
            if (result.Status == ResultStatus.Failed)
            {
                // something went wrong (exception)
                if (result.HasError)
                    return StatusCode(500, result);

                // result not found
                if (!result.HasValue || result.MessageCode == MessageCode.NotFound)
                    return NotFound(result);

                // user is not authorized
                if (result.MessageCode.Equals(MessageCode.Unauthorized))
                    return StatusCode(StatusCodes.Status403Forbidden, result);

                //if nothing bad request
                return BadRequest(result);
            }

            return CreatedAtAction(nameof(Get), new { result.Value.Id }, result);
        }

        /// <summary>
        /// update the CreditNote informations
        /// </summary>
        /// <param name="CreditNoteModel">the model to use for updating the CreditNote</param>
        /// <param name="CreditNoteId">the id of the CreditNote to be updated</param>
        /// <returns>the updated CreditNote</returns>
        [HttpPut("{id}/Update")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result<CreditNoteModel>>> Update([FromBody] CreditNotePutModel CreditNoteModel, [FromRoute(Name = "id")] string CreditNoteId)
            => ActionResultForAsync(_service.UpdateAsync<CreditNoteModel, CreditNotePutModel>(CreditNoteId, CreditNoteModel));

        /// <summary>
        /// delete the CreditNote with the given id
        /// </summary>
        /// <param name="CreditNoteId">the id of the CreditNote to be deleted</param>
        /// <returns>the operation result</returns>
        [HttpDelete("{id}/Delete")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result>> Delete([FromRoute(Name = "id")] string CreditNoteId)
            => ActionResultForAsync(_service.DeleteAsync(CreditNoteId));

        /// <summary>
        /// check if the given reference is unique, returns true if unique, false if not
        /// </summary>
        /// <param name="reference">the reference to be checked</param>
        /// <returns>true if unique, false if not</returns>
        [HttpGet("Check/Reference/{reference}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<bool>> CheckReferenceIfUnique([FromRoute(Name = "reference")] string reference)
            => await _service.IsRefrenceUniqueAsync(reference);

        /// <summary>
        /// duplicate the CreditNote with the given id
        /// </summary>
        /// <param name="CreditNoteId">the id of the CreditNote to be duplicated</param>
        /// <returns>the generated CreditNote</returns>
        [HttpGet("Duplicate/{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result<CreditNoteModel>>> EmailDocument([FromRoute(Name = "id")] string CreditNoteId)
            => ActionResultForAsync(_service.DuplicateAsync<CreditNoteModel>(CreditNoteId));

        /// <summary>
        /// retrieve Invoice with the given id
        /// </summary>
        /// <param name="InvoiceID">the id of the Invoice to be retrieved</param>
        /// <returns>the Quote</returns>
        [HttpPost("{id}/Export")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result<byte[]>>> Export([FromRoute(Name = "id")] string CreditNoteID, [FromBody] DataExportOptions exportOptions)
            => ActionResultForAsync(_service.ExportDataAsync(CreditNoteID, exportOptions));
    }

    /// <summary>
    /// partial part for <see cref="CreditNoteController"/>
    /// </summary>
    public partial class CreditNoteController : BaseController<CreditNote>
    {
        private readonly ICreditNoteService _service;

        public CreditNoteController(
            ICreditNoteService creditNoteService,
            ILoggedInUserService loggedInUserService,
            ITranslationService translationService,
            ILoggerFactory loggerFactory)
            : base(loggedInUserService, translationService, loggerFactory)
        {
            _service = creditNoteService;
        }
    }
}