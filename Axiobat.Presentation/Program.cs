﻿namespace Axiobat.Presentation
{
    using Microsoft.AspNetCore;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.Extensions.Hosting;
    using Serilog;

    /// <summary>
    /// the program class that defines the main entry point for the application
    /// </summary>
    public static class Program
    {
        /// <summary>
        /// the main entry point of the application
        /// </summary>
        /// <param name="args">the arguments pass to the application</param>
        public static void Main(string[] args)
        {
            try
            {
                CreateWebHostBuilder(args)
                    .Build()
                    .Initialize()
                    .Run();
            }
            catch (System.Exception ex)
            {
                Log.Error(ex, "app is terminated with an exception");
            }
        }

        /// <summary>
        /// this function create a web host builder
        /// </summary>
        /// <param name="args">the arguments pass to the application</param>
        /// <returns>a web host builder</returns>
        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .UseUrls("http://localhost:5050")
                .UseSerilog();
    }
}
