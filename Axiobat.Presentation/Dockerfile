FROM mcr.microsoft.com/dotnet/core/sdk:2.1 AS build
WORKDIR /app

# copy csproj and restore as distinct layers
COPY *.sln .

COPY Axiobat.Domain/*.csproj ./Axiobat.Domain/
COPY Axiobat.Application/*.csproj ./Axiobat.Application/
COPY Axiobat.Presentation/*.csproj ./Axiobat.Presentation/
COPY Axiobat.Infrastructure/*.csproj ./Axiobat.Infrastructure/
COPY Axiobat.Persistence/*.csproj ./Axiobat.Persistence/
COPY Axiobat.Services/*.csproj ./Axiobat.Services/
COPY Axiobat.Test/*.csproj ./Axiobat.Test/

RUN dotnet restore  -s https://api.nuget.org/v3/index.json -s http://demo.artinove.net/packages/nuget

# copy everything else and build app
COPY Axiobat.Presentation/. ./Axiobat.Presentation/
COPY Axiobat.Infrastructure/. ./Axiobat.Infrastructure/
COPY Axiobat.Persistence/. ./Axiobat.Persistence/
COPY Axiobat.Services/. ./Axiobat.Services/
COPY Axiobat.Domain/. ./Axiobat.Domain/
COPY Axiobat.Application/. ./Axiobat.Application/
COPY Axiobat.Test/. ./Axiobat.Test/

WORKDIR /app
RUN dotnet publish -c Release -o out

FROM mcr.microsoft.com/dotnet/core/aspnet:2.1 AS runtime
WORKDIR /app

COPY --from=build /app/Axiobat.Presentation/out ./
ENTRYPOINT ["dotnet", "Axiobat.Presentation.dll"]