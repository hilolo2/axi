﻿namespace Axiobat.Presentation
{
    using App.Common;
    using Axiobat.Presentation.BackgroundServices;
    using Coravel;
    using CustomMiddlewares;
    using FluentValidation.AspNetCore;
    using Infrastructure;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Newtonsoft.Json.Serialization;
    using Persistence;
    using Presentation.AuthManagement;
    using Presentation.CustomFilters;
    using Presentation.Models.Validations;
    using Serilog;
    using Services;
    using Swashbuckle.AspNetCore.Swagger;
    using Swashbuckle.AspNetCore.SwaggerUI;
    using System;
    using System.IO;
    using System.Reflection;

    /// <summary>
    /// the startup class
    /// </summary>
    public partial class Startup
    {
        /// <summary>
        /// This method gets called by the runtime. Use this method to add services to the container.
        /// </summary>
        /// <param name="services">the service collection</param>
        public void ConfigureServices(IServiceCollection services)
        {
            var (secrets, settings) = GetGlobalConfiguration(services);

            ConfigureSwagger(services);
            ConfigureLogger(secrets.ConnectionStrings.Serilog);
            ConfigureApplicationsServices(services, secrets, settings);

            services.AddCors();
            services.AddHttpClient();
            services.AddHttpContextAccessor();
            services.ConfigureBackgroundService();

            services.AddMvc(options =>
            {
                options.Filters.Add<GlobalExceptionFilter>();
                options.Filters.Add(new ProducesAttribute("application/json"));
                options.Filters.Add(new ConsumesAttribute("application/json"));
            })
            .AddFluentValidation(config =>
            {
                config.RunDefaultMvcValidationAfterFluentValidationExecutes = false;
                config.RegisterValidatorsFromAssemblyContaining(typeof(BaseValidator<>));
            })
            .AddJsonOptions(options =>
            {
                options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
                options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            })
            .SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
        }

        /// <summary>
        /// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        /// </summary>
        /// <param name="app">the application builder</param>
        /// <param name="env">the hosting environment</param>
        public static void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCors(builder =>
            {
                builder
                    .AllowAnyHeader()
                    .AllowAnyMethod()
                    .SetIsOriginAllowed(_ => true)
                    .AllowCredentials();
            });

            app.UseSwaggerDisplay();
            app.UseAuthentication();
            app.UseStaticFiles();
            app.UseMvc();
        }
    }

    /// <summary>
    /// partial part <see cref="Startup"/>
    /// </summary>
    public partial class Startup
    {
        /// <summary>
        /// the configuration instant
        /// </summary>
        public IConfiguration Configuration { get; }

        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="configuration">the configuration object</param>
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        /// <summary>
        /// the application services configurations
        /// </summary>
        /// <param name="services">the services instant</param>
        /// <param name="secrets">the application secrets</param>
        /// <param name="settings">the application settings</param>
        private static void ConfigureApplicationsServices(IServiceCollection services, ApplicationSecrets secrets, ApplicationSettings settings)
        {
            services.AddAppConfiguration(settings.ProjectId)
                .AddPresentation()
                .AddServices()
                .AddInfrastructure(options =>
                {
                    options.EmailServiceURL = settings.SharedServicesConfigurations.EmailServiceURL;
                    options.FileServiceURL = settings.SharedServicesConfigurations.FileServiceURL;
                })
                .AddAuthConfig(options =>
                {
                    options.AuthenticationTokenExpiration = settings.AuthenticationOptions.AuthenticationTokenExpiration;
                    options.Audience = settings.AuthenticationOptions.Audience;
                    options.Issuer = settings.AuthenticationOptions.Issuer;
                    options.SignInKey = secrets.Authentication.SecretKey;
                })
                .AddPersistence(options =>
                {
                    options.DatabaseConnectionString = secrets.ConnectionStrings.ApplicationDatabase;
                });
        }

        /// <summary>
        /// get and register the Secrets and global configurations instants
        /// </summary>
        /// <param name="services">the services collection</param>
        /// <returns>the <see cref="ApplicationSecrets"/> and <see cref="ApplicationSettings"/> instants</returns>
        private (ApplicationSecrets secrets, ApplicationSettings settings) GetGlobalConfiguration(IServiceCollection services)
        {
            // set the globalConfig
            services.Configure<ApplicationSecrets>(Configuration.GetSection("ApplicationSecrets"));
            services.Configure<ApplicationSettings>(Configuration.GetSection("ApplicationSettings"));

            var secrets = new ApplicationSecrets();
            var globalConfig = new ApplicationSettings();

            Configuration.Bind("ApplicationSettings", globalConfig);
            Configuration.Bind("ApplicationSecrets", secrets);

            return (secrets, globalConfig);
        }

        /// <summary>
        /// configure Swagger for API Documentation
        /// </summary>
        /// <param name="services">the DI service Collection</param>
        internal void ConfigureSwagger(IServiceCollection services)
        {
            services.AddSwaggerGen(c =>
            {
                // Configure the XML comments file path for the Swagger JSON and UI.
                var xmlFile = Path.Combine(AppContext.BaseDirectory, $"{Assembly.GetExecutingAssembly().GetName().Name}.xml");
                if (File.Exists(xmlFile))
                    c.IncludeXmlComments(xmlFile);

                c.DescribeAllEnumsAsStrings();
                c.DescribeStringEnumsInCamelCase();
                c.SwaggerDoc("v1", new Info
                {
                    Version = "v3",
                    Title = "Axiobat API",
                    Description = @"ONLINE INVOICING SOFTWARE FOR SMALL BUSINESS.
Quickly create your bill and quote and manage your expenses online.",
                    TermsOfService = "http://www.Axiobat.fr/mentions-legales.html",
                    Contact = new Contact()
                    {
                        Email = "contact@Axiobat.fr",
                        Url = "https://www.Axiobat.fr/",
                        Name = "Axiobat"
                    }
                });

                c.AddSecurityDefinition("Bearer", new ApiKeyScheme
                {
                    Description = "JWT Authorization header using the Bearer scheme. Example: \"Bearer {token}\"",
                    Name = "Authorization",
                    In = "header",
                    Type = "apiKey"
                });

                c.OperationFilter<SecurityOperationFilter>();
            });
        }

        /// <summary>
        /// this method is user to configure the logger
        /// </summary>
        internal void ConfigureLogger(string connectionString)
        {
            Log.Logger = new LoggerConfiguration()
                .Destructure.ToMaximumDepth(4)
                .ReadFrom.Configuration(Configuration)
                .WriteTo.Console(Serilog.Events.LogEventLevel.Debug)
                .WriteTo.MySQL(
                    connectionString: connectionString,
                    restrictedToMinimumLevel: Serilog.Events.LogEventLevel.Warning)
                .CreateLogger();
        }
    }
}
