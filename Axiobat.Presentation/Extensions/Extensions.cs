﻿namespace Axiobat.Presentation
{
    using App.Common;
    using Axiobat.Presentation.BackgroundServices;
    using Coravel;
    using Coravel.Scheduling.Schedule.Interfaces;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Http.Internal;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Persistence.DataContext;
    using Swashbuckle.AspNetCore.SwaggerUI;
    using System.IO;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// a class for adding application extensions and register application dependencies
    /// </summary>
    internal static class Extensions
    {
        /// <summary>
        /// get the service from the given service scope
        /// </summary>
        /// <typeparam name="TService">the type of the service to retrieve</typeparam>
        /// <param name="scope">the scope service instant</param>
        /// <returns>the service instant</returns>
        internal static TService GetService<TService>(this IServiceScope scope)
            => scope.ServiceProvider.GetRequiredService<TService>();

        /// <summary>
        /// get the content from the body of the request as string
        /// </summary>
        /// <param name="request">the request instant</param>
        /// <returns>the body as string</returns>
        internal static string GetContent(this HttpRequest request)
            => GetContentAsync(request).GetAwaiter().GetResult();

        /// <summary>
        /// get the content from the body of the request as string
        /// </summary>
        /// <param name="request">the request instant</param>
        /// <returns>the body as string</returns>
        internal static async Task<string> GetContentAsync(this HttpRequest request)
        {
            var body = string.Empty;
            request.EnableRewind();
            using (var memoryStream = new MemoryStream())
            {
                await request.Body.CopyToAsync(memoryStream);
                var bodyAsArray = memoryStream.ToArray();
                memoryStream.Position = 0;
                request.Body.Position = 0;
                body = Encoding.UTF8.GetString(bodyAsArray, 0, bodyAsArray.Length);
            }

            return body;
        }

        /// <summary>
        /// add swagger UI integration
        /// </summary>
        /// <param name="app">the <see cref="IApplicationBuilder"/> instant</param>
        /// <returns>the <see cref="IApplicationBuilder"/> instant</returns>
        public static IApplicationBuilder UseSwaggerDisplay(this IApplicationBuilder app)
        {
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.RoutePrefix = string.Empty;
                c.DocExpansion(DocExpansion.None);

                var baseUrl = !c.RoutePrefix.IsValid() ? "." : "..";
                c.SwaggerEndpoint($"{baseUrl}/swagger/v1/swagger.json", "V1");
            });

            return app;
        }

        /// <summary>
        /// this method is used to automatically Initialize the required data and configurations of the application
        /// </summary>
        /// <param name="webHost">the web-host instant</param>
        internal static IWebHost Initialize(this IWebHost webHost)
        {
            using (var serviceScope = webHost.Services.CreateScope())
            {
                // extract services
                var configuration = serviceScope.GetService<IConfiguration>();
                var context = serviceScope.GetService<ApplicationDbContext>();
                var seedData = configuration.GetValue<bool>("SeedData");
                var updateEquipmentIds = configuration.GetValue<bool>("UpdateEquipmentIds");

                // apply pending migration
                context.Database.Migrate();

                // seed data
                context.SeedData(seedData, updateEquipmentIds);

                // register background services
                webHost.RegesterBackgroundService();

                // return web-host instant
                return webHost;
            }
        }

        /// <summary>
        /// register the background services
        /// </summary>
        private static void RegesterBackgroundService(this IWebHost webHost)
        {
            webHost.Services.UseScheduler(scheduler =>
            {
                scheduler.Schedule<TurnoverGoalGeneratorBackgroundService>()
                    .EndOfYear();

                scheduler.Schedule<RecurringDocumentBackgroundService>()
                    .DailyAt(23, 30);
            });
        }

        public static IScheduledEventConfiguration EndOfYear(this IScheduleInterval interval)
            => interval.Cron($"0 23 31 12 *");

        /// <summary>
        /// configure background services
        /// </summary>
        /// <param name="services">the services collections</param>
        internal static void ConfigureBackgroundService(this IServiceCollection services)
        {
            // first register the background services
            services.AddTransient<TurnoverGoalGeneratorBackgroundService>();
            services.AddTransient<RecurringDocumentBackgroundService>();

            // register the background services with Coravel Manager
            services.AddScheduler();
        }
    }
}