﻿namespace Axiobat.Presentation.Models
{
    using Application.Enums;
    using Application.Models;
    using Application.Services.Configuration;
    using Microsoft.Extensions.Options;
    using System;
    using System.Threading.Tasks;

    /// <summary>
    /// the implementation for <see cref="IApplicationSecretAccessor"/>
    /// </summary>
    public partial class ApplicationSecretAccessor
    {
        /// <summary>
        /// get the connection string based on the given type
        /// </summary>
        /// <param name="type">the type of the connection string</param>
        /// <returns>the connection string value</returns>
        public Task<string> GetAppDbConnectionStringAsync(ConnectionStringType type)
        {
            switch (type)
            {
                case ConnectionStringType.ApplicationDb:
                    return Task.FromResult(_secrets.ConnectionStrings.ApplicationDatabase);
                default:
                    throw new NotImplementedException("the given connection string type is not implemented");
            }
        }

        /// <summary>
        /// get the Google Calendar Options
        /// </summary>
        /// <returns>the <see cref="GoogleCalendarOptions"/> instant</returns>
        public Task<GoogleCalendarOptions> GetGoogleCalendarOptions() => Task.FromResult(_secrets.GoogleCalendar);

        /// <summary>
        /// get the authentication SignInKey
        /// </summary>
        /// <returns>the SignInKey value</returns>
        public Task<string> GetSignInKeyAsync() => Task.FromResult(_secrets.Authentication.SecretKey);
    }

    /// <summary>
    /// partial part for <see cref="ApplicationSecretAccessor"/>
    /// </summary>
    public partial class ApplicationSecretAccessor : IApplicationSecretAccessor
    {
        private readonly IDisposable _secretsChangedDisposable;
        private ApplicationSecrets _secrets;

        /// <summary>
        /// create an instant of <see cref="ApplicationSecretAccessor"/>
        /// </summary>
        /// <param name="options">the options monitor instant</param>
        public ApplicationSecretAccessor(IOptionsMonitor<ApplicationSecrets> options)
        {
            _secrets = options.CurrentValue;
            _secretsChangedDisposable = options.OnChange(e => _secrets = e);
        }

        /// <summary>
        /// the object destructor
        /// </summary>
        ~ApplicationSecretAccessor()
        {
            _secretsChangedDisposable?.Dispose();
        }
    }
}
