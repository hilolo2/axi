﻿namespace Axiobat.Presentation.Models.Validations
{
    using Application.Models;
    using Application.Services.AccountManagement;
    using FluentValidation;
    using Microsoft.Extensions.Logging;
    using System;
    using System.Threading;
    using System.Threading.Tasks;

    /// <summary>
    /// the update model for <see cref="UserUpdateModel"/>
    /// </summary>
    public class UserUpdateModelValidation : BaseValidator<UserUpdateModel>
    {
        private readonly IRoleManagementService _roleService;

        public UserUpdateModelValidation(
            IRoleManagementService roleService,
            ILoggerFactory loggerFactory)
            : base(loggerFactory)
        {
            _roleService = roleService;

            RuleFor(e => e.RoleId)
                .MustAsync(RoleExistAsync)
                    .WithMessage("the specified role does not exist!")
                    .WithErrorCode(MessageCode.RoleNotExist);
        }

        private Task<bool> RoleExistAsync(Guid roleId, CancellationToken cancellationToken)
            => _roleService.IsRoleExistAsync(roleId);
    }
}

