﻿namespace Axiobat.Presentation.Models.Validations
{
    using App.Common;
    using DnsClient;
    using FluentValidation;
    using System;
    using System.Linq;
    using System.Net.Mail;
    using System.Threading;
    using System.Threading.Tasks;

    /// <summary>
    /// this class hold the validator extensions
    /// </summary>
    public static class ValidatorExtensions
    {
        /// <summary>
        /// specifies a custom error code to use if validation fails.
        /// </summary>
        /// <param name="rule"> The current rule</param>
        /// <param name="MessageCode">The error code to use</param>
        /// <returns>the <see cref="IRuleBuilderOptions{T, TProperty}"/></returns>
        public static IRuleBuilderOptions<T, TProperty> WithErrorCode<T, TProperty>(this IRuleBuilderOptions<T, TProperty> rule, MessageCode MessageCode)
            => rule.WithErrorCode(Code(MessageCode));

        /// <summary>
        /// get the <see cref="MessageCode"/> value as string
        /// </summary>
        /// <param name="MessageCode">the <see cref="MessageCode"/> value</param>
        /// <returns>the string value of the enum</returns>
        public static string Code(MessageCode MessageCode)
            => ((int)MessageCode).ToString();

        /// <summary>
        /// validate the email property
        /// </summary>
        /// <typeparam name="T">the type of the entity being validated</typeparam>
        /// <param name="ruleBuilder">the rule builder</param>
        /// <param name="maxLength">the max email length</param>
        /// <returns>the rule builder instant</returns>
        public static IRuleBuilderOptions<T, string> IsValidEmail<T>(this IRuleBuilder<T, string> ruleBuilder, int maxLength = 50)
        {
            return ruleBuilder
                .MaximumLength(maxLength)
                    .WithErrorCode(MessageCode.EmailIsTooLong)
                    .WithMessage($"only {maxLength} characters are allowed")
                .NotEmpty()
                    .WithErrorCode(MessageCode.EmailIsRequired)
                    .WithMessage("Email is required")
                .NotNull()
                    .WithErrorCode(MessageCode.EmailIsRequired)
                    .WithMessage("Email is required")
                .MustAsync(HasValidHostAsync)
                    .WithErrorCode(MessageCode.InvalidEmailHost)
                    .WithMessage("the email has invalid host")
                .Must(e => RegexHelper.IsValidEmailAddress(e))
                    .WithErrorCode(MessageCode.InvalidEmailFormat)
                    .WithMessage("the email has an invalid format");
        }

        /// <summary>
        /// use this validator to validate the email host
        /// </summary>
        /// <param name="email">the email to validate</param>
        /// <param name="customContext">the <see cref="CustomContext"/> instant</param>
        /// <param name="cancellationToken">the <see cref="CancellationToken"/> instant</param>
        private static Task<bool> HasValidHostAsync(string email, CancellationToken cancellationToken)
        {
            return Task.FromResult(true);

//            try
//            {
//                if (cancellationToken.IsCancellationRequested)
//                    return false;

//                var mailAddress = new MailAddress(email);
//                var host = mailAddress.Host;

//                var lookup = new LookupClient
//                {
//                    Timeout = TimeSpan.FromSeconds(5)
//                };

//                return (await lookup
//                   .QueryAsync(host, QueryType.ANY)
//                   .ConfigureAwait(false))?
//                   .Answers?
//                   .Where(record =>
//                       record.RecordType == DnsClient.Protocol.ResourceRecordType.A ||
//                       record.RecordType == DnsClient.Protocol.ResourceRecordType.AAAA ||
//                       record.RecordType == DnsClient.Protocol.ResourceRecordType.MX)?
//                   .Any() ?? false;
//            }
//#pragma warning disable CA1031 // Do not catch general exception types
//            catch (Exception)
//            {
//                return false;
//            }
//#pragma warning restore CA1031 // Do not catch general exception types
        }
    }
}
