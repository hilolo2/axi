﻿namespace Axiobat.Presentation.Models.Validations
{
    using App.Common;
    using Application.Models;
    using Domain.Constants;
    using FluentValidation;
    using Microsoft.Extensions.Logging;

    /// <summary>
    /// the <see cref="DataExportOptions"/> validation
    /// </summary>
    public class DataExportOptionsValidation : BaseValidator<DataExportOptions>
    {
        /// <summary>
        /// generate an instant of <see cref="DataExportOptions"/>
        /// </summary>
        /// <param name="loggerFactory">the logger factory</param>
        public DataExportOptionsValidation(ILoggerFactory loggerFactory)
            : base(loggerFactory)
        {
            //When(e => e.DateStart.IsValid(), () => {
            //    RuleFor(e => e.DateStart)
            //        .Must(e => e.IsValidDate())
            //            .WithErrorCode(MessageCode.invalidDateFormat)
            //            .WithMessage("you must supply a valid date format, 'yyyy/MM/dd'");
            //});

            //When(e => e.DateEnd.IsValid(), () => {
            //    RuleFor(e => e.DateEnd)
            //        .Must(e => e.IsValidDate())
            //            .WithErrorCode(MessageCode.invalidDateFormat)
            //            .WithMessage("you must supply a valid date format, 'yyyy/MM/dd'");
            //});

            //When(e => e.Status.IsValid(), () => {
            //    RuleFor(e => e.Status)
            //        .Must(e => DocumentStatus.ValidStatus(e))
            //            .WithErrorCode(MessageCode.InvalidDocumentStatus)
            //            .WithMessage("the given status is not a valid document status");
            //});
        }
    }
}
