﻿namespace Axiobat.Presentation.Models.Validations
{
    using Application.Models;
    using Axiobat.Application.Services;
    using Domain.Entities;
    using Microsoft.Extensions.Logging;

    /// <summary>
    /// the base validator for all documents
    /// </summary>
    /// <typeparam name="TDocument">the type of the document</typeparam>
    public class BaseDocumentModelValidation<TDocument, TEntity> : BaseValidator<TDocument>
        where TDocument : DocumentPutModel<TEntity>
        where TEntity : Document
    {
        private readonly IConstructionWorkshopService _workshopService;

        /// <summary>
        /// create and instant of <seealso cref="BaseDocumentModelValidation"/>
        /// </summary>
        /// <param name="loggerFactory"></param>
        public BaseDocumentModelValidation(ILoggerFactory loggerFactory, IConstructionWorkshopService workshopService)
            : base(loggerFactory)
        {
            _workshopService = workshopService;

        }
    }
}
