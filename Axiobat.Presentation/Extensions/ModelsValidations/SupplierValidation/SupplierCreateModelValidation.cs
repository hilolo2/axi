﻿namespace Axiobat.Presentation.Models.Validations
{
    using App.Common;
    using Application.Models;
    using FluentValidation;
    using Microsoft.Extensions.Logging;

    /// <summary>
    /// supplier model validation
    /// </summary>
    public class SupplierCreateModelValidation : BaseValidator<SupplierModel>
    {
        /// <summary>
        /// default constructor
        /// </summary>
        public SupplierCreateModelValidation(
            ILoggerFactory logger)
            : base(logger)
        {
            RuleFor(e => e.Reference)
                .NotEmpty()
                    .WithErrorCode(MessageCode.ReferenceIsRequired)
                    .WithMessage("Reference is required")
                .NotNull()
                    .WithErrorCode(MessageCode.ReferenceIsRequired)
                    .WithMessage("Reference is required");

            RuleFor(e => e.FirstName)
                .NotEmpty()
                    .WithErrorCode(MessageCode.NameIsRequired)
                    .WithMessage("supplier Name is required")
                .NotNull()
                    .WithErrorCode(MessageCode.NameIsRequired)
                    .WithMessage("supplier Name is required");

            When(e => e.Email.IsValid(), () =>
            {
                RuleFor(e => e.Email)
                    .IsValidEmail();
            });
        }
    }
}
