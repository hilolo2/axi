﻿namespace Axiobat.Presentation.Models.Validations
{
    using App.Common;
    using Application.Models;
    using FluentValidation;
    using Microsoft.Extensions.Logging;

    /// <summary>
    /// validator for <see cref="SendEmailOptions"/>
    /// </summary>
    public class SendEmailModelValidation : BaseValidator<SendEmailOptions>
    {
        /// <summary>
        /// default constructor
        /// </summary>
        /// <param name="logger">the logger</param>
        public SendEmailModelValidation(ILoggerFactory logger)
            : base(logger)
        {
            RuleFor(e => e.Subject)
                .NotNull()
                    .WithErrorCode(MessageCode.EmailSubjectIsRequired)
                    .WithMessage("Email Subject is required")
               .NotEmpty()
                    .WithErrorCode(MessageCode.EmailSubjectIsRequired)
                    .WithMessage("Email Subject is required");

            RuleFor(e => e.Body)
                .NotNull()
                    .WithErrorCode(MessageCode.EmailBodyIsRequired)
                    .WithMessage("Email Body is required")
               .NotEmpty()
                    .WithErrorCode(MessageCode.EmailBodyIsRequired)
                    .WithMessage("Email Body is required");

            RuleFor(e => e.To)
                .NotEmpty()
                    .WithErrorCode(MessageCode.EmailIsRequired)
                    .WithMessage("'To' Email is required")
                .NotNull()
                    .WithErrorCode(MessageCode.EmailIsRequired)
                    .WithMessage("'To' Email is required")
                .ForEach(e => e.IsValidEmail());

            When(e => e.From.IsValid(), () =>
            {
                RuleFor(e => e.From)
                    .IsValidEmail();
            });

            When(e => e.Attachments.Count > 0, () =>
            {
                RuleForEach(e => e.Attachments)
                    .SetValidator(new AttachmentModelValidation(logger));
            });
        }
    }
}
