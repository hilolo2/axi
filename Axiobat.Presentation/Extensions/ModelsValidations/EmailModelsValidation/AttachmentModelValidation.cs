﻿namespace Axiobat.Presentation.Models.Validations
{
    using App.Common;
    using Application.Models;
    using FluentValidation;
    using Microsoft.Extensions.Logging;

    /// <summary>
    /// the PieceJoin model validation
    /// </summary>
    public class AttachmentModelValidation : BaseValidator<AttachmentModel>
    {
        /// <summary>
        /// the default constructor
        /// </summary>
        /// <param name="loggerFactory">the logger service</param>
        public AttachmentModelValidation(ILoggerFactory loggerFactory)
            : base(loggerFactory)
        {
            //RuleFor(e => e.Content)
            //    .NotNull()
            //        .WithErrorCode(MessageCode.FileMustHaveContent)
            //        .WithMessage("file must have a valid base64 content")
            //    .NotEmpty()
            //        .WithErrorCode(MessageCode.FileMustHaveContent)
            //        .WithMessage("file must have a valid base64 content");

            RuleFor(e => e.FileType)
                .NotNull()
                    .WithErrorCode(MessageCode.FileTypeIsRequired)
                    .WithMessage("the file type is required")
                .NotEmpty()
                    .WithErrorCode(MessageCode.FileTypeIsRequired)
                    .WithMessage("the file type is required")
                .Must(t => FileHelper.IsMimeTypeValid(t))
                    .WithErrorCode(MessageCode.FileMimeTypeIsNotValid)
                    .WithMessage("the give type is not a valid mime type");

            RuleFor(e => e.FileName)
                .NotNull()
                    .WithErrorCode(MessageCode.FileNameIsRequired)
                    .WithMessage("the file name is required")
                .NotEmpty()
                    .WithErrorCode(MessageCode.FileNameIsRequired)
                    .WithMessage("the file name is required")
                .Must(f => FileHelper.FileNameHasExtention(f))
                    .WithErrorCode(MessageCode.FileNameWithExtensionIsRequired)
                    .WithMessage("the file name must have a valid extension (ex: .pdf, .png, etc)");

            When(e => FileHelper.IsMimeTypeValid(e.FileType) && FileHelper.FileNameHasExtention(e.FileName), () =>
            {
                RuleFor(f => f.FileType)
                    .Must((f, t) => FileHelper.IsValidMimeTypeForExtention(t, f.FileName))
                        .WithErrorCode(MessageCode.FileExtensionDontMatchMimeType)
                        .WithMessage("the given extension don't match the given file mime type (ex: application/pdf, image/png, etc)");
            });
        }
    }
}
