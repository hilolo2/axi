﻿namespace Axiobat.Presentation.Models.Validations
{
    using Application.Models;
    using FluentValidation;
    using Microsoft.Extensions.Logging;

    /// <summary>
    /// the memo model validation
    /// </summary>
    public class MemoModelValidation : BaseValidator<MemoModel>
    {
        /// <summary>
        /// the default constructor
        /// </summary>
        /// <param name="loggerFactory">the logger service</param>
        public MemoModelValidation(ILoggerFactory loggerFactory)
            : base(loggerFactory)
        {
            When(e => e.Attachments.Count > 0, () =>
            {
                RuleForEach(e => e.Attachments)
                    .SetValidator(new AttachmentModelValidation(loggerFactory));
            });
        }
    }
}
