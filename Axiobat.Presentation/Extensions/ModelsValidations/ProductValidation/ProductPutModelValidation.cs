﻿namespace Axiobat.Presentation.Models.Validations
{
    using Application.Models;
    using Axiobat.Application.Services.Products;
    using FluentValidation;
    using Microsoft.Extensions.Logging;

    public class ProductPutModelValidation : BaseValidator<ProductPutModel>
    {
        public ProductPutModelValidation(
            ILoggerFactory loggerFactory,
            IProductService productService)
            : base(loggerFactory)
        {
            RuleFor(e => e.ProductCategoryTypeId)
                .NotNull()
                    .WithMessage("you must supply a valid ProductCategoryTypeId")
                    .WithErrorCode(MessageCode.RequiredValue)
                .NotEmpty()
                    .WithMessage("you must supply a valid ProductCategoryTypeId")
                    .WithErrorCode(MessageCode.RequiredValue)
                .MustAsync((e, c) => productService.IsProductCategoryTypeExistAsync(e))
                    .WithMessage("the given product category type not exist")
                    .WithErrorCode(MessageCode.NotFound);
        }
    }
}
