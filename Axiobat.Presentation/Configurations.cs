﻿namespace Axiobat.Presentation
{
    using Application.Services.Configuration;
    using Application.Services.Localization;
    using Microsoft.Extensions.DependencyInjection;
    using Presentation.Models;

    /// <summary>
    /// the Configurations class for registering the layer services
    /// </summary>
    public static class Configurations
    {
        /// <summary>
        /// add the services from the services layer
        /// </summary>
        /// <param name="builder">the extensions builder instant</param>
        /// <returns><see cref="ConfigurationsBuilder"/> instant</returns>
        public static ConfigurationsBuilder AddPresentation(this ConfigurationsBuilder builder)
        {
            builder.Services.AddSingleton<ITranslationService, TranslationService>();
            builder.Services.AddSingleton<IApplicationSecretAccessor, ApplicationSecretAccessor>();
            builder.Services.AddSingleton<IApplicationSettingsAccessor, ApplicationSettingsAccessor>();
            return builder;
        }
    }
}
