﻿namespace Axiobat.Services.AccountManagement
{
    using App.Common;
    using Application.Data;
    using Application.Exceptions;
    using Application.Models;
    using Application.Services.AccountManagement;
    using Application.Services.Configuration;
    using Application.Services.FileService;
    using Application.Services.Localization;
    using Application.Services.MailService;
    using AutoMapper;
    using Domain.Entities;
    using Domain.Enums;
    using Microsoft.AspNetCore.Identity;
    using Microsoft.Extensions.Logging;
    using Services.AccountManagement.Utilities;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// this service is used to manage users
    /// </summary>
    public partial class UsersManagementService
    {
        /// <summary>
        /// check if the given email is in use by another user, if exist or not
        /// </summary>
        /// <param name="email">email to validate</param>
        /// <returns>true if exist, false if not</returns>
        public Task<bool> IsUserEmailExistAsync(string email)
            => _userDataAccess.IsExistAsync(e => e.NormalizedEmail == NormalizeKey(email));

        /// <summary>
        /// check if the given userName is in use by another user
        /// </summary>
        /// <param name="userName"></param>
        /// <returns>true if unique, false if not</returns>
        public Task<bool> IsUserNameExistAsync(string userName)
            => _userDataAccess.IsExistAsync(e => e.NormalizedUserName == NormalizeKey(userName));

        /// <summary>
        /// get the admin company
        /// </summary>
        /// <returns>the user instant</returns>
        public Task<User> GetAdminUserAsync() => _dataAccess.GetAdminUserAsync();

        /// <summary>
        /// send an email confirmation URL to the user with the given email
        /// </summary>
        /// <param name="email">the email to confirm</param>
        /// <returns>the operation result</returns>
        public async Task SendEmailConfirmationAsync(string email)
        {
            var user = await _userDataAccess.GetByEmailAsync(NormalizeKey(email));
            if (user is null)
            {
                _logger.LogDebug(LogEvent.SendingConfiramtionEmail, "a request for sending a confirmation email has been request form a non existed user");
                return; ;
            }

            var token = await _userManager.GenerateEmailConfirmationTokenAsync(user);
            await _mailService.SendConfirmEmailUrlAsync(user, token);
            _logger.LogDebug(LogEvent.SendingConfiramtionEmail, "confirmation email has been sent successfully");
        }

        /// <summary>
        /// confirm the user email
        /// </summary>
        /// <param name="model">the email confirmation model</param>
        /// <returns>the operation result</returns>
        public async Task ConfirmUserEmailAsync(UserConfirmEmailModel model)
        {
            var user = await _userDataAccess.GetByIdAsync(model.UserId);
            if (user is null)
            {
                _logger.LogDebug(LogEvent.ConfirmEmail, "Failed to retrieve the user with [id: {userId}]", model.UserId);
                throw new NotFoundException("the user with given id not exist");
            }

            var result = await _userManager.ConfirmEmailAsync(user, model.Token);
            if (!result.Succeeded)
            {
                _logger.LogWarning(LogEvent.ConfirmEmail, "Failed to validate the email confirmation token, validation result: {@result}", result);

                if (result.Errors.Any(e => e.Code.Equals("InvalidToken", StringComparison.OrdinalIgnoreCase)))
                    throw new ValidationException("Invalid Token", MessageCode.InvalidToken);

                return;
            }

            _logger.LogInformation(LogEvent.ConfirmEmail, "the email has been confirmed successfully for user with id: [{idUser}]", user.Id);
        }

        /// <summary>
        /// Handel user forget password, send an email with a URL contains a token to update the password
        /// </summary>
        /// <param name="forgetPasswordModel">the password model</param>
        /// <returns>the operation result</returns>
        public async Task HandleForgetPasswordAsync(UserForgetPasswordModel forgetPasswordModel)
        {
            var user = await _userDataAccess.GetByEmailAsync(NormalizeKey(forgetPasswordModel.Email));
            if (user is null)
            {
                _logger.LogWarning(LogEvent.ForgetPassword, "the user with email [{userEmail}] is not exist and hi is trying to make a forget password operation", forgetPasswordModel.Email);
                throw new NotFoundException("the user with given email not exist");
            }

            var token = await _userManager.GeneratePasswordResetTokenAsync(user);
            await _mailService.SendRestPasswordEmailAsync(user, token);
            _logger.LogWarning(LogEvent.ForgetPassword, "forget password email has been sent successfully to [{email}]", forgetPasswordModel.Email);
        }

        /// <summary>
        /// update the user password
        /// </summary>
        /// <param name="model">a model that hold the update requirement</param>
        /// <returns>a result instant</returns>
        public async Task UpdateUserPassword(UserUpdatePasswordModel model)
        {
            var user = await _userDataAccess.GetByIdAsync(_loggedInUserService.User.UserId);
            if (user is null)
            {
                _logger.LogDebug(LogEvent.ConfirmEmail, "Failed to retrieve the user with [id: {userId}]", _loggedInUserService.User.UserId);
                throw new NotFoundException("the user with given id not exist");
            }

            var updateResult = await _userManager.ChangePasswordAsync(user, model.OldPassword, model.NewPassword);
            if (!updateResult.Succeeded)
            {
                _logger.LogWarning(LogEvent.UpdatePassword, "Failed to validate the password, validation result: {@result}", updateResult);
                if (updateResult.Errors.Any(e => e.Code.Equals("PasswordMismatch", StringComparison.OrdinalIgnoreCase)))
                    throw new ValidationException("the given current password doesn't match the current one in database", MessageCode.PasswordMismatch);

                return;
            }

            _logger.LogInformation(LogEvent.UpdatePassword, "Password updated successfully for user with [id= {userId}]", _loggedInUserService.User.UserId);
        }

        /// <summary>
        /// rest the user password using the given rest model
        /// </summary>
        /// <param name="model">the rest password model</param>
        /// <returns>the operation result</returns>
        public async Task RestPasswordAsync(UserRestPasswordModel model)
        {
            var user = await _userDataAccess.GetByIdAsync(model.UserId);
            if (user is null)
            {
                _logger.LogDebug(LogEvent.ConfirmEmail, "Failed to retrieve the user with [id: {userId}]", model.UserId);
                throw new NotFoundException("the user with given id not exist");
            }

            var result = await _userManager.ResetPasswordAsync(user, model.Token, model.Password);
            if (!result.Succeeded)
            {
                _logger.LogWarning(LogEvent.UpdatePassword, "failed to validate the rest password token, validation result: {@result}", result);
                if (result.Errors.Any(e => e.Code.Equals("InvalidToken", StringComparison.OrdinalIgnoreCase)))
                    throw new ValidationException("Invalid Token", MessageCode.InvalidToken);

                return;
            }

            _logger.LogInformation(LogEvent.UpdatePassword, "the rest operation has been done successfully for user with [id= {userId}]", model.UserId);
        }

        /// <summary>
        /// validate the user logins
        /// </summary>
        /// <param name="loginModel">the login model used for validation</param>
        /// <returns>the user value</returns>
        public async Task<User> ValidateUserLoginAsync(UserLoginModel loginModel)
        {
            _logger.LogInformation(LogEvent.ValidateUserLogin, "validating user login using model {@model}", loginModel);
            var user = await _userDataAccess.GetByUserNameAsync(NormalizeKey(loginModel.UserName));
            if (user is null)
            {
                _logger.LogDebug(LogEvent.ValidateUserLogin, "Failed to retrieve the user with the given user name: [{userName}]", loginModel.UserName);
                throw new InvalidLoginCredentialsException();
            }

            if (!await _userManager.CheckPasswordAsync(user, loginModel.Password))
            {
                _logger.LogWarning(LogEvent.ValidateUserLogin, "user [id: {userId}] has entered a non valid password, incrementing the access failed counter", user.Id);
                var result = await _userManager.AccessFailedAsync(user);
                if (!result.Succeeded)
                    _logger.LogCritical(LogEvent.ValidateUserLogin, "Failed to increment the access failed counter for user with [id= {userId}], result: {@result}", user.Id, result);

                throw new InvalidLoginCredentialsException();
            }

            if (await _userManager.IsLockedOutAsync(user))
            {
                _logger.LogWarning(LogEvent.ValidateUserLogin, "the user [id: {userId}], has been lockedOut and he is trying to log in", user.Id);
                throw new UnauthorizedException("you have been locked out, too many failed login in attempts", MessageCode.UserIsLockedOut);
            }

            if (user.Statut == Status.Inactive)
            {
                _logger.LogWarning(LogEvent.ValidateUserLogin, "inactive user [id: {userId}] is trying to log in", user.Id);
                throw new InvalidLoginCredentialsException();
            }

            var restResult = await _userManager.ResetAccessFailedCountAsync(user);
            if (!restResult.Succeeded)
                _logger.LogCritical(LogEvent.ValidateUserLogin, "Failed to Reset Access Failed counter for user with [id= {userId}], the result: {@result}", user.Id, restResult);

            _logger.LogInformation(LogEvent.ValidateUserLogin, "the login credentials for user with [id= {userId}], has been validated successfully");
            return user;
        }

        public async Task<ListResult<TOut>> GetByRoleTypeAsync<TOut>(UserRetrievealByRoleType model)
        {
            var users = await _dataAccess.GetByRoleTypesAsync(model.RoleTypes);
            return Result.ListSuccess(Map<IEnumerable<TOut>>(users));
        }

        /// <summary>
        /// update the password of the user
        /// </summary>
        /// <param name="model">the model user for updating the user</param>
        /// <returns>the operation result</returns>
        public async Task<Result> UpdateUserPassword(UpdateUserPasswordModel model)
        {
            var user = await _dataAccess.GetByIdAsync(model.UserId);
            user.PasswordHash = HashPassword(user, model.Password);

            return await _dataAccess.UpdateAsync(user);
        }

        /// <summary>
        /// update the last time the user has logged in
        /// </summary>
        /// <param name="id">the id of the user</param>
        public Task UpdateLastTimeLoggedInAsync(Guid id)
        {
            return _dataAccess.UpdateLastTimeLoggedInAsync(id, DateTime.Now);
        }

        /// <summary>
        /// check if the given reference is unique
        /// </summary>
        /// <param name="reference">the reference to be checked</param>
        /// <returns>true if unique, false if not</returns>
        public async Task<bool> IsRefrenceUniqueAsync(string reference)
            => !await _dataAccess.IsExistAsync(e => e.Reference == reference);

        public async Task<dynamic> SynchronizeAsync(DateTimeOffset? lastSyncDate, IEnumerable<SyncObject> ObjectsToSync)
        {
            // if the first synchronization rerun everything
            if (!lastSyncDate.HasValue)
            {
                var users = await _dataAccess.GetAllAsync(options =>
                {
                    options.AddInclude(user => user.Role);
                    options.AddDefaultIncludes(false);
                });

                return new
                {
                    docType = DocumentType.Agent,
                    objects = _mapper.Map<UserMinimalModel>(users),
                };
            }

            var newUsers = await _dataAccess.GetAllAsync(options =>
            {
                options.AddDefaultIncludes(false);
                options.AddInclude(user => user.Role);
                options.AddPredicate(e => e.LastModifiedOn > lastSyncDate);
            });

            return new
            {
                docType = DocumentType.Agent,
                objects = _mapper.Map<UserMinimalModel>(newUsers),
            };
        }
    }

    /// <summary>
    /// the partial part of the <see cref="UsersManagementService"/>
    /// </summary>
    public partial class UsersManagementService : DataService<User, Guid>, IUsersManagementService
    {
        private readonly IUserDataAccess _userDataAccess;
        private readonly IValidationService _validationService;
        private readonly IEmailService _mailService;
        private readonly IRoleDataAccess _roleDataAccess;
        private readonly IValidationService _ensure;
        private readonly IHistoryService _historyService;
        private readonly UserManager<User> _userManager;
        private new IUserDataAccess _dataAccess => base._dataAccess as IUserDataAccess;

        /// <summary>
        /// name of the entity
        /// </summary>
        protected override string EntityName => nameof(User);

        public UsersManagementService(
            IUserDataAccess accountDataAccess,
            IEmailService mailService,
            IRoleDataAccess roleDataAccess,
            UserManager<User> userManager,
            IValidationService validationService,
            IHistoryService historyService,
            IFileService fileService,
            ILoggedInUserService loggedInUserService,
            ITranslationService translationService,
            IApplicationConfigurationService appConfigService,
            ILoggerFactory _loggerFactory,
            IMapper mapper)
            : base(accountDataAccess, fileService, validationService, historyService, loggedInUserService, appConfigService, translationService, _loggerFactory, mapper)
        {
            _userDataAccess = accountDataAccess;
            _validationService = validationService;
            _mailService = mailService;
            _roleDataAccess = roleDataAccess;
            _userManager = userManager;
            _ensure = validationService;
            _historyService = historyService;
        }

        /// <summary>
        /// create a new user using the given informations
        /// </summary>
        /// <param name="roleId">the id of the role</param>
        /// <param name="userName">the user name</param>
        /// <param name="email">the email</param>
        /// <param name="password">the password</param>
        /// <returns>the user instant</returns>
        private User CreateNewUser(string userName, string email, string password)
        {
            var user = new User()
            {
                NormalizedUserName = NormalizeKey(userName),
                NormalizedEmail = NormalizeKey(email),
                SecurityStamp = Security.NewSecurityStamp(),
                LockoutEnabled = true,
            };

            user.PasswordHash = HashPassword(user, password);
            return user;
        }

        /// <summary>
        /// update the user identity information, and set the security stamp
        /// </summary>
        /// <param name="user">the user to update</param>
        private void UpdateUserIdentity(User user)
        {
            user.SecurityStamp = Security.NewSecurityStamp();
            user.LockoutEnabled = true;
            user.NormalizedEmail = NormalizeKey(user.Email);
            user.NormalizedUserName = NormalizeKey(user.UserName);
        }

        /// <summary>
        /// update the user identity information, and set the security stamp
        /// </summary>
        /// <param name="user">the user to update</param>
        private void UpdateUserIdentity(User user, string password)
        {
            user.PasswordHash = HashPassword(user, password);
            UpdateUserIdentity(user);
        }

        /// <summary>
        /// normalize the given key
        /// </summary>
        /// <param name="key">the key to normalize</param>
        /// <returns>the string normalized</returns>
        private string NormalizeKey(string key)
            => _userManager.NormalizeKey(key);

        /// <summary>
        /// has the user password
        /// </summary>
        /// <param name="user">the user instant</param>
        /// <param name="password">the password</param>
        /// <returns>the hashed password</returns>
        private string HashPassword(User user, string password)
            => _userManager.PasswordHasher.HashPassword(user, password);

        /// <summary>
        /// validate the entity for update
        /// </summary>
        /// <param name="entity">the entity to be validated</param>
        protected override async Task InUpdate_ValidateEntityAsync(User entity, IUpdateModel<User> updateModel)
        {
            if (updateModel is UserUpdateModel model)
            {
                if (model.UserName.IsValid() && NormalizeKey(model.UserName) != entity.NormalizedUserName && await _dataAccess.IsExistAsync(e => e.NormalizedUserName == NormalizeKey(model.UserName)))
                    throw new ValidationException("the given user name already exist", MessageCode.DuplicateEntry);

                if (model.Email.IsValid() && NormalizeKey(model.Email) != entity.NormalizedEmail && await _dataAccess.IsExistAsync(e => e.NormalizedEmail == NormalizeKey(model.Email)))
                    throw new ValidationException("the given email already exist", MessageCode.DuplicateEntry);
            }
        }

        /// <summary>
        /// update the user identity before inserting to database information
        /// </summary>
        /// <param name="user">the user entity instant</param>
        protected override Task InCreate_BeforInsertAsync<TCreateModel>(User user, TCreateModel createModel)
        {
            var password = user.PasswordHash;
            if (createModel is UserCreateModel model)
                password = model.Password;

            if (!user.Email.IsValid())
                user.Email = null;

            UpdateUserIdentity(user, password);
            return base.InCreate_BeforInsertAsync(user, createModel);
        }

        /// <summary>
        /// before updating the entity
        /// </summary>
        /// <typeparam name="TUpdateModel">the type of the update model</typeparam>
        /// <param name="user">the user instant</param>
        /// <param name="updateModel">the update model instant</param>
        protected override Task InUpdate_BeforUpdateAsync<TUpdateModel>(User user, TUpdateModel updateModel)
        {
            user.Role = null;
            UpdateUserIdentity(user);
            return base.InUpdate_BeforUpdateAsync(user, updateModel);
        }

        protected override async Task InDelete_BeforDeleteAsync(User user)
        {
            var validation = await _validate.HasDocumentsAsync(user);
            if (!validation.IsSuccess)
                throw new ValidationException(
                    message: "you can't delete this user is associated with other documents",
                    messageCode: MessageCode.DocumentRelationshipConstraint,
                    validationInfo: validation);
        }
    }
}
