﻿namespace Axiobat.Services.Maintenance
{
    using App.Common;
    using Application.Data;
    using Application.Exceptions;
    using Application.Models;
    using Application.Services;
    using Application.Services.AccountManagement;
    using Application.Services.Configuration;
    using Application.Services.FileService;
    using Application.Services.Localization;
    using Application.Services.Maintenance;
    using AutoMapper;
    using Domain.Constants;
    using Domain.Entities;
    using Microsoft.Extensions.Logging;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// the service implementation for <see cref="IMaintenanceVisitService"/>
    /// </summary>
    public partial class MaintenanceVisitService
    {
        public Task UpdateStatusAsync(string maintenanceVisitId, string status)
        {
            return _dataAccess.UpdateStatusAsync(maintenanceVisitId, status);
        }

        /// <summary>
        /// get the list of all contract maintenance visits
        /// </summary>
        /// <typeparam name="TOut">the type of the output</typeparam>
        /// <param name="contractId">the id of the contract</param>
        /// <returns>list of contract maintenance visits</returns>
        public async Task<ListResult<MaintenanceVisitModel>> GetAllContractMaintenanceVisitAsync(string contractId)
        {
            // first get the contract
            var contract = await _maintenanceContractService.GetByIdAsync<MaintenanceContract>(contractId);
            if (!contract.IsSuccess)
                return Result.ListFrom<MaintenanceVisitModel, MaintenanceContract>(contract);

            // generate the maintenance visit and return the result
            return GenerateMaintenanceVisitAsModel(contract);
        }

        /// <summary>
        /// create a maintenance contract that represent the maintenance operation for the given year and month
        /// </summary>
        /// <typeparam name="TOut">the type of the output</typeparam>
        /// <param name="maintenanceContractId">the id of the contract</param>
        /// <param name="year">the year</param>
        /// <param name="month">the month</param>
        /// <returns>the operation result</returns>
        public async Task<TOut> CreateMaintenanceVisitAsync<TOut>(string maintenanceContractId, int year, int month, ClientDocument client = null)
        {
            // first get the contract
            var contract = await _maintenanceContractService.GetByIdAsync<MaintenanceContract>(maintenanceContractId);
            if (!contract.IsSuccess)
                throw new NotFoundException("there is no contract with the given id");

            // generate the maintenance visit get the maintenanceVisit for the given month and year
            var maintenanceVisits = GenerateMaintenanceVisit(contract)
                .FirstOrDefault(e => e.Date.Year == year && e.Date.Month == month);

            // check if maintenanceVisits exist
            if (maintenanceVisits is null)
                return default;

            // check if the maintenanceVisits already exist by checking the status
            if (maintenanceVisits.Status == MaintenanceVisitStatus.Planned)
                return Map<TOut>(maintenanceVisits);

            // we set the status
            maintenanceVisits.MaintenanceContract = null;
            maintenanceVisits.Status = MaintenanceVisitStatus.Planned;

            if (!(client is null))
            {
                maintenanceVisits.Client = client;
            }
            else
            {
                maintenanceVisits.Client = contract.Value.Client;
            }

            // we need to insert the maintenanceVisits
            var addResult = await _dataAccess.AddAsync(maintenanceVisits);
            if (!addResult.IsSuccess)
                return default;

            // re-set the values for the contract and client
            maintenanceVisits.MaintenanceContract = contract.Value;
            //maintenanceVisits.Client = contract.Value.Client;

            // return the result
            return Map<TOut>(maintenanceVisits);
        }

        /// <summary>
        /// get the maintenance visit using the given options
        /// </summary>
        /// <typeparam name="TOut">the type of the output</typeparam>
        /// <param name="options">the options</param>
        /// <returns>the object to return</returns>
        public async Task<Result<MaintenanceVisitModel>> GetMaintenanceVisitAsync(MaintenanceVisitRetrieveOptions options)
        {
            // first get the contract
            var contract = await _maintenanceContractService.GetByIdAsync<MaintenanceContract>(options.ContractId);
            if (!contract.IsSuccess)
                throw new NotFoundException("there is no contract with the given id");

            // generate the maintenance visit get the maintenanceVisit for the given month and year
            return GenerateMaintenanceVisitAsModel(contract)
                .FirstOrDefault(e => e.Year == options.Year && e.Month == options.Month);
        }

        public override async Task<PagedResult<TOut>> GetAsPagedResultAsync<TOut, IFilter>(IFilter filterOption)
        {
            // check the type of the filter
            if (filterOption is MaintenanceVisitFilterOptions filter)
            {
                // check the status if equals to MaintenanceVisitStatus.Planned
                if (filter.Status.IsValid() && filter.Status == MaintenanceVisitStatus.Planned)
                    return await base.GetAsPagedResultAsync<TOut, IFilter>(filterOption);

                // get the list of contract
                var contractResults = await _maintenanceContractService.GetAsPagedResultAsync<MaintenanceContract, MaintenanceVisitFilterOptions>(new MaintenanceVisitFilterOptions
                {
                    ExternalPartnerId = filter.ExternalPartnerId,
                    IgnorePagination = true,
                });

                if (!contractResults.IsSuccess)
                    return Result.PagedFailed<TOut>("Failed to retrieve the Maintenance Visit", MessageCode.OperationFailed);

                // generate maintenance visit
                var maintenanceVisit = contractResults.Value.SelectMany(e => GenerateMaintenanceVisit(e));
                var totalElements = maintenanceVisit.Count();

                // set the filters
                if (filter.Year.HasValue)
                {
                    maintenanceVisit = maintenanceVisit.Where(e => e.Date.Year == filter.Year);
                    var res = maintenanceVisit.ToList();
                }

                if (filter.Months.Count() > 0)
                    maintenanceVisit = maintenanceVisit.Where(e => filter.Months.Contains(e.Date.Month));

                if (filter.Status.IsValid() && filter.Status == MaintenanceVisitStatus.Unplanned)
                    maintenanceVisit = maintenanceVisit.Where(e => e.Status == filter.Status);

                // set the pagination
                if (!filter.IgnorePagination)
                {
                    var skip = PagedResult<TOut>.CalculateSkip(filter.Page, filter.PageSize);
                    maintenanceVisit = maintenanceVisit
                        .Skip(skip)
                        .Take(filter.PageSize);
                }

                // return the result
                return Result.PagedSuccess(Map<IEnumerable<TOut>>(maintenanceVisit), filter.Page, filter.PageSize, totalElements);
            }

            return await base.GetAsPagedResultAsync<TOut, IFilter>(filterOption);
        }
    }

    /// <summary>
    /// partial part for <see cref="MaintenanceVisitService"/>
    /// </summary>
    public partial class MaintenanceVisitService : DataService<MaintenanceVisit, string>, IMaintenanceVisitService
    {
        private new IMaintenanceVisitDataAccess _dataAccess => base._dataAccess as IMaintenanceVisitDataAccess;
        private readonly IMaintenanceContractService _maintenanceContractService;
        private readonly IDateService _dateService;

        public MaintenanceVisitService(
            IMaintenanceContractService maintenanceContractService,
            IMaintenanceVisitDataAccess dataAccess,
            IFileService fileService,
            IDateService dateService,
            IValidationService validation,
            IHistoryService historyService,
            ILoggedInUserService loggedInUserService,
            IApplicationConfigurationService appSetting,
            ITranslationService transalationService,
            ILoggerFactory loggerFactory,
            IMapper mapper)
            : base(dataAccess, fileService, validation, historyService, loggedInUserService, appSetting, transalationService, loggerFactory, mapper)
        {
            _maintenanceContractService = maintenanceContractService;
            _dateService = dateService;
        }

        /// <summary>
        /// generate the Maintenance Visit for the given contract
        /// </summary>
        /// <param name="maintenanceContract">the maintenance contract</param>
        private List<MaintenanceVisit> GenerateMaintenanceVisit(MaintenanceContract maintenanceContract)
        {
            // initialize list of maintenance visit
            var maintenanceVisitList = new List<MaintenanceVisit>();

            // iterate over the date range of the contract start date and end date
            foreach (var date in _dateService.GetDates(maintenanceContract.StartDate, maintenanceContract.EndDate))
            {
                // check if there maintenance already exist
                var maintenanceVisit = maintenanceContract.GetMaintenanceVisits(date);
                if (!(maintenanceVisit is null))
                {
                    maintenanceVisitList.Add(maintenanceVisit);
                    continue;
                }

                // get the equipment of current date month
                var equipments = maintenanceContract.GetEquipmentMaintenanceOf(date);

                // check we have equipments
                if (equipments.Count() <= 0)
                    continue;

                // create a new instant
                maintenanceVisit = new MaintenanceVisit
                {
                    Date = date,
                    EquipmentDetails = equipments,
                    CreatedOn = DateTimeOffset.Now,
                    Site = maintenanceContract.Site,
                    LastModifiedOn = DateTimeOffset.Now,
                    Client = maintenanceContract.Client,
                    ClientId = maintenanceContract.ClientId,
                    MaintenanceContract = maintenanceContract,
                    Status = MaintenanceVisitStatus.Unplanned,
                    MaintenanceContractId = maintenanceContract.Id,
                };

                // set the search terms
                maintenanceVisit.BuildSearchTerms();

                // add and create the maintenance visit to the list of result
                maintenanceVisitList.Add(maintenanceVisit);
            }

            // return the list
            return maintenanceVisitList;
        }

        private List<MaintenanceVisitModel> GenerateMaintenanceVisitAsModel(MaintenanceContract maintenanceContract)
        {
            var idCounter = 1;

            // initialize list of maintenance visit
            var maintenanceVisitList = new List<MaintenanceVisitModel>();

            // iterate over the date range of the contract start date and end date
            foreach (var date in _dateService.GetDates(maintenanceContract.StartDate, maintenanceContract.EndDate))
            {
                // check if there maintenance already exist
                var maintenanceVisit = maintenanceContract.GetMaintenanceVisits(date);
                if (!(maintenanceVisit is null))
                {
                    var model = Map<MaintenanceVisitModel>(maintenanceVisit);
                    model.PreviousMaintenanceVisits = Map<ICollection<MaintenanceVisitMinimalModel>>(maintenanceVisitList);

                    maintenanceVisitList.Add(model);
                    continue;
                }

                // get the equipment of current date month
                var equipments = maintenanceContract.GetEquipmentMaintenanceOf(date);

                // check we have equipments
                if (equipments.Count() <= 0)
                    continue;

                var newMaintenanceVisit = new MaintenanceVisitModel
                {
                    Year = date.Year,
                    Month = date.Month,
                    EquipmentDetails = equipments,
                    Site = maintenanceContract.Site,
                    MaintenanceOperationSheetId = "",
                    ContractId = maintenanceContract.Id,
                    MaintenanceOperationSheetReference = "",
                    ClientId = maintenanceContract.ClientId,
                    Status = MaintenanceVisitStatus.Unplanned,
                    Client = maintenanceContract.Client,
                    PreviousMaintenanceVisits = Map<ICollection<MaintenanceVisitMinimalModel>>(maintenanceVisitList),
                };

                newMaintenanceVisit.Id += idCounter++;

                // add and create the maintenance visit to the list of result
                maintenanceVisitList.Add(newMaintenanceVisit);
            }

            // return the list
            return maintenanceVisitList;
        }
    }
}
