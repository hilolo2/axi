﻿namespace Axiobat.Services.Maintenance
{
    using App.Common;
    using Application.Data;
    using Application.Exceptions;
    using Application.Models;
    using Application.Services.AccountManagement;
    using Application.Services.Configuration;
    using Application.Services.Contacts;
    using Application.Services.FileService;
    using Application.Services.Localization;
    using Application.Services.Maintenance;
    using AutoMapper;
    using Domain.Entities;
    using Domain.Enums;
    using Microsoft.Extensions.Logging;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// the service implementation for <see cref="IMaintenanceContractService"/>
    /// </summary>
    public partial class MaintenanceContractService
    {
        /// <summary>
        /// check if the given reference is unique
        /// </summary>
        /// <param name="reference">the reference to be checked</param>
        /// <returns>true if unique, false if not</returns>
        public async Task<bool> IsRefrenceUniqueAsync(string reference)
            => !await _dataAccess.IsExistAsync(e => e.Reference == reference);

        /// <summary>
        /// save the memo to the client with the given id
        /// </summary>
        /// <param name="entityId">the id of the client to add memo to it</param>
        /// <param name="memos">the memo to add</param>
        /// <returns>an operation result</returns>
        public async Task<Result<Memo>> SaveMemoAsync(string entityId, MemoModel model)
        {
            var memo = new Memo
            {
                Id = model.Id,
                Comment = model.Comment,
                CreatedOn = DateTime.Now,
                Attachments = await _fileService.SaveAsync(model.Attachments.ToArray()),
                User = new MinimalUser(_loggedInUserService.User.UserId, _loggedInUserService.User.UserName),
            };

            var updateResult = await _dataAccess.UpdateAsync(entityId, entity => entity.Memos.Add(memo));
            if (!updateResult.HasValue)
                return Result.From<Memo>(updateResult);

            _logger.LogDebug(LogEvent.SavingMemo, "a new memo with id: [{memoId}] has been saved", memo.Id);
            return memo;
        }

        /// <summary>
        /// update the memo with the given id
        /// </summary>
        /// <param name="entityId">the id of the entity to be updated</param>
        /// <param name="memoId">the id of the memo to be updated</param>
        /// <param name="memoModel">the new memo</param>
        /// <returns>the updated version of the memo</returns>
        public async Task<Result<Memo>> UpdateMemoAsync(string entityId, string memoId, MemoModel memoModel)
        {
            // retrieve entity
            var entity = await _dataAccess.GetByIdAsync(entityId);
            if (entity is null)
                return Result.Failed<Memo>("Failed to update the memo, there is no entity with the given id");

            // retrieve memo
            var memo = entity.Memos.FirstOrDefault(e => e.Id == memoId);
            if (memo is null)
                return Result.Failed<Memo>("Failed to update the memo, there is no memo with the given id");

            // update the memo
            memo.Comment = memoModel.Comment;

            // set the attachment deferences
            var intersection = memo.Attachments.Select(e => new AttachmentModel
            {
                FileId = e.FileId,
                FileName = e.FileName,
                FileType = e.FileType
            })
            .Intersection(memoModel.Attachments);

            // delete attachments from the server and memo
            var removed = intersection.Removed.Select(e => new Attachment(e.FileId, e.FileName, e.FileType)).ToArray();
            var Added = memoModel.Attachments.Where(e => intersection.Added.Select(a => a.FileId).Contains(e.FileId));
            var attachementToRemove = memo.Attachments.Where(e => removed.Select(a => a.FileId).Contains(e.FileId)).ToArray();

            await _fileService.DeleteAsync(removed);

            foreach (var attachment in attachementToRemove)
                memo.Attachments.Remove(attachment);

            // add the new attachments
            var newAttachments = await _fileService.SaveAsync(Added.ToArray());

            foreach (var attachment in newAttachments)
                memo.Attachments.Add(attachment);

            // update the entity
            var updateResult = await _dataAccess.UpdateAsync(entity);
            if (!updateResult.IsSuccess)
                return Result.Failed<Memo>("failed to update the entity");

            return memo;
        }

        /// <summary>
        /// update the status of the operation Sheet
        /// </summary>
        /// <param name="operationSheetId">the id of the operation Sheet to update</param>
        /// <param name="model">the model used for updating the status</param>
        /// <returns>the operation result</returns>
        public async Task<Result> UpdateStatusAsync(string maintenanceContratId, DocumentUpdateStatusModel model)
        {
            // retrieve the document
            var document = await _dataAccess.GetByIdAsync(maintenanceContratId);
            if (document is null)
                throw new NotFoundException("there is no contrat with the given id");

            // update the devis status
            document.Status = model.Status;
            return await _dataAccess.UpdateAsync(document);
        }

        /// <summary>
        /// delete <see cref="Memo"/> form the <see cref="TEntity"/> using the given <see cref="DeleteMemoModel"/>
        /// </summary>
        /// <param name="model">the <see cref="DeleteMemoModel"/></param>
        /// <returns>the operation result</returns>
        public async Task<Result> DeleteMemosAsync(string entityId, DeleteMemoModel model)
        {
            var entity = await _dataAccess.GetByIdAsync(entityId);
            if (entity is null)
                throw new NotFoundException("entity not found");

            var memosToDelete = entity.Memos.Where(e => model.MemosIds.Contains(e.Id)).ToList();
            var attachments = memosToDelete.SelectMany(e => e.Attachments).ToArray();

            foreach (var memo in memosToDelete)
                entity.Memos.Remove(memo);

            var deleteResult = await _dataAccess.UpdateAsync(entity);
            if (!deleteResult.IsSuccess)
            {
                _logger.LogCritical(LogEvent.DeleteMemos, "Failed to delete the memos check previous logs");
                Result.Failed("Failed to delete memos");
            }

            var deleted = await _fileService.DeleteAsync(attachments);
            _logger.LogInformation(LogEvent.DeleteMemos, "[{deletedFiles}] file has been deleted out of [{totalFiles}]", deleted, attachments.Length);

            return Result.Success();
        }

        /// <summary>
        /// duplicate the document with the given id
        /// </summary>
        /// <typeparam name="TOut">the output type</typeparam>
        /// <param name="documentId"></param>
        /// <returns>the desired output</returns>
        public async Task<Result<TOut>> DuplicateAsync<TOut>(string documentId)
        {
            // retrieve the quote
            var maintenanceContract = await _dataAccess.GetByIdAsync(documentId);
            if (maintenanceContract is null)
                throw new NotFoundException("there is no maintenance Contract with the given id");

            // generate a new reference
            var reference = await GenerateReferenceAsync();

            // set the entity informations
            maintenanceContract.Id = maintenanceContract.GenerateId();
            maintenanceContract.Reference = reference;

            // insert the new entity
            var addResult = await _dataAccess.AddAsync(maintenanceContract);
            if (!addResult.IsSuccess)
                return Result.From<TOut>(addResult);

            // increment reference
            await IncrementRefenereceAsync();

            // return result
            return Map<TOut>(maintenanceContract);
        }

        /// <summary>
        /// export the journal using the given options
        /// </summary>
        /// <param name="documentId"></param>
        /// <returns>the exported file as a byte array</returns>
        public async Task<Result<byte[]>> ExportContratAsync(string documentId)
        {
            var maintenanceContract = await _dataAccess.GetByIdAsync(documentId);
            return _fileService.ExportContrat(maintenanceContract);
        }

        public async Task<ListResult<PublishingContractMinimal>> UpdateContractPublishingDocumentAsync(string maintenanceContractId, ICollection<PublishingContractMinimal> model)
        {
            var contract = await _dataAccess.GetSingleAsync(options =>
            {
                options.AddDefaultIncludes(false);
                options.AddPredicate(c => c.Id == maintenanceContractId);
            });

            if (contract is null)
                throw new NotFoundException("there is no contract with the given id");

            contract.PublishingContracts = model;

            _history.Recored(
                contract,
                ChangesHistoryType.Updated,
                fields: new[]
                {
                    new ChangedField
                    {
                        Champ = "publish contract",
                        PropName = nameof(MaintenanceContract.PublishingContracts),
                        ValeurInitial = "changement dans la list",
                        ValeurFinal = "changement dans la list",
                    }
                });


            var updateResult = await _dataAccess.UpdateAsync(contract);
            return Result.ListSuccess<PublishingContractMinimal>(contract.PublishingContracts);
        }
    }

    /// <summary>
    /// partial part for <see cref="MaintenanceContractService"/>
    /// </summary>
    public partial class MaintenanceContractService : SynchronizeDataService<MaintenanceContract, string>, IMaintenanceContractService
    {
        public MaintenanceContractService(
            IMaintenanceContractDataAccess dataAccess,
            ISynchronizationResolverService synchronizationResolver,
            IFileService fileService,
            IValidationService validation,
            IHistoryService historyService,
            ILoggedInUserService loggedInUserService,
            IApplicationConfigurationService appSetting,
            ITranslationService transalationService,
            ILoggerFactory loggerFactory,
            IMapper mapper)
            : base(dataAccess, synchronizationResolver, fileService, validation, historyService, loggedInUserService, appSetting, transalationService, loggerFactory, mapper)
        {
        }

        private async Task<string> GenerateReferenceAsync()
        {
            var reference = await _configuration.GenerateRefereceAsync(DocumentType.MaintenanceContract);
            if (!await IsRefrenceUniqueAsync(reference))
            {
                await IncrementRefenereceAsync();
                return await GenerateReferenceAsync();
            }

            return reference;
        }

        /// <summary>
        /// increment the reference of the document
        /// </summary>
        protected Task IncrementRefenereceAsync()
            => _configuration.IncrementReferenceAsync<MaintenanceContract>();

        protected override async Task InCreate_BeforInsertAsync<TCreateModel>(MaintenanceContract entity, TCreateModel createModel)
        {
            if (createModel is MaintenanceContractPutModel model)
            {
                // if there is any attachments we should add it
                entity.Attachments = await _fileService.SaveAsync(model.Attachments.ToArray());

                //if (model.ClientId.IsValid())
                //{
                //    var client = await _clientService.GetByIdAsync<ClientDocument>(model.ClientId);
                //    if (!client.IsSuccess)
                //        throw new ValidationException(client.Message, client.MessageCode);

                //    entity.Client = client;
                //}
            }
        }

        protected override async Task InUpdate_BeforUpdateAsync<TUpdateModel>(MaintenanceContract entity, TUpdateModel updateModel)
        {
            if (updateModel is MaintenanceContractPutModel model)
            {
                // set the attachment deferences
                var intersection = entity.Attachments.Select(e => new AttachmentModel
                {
                    FileId = e.FileId,
                    FileName = e.FileName,
                    FileType = e.FileType
                })
                .Intersection(model.Attachments);

                // delete attachments from the server and memo
                var removed = intersection.Removed.Select(e => new Attachment(e.FileId, e.FileName, e.FileType)).ToArray();
                var Added = model.Attachments.Where(e => intersection.Added.Select(a => a.FileId).Contains(e.FileId));

                // remove the attachments from the list
                foreach (var attachment in entity.Attachments.Where(e => removed.Select(a => a.FileId).Contains(e.FileId)))
                    entity.Attachments.Remove(attachment);

                // add the new attachments
                foreach (var attachment in await _fileService.SaveAsync(Added.ToArray()))
                    entity.Attachments.Add(attachment);

                // remove attachments from server
                await _fileService.DeleteAsync(removed);

                //if (model.ClientId.IsValid())
                //{
                //    var client = await _clientService.GetByIdAsync<ClientDocument>(model.ClientId);
                //    if (!client.IsSuccess)
                //        throw new ValidationException(client.Message, client.MessageCode);

                //    entity.Client = client;
                //}
            }
        }

        protected override Task InGet_AfterMappingAsync<TOut>(MaintenanceContract entity, TOut mappedEntity)
        {
            if (mappedEntity is MaintenanceContractModel model)
            {
                var documents = Map<IEnumerable<AssociatedDocument>>(entity.Invoices);

                foreach (var item in documents)
                    model.AssociatedDocuments.Add(item);
            }

            return base.InGet_AfterMappingAsync(entity, mappedEntity);
        }

    }
}
