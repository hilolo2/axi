﻿namespace Axiobat.Services.Configuration
{
    using App.Common;
    using Application.Data;
    using Application.Exceptions;
    using Application.Models;
    using Application.Services.AccountManagement;
    using Application.Services.Configuration;
    using Application.Services.FileService;
    using Application.Services.Localization;
    using AutoMapper;
    using Domain.Entities;
    using Domain.Enums;
    using Microsoft.Extensions.Logging;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    public partial class ChartOfAccountsService
    {
        public async Task PutAsync(ChartAccountItemPutModel[] model)
        {
            await ValidatePutModelAsync(model);

            var categoriesToAddAsModel = model.Where(category => !category.Id.IsValid());
            if (categoriesToAddAsModel.Any())
            {
                var categoriesToAdd = categoriesToAddAsModel.Select(categoryModel =>
                {
                    var category = _mapper.Map<ChartAccountItem>(categoryModel);
                    category.Type = ChartAccountType.SubCategory;
                    category.CategoryType = CategoryType.Custom;
                    return category;
                });

                var insertResult = await _dataAccess.AddRangeAsync(categoriesToAdd);
            }

            var categoriesToUpdateAsModel = model.Where(category => category.Id.IsValid()).ToDictionary(category => category.Id);
            if (categoriesToUpdateAsModel.Any())
            {
                var categoriesIoUpdate = await _dataAccess.GetByIdAsync(categoriesToUpdateAsModel.Select(e => e.Key).ToArray());

                foreach (var category in categoriesIoUpdate)
                {
                    var categoryModel = categoriesToUpdateAsModel[category.Id];
                    categoryModel.Update(category);
                }

                var updateResult = await _dataAccess.UpdateRangeAsync(categoriesIoUpdate);
            }
        }

        public async Task<TOut[]> GetCategoriesByTypeAsync<TOut>(params ChartAccountType[] categoryTypes)
        {
            var charts = await _dataAccess.GetAllAsync(option =>
            {
                option.AddInclude(e => e.SubCategories);
                option.AddPredicate(e => categoryTypes.Contains(e.Type) && e.ParentId == null);
            });

            return _mapper.Map<TOut[]>(charts);
        }

        public async Task<ChartOfAccounts> GetChartOfAccountsAsync()
        {
            var charts = await _dataAccess.GetAllAsync(option =>
            {
                option.AddInclude(e => e.SubCategories);
                option.AddPredicate(e =>
                    (e.Type == ChartAccountType.Default && e.ParentId == null) ||
                    (e.Type == ChartAccountType.Expense && e.ParentId == null) ||
                    (e.Type == ChartAccountType.Sales && e.ParentId == null) ||
                    (e.Type == ChartAccountType.Assets && e.ParentId == null) ||
                    (e.Type == ChartAccountType.VAT && e.ParentId == null)
                );
            });

            return ChartOfAccounts.Create(charts);
        }

        public async Task<ListResult<TOut>> GetAllAsync<TOut>()
        {
            var charts = await _dataAccess.GetAllAsync(option =>
            {
                option.AddInclude(e => e.SubCategories);
                option.AddPredicate(e =>
                    (e.Type == ChartAccountType.Default && e.Id != "1") ||
                    (e.Type == ChartAccountType.VAT && e.ParentId == null) ||
                    (e.Type == ChartAccountType.Expense && e.ParentId == null) ||
                    (e.Type == ChartAccountType.Assets && e.ParentId == null) ||
                    (e.Type == ChartAccountType.Sales && e.ParentId == null)
                );
            });

            return Result.ListSuccess(_mapper.Map<IEnumerable<TOut>>(charts));
        }

        public async Task<Result> DeleteAsync(string categoryId)
        {
            var category = await _dataAccess.GetByIdAsync(categoryId);
            if (category is null)
                throw new NotFoundException($"failed to delete the chart of account not found");

            if (category.Type == ChartAccountType.Default ||
               (category.Type == ChartAccountType.VAT && category.ParentId == null) ||
               (category.Type == ChartAccountType.Expense && category.ParentId == null) ||
               (category.Type == ChartAccountType.Sales && category.ParentId == null) ||
               (category.Type == ChartAccountType.Assets && category.ParentId == null))
            {
                throw new ValidationException(
                    "you can't delete a parent/default category, only subcategories are allowed to be deleted",
                    MessageCode.CannotDeleteParentCategory);
            }

            if (category.Classifications.Any())
            {
                var result = Result.Failed<IEnumerable<Classification>>(
                    "you can't delete this category because it associated with one or more classifications",
                    MessageCode.CategoryAssociatedWithClassifications);

                result.Value = category.Classifications;
                return result;
            }

            return await _dataAccess.DeleteAsync(category);
        }

        public async Task<Result<TOut>> GetByIdAsync<TOut>(string categoryId)
        {
            var entity = await _dataAccess.GetByIdAsync(categoryId);
            if (entity is null)
                throw new NotFoundException($"the chart of account not exist");

            var mappedEntity = _mapper.Map<TOut>(entity);

            return mappedEntity;
        }

        public Task<bool> IsAllExistAsync(string[] chartOfAccountsIds)
            => _dataAccess.IsAllkeysExistAsync(chartOfAccountsIds);
    }

    public partial class ChartOfAccountsService : IChartOfAccountsService
    {
        private readonly IChartOfAccountsDataAccess _dataAccess;
        private readonly IMapper _mapper;

        public ChartOfAccountsService(
            IChartOfAccountsDataAccess dataAccess,
            IMapper mapper)
        {
            _dataAccess = dataAccess;
            _mapper = mapper;
        }

        private async Task ValidatePutModelAsync(ChartAccountItemPutModel[] model)
        {
            if (!model.Where(category => !category.Id.IsValid()).All(category => category.ParentId.IsValid()))
                throw new ValidationException("the categories marked for insert must all have a valid parent Id", MessageCode.InvalidCategoryParentId);

            var parentCategoriesIds = model
                .Where(category => category.ParentId.IsValid())
                .Select(category => category.ParentId)
                .Distinct()
                .ToArray();

            if (!await _dataAccess.IsAllkeysExistAsync(parentCategoriesIds))
                throw new ValidationException("one of the given categories has parent ids of a category that not exist", MessageCode.ParentCategoryNotFound);

            var categoriesToUpdateIds = model
                .Where(category => category.Id.IsValid())
                .Select(category => category.Id)
                .Distinct()
                .ToArray();

            if (!await _dataAccess.IsAllkeysExistAsync(categoriesToUpdateIds))
                throw new ValidationException("one of the categories to update not exist", MessageCode.NotFound);
        }
    }
}
