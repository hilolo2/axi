﻿namespace Axiobat.Services.Accounting
{
    using App.Common;
    using Application.Models;
    using Domain.Constants;
    using Domain.Entities;
    using Domain.Enums;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// the accounting service
    /// </summary>
    public partial class AccountingService
    {
        /// <summary>
        /// get the Financial Summary of the workshop with the given id
        /// </summary>
        /// <param name="constructionWorkshopId">the id of the workshop</param>
        /// <returns>the workshop Financial Summary instant</returns>
        public async Task<Result<WorkshopFinancialSummary>> GetFinancialSummaryAsync(string constructionWorkshopId)
        {
            // get the workshop
            var workshop = await _dataAccess.GetWorkshopsByIdAsync(constructionWorkshopId);
            if (workshop is null)
                return Result.Failed<WorkshopFinancialSummary>("there is no workshop with the given id");

            // get default values parameter
            var defaultValues = await _configuration.GetAsync<DefaultValue>(ApplicationConfigurationType.DefaultValues);

            // the workshop Financial Summary
            var financialSummary = new WorkshopFinancialSummary
            {
                Predictions = GetPredictions(workshop, defaultValues)
            };

            financialSummary.BillingTreasury = GetBillingTreasury(workshop, defaultValues, financialSummary.Predictions);
            financialSummary.TotalWorkshopTreasury = GetTotalWorkshopTreasury(workshop);

            // return the results
            return financialSummary;
        }

        public BillingTreasury GetBillingTreasury(ConstructionWorkshop workshop, DefaultValue defaultValue, Predictions predictions)
        {
            // get required data
            var quotes = workshop.Quotes.ToLookup(e => e.Id);
            var invoices = workshop.Invoices.Where(e => e.Status != InvoiceStatus.Canceled && e.Status != InvoiceStatus.Draft);
            var expenses = workshop.Expenses.Where(e => e.Status != ExpenseStatus.Canceled && e.Status != ExpenseStatus.Draft);
            var operationSheets = workshop.OperationSheets.Where(e => e.Status == OperationSheetStatus.Realized || e.Status == OperationSheetStatus.Billed);

            // initialize the BillingTreasury
            var billingTreasury = new BillingTreasury();

            // set the invoices status
            GetInvoicesDetails(predictions, invoices, billingTreasury);

            // set the expenditures
            GetExpendituresDetails(defaultValue, predictions, expenses, operationSheets, billingTreasury);

            // set the margins
            var productsDetails = invoices.Where(e => e.TypeInvoice == InvoiceType.General).Select(e => e.OrderDetails.GetCostDetails());
            var SituationInvoices = invoices.Where(e => e.TypeInvoice == InvoiceType.Situation || e.TypeInvoice == InvoiceType.Acompte && e.QuoteId.IsValid());
            var clotureInvoices = invoices.Where(e => e.TypeInvoice == InvoiceType.Cloture);

            var purchasesExpense = billingTreasury.Expenditures.ExpenseDetails
                .Where(e => e.ProductCategoryType.Type == CategoryType.Purchases)
                .Sum(e => e.Total);

            var Subcontracting = billingTreasury.Expenditures.ExpenseDetails
                .Where(e => e.ProductCategoryType.Type == CategoryType.SubContracting)
                .Sum(e => e.Total);

            var totalMaterialCost = invoices
                .Sum(e =>
                {
                    if (e.TypeInvoice == InvoiceType.General)
                        return e.OrderDetails.GetAllProduct()
                            .Sum(p => p.Quantity * p.Product.MaterialCost);

                    var quoteItem = quotes[e.QuoteId].FirstOrDefault();
                    if (quoteItem is null)
                        return 0;

                    return quoteItem.OrderDetails
                        .GetAllProduct()
                        .Sum(p => p.Quantity * p.Product.MaterialCost) * e.Situation / 100;
                });

            var totalSubContracting = invoices
                .Sum(e =>
                {
                    if (e.TypeInvoice == InvoiceType.General)
                        return e.OrderDetails.GetAllProduct()
                            .Sum(p => p.Quantity * p.Product.TotalHours * p.Product.HourlyCost);

                    var quoteItem = quotes[e.QuoteId]?.FirstOrDefault();
                    if (quoteItem is null)
                        return 0;

                    return quoteItem.OrderDetails
                        .GetAllProduct()
                        .Sum(p => p.Quantity * p.Product.TotalHours * p.Product.HourlyCost) * e.Situation / 100;
                });

            var totalHoldBack = invoices
                .Sum(e =>
                {
                    if (e.OrderDetails.HoldbackDetails is null)
                        return 0;

                    var totalHt = e.GetTotalHT();
                    return totalHt * e.OrderDetails.HoldbackDetails.Holdback / 100;
                });

            billingTreasury.Margin.MaterialMargin = totalMaterialCost - purchasesExpense;
            billingTreasury.Margin.PercentMaterialMargin = (totalMaterialCost - purchasesExpense) * 100 / totalMaterialCost;
            billingTreasury.Margin.WorkforceMargin = totalSubContracting - Subcontracting;
            billingTreasury.Margin.PercentWorkforceMargin = (totalSubContracting - Subcontracting) * 100 / totalSubContracting;
            billingTreasury.Margin.TotalHoldback = totalHoldBack;
            billingTreasury.Margin.Deference = (billingTreasury.Margin.MaterialMargin + billingTreasury.Margin.WorkforceMargin) / predictions.Margin.Total * 100;
            billingTreasury.Margin.Percent = (billingTreasury.Expenditures.Total - billingTreasury.InvoicesDetails.Total) * 100 / billingTreasury.Expenditures.Total;

            // return the result
            return billingTreasury;
        }

        private void GetExpendituresDetails(DefaultValue defaultValue, Predictions predictions, IEnumerable<Expense> expenses, IEnumerable<OperationSheet> operationSheets, BillingTreasury billingTreasury)
        {
            billingTreasury.Expenditures.TotalExpenses = expenses.Sum(e => e.OrderDetails.TotalTTC);

            var expensesProducts = expenses.SelectMany(expense => expense
                .OrderDetails.GetAllProduct()
                .Select(product => new
                {
                    product.Quantity,
                    product.Product,
                    expense.SupplierId,
                    productCategoryType = product.Product.ProductCategoryType,
                }))
                .Where(product => !(product.productCategoryType is null));

            foreach (var group in expensesProducts.GroupBy(productDetails => productDetails.productCategoryType))
            {
                var totalAmount = group.Sum(p => p.Quantity * p.Product.GetPrice(p.SupplierId));
                //var totalPaid = group.Sum(e => e.GetTotalPaid());
                //var restToPay = totalAmount - totalPaid;
                var exepenseDetail = new ExpendituresExpenseDetails
                {
                    Total = totalAmount,
                    ProductCategoryType = group.Key,
                    TotalPaid = 0,
                    RestToPay = 0,
                    Percent = totalAmount / billingTreasury.Expenditures.TotalExpenses * 100,
                };

                if (group.Key.Type == CategoryType.Purchases)
                    exepenseDetail.Deference = exepenseDetail.Total / predictions.Expenses.TotalMaterialPurchases * 100;

                if (group.Key.Type == CategoryType.SubContracting)
                    exepenseDetail.Deference = exepenseDetail.Total / predictions.Expenses.SubContracting * 100;

                billingTreasury.Expenditures.ExpenseDetails.Add(exepenseDetail);
            }

            // set OperationSheetDetails
            var totalHours = (float)operationSheets.Sum(e => _dateService.GetTotalWorkingHours(e.StartDate, e.EndDate, defaultValue.StartingHour, defaultValue.EndingHour));
            var totalVists = operationSheets.Sum(e => e.VisitsCount);
            var totalVistsCost = totalVists * defaultValue.DisplacementCost;
            var totalBaskets = operationSheets.Sum(e => e.TotalBasketConsumption);
            var totalBasketsCost = totalBaskets * defaultValue.BasketCost;

            billingTreasury.Expenditures.OperationSheetDetails.TotalVisits = totalVists;
            billingTreasury.Expenditures.OperationSheetDetails.TotalBaskets = totalBaskets;
            billingTreasury.Expenditures.OperationSheetDetails.TotalWorkingHours = totalHours;
            billingTreasury.Expenditures.OperationSheetDetails.TotalVisitsCost = totalVistsCost;
            billingTreasury.Expenditures.OperationSheetDetails.TotalBasketsCost = totalBasketsCost;
            billingTreasury.Expenditures.OperationSheetDetails.TotalWorkingHoursCost = totalHours * defaultValue.BuyingPrice;

            if (predictions.Expenses.TotalWorkforcePurchases > 0)
                billingTreasury.Expenditures.OperationSheetDetails.Deference =
                    billingTreasury.Expenditures.OperationSheetDetails.Total /
                    predictions.Expenses.TotalWorkforcePurchases * 100;

            // set percentages and deferences
            billingTreasury.Expenditures.OperationSheetDetails.Percent = billingTreasury.Expenditures.OperationSheetDetails.Total / billingTreasury.Expenditures.Total * 100;
            billingTreasury.Expenditures.PercentExpenses = billingTreasury.Expenditures.TotalExpenses / billingTreasury.Expenditures.Total * 100;
        }

        private static void GetInvoicesDetails(Predictions predictions, IEnumerable<Invoice> invoices, BillingTreasury billingTreasury)
        {
            billingTreasury.InvoicesDetails.Total = invoices.Sum(e => e.OrderDetails.TotalTTC);
            billingTreasury.InvoicesDetails.TotalPaid = (float)invoices.Sum(e => e.GetTotalPaid());
            billingTreasury.InvoicesDetails.TotalRestToPay = (float)invoices.Sum(e => e.GetRestToPay());
            billingTreasury.InvoicesDetails.Deference = predictions.Quotes.Total / billingTreasury.InvoicesDetails.Total * 100;

            billingTreasury.InvoicesDetails.TotalMaterialSales = invoices
                .SelectMany(invoice => invoice.OrderDetails.GetAllProduct())
                .Where(productDetail => productDetail.Product.ProductCategoryType.Type == CategoryType.Purchases)
                .Sum(productDetail => productDetail.Product.MaterialCost * productDetail.Quantity);

            billingTreasury.InvoicesDetails.TotalWorkforceSales = invoices
                .SelectMany(invoice => invoice.OrderDetails.GetAllProduct())
                .Where(productDetail => productDetail.Product.ProductCategoryType.Type == CategoryType.WorkforcePurchase)
                .Sum(productDetail => productDetail.Product.TotalHours * productDetail.Product.HourlyCost * productDetail.Quantity);
        }

        private float GetTotalWorkshopTreasury(ConstructionWorkshop workshop)
        {
            var totalPaidInvoices = (float)workshop.Invoices.Sum(e => e.GetTotalPaid());
            var totalPaidExpenses = (float)workshop.Expenses.Sum(e => e.GetTotalPaid());
            return totalPaidInvoices - totalPaidExpenses;
        }

        private Predictions GetPredictions(ConstructionWorkshop workshop, DefaultValue defaultValues)
        {
            // quotes
            var quotes = workshop.Quotes.Where(e => e.Status != QuoteStatus.Canceled && e.Status != QuoteStatus.Refused);

            // initialize the prediction object
            var predictions = new Predictions();

            // => get the total quotes
            var products = quotes.SelectMany(e => e.OrderDetails.GetAllProduct());

            predictions.Quotes.MaterialSales = products.Sum(e => e.Quantity * e.Product.MaterialCost);
            predictions.Quotes.TotalWorkforceWorkHours = products.Sum(e => e.Product.TotalHours);
            predictions.Quotes.WorkforceSales = products.Sum(e => e.Quantity * e.Product.TotalHours * defaultValues.SalesPrice);

            // => get the Expenses predictions
            predictions.Expenses.TotalMaterialPurchases = products.Sum(e => (e.Product.DefaultSupplier?.Price ?? 0) * e.Quantity);
            predictions.Expenses.TotalWorkforcePurchases = products.Sum(e => e.Product.TotalHours * defaultValues.SalesPrice * e.Quantity);
            predictions.Expenses.TotalWorkforceHours = products.Sum(e => e.Product.TotalHours);
            predictions.Expenses.SubContracting = workshop.Amount;
            predictions.Expenses.SubContractingHours = workshop.TotalHours;

            // => margins
            predictions.Margin = new MarginPredictions(predictions.Quotes.Total - predictions.Expenses.Total, (predictions.Quotes.Total - predictions.Expenses.Total) * 100 / predictions.Expenses.Total)
            {
                TotalHoldback = quotes.Where(e => !(e.OrderDetails.HoldbackDetails is null)).Sum(e => (e.OrderDetails.HoldbackDetails.Holdback * e.OrderDetails.TotalHT) / 100),
                MaterialMargin = predictions.Quotes.MaterialSales - predictions.Expenses.TotalMaterialPurchases,
                WorkforceMargin = predictions.Quotes.WorkforceSales - predictions.Expenses.TotalWorkforcePurchases
            };

            return predictions;
        }
    }
}
