﻿namespace Axiobat.Services.Contacts
{
    using Application.Data;
    using Application.Services.AccountManagement;
    using Application.Services.Configuration;
    using Application.Services.Contacts;
    using Application.Services.FileService;
    using Application.Services.Localization;
    using AutoMapper;
    using Domain.Entities;
    using Microsoft.Extensions.Logging;
    using System.Threading.Tasks;

    /// <summary>
    /// a service implementation for <see cref="IGroupService"/>
    /// </summary>
    public partial class GroupService : DataService<Group, int>, IGroupService
    {
        /// <summary>
        /// check if the group with the given id exist
        /// </summary>
        /// <param name="groupId">the id of the group</param>
        /// <returns>true if exist, false if not</returns>
        public Task<bool> IsGroupExistAsync(int groupId)
            => _dataAccess.IsExistAsync(e => e.Id == groupId);

        /// <summary>
        /// check if the group name is unique or not
        /// </summary>
        /// <param name="groupName">name of the group to be checked</param>
        /// <returns>true if unique, false if not</returns>
        public async Task<bool> IsUniqueGroupNameAsync(string groupName)
            => !await _dataAccess.IsExistAsync(e => e.Name == groupName);
    }

    /// <summary>
    /// partial part for <see cref="GroupService"/>
    /// </summary>
    public partial class GroupService
    {
        protected override string EntityName => typeof(Group).Name;

        public GroupService(
                    IGroupDataAccess dataAccess,
                    IFileService fileService,
                    IValidationService validation,
                    IHistoryService historyService,
                    ILoggedInUserService loggedInUserService,
                    IApplicationConfigurationService appSetting,
                    ITranslationService transalationService,
                    ILoggerFactory loggerFactory,
                    IMapper mapper)
                    : base(dataAccess, fileService, validation, historyService, loggedInUserService, appSetting, transalationService, loggerFactory, mapper)
        {
        }
    }
}
