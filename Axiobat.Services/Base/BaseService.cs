﻿namespace Axiobat.Services
{
    using Application.Services;
    using Application.Services.AccountManagement;
    using Application.Services.Configuration;
    using Application.Services.Localization;
    using AutoMapper;
    using Microsoft.Extensions.Logging;
    using System.Diagnostics;

    /// <summary>
    /// the base Data Service class, here only put shared properties and methods,
    /// that all other data services will use if you want to share logic use <see cref="DataService{TKey, TEntity}"/>
    /// </summary>
    [DebuggerStepThrough]
    public abstract partial class BaseService
    {
        /// <summary>
        /// get the localized string version of the given string
        /// </summary>
        /// <param name="stringToLocalize">the string value to localize</param>
        /// <returns>the localized version</returns>
        protected string T(string stringToLocalize, params object[] args)
            => _translator.Get(stringToLocalize, args);

        /// <summary>
        /// Execute a mapping from the source object to a new destination object. The source
        /// type is inferred from the source object.
        /// </summary>
        /// <typeparam name="TOut">Destination type to create</typeparam>
        /// <param name="obj">Source object to map from</param>
        /// <returns>Mapped destination object</returns>
        protected TOut Map<TOut>(object obj) => _mapper.Map<TOut>(obj);
    }

    /// <summary>
    /// the partial part for <see cref="BaseService"/>
    /// </summary>
    public abstract partial class BaseService : IBaseDataService
    {
        protected readonly IMapper _mapper;
        protected readonly ILogger _logger;
        protected readonly ITranslationService _translator;
        protected readonly IApplicationConfigurationService _configuration;
        protected readonly ILoggedInUserService _loggedInUserService;

        /// <summary>
        /// get the name of the entity being processed
        /// </summary>
        protected abstract string EntityName { get; }

        /// <summary>
        /// the default constructor for <see cref="BaseService"/>
        /// </summary>
        /// <param name="loggedInUserService">the current logged in user service</param>
        /// <param name="configurationService">the <see cref="IApplicationConfigurationService"/> service</param>
        /// <param name="translationService">the <see cref="ITranslationService"/> service</param>
        /// <param name="loggerFactory">the <see cref="ILoggerFactory"/> instant</param>
        /// <param name="mapper"><see cref="IMapper"/> instant</param>
        protected BaseService(
            ILoggedInUserService loggedInUserService,
            IApplicationConfigurationService configurationService,
            ITranslationService translationService,
            ILoggerFactory loggerFactory,
            IMapper mapper)
        {
            _mapper = mapper;
            _configuration = configurationService;
            _translator = translationService;
            _loggedInUserService = loggedInUserService;
            _logger = loggerFactory.CreateLogger($"{EntityName}.DataService");
        }
    }
}
