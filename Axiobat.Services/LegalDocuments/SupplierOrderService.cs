﻿namespace Axiobat.Services.Documents
{
    using App.Common;
    using Application.Data;
    using Application.Exceptions;
    using Application.Models;
    using Application.Services.AccountManagement;
    using Application.Services.Configuration;
    using Application.Services.Contacts;
    using Application.Services.Documents;
    using Application.Services.FileService;
    using Application.Services.Localization;
    using AutoMapper;
    using Domain.Constants;
    using Domain.Entities;
    using Domain.Entities.Configuration;
    using Domain.Enums;
    using Microsoft.Extensions.Logging;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// the service for <see cref="SupplierOrder"/>
    /// </summary>
    public partial class SupplierOrderService
    {
        /// <summary>
        /// save the memo to the client with the given id
        /// </summary>
        /// <param name="entityId">the id of the client to add memo to it</param>
        /// <param name="memos">the memo to add</param>
        /// <returns>an operation result</returns>
        public async Task<Result<Memo>> SaveMemoAsync(string entityId, MemoModel model)
        {
            var memo = new Memo
            {
                Id = model.Id,
                Comment = model.Comment,
                CreatedOn = DateTime.Now,
                Attachments = await _fileService.SaveAsync(model.Attachments.ToArray()),
                User = new MinimalUser(_loggedInUserService.User.UserId, _loggedInUserService.User.UserName),
            };

            var updateResult = await _dataAccess.UpdateAsync(entityId, entity => entity.Memos.Add(memo));
            if (!updateResult.HasValue)
                return Result.From<Memo>(updateResult);

            return memo;
        }

        /// <summary>
        /// update the memo with the given id
        /// </summary>
        /// <param name="entityId">the id of the entity to be updated</param>
        /// <param name="memoId">the id of the memo to be updated</param>
        /// <param name="memoModel">the new memo</param>
        /// <returns>the updated version of the memo</returns>
        public async Task<Result<Memo>> UpdateMemoAsync(string entityId, string memoId, MemoModel memoModel)
        {
            // retrieve entity
            var entity = await _dataAccess.GetByIdAsync(entityId);
            if (entity is null)
                return Result.Failed<Memo>("Failed to update the memo, there is no entity with the given id");

            // retrieve memo
            var memo = entity.Memos.FirstOrDefault(e => e.Id == memoId);
            if (memo is null)
                return Result.Failed<Memo>("Failed to update the memo, there is no memo with the given id");

            // update the memo
            memo.Comment = memoModel.Comment;

            // set the attachment deferences
            var intersection = memo.Attachments.Select(e => new AttachmentModel
            {
                FileId = e.FileId,
                FileName = e.FileName,
                FileType = e.FileType
            })
            .Intersection(memoModel.Attachments);

            // delete attachments from the server and memo
            var removed = intersection.Removed.Select(e => new Attachment(e.FileId, e.FileName, e.FileType)).ToArray();
            var Added = memoModel.Attachments.Where(e => intersection.Added.Select(a => a.FileId).Contains(e.FileId));
            var attachementToRemove = memo.Attachments.Where(e => removed.Select(a => a.FileId).Contains(e.FileId)).ToArray();

            await _fileService.DeleteAsync(removed);

            foreach (var attachment in attachementToRemove)
                memo.Attachments.Remove(attachment);

            // add the new attachments
            var newAttachments = await _fileService.SaveAsync(Added.ToArray());

            foreach (var attachment in newAttachments)
                memo.Attachments.Add(attachment);

            // update the entity
            var updateResult = await _dataAccess.UpdateAsync(entity);
            if (!updateResult.IsSuccess)
                return Result.Failed<Memo>("failed to update the entity");

            return memo;
        }

        /// <summary>
        /// delete <see cref="Memo"/> form the <see cref="TEntity"/> using the given <see cref="DeleteMemoModel"/>
        /// </summary>
        /// <param name="model">the <see cref="DeleteMemoModel"/></param>
        /// <returns>the operation result</returns>
        public async Task<Result> DeleteMemosAsync(string entityId, DeleteMemoModel model)
        {
            var entity = await _dataAccess.GetByIdAsync(entityId);
            if (entity is null)
                throw new NotFoundException("entity not found");

            var memosToDelete = entity.Memos.Where(e => model.MemosIds.Contains(e.Id)).ToList();
            var attachments = memosToDelete.SelectMany(e => e.Attachments).ToArray();

            foreach (var memo in memosToDelete)
                entity.Memos.Remove(memo);

            var deleteResult = await _dataAccess.UpdateAsync(entity);
            if (!deleteResult.IsSuccess)
            {
                _logger.LogCritical(LogEvent.DeleteMemos, "Failed to delete the memos check previous logs");
                Result.Failed("Failed to delete memos");
            }

            var deleted = await _fileService.DeleteAsync(attachments);
            _logger.LogInformation(LogEvent.DeleteMemos, "[{deletedFiles}] file has been deleted out of [{totalFiles}]", deleted, attachments.Length);

            return Result.Success();
        }

        /// <summary>
        /// generate the Supplier Orders from the given quote
        /// </summary>
        /// <param name="document">the Quote document instant</param>
        /// <returns>the operation result</returns>
        public async Task<ListResult<MinimalDocumentModel>> GenerateFromQuoteAsync(Quote document)
        {
            // retrieve the document configuration
            var configuration = await _configuration.GetDocumentConfigAsync(DocumentType.SupplierOrder);

            // retrieve document numerator configuration
            var numerators = await _configuration.GetAsync<IEnumerable<Numerator>>(ApplicationConfigurationType.Counter);
            var documentNumerator = numerators.First(e => e.DocumentType == DocumentType.SupplierOrder);

            // extract the list products
            if (document.OrderDetails.ProductsDetailsType == OrderProductsDetailsType.File)
                return Result.ListFailed<MinimalDocumentModel>("the products details is a file", MessageCode.InvalidProductDetailsType);

            // var list of products
            var products = document.OrderDetails.GetAllProduct();

            // list of suppliers
            var suppliers = (await _supplierService.GetAllAsync<SupplierDocuement>(
                products.Select(productDetails => productDetails.Product.DefaultSupplier.SupplierId)
                .Distinct().ToArray()))
                .ToDictionary(e => e.Id);

            //generate the list of supplier orders
            var supplierOrders = products
                .GroupBy(e => e.Product.DefaultSupplier.SupplierId)
                .Select(e => GenerateSupplierOrder(e, document.Id, document.WorkshopId, configuration, documentNumerator, suppliers))
                .ToList();

            // insert the documents
            var insertResult = await _dataAccess.AddRangeAsync(supplierOrders);
            if (!insertResult.IsSuccess)
                return Result.ListFailed<MinimalDocumentModel>("Failed to insert the supplier orders", insertResult.MessageCode);

            // save the numerator
            await _configuration.UpdateAsync(ApplicationConfigurationType.Counter, numerators.ToJson());

            // all done
            return Result.ListSuccess(Map<IEnumerable<MinimalDocumentModel>>(supplierOrders));
        }

        /// <summary>
        /// update the status of the given supplier orders
        /// </summary>
        /// <param name="status">the status to assign</param>
        /// <param name="supplierOrdersIds">the list of supplier orders to be updated</param>
        /// <returns>the operation result</returns>
        public async Task UpdateStatusAsync(string status, params string[] supplierOrdersIds)
        {
            // update the status of the suppliers with the given ids
            var updated = await _dataAccess.UpdateStatusAsync(status, supplierOrdersIds);

            // log result
            if (updated < supplierOrdersIds.Length)
                _logger.LogError("Failed to update the status of given suppliers orders to [{status}], updated [{CountUpdated}] out of [{total}]", status, updated, supplierOrdersIds.Length);
        }

        /// <summary>
        /// get the workshop supplier orders with the given id
        /// </summary>
        /// <param name="workshopId">the id of the workshop</param>
        /// <param name="supplierId">the id of the supplier</param>
        /// <param name="status">the documents status</param>
        /// <returns>the list of the workshops</returns>
        public async Task<ListResult<SupplierOrderModel>> GetWorkshopSupplierOrdersBySupplierIdAsync(string workshopId, string supplierId, params string[] status)
        {
            var result = await _dataAccess.GetByWorkshopIdSupplierIdStatusAsync(workshopId, supplierId, status);
            return Result.ListSuccess(Map<IEnumerable<SupplierOrderModel>>(result));
        }
    }

    /// <summary>
    /// partial part for <see cref="SupplierOrderService"/>
    /// </summary>
    public partial class SupplierOrderService : DocumentService<SupplierOrder>, ISupplierOrderService
    {
        private readonly ISupplierService _supplierService;
        private new ISupplierOrderDataAccess _dataAccess => base._dataAccess as ISupplierOrderDataAccess;

        public SupplierOrderService(
            ISupplierService supplierService,
            ISupplierOrderDataAccess dataAccess,
            ISynchronizationResolverService synchronizeDataService,
            IFileService fileService,
            IValidationService validation,
            IHistoryService historyService,
            ILoggedInUserService loggedInUserService,
            IApplicationConfigurationService appSetting,
            ITranslationService transalationService,
            ILoggerFactory loggerFactory,
            IMapper mapper)
            : base(dataAccess, synchronizeDataService, fileService, validation, historyService, loggedInUserService, appSetting, transalationService, loggerFactory, mapper)
        {
            _supplierService = supplierService;
        }

        /// <summary>
        /// after getting the entity by the id
        /// </summary>
        /// <typeparam name="TOut">the output type</typeparam>
        /// <param name="entity">the entity instant</param>
        /// <param name="mappedEntity">the mapped entity instant</param>
        protected override Task InGet_AfterMappingAsync<TOut>(SupplierOrder entity, TOut mappedEntity)
        {
            if (mappedEntity is SupplierOrderModel quoteModel)
            {
                var documents = entity.Expenses.Select(e => new AssociatedDocument
                {
                    Id = e.Expense.Id,
                    Status = e.Expense.Status,
                    Reference = e.Expense.Reference,
                    DocumentType = DocumentType.Expenses,
                    CreationDate = e.Expense.CreationDate,
                });

                foreach (var item in documents)
                    quoteModel.AssociatedDocuments.Add(item);
            }

            return base.InGet_AfterMappingAsync(entity, mappedEntity);
        }

        protected override Task InUpdate_BeforUpdateAsync<TUpdateModel>(SupplierOrder supplierOrder, TUpdateModel updateModel)
        {
            supplierOrder.Workshop = null;
            return base.InUpdate_BeforUpdateAsync(supplierOrder, updateModel);
        }

        /// <summary>
        /// create a new supplier order and generate a reference for it also increment the reference
        /// </summary>
        /// <param name="group">the supplier-product group</param>
        /// <param name="quoteId">the id of the quote<param>
        /// <param name="configuration">the document configuration<param>
        /// <param name="workshopId">the workshop Id<param>
        /// <param name="documentNumerator">the document numerotation</param>
        /// <returns>the supplier order instant</returns>
        private SupplierOrder GenerateSupplierOrder(
            IGrouping<string, ProductsDetails> group,
            string quoteId, string workshopId,
            DocumentConfiguration configuration,
            Numerator documentNumerator,
            Dictionary<string, SupplierDocuement> suppliers)
        {
            suppliers.TryGetValue(group.Key, out SupplierDocuement supplier);

            var supplierOrder = new SupplierOrder
            {
                QuoteId = quoteId,
                Supplier = supplier,
                SupplierId = group.Key,
                WorkshopId = workshopId,
                Note = configuration.Note,
                CreationDate = DateTime.Now,
                Purpose = configuration.Purpose,
                Status = SupplierOrderStatus.Draft,
                Reference = documentNumerator.Generate(true),
                PaymentCondition = configuration.PaymentCondition,
                DueDate = DateTime.Now.AddDays(configuration.Validity),
                OrderDetails = new OrderDetails
                {
                    LineItems = group.Select(e => new OrderProductDetails
                    {
                        Type = ProductType.Product,
                        Quantity = e.Quantity,
                        Product = e.Product,
                    })
                   .ToList(),
                },
            };

            supplierOrder.OrderDetails.CalculateTotalHT();
            supplierOrder.OrderDetails.CalculateTotalTI();

            supplierOrder.Id = EntityId.Generate<SupplierOrder>(DateTime.Now.AddSeconds(1));

            _history.Recored(supplierOrder.ChangesHistory, ChangesHistoryType.Added);
            return supplierOrder;
        }
    }
}
