﻿namespace Axiobat.Services.Documents
{
    using App.Common;
    using Application.Data;
    using Application.Enums;
    using Application.Exceptions;
    using Application.Models;
    using Application.Services;
    using Application.Services.AccountManagement;
    using Application.Services.Configuration;
    using Application.Services.Contacts;
    using Application.Services.Documents;
    using Application.Services.FileService;
    using Application.Services.Localization;
    using Application.Services.MailService;
    using AutoMapper;
    using Domain.Constants;
    using Domain.Entities;
    using Domain.Enums;
    using Microsoft.Extensions.Logging;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// the service for <see cref="QuoteService"/>
    /// </summary>
    public partial class QuoteService
    {
        /// <summary>
        /// save the memo to the client with the given id
        /// </summary>
        /// <param name="entityId">the id of the client to add memo to it</param>
        /// <param name="memos">the memo to add</param>
        /// <returns>an operation result</returns>
        public async Task<Result<Memo>> SaveMemoAsync(string entityId, MemoModel model)
        {
            var memo = new Memo
            {
                Id = model.Id,
                Comment = model.Comment,
                CreatedOn = DateTime.Now,
                Attachments = await _fileService.SaveAsync(model.Attachments.ToArray()),
                User = new MinimalUser(_loggedInUserService.User.UserId, _loggedInUserService.User.UserName),
            };

            var updateResult = await _dataAccess.UpdateAsync(entityId, entity => {
                if( entity.Memos is null) { entity.Memos = new HashSet<Memo>(); }
                entity.Memos.Add(memo);
            });
            if (!updateResult.HasValue)
                return Result.From<Memo>(updateResult);

            return memo;
        }

        /// <summary>
        /// update the memo with the given id
        /// </summary>
        /// <param name="entityId">the id of the entity to be updated</param>
        /// <param name="memoId">the id of the memo to be updated</param>
        /// <param name="memoModel">the new memo</param>
        /// <returns>the updated version of the memo</returns>
        public async Task<Result<Memo>> UpdateMemoAsync(string entityId, string memoId, MemoModel memoModel)
        {
            // retrieve entity
            var entity = await _dataAccess.GetByIdAsync(entityId);
            if (entity is null)
                return Result.Failed<Memo>("Failed to update the memo, there is no entity with the given id");

            // retrieve memo
            var memo = entity.Memos.FirstOrDefault(e => e.Id == memoId);
            if (memo is null)
                return Result.Failed<Memo>("Failed to update the memo, there is no memo with the given id");

            // update the memo
            memo.Comment = memoModel.Comment;

            // set the attachment deferences
            var intersection = memo.Attachments.Select(e => new AttachmentModel
            {
                FileId = e.FileId,
                FileName = e.FileName,
                FileType = e.FileType
            })
            .Intersection(memoModel.Attachments);

            // delete attachments from the server and memo
            var removed = intersection.Removed.Select(e => new Attachment(e.FileId, e.FileName, e.FileType)).ToArray();
            var Added = memoModel.Attachments.Where(e => intersection.Added.Select(a => a.FileId).Contains(e.FileId));
            var attachementToRemove = memo.Attachments.Where(e => removed.Select(a => a.FileId).Contains(e.FileId)).ToArray();

            await _fileService.DeleteAsync(removed);

            foreach (var attachment in attachementToRemove)
                memo.Attachments.Remove(attachment);

            // add the new attachments
            var newAttachments = await _fileService.SaveAsync(Added.ToArray());

            foreach (var attachment in newAttachments)
                memo.Attachments.Add(attachment);

            // update the entity
            var updateResult = await _dataAccess.UpdateAsync(entity);
            if (!updateResult.IsSuccess)
                return Result.Failed<Memo>("failed to update the entity");

            return memo;
        }

        /// <summary>
        /// delete <see cref="Memo"/> form the <see cref="TEntity"/> using the given <see cref="DeleteMemoModel"/>
        /// </summary>
        /// <param name="model">the <see cref="DeleteMemoModel"/></param>
        /// <returns>the operation result</returns>
        public async Task<Result> DeleteMemosAsync(string entityId, DeleteMemoModel model)
        {
            var entity = await _dataAccess.GetByIdAsync(entityId);
            if (entity is null)
                throw new NotFoundException("entity not found");

            var memosToDelete = entity.Memos.Where(e => model.MemosIds.Contains(e.Id)).ToList();
            var attachments = memosToDelete.SelectMany(e => e.Attachments).ToArray();

            foreach (var memo in memosToDelete)
                entity.Memos.Remove(memo);

            var deleteResult = await base._dataAccess.UpdateAsync(entity);
            if (!deleteResult.IsSuccess)
            {
                _logger.LogCritical(LogEvent.DeleteMemos, "Failed to delete the memos check previous logs");
                Result.Failed("Failed to delete memos");
            }

            var deleted = await _fileService.DeleteAsync(attachments);
            _logger.LogInformation(LogEvent.DeleteMemos, "[{deletedFiles}] file has been deleted out of [{totalFiles}]", deleted, attachments.Length);

            return Result.Success();
        }

        /// <summary>
        /// send the quote with the given id with an email using the given options
        /// </summary>
        /// <param name="quoteId">the id of the document</param>
        /// <param name="emailOptions">the email options</param>
        /// <returns>the operation result</returns>
        public async Task<Result> SendAsync(string quoteId, SendEmailOptions emailOptions)
        {
            // retrieve the document
            var document = await _dataAccess.GetByIdAsync(quoteId);
            if (document is null)
                throw new NotFoundException("there is no quote with the given id");

            // generate the document Pdf
            //var filePDF = await ExportDataAsync(quoteId, new DataExportOptions { ExportType = ExportType.PDF });
            //if (!filePDF.IsSuccess)
            //    return filePDF;

            // generate an id to the email

            // add the PDF file to the email attachments
            //emailOptions.Attachments.Add(new AttachmentModel
            //{
            //    FileType = "application/pdf",
            //    Content = Convert.ToBase64String(filePDF),
            //    FileName = $"{DocumentType.Quote}-{DateHelper.Timestamp}.pdf",
            //});

            emailOptions.Id = $"{quoteId}-".AppendTimeStamp();
            // send the email
            var result = await _emailService.SendEmailAsync(emailOptions);
            if (!result.IsSuccess)
                return result;

            // add the email document
            document.Emails.Add(new DocumentEmail
            {
                To = emailOptions.To,
                SentOn = DateTime.Now,
                MailId = emailOptions.Id,
                Content = emailOptions.Body,
                Subject = emailOptions.Subject,
                User = _loggedInUserService.GetMinimalUser(),
            });

            // update the document
            return await _dataAccess.UpdateAsync(document);
        }

        /// <summary>
        /// update the status of the quote
        /// </summary>
        /// <param name="quoteId">the id of the quote to update</param>
        /// <param name="model">the model used for updating the status</param>
        /// <returns>the operation result</returns>
        public async Task<Result> UpdateStatusAsync(string quoteId, DocumentUpdateStatusModel model)
        {
            // retrieve the document
            var document = await _dataAccess.GetByIdAsync(quoteId);
            if (document is null)
                throw new NotFoundException("there is no quote with the given id");

            // update the status of the workshop
            if (model.Status == QuoteStatus.Accepted && document.WorkshopId.IsValid())
            {
                // update the workshop status
                await _workshopService.UpdateStatusAsync(document.WorkshopId, new WorkshopUpdateStatusModel
                {
                    Status = WorkshopStatus.Accepted
                });
            }

            _history.Recored(
                document,
                ChangesHistoryType.Updated,
                fields: new[]
                {
                    new ChangedField
                    {
                        Champ = nameof(Quote.Status).ToLower(),
                        PropName = nameof(Quote.Status),
                        ValeurInitial =document.Status,
                        ValeurFinal = model.Status,
                    }
                });

            document.Status = model.Status;
            return await _dataAccess.UpdateAsync(document);
        }

        /// <summary>
        /// generate Suppliers orders from the given quote
        /// </summary>
        /// <param name="quoteId">the id of the quote</param>
        /// <returns>the operation result</returns>
        public async Task<ListResult<MinimalDocumentModel>> GenerateSupplierOrderAsync(string quoteId)
        {
            // retrieve the document
            var document = await _dataAccess.GetByIdAsync(quoteId);
            if (document is null)
                throw new NotFoundException("there is no quote with the given id");

            return await _supplierOrderService.GenerateFromQuoteAsync(document);
        }

        /// <summary>
        /// generate an invoice from the quote with given id
        /// </summary>
        /// <param name="quoteId">the id of the quote</param>
        /// <returns>the operation result</returns>
        public async Task<Result<InvoiceModel>> GenerateInvoiceFromQuoteAsync(string quoteId)
        {
            // retrieve the document
            var document = await _dataAccess.GetByIdAsync(quoteId);
            if (document is null)
                throw new NotFoundException("there is no quote with the given id");

            // generate a reference for the invoice
            var reference = await _configuration.GenerateRefereceAsync(DocumentType.Invoice);

            // create invoice by mapping it form the quote
            var invoice = Map<Invoice>(document);
            invoice.Id = Generator.GenerateRandomId();
            invoice.Status = InvoiceStatus.InProgress;
            invoice.Reference = reference;

            // other properties
            var addResult = await _invoiceDataAccess.AddAsync(invoice);
            if (!addResult.IsSuccess)
                return Result.From<InvoiceModel>(addResult);

            // increment reference
            await _configuration.IncrementReferenceAsync(DocumentType.Invoice);

            // return the result
            return Map<InvoiceModel>(addResult.Value);
        }

        /// <summary>
        /// duplicate the quote with the given id
        /// </summary>
        /// <param name="quoteId">the id of the quote to be duplicated</param>
        /// <returns>the generated quote</returns>
        public async Task<Result<TOut>> DuplicateAsync<TOut>(string quoteId)
        {
            // retrieve the quote
            var quote = await _dataAccess.GetByIdAsync(quoteId);
            if (quote is null)
                throw new NotFoundException("there is no quote with the given id");

            // generate a new reference
            var reference = await GenerateReferenceAsync();

            // empty the quote date
            quote.Id = quote.GenerateId();
            quote.Reference = reference;
            quote.Workshop = null;
            quote.Invoices.Clear();
            quote.SupplierOrders.Clear();
            quote.ChangesHistory.Clear();

            // insert the new quote
            var addResult = await _dataAccess.AddAsync(quote);
            if (!addResult.IsSuccess)
                return Result.From<TOut>(addResult);

            // increment reference
            await IncrementRefenereceAsync();

            // return result
            return Map<TOut>(quote);
        }

        /// <summary>
        /// update the Quote situation list, id the situation exist it will be updated, if not it will be added
        /// </summary>
        /// <param name="quoteId">the id of the quote</param>
        /// <param name="quoteSituations">the quoteSituations</param>
        public async Task UpdateListSituationsAsync(string quoteId, QuoteSituations quoteSituations)
        {
            // get the quote
            var quote = await _dataAccess.GetByIdAsync(quoteId);
            if (quote is null)
            {
                _logger.LogError("failed to retrieve the quote with Id: [{quoteId}] to add the situation for the invoice with id: {invoiceId}", quoteId, quoteSituations.InvoiceId);
                return;
            }

            // set the quote situation
            SetQuoteSituations(quoteSituations, quote);

            // recode the history


            // update the quote
            await _dataAccess.UpdateAsync(quote);
        }

        /// <summary>
        /// remove the invoice form the list of situation
        /// </summary>
        /// <param name="quoteId">the id of the quote to be updated</param>
        /// <param name="invoiceId">the id of the invoice to be deleted</param>
        public async Task DeleteInvoiceFromListSituationsAsync(string quoteId, string invoiceId)
        {
            // get the quote
            var quote = await _dataAccess.GetByIdAsync(quoteId);
            if (quote is null)
            {
                _logger.LogError("failed to retrieve the quote with Id: [{quoteId}] to delete the situation for the invoice with id: {invoiceId}", quoteId, invoiceId);
                return;
            }

            // get the invoice situation
            var situation = quote.Situations.FirstOrDefault(e => e.InvoiceId == invoiceId);
            if (situation is null)
                return;

            // delete the invoice situation
            quote.Situations.Remove(situation);

            // update the quote
            await _dataAccess.UpdateAsync(quote);
        }

        /// <summary>
        /// after invoice has been Deleted/Canceled this method will remove quote related data
        /// and update it status
        /// </summary>
        /// <param name="quoteId">the id of the quote to be updated</param>
        /// <param name="invoiceId">the id of the deleted/canceled invoice</param>
        public async Task QuoteInvoiceDeletedAsync(string quoteId, string invoiceId)
        {
            // get the quote
            var quote = await _dataAccess.GetByIdAsync(quoteId);
            if (quote is null)
            {
                _logger.LogError("failed to retrieve the quote, {@data}", new
                {
                    quoteId,
                    invoiceId,
                    method = "[QuoteService.DeleteQuoteInvoiceAsync]",
                    operation = "delete the invoice relationship with quote",
                });
                return;
            }

            // get the invoice situation
            var situation = quote.Situations.FirstOrDefault(e => e.InvoiceId == invoiceId);
            if (!(situation is null))
            {
                // delete the invoice situation
                quote.Situations.Remove(situation);
            }

            if (quote.Status == QuoteStatus.Billed && !quote.Situations.Any())
            {
                quote.Status = QuoteStatus.InProgress;

                if (!(quote.ClientSignature is null))
                    quote.Status = QuoteStatus.Signed;

                _history.Recored(
                    entity: quote,
                    actionType: ChangesHistoryType.Updated,
                    fields: new[]
                    {
                        new ChangedField
                        {
                            PropName = nameof(Quote.Status),
                            Champ = nameof(Quote.Status).ToLower(),
                            ValeurInitial = QuoteStatus.Billed,
                            ValeurFinal = quote.Status,
                        }
                    });
            }

            // update the quote
            await _dataAccess.UpdateAsync(quote);
        }



        /// <summary>

        /// </summary>
        /// <param name="quoteId">the id of the quote to be updated</param>
        /// <param name="invoiceId">the id of the canceled invoice</param>
        public async Task QuoteInvoiceChangeStatutCancelAsync(string quoteId, string invoiceId)
        {
            // get the quote
            var quote = await _dataAccess.GetByIdAsync(quoteId);
            if (quote is null)
            {
                _logger.LogError("failed to retrieve the quote, {@data}", new
                {
                    quoteId,
                    invoiceId,
                    method = "[QuoteService.DeleteQuoteInvoiceAsync]",
                    operation = "delete the invoice relationship with quote",
                });
                return;
            }

            // get the invoice situation
            var situation = quote.Situations.FirstOrDefault(e => e.InvoiceId == invoiceId);
            if (!(situation is null))
            {
                quote.Situations.Where(e => e.InvoiceId == invoiceId).FirstOrDefault().Status = InvoiceStatus.Canceled;

            }



            if (quote.Status == QuoteStatus.Billed && quote.Situations.Where(e=>e.Status!= InvoiceStatus.Canceled).Count()==0 )
            {
                quote.Status = QuoteStatus.InProgress;

                if (!(quote.ClientSignature is null))
                    quote.Status = QuoteStatus.Signed;

                _history.Recored(
                    entity: quote,
                    actionType: ChangesHistoryType.Updated,
                    fields: new[]
                    {
                        new ChangedField
                        {
                            PropName = nameof(Quote.Status),
                            Champ = nameof(Quote.Status).ToLower(),
                            ValeurInitial = QuoteStatus.Billed,
                            ValeurFinal = quote.Status,
                        }
                    });
            }
            // update the quote
            await _dataAccess.UpdateAsync(quote);
        }


        /// <summary>
        /// update the signature of the operation Sheet
        /// </summary>
        /// <param name="quoteId">the id of the quote to update</param>
        /// <param name="model">the model used for updating the signature</param>
        /// <returns>the operation result</returns>
        public async Task<Result> UpdateSignatureAsync(string quoteId, DocumentUpdateSignatureModel model)
        {
            // retrieve the document
            var document = await _dataAccess.GetByIdAsync(quoteId);
            if (document is null)
                throw new NotFoundException("there is no quote with the given id");

            // update the devis status

            if (document.WorkshopId.IsValid())
                await _workshopService.UpdateStatusAsync(document.WorkshopId, new WorkshopUpdateStatusModel { Status = WorkshopStatus.Accepted });

            document.Status = QuoteStatus.Signed;
            document.ClientSignature = model.ClientSignature;
            return await _dataAccess.UpdateAsync(document);
        }
    }

    /// <summary>
    /// partial part for <see cref="QuoteService"/>
    /// </summary>
    public partial class QuoteService : DocumentService<Quote>, IQuoteService
    {
        private readonly IConstructionWorkshopService _workshopService;
        private readonly ISupplierOrderService _supplierOrderService;
        private readonly IInvoiceDataAccess _invoiceDataAccess;
        private readonly IEmailService _emailService;

        /// <summary>
        /// create an instant of <see cref="QuoteService"/>
        /// </summary>
        /// <param name="dataAccess">the data access service</param>
        /// <param name="fileService">the file service</param>
        /// <param name="validation">the validation service</param>
        /// <param name="historyService">the history service</param>
        /// <param name="loggedInUserService">the logged in user service</param>
        /// <param name="appSetting">the application configuration service</param>
        /// <param name="transalationService">the transaction service</param>
        /// <param name="loggerFactory">the logger factory service</param>
        /// <param name="mapper">the mapper</param>
        public QuoteService(
            IConstructionWorkshopService workshopService,
            ISupplierOrderService supplierOrderService,
            ISynchronizationResolverService synchronizeDataService,
            IInvoiceDataAccess invoiceDataAccess,
            IQuoteDataAccess dataAccess,
            IFileService fileService,
            IEmailService emailService,
            IValidationService validation,
            IHistoryService historyService,
            ILoggedInUserService loggedInUserService,
            IApplicationConfigurationService appSetting,
            ITranslationService transalationService,
            ILoggerFactory loggerFactory,
            IMapper mapper)
            : base(dataAccess, synchronizeDataService, fileService, validation, historyService, loggedInUserService, appSetting, transalationService, loggerFactory, mapper)
        {
            _workshopService = workshopService;
            _supplierOrderService = supplierOrderService;
            _invoiceDataAccess = invoiceDataAccess;
            _emailService = emailService;
        }

        protected override Task InCreate_AfterInsertAsync<TCreateModel>(Quote quote, TCreateModel createModel)
        {
            //if (quote.WorkshopId.IsValid())
            //    await _workshopService.UpdateStatusAsync(quote.WorkshopId, new WorkshopUpdateStatusModel { Status = WorkshopStatus.Accepted });

            return Task.CompletedTask;
        }

        protected override Task InUpdate_BeforUpdateAsync<TUpdateModel>(Quote quote, TUpdateModel updateModel)
        {
            if (!(updateModel is QuotePutModel))
                throw new ModelTypeIsNotValidException(nameof(updateModel), typeof(QuotePutModel));

            quote.Workshop = null;

            return Task.CompletedTask;
        }

        /// <summary>
        /// after getting the entity by the id
        /// </summary>
        /// <typeparam name="TOut">the output type</typeparam>
        /// <param name="entity">the entity instant</param>
        /// <param name="mappedEntity">the mapped entity instant</param>
        protected override Task InGet_AfterMappingAsync<TOut>(Quote entity, TOut mappedEntity)
        {
            if (mappedEntity is QuoteModel quoteModel)
            {
                var documents = Map<IEnumerable<AssociatedDocument>>(entity.Invoices)
                    .Concat(Map<IEnumerable<AssociatedDocument>>(entity.OperationSheets))
                    .Concat(Map<IEnumerable<AssociatedDocument>>(entity.SupplierOrders));

                foreach (var item in documents)
                    quoteModel.AssociatedDocuments.Add(item);
            }

            return base.InGet_AfterMappingAsync(entity, mappedEntity);
        }

        private static void SetQuoteSituations(QuoteSituations quoteSituations, Quote quote)
        {
            var existed = quote.Situations.FirstOrDefault(e => e.InvoiceId == quoteSituations.InvoiceId);
            if (existed is null)
            {
                quote.Situations.Add(quoteSituations);
                return;
            }

            existed.Situation = quoteSituations.Situation;
            existed.TotalHT = quoteSituations.TotalHT;
            existed.TotalTTC = quoteSituations.TotalTTC;
            existed.Status = quoteSituations.Status;
            existed.DueDate = quoteSituations.DueDate;
            existed.CreationDate = quoteSituations.CreationDate;
            existed.Purpose= quoteSituations.Purpose;


        }

        private async Task<string> GenerateReferenceAsync()
        {
            var reference = await _configuration.GenerateRefereceAsync(DocumentType.Quote);
            if (!await IsRefrenceUniqueAsync(reference))
            {
                await IncrementRefenereceAsync();
                return await GenerateReferenceAsync();
            }

            return reference;
        }
    }
}
