﻿namespace Axiobat.Persistence.DataAccess
{
    using Application.Data;
    using Domain.Entities;
    using DataContext;
    using Microsoft.Extensions.Logging;

    /// <summary>
    /// data access implementation for <see cref="IExternalPartnersDataAccess{TEntity}"/>
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    public partial class ExternalPartnersDataAccess<TEntity>
        where TEntity : ExternalPartner
    {

    }

    /// <summary>
    /// partial part for <see cref="ExternalPartnersDataAccess{TEntity}"/>
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    public partial class ExternalPartnersDataAccess<TEntity> : DataAccess<TEntity, string>, IExternalPartnersDataAccess<TEntity>
    {
        public ExternalPartnersDataAccess(ApplicationDbContext context, ILoggerFactory loggerFactory) : base(context, loggerFactory)
        {
        }
    }
}
