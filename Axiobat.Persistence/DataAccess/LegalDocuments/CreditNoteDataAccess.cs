﻿namespace Axiobat.Persistence.DataAccess
{
    using App.Common;
    using Application.Data;
    using Application.Models;
    using Axiobat.Domain.Constants;
    using DataContext;
    using Domain.Entities;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Logging;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;

    /// <summary>
    /// the data access implementation for <see cref="ICreditNoteDataAccess"/>
    /// </summary>
    public partial class CreditNoteDataAccess
    {

    }

    /// <summary>
    /// partial part for <see cref="CreditNoteDataAccess"/>
    /// </summary>
    public partial class CreditNoteDataAccess : DocumentDataAccess<CreditNote>, ICreditNoteDataAccess
    {
        /// <summary>
        /// create an instant of <see cref="CreditNoteDataAccess"/>
        /// </summary>
        /// <param name="context">the data db context</param>
        /// <param name="loggerFactory">the logger factory instant</param>
        public CreditNoteDataAccess(
            ApplicationDbContext context,
            ILoggerFactory loggerFactory)
            : base(context, loggerFactory)
        {
        }

        protected override IQueryable<CreditNote> SetDefaultIncludsForSingleRetrieve(IQueryable<CreditNote> query)
            => query
                .Include(e => e.Invoice)
                .Include(e => e.Payments)
                .Include(e => e.Workshop);

        protected override IQueryable<CreditNote> SetPagedResultFilterOptions<IFilter>(IQueryable<CreditNote> query, IFilter filterOption)
        {
            if (filterOption is DocumentFilterOptions filter)
            {
                if (filter.DateStart.HasValue)
                    query = query.Where(e => e.CreationDate >= filter.DateStart);

                if (filter.DateEnd.HasValue)
                    query = query.Where(e => e.CreationDate < filter.DateEnd);

                if (filter.WorkshopId.IsValid())
                    query = query.Where(e => e.WorkshopId == filter.WorkshopId);

                if (filter.ExternalPartnerId.IsValid())
                    query = query.Where(e => e.ClientId == filter.ExternalPartnerId);

                if (filter.Status.Any())
                {
                    if (filter.Status.Contains(QuoteStatus.Late))
                    {
                        var status = new HashSet<string>(filter.Status);
                        status.Remove(CreditNoteStatus.Expired);

                        var predicate = PredicateBuilder.True<CreditNote>()
                            .And(e => e.Status == QuoteStatus.InProgress && e.DueDate.Date < DateTime.Today);

                        if (status.Any())
                        {
                            predicate = predicate.Or(e => status.Contains(e.Status));
                        }
                    }
                    else
                    {
                        query = query.Where(e => filter.Status.Contains(e.Status));
                    }
                }
            }

            return query;
        }

        protected static Expression<Func<CreditNote, bool>> GetStatusPredicate(IEnumerable<string> status)
        {
            var newStatus = new HashSet<string>(status);
            var predicate = PredicateBuilder.True<CreditNote>();
            var isFirst = true;

            if (status.Contains(CreditNoteStatus.Expired))
            {
                predicate = predicate.And(e => e.Status == CreditNoteStatus.InProgress && e.DueDate.Date < DateTime.Today);
                newStatus.Remove(CreditNoteStatus.Expired);
                isFirst = false;
            }

            if (status.Contains(CreditNoteStatus.InProgress))
            {
                if (isFirst)
                    predicate = predicate.And(e => e.Status == CreditNoteStatus.InProgress && e.DueDate.Date > DateTime.Today);
                else
                    predicate = predicate.Or(e => e.Status == CreditNoteStatus.InProgress && e.DueDate.Date > DateTime.Today);

                newStatus.Remove(CreditNoteStatus.InProgress);
                isFirst = false;
            }

            if (newStatus.Any())
            {
                if (isFirst)
                    predicate = predicate.And(e => newStatus.Contains(e.Status));
                else
                    predicate = predicate.Or(e => newStatus.Contains(e.Status));
            }

            return predicate;
        }
    }
}
