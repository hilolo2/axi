﻿namespace Axiobat.Persistence.DataAccess
{
    using Application.Data;
    using Application.Models;
    using DataContext;
    using Domain.Entities;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Logging;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// the data access implementation for <see cref="IPaymentDataAccess"/>
    /// </summary>
    public partial class PaymentDataAccess
    {
        /// <summary>
        /// remove the list of payment invoices
        /// </summary>
        /// <param name="entities">the list of payment invoices</param>
        public async Task RemovePaymentinvoicesAsync(IEnumerable<Invoices_Payments> entities)
        {
            try
            {
                if (entities.Count() <= 0)
                    return;

                _context.Invoices_Payments.AddRange(entities);
                await _context.SaveChangesAsync();
            }
            catch (System.Exception ex)
            {
                _logger.LogCritical(ex, "failed to add the list of Invoices Payments");
            }
            finally
            {
                _logger.LogTrace("Detach All Entities from the dbContext");
                _context.DetachAllEntities();
            }
        }

        /// <summary>
        /// add the list of payment invoices
        /// </summary>
        /// <param name="entities">the list of payment invoices</param>
        public async Task AddPaymentinvoicesAsync(IEnumerable<Invoices_Payments> entities)
        {
            try
            {
                if (entities.Count() <= 0)
                    return;

                _context.Invoices_Payments.RemoveRange(entities);
                await _context.SaveChangesAsync();
            }
            catch (System.Exception ex)
            {
                _logger.LogCritical(ex, "failed to remove the list of Invoices Payments");
            }
            finally
            {
                _logger.LogTrace("Detach All Entities from the dbContext");
                _context.DetachAllEntities();
            }
        }

        /// <summary>
        /// update list of payment invoices
        /// </summary>
        /// <param name="entities">the list of payment invoices</param>
        public async Task UpdatePaymentinvoicesAsync(IEnumerable<Invoices_Payments> entities)
        {
            try
            {
                if (entities.Count() <= 0)
                    return;

                _context.Invoices_Payments.UpdateRange(entities);
                var reult = await _context.SaveChangesAsync();
            }
            catch (System.Exception ex)
            {
                _logger.LogCritical(ex, "failed to remove the list of Invoices Payments");
            }
            finally
            {
                _logger.LogTrace("Detach All Entities from the dbContext");
                _context.DetachAllEntities();
            }
        }

        /// <summary>
        /// remove all payments of the expenses
        /// </summary>
        /// <param name="entities">the list of entities to be removed</param>
        public async Task RemoveExpensePaymentAsync(IEnumerable<Expenses_Payments> entities)
        {
            try
            {
                if (entities.Count() <= 0)
                    return;

                _context.Expenses_Payments.RemoveRange(entities);
                await _context.SaveChangesAsync();
            }
            catch (System.Exception ex)
            {
                _logger.LogCritical(ex, "failed to update the list of expenses Payments");
            }
            finally
            {
                _logger.LogTrace("Detach All Entities from the dbContext");
                _context.DetachAllEntities();
            }
        }

        /// <summary>
        /// add the list of payment expenses
        /// </summary>
        /// <param name="entities">the list of payment expenses</param>
        public async Task AddExpensePaymentAsync(IEnumerable<Expenses_Payments> entities)
        {
            try
            {
                if (entities.Count() <= 0)
                    return;

                _context.Expenses_Payments.AddRange(entities);
                await _context.SaveChangesAsync();
            }
            catch (System.Exception ex)
            {
                _logger.LogCritical(ex, "failed to add the list of expenses Payments");
            }
            finally
            {
                _logger.LogTrace("Detach All Entities from the dbContext");
                _context.DetachAllEntities();
            }
        }

        /// <summary>
        /// update list of payment expenses
        /// </summary>
        /// <param name="entities">the list of payment expenses</param>
        public async Task UpdateExpensePaymentAsync(IEnumerable<Expenses_Payments> entities)
        {
            try
            {
                if (entities.Count() <= 0)
                    return;

                _context.Expenses_Payments.UpdateRange(entities);
                await _context.SaveChangesAsync();
            }
            catch (System.Exception ex)
            {
                _logger.LogCritical(ex, "failed to update the list of expenses Payments");
            }
            finally
            {
                _logger.LogTrace("Detach All Entities from the dbContext");
                _context.DetachAllEntities();
            }
        }

        /// <summary>
        /// get all payments of type expense and invoice
        /// </summary>
        /// <returns>the list of payments</returns>
        public Task<IEnumerable<Payment>> GetAllInvoiceExpensePaymentsAsync()
            => _context.Payments
                .Include(e => e.Invoices)
                .Include(e => e.Expenses)
                .Where(e => e.Type == Domain.Enums.PaymentType.Expense || e.Type == Domain.Enums.PaymentType.Invoice)
                .ToIEnumerableAsync();
    }

    /// <summary>
    /// partial part for <see cref="PaymentDataAccess"/>
    /// </summary>
    public partial class PaymentDataAccess : DataAccess<Payment, string>, IPaymentDataAccess
    {
        /// <summary>
        /// create an instant of <see cref="PaymentDataAccess"/>
        /// </summary>
        /// <param name="context">the data db context</param>
        /// <param name="loggerFactory">the logger factory instant</param>
        public PaymentDataAccess(
            ApplicationDbContext context,
            ILoggerFactory loggerFactory)
            : base(context, loggerFactory)
        {
        }

        protected override IQueryable<Payment> SetPagedResultFilterOptions<IFilter>(IQueryable<Payment> query, IFilter filterOption)
        {
            if (filterOption is PaymentFilterOptions filter)
            {
                if (filter.DateStart.HasValue)
                    query = query.Where(e => e.DatePayment >= filter.DateStart);

                if (filter.DateStart.HasValue)
                    query = query.Where(e => e.DatePayment < filter.DateEnd);

                if (filter.AccountId.HasValue)
                    query = query.Where(e => e.AccountId == filter.AccountId);
            }

            return query;
        }

        protected override IQueryable<Payment> SetDefaultIncludsForListRetrieve(IQueryable<Payment> query)
            => query.Include(e => e.Account)
                .Include(e => e.PaymentMethod)
                .Include(e => e.Expenses)
                .Include(e => e.Invoices);

        protected override IQueryable<Payment> SetDefaultIncludsForSingleRetrieve(IQueryable<Payment> query)
            => query.Include(e => e.CreditNote)
                .Include(e => e.PaymentMethod)
                .Include(e => e.Account)
                .Include(e => e.Expenses)
                    .ThenInclude(e => e.Expense)
                .Include(e => e.Invoices)
                    .ThenInclude(e => e.Invoice);
    }
}
