﻿namespace Axiobat.Persistence.DataAccess
{
    using App.Common;
    using Application.Data;
    using Application.Models;
    using Domain.Enums;
    using Domain.Entities;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Logging;
    using Persistence.DataContext;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// the data access implementation for <see cref="IApplicationConfigurationDataAccess"/>
    /// </summary>
    public partial class ApplicationConfigurationDataAccess
    {
        /// <summary>
        /// return an <see cref="IQueryable{T}"/> collection of <see cref="TEntity"/>
        /// </summary>
        /// <typeparam name="TEntity">the type of the entity</typeparam>
        public IQueryable<TEntity> Entities<TEntity>() where TEntity : Entity => _context.Set<TEntity>();

        /// <summary>
        /// get the configuration of the given types
        /// </summary>
        /// <param name="configurationType">the type of the configuration to retrieve</param>
        /// <param name="LastModifiedOn">the time of the last modified on should be greater than</param>
        /// <returns>list of configuration</returns>
        public virtual Task<ApplicationConfiguration[]> GetConfigurationAsync(string[] configurationType, DateTimeOffset? LastModifiedOn)
        {
            var query = _context.ApplicationConfigurations
                .Where(e => configurationType.Contains(e.ConfigurationType));

            if (!(LastModifiedOn is null))
            {
                query = query
                    .Where(e => e.LastModifiedOn > LastModifiedOn);
            }

            return query.ToArrayAsync();
        }

        /// <summary>
        /// check if is there any application configuration with the given type
        /// </summary>
        /// <param name="configurationType">the type of the configuration to be retrieved, must be one of <see cref="ApplicationConfiguration"/> values</param>
        /// <returns>true if exist, false if not</returns>
        public Task<bool> IsConfigurationExistAsync(string configurationType)
            => _context.ApplicationConfigurations.AnyAsync(e => e.ConfigurationType == configurationType);

        /// <summary>
        /// get the <see cref="ApplicationConfiguration"/> instant associated with the given type
        /// </summary>
        /// <param name="configurationType">the type of the configuration to be retrieved, must be one of <see cref="ApplicationConfiguration"/> values</param>
        /// <returns>the <see cref="ApplicationConfiguration"/> instant</returns>
        public Task<ApplicationConfiguration> GetConfigurationAsync(string configurationType)
            => _context.ApplicationConfigurations.FirstOrDefaultAsync(e => e.ConfigurationType == configurationType);

        /// <summary>
        /// update the given configuration
        /// </summary>
        /// <param name="configuration">the configuration instant</param>
        /// <returns>the updated application configuration</returns>
        public async Task<Result<ApplicationConfiguration>> UpdateConfigurationAsync(ApplicationConfiguration configuration)
        {
            try
            {
                var entry = _context.ApplicationConfigurations.Update(configuration);
                var operationResult = await _context.SaveChangesAsync();

                if (operationResult <= 0)
                {
                    _logger.LogWarning(LogEvent.UpdatingData, "Failed to update the Application Configuration, unknown reason");
                    return Result.Failed<ApplicationConfiguration>($"Failed to update the Application Configuration", MessageCode.OperationFailedUnknown);
                }

                _logger.LogInformation(LogEvent.UpdatingData, "Application Configuration [type: {configurationType}] updated successfully", configuration.ConfigurationType);
                return entry.Entity;
            }
            catch (DbUpdateConcurrencyException ex)
            {
                _logger.LogError(LogEvent.UpdatingData, ex, "Failed to update Application Configuration cause of 'DbUpdateConcurrency'");
                return Result.Failed<ApplicationConfiguration>($"Failed to update the Application Configuration", MessageCode.OperationFailedException, ex);
            }
            catch (Exception ex)
            {
                _logger.LogError(LogEvent.UpdatingData, ex, "Failed to update Application Configuration [type: {configurationType}]", configuration.ConfigurationType);
                return Result.Failed<ApplicationConfiguration>($"Failed to update the Application Configuration", MessageCode.OperationFailedException, ex);
            }
            finally
            {
                _logger.LogTrace("Detach All Entities from the dbContext");
                _context.DetachAllEntities();
            }
        }

        /// <summary>
        /// get list of all categories
        /// </summary>
        /// <returns>the list of categories</returns>
        public Task<IEnumerable<ChartAccountItem>> GetAllCategoriesAsync()
            => _context.ChartOfAccounts.AsNoTracking()
                .Where(e =>
                    e.Type == ChartAccountType.Default ||
                    (e.Type == ChartAccountType.VAT && e.ParentId == null) ||
                    (e.Type == ChartAccountType.Expense && e.ParentId == null) ||
                    (e.Type == ChartAccountType.Sales && e.ParentId == null)
                )
                .Include(e => e.SubCategories)
                .ToIEnumerableAsync();

        /// <summary>
        /// get paged result of the entity using the given filter options
        /// </summary>
        /// <typeparam name="TEntity">the type of the entity</typeparam>
        /// <param name="filter">the filter instant</param>
        /// <returns>the paged result</returns>
        public Task<PagedResult<TEntity>> GetAllAsync<TEntity>(FilterOptions filterOption) where TEntity : Entity
        {
            var query = Entities<TEntity>();

            if (filterOption.SearchQuery.IsValid())
                query = query.Where(e => e.SearchTerms.Contains(filterOption.SearchQuery));

            return query.DynamicOrderBy(filterOption.OrderBy, filterOption.SortDirection)
                .AsPagedResultAsync(filterOption.Page, filterOption.PageSize, filterOption.IgnorePagination, _logger);
        }

        /// <summary>
        /// get list of all entities
        /// </summary>
        /// <typeparam name="TEntity">the type of the entity to be retrieved</typeparam>
        /// <returns>the list of entity</returns>
        public Task<IEnumerable<TEntity>> GetAllAsync<TEntity>() where TEntity : Entity
            => Entities<TEntity>().ToIEnumerableAsync();

        /// <summary>
        /// get the entity with the given id
        /// </summary>
        /// <typeparam name="TEntity">the type of the entity</typeparam>
        /// <typeparam name="Tkey">the type of the key</typeparam>
        /// <param name="entityId">the id of the entity</param>
        /// <returns></returns>
        public Task<TEntity> GetbyIdAsync<TEntity, Tkey>(Tkey entityId) where TEntity : Entity<Tkey>
            => Entities<TEntity>().SingleOrDefaultAsync(e => e.Id.Equals(entityId));

        public async Task<Result<TEntity>> AddAsync<TEntity, TKey>(TEntity entity) where TEntity : Entity<TKey>
        {
            try
            {
                entity.CreatedOn = DateTimeOffset.UtcNow;
                entity.LastModifiedOn = DateTimeOffset.UtcNow;
                entity.BuildSearchTerms();

                var entry = await _context.Set<TEntity>().AddAsync(entity);
                var operationResult = await _context.SaveChangesAsync();

                if (operationResult <= 0)
                {
                    _logger.LogWarning(LogEvent.InsertARecored, "Failed to add the [{EntityType}], unknown reason", nameof(TEntity));
                    return Result.Failed<TEntity>($"failed adding the {nameof(TEntity)}, unknown reason", MessageCode.OperationFailedUnknown);
                }

                _logger.LogInformation(LogEvent.InsertARecored, "[{EntityType}] Added successfully with id [{entityId}]", nameof(TEntity), entry.Entity.Id);
                return entry.Entity;
            }
            catch (Exception ex)
            {
                _logger.LogError(LogEvent.InsertARecored, ex, "Failed to add the [{EntityType}]", nameof(TEntity));
                return Result.Failed<TEntity>($"Failed adding the {nameof(TEntity)}s", MessageCode.OperationFailedException, ex);
            }
            finally
            {
                _logger.LogTrace("Detach All Entities from the dbContext");
                _context.DetachAllEntities();
            }
        }

        public async Task<Result<TEntity>> UpdateAsync<TEntity, TKey>(TEntity entity) where TEntity : Entity<TKey>
        {
            try
            {
                entity.LastModifiedOn = DateTimeOffset.UtcNow;
                entity.BuildSearchTerms();

                var entry = _context.Set<TEntity>().Update(entity);
                var operationResult = await _context.SaveChangesAsync();

                if (operationResult <= 0)
                {
                    _logger.LogWarning(LogEvent.UpdatingData, "Failed to update the [{EntityType}], unknown reason", nameof(TEntity));
                    return Result.Failed<TEntity>($"Failed to update the {nameof(TEntity)}", MessageCode.OperationFailedUnknown);
                }

                _logger.LogInformation(LogEvent.UpdatingData, "[{EntityType}] [id: {entityId}] updated successfully", nameof(TEntity), entry.Entity.Id);
                return entry.Entity;
            }
            catch (DbUpdateConcurrencyException ex)
            {
                _logger.LogError(LogEvent.UpdatingData, ex, "Failed to update [{EntityType}] cause of 'DbUpdateConcurrency'", nameof(TEntity));
                return Result.Failed<TEntity>($"Failed to update the {nameof(TEntity)}", MessageCode.OperationFailedException, ex);
            }
            catch (Exception ex)
            {
                _logger.LogError(LogEvent.UpdatingData, ex, "Failed to update [{EntityType}] [id: {entityId}]", nameof(TEntity), entity.Id);
                return Result.Failed<TEntity>($"Failed to update the {nameof(TEntity)}", MessageCode.OperationFailedException, ex);
            }
            finally
            {
                _logger.LogTrace("Detach All Entities from the dbContext");
                _context.DetachAllEntities();
            }
        }

        /// <summary>
        /// get list of all categories with the given type
        /// </summary>
        /// <param name="categoryType">the type of the categories to retrieve</param>
        /// <returns>list of <see cref="ChartAccountItem"/></returns>
        public Task<IEnumerable<ChartAccountItem>> GetAllCategoriesByTypeAsync(ChartAccountType categoryType)
            => _context.ChartOfAccounts.AsNoTracking()
                .Include(e => e.SubCategories)
                .Where(e => e.Type == categoryType && e.ParentId == null)
                .ToIEnumerableAsync();

        public async Task<Result> DeleteAsync<TEntity>(TEntity entity) where TEntity : Entity
        {
            try
            {
                _context.Set<TEntity>().Remove(entity);
                var operationResult = await _context.SaveChangesAsync();

                if (operationResult <= 0)
                {
                    _logger.LogWarning(LogEvent.DeleteARangeOfRecoreds, "Failed to delete [{EntityType}](s), unknown reason", typeof(TEntity));
                    return Result.Failed($"failed to delete {typeof(TEntity).Name}(s)", MessageCode.OperationFailedUnknown);
                }

                _logger.LogInformation(LogEvent.DeleteARangeOfRecoreds, "[{EntityType}](s) deleted successfully", typeof(TEntity));
                return Result.Success();
            }
            catch (Exception ex)
            {
                _logger.LogError(LogEvent.DeleteARangeOfRecoreds, ex, "Failed to delete [{EntityType}](s)", typeof(TEntity));
                return Result.Failed($"Failed to delete the {typeof(TEntity).Name}s", MessageCode.OperationFailedException, ex);
            }
            finally
            {
                _logger.LogTrace("Detach All Entities from the dbContext");
                _context.DetachAllEntities();
            }
        }

        public Task<WorkshopDocumentType> GetSpecialDocumentTypeAsync()
            => _context.WorkshopDocumentTypes.FirstOrDefaultAsync(e => e.AffectedStatus != null && e.NewStatus != null);

        public Task<PaymentMethod> GetCreditNotePaymentMethodAsync()
            => _context.PaymentMethod.AsNoTracking().FirstOrDefaultAsync(e => e.Type == PaymentMethodType.CreditNote);

        public Task<IEnumerable<TurnoverGoal>> GetTurnoverGoalsAsync()
            => _context.TurnoverGoals.ToIEnumerableAsync();

        public Task<int> GetPaymentMethodPaymentsCountAsync(int paymentMethodId)
            => _context.Payments.CountAsync(e => e.PaymentMethodId == paymentMethodId);
    }

    /// <summary>
    /// partial part for <see cref="IApplicationConfigurationDataAccess"/>
    /// </summary>
    public partial class ApplicationConfigurationDataAccess : IApplicationConfigurationDataAccess
    {
        private readonly ApplicationDbContext _context;
        private readonly ILogger<ApplicationConfigurationDataAccess> _logger;

        /// <summary>
        /// create an instant of <see cref="ApplicationConfigurationDataAccess"/>
        /// </summary>
        public ApplicationConfigurationDataAccess(
            ApplicationDbContext context,
            ILogger<ApplicationConfigurationDataAccess> logger)
        {
            _context = context;
            _logger = logger;
        }
    }
}
