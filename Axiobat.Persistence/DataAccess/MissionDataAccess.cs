﻿namespace Axiobat.Persistence.DataAccess
{
    using App.Common;
    using Application.Data;
    using Axiobat.Application.Models;
    using Axiobat.Domain.Constants;
    using DataContext;
    using Domain.Entities;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Logging;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;

    /// <summary>
    /// the data access implementation for <see cref="IMissionDataAccess"/>
    /// </summary>
    public partial class MissionDataAccess
    {

    }

    /// <summary>
    /// partial part for <see cref="IMissionDataAccess"/>
    /// </summary>
    public partial class MissionDataAccess : DataAccess<Mission, string>, IMissionDataAccess
    {
        public MissionDataAccess(
            ApplicationDbContext context,
            ILoggerFactory loggerFactory)
            : base(context, loggerFactory)
        {
        }

        protected override IQueryable<Mission> SetDefaultIncludsForSingleRetrieve(IQueryable<Mission> query)
            => query.Include(e => e.Technician)
                .Include(e => e.Workshop)
                .Include(e => e.Client);

        protected override IQueryable<Mission> SetDefaultIncludsForListRetrieve(IQueryable<Mission> query)
            => query.Include(e => e.Technician)
                .Include(e => e.Workshop)
                .Include(e => e.Client);

        protected override IQueryable<Mission> SetPagedResultFilterOptions<IFilter>(IQueryable<Mission> query, IFilter filterOption)
        {
            if (filterOption is MissionFilterOptions filter)
            {
                if (filter.DateStart.HasValue)
                    query = query.Where(e => e.StartingDate.Date >= filter.DateStart.Value.Date);
                if (filter.DateEnd.HasValue)
                    query = query.Where(e => e.EndingDate < filter.DateEnd);

                if (filter.TechnicianIds.Any())
                    query = query.Where(e => filter.TechnicianIds.Contains(e.TechnicianId));

                if (filter.ClientId.IsValid())
                    query = query.Where(e => filter.ClientId == e.ClientId);

                if (filter.Kinds.Any())
                    query = query.Where(e => filter.Kinds.Contains(e.MissionKind));

                if (filter.Types.Any())
                    query = query.Where(e => filter.Types.Contains(e.Type));

                if (filter.Status.Any())
                    query = query.Where(GetStatusPredicate(filter.Status));
            }

            return query;
        }

        protected static Expression<Func<Mission, bool>> GetStatusPredicate(IEnumerable<string> status)
        {
            var newStatus = new HashSet<string>(status);
            var predicate = PredicateBuilder.True<Mission>();
            var isFirst = true;

            if (status.Contains(MissionStatus.Late))
            {
                predicate = predicate.And(e =>
                    (e.EndingDate != null && e.Status == MissionStatus.InProgress && e.EndingDate < DateTime.Today) ||
                    (e.EndingDate == null && e.Status == MissionStatus.InProgress && e.StartingDate < DateTime.Today));

                newStatus.Remove(MissionStatus.Late);
                isFirst = false;
            }

            if (newStatus.Any())
            {
                if (isFirst)
                    predicate = predicate.And(e => newStatus.Contains(e.Status));
                else
                    predicate = predicate.Or(e => newStatus.Contains(e.Status));
            }

            return predicate;
        }
    }
}
