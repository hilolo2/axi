﻿namespace Axiobat.Persistence.DataAccess
{
    using Application.Data;
    using Domain.Entities;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Logging;
    using Persistence.DataContext;
    using System.Linq;

    /// <summary>
    /// data access implementation for <see cref="ChartAccountCategoryDataAccess"/>
    /// </summary>
    public partial class ChartAccountCategoryDataAccess
    {
    }

    /// <summary>
    /// partial part for <see cref="ChartAccountCategoryDataAccess"/>
    /// </summary>
    public partial class ChartAccountCategoryDataAccess : DataAccess<ChartAccountItem, string>, IChartOfAccountsDataAccess
    {
        public ChartAccountCategoryDataAccess(
            ApplicationDbContext context,
            ILoggerFactory loggerFactory)
            : base(context, loggerFactory)
        {
        }

        protected override IQueryable<ChartAccountItem> SetDefaultIncludsForSingleRetrieve(IQueryable<ChartAccountItem> query)
            => query.Include(e => e.Classifications);
    }
}
