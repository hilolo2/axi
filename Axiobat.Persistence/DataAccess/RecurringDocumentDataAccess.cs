﻿namespace Axiobat.Persistence.DataAccess
{
    using Application.Data;
    using Application.Models;
    using DataContext;
    using Domain.Entities;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Logging;
    using System.Linq;

    /// <summary>
    /// data access implementation for <see cref="RecurringDocument"/>
    /// </summary>
    public partial class RecurringDocumentDataAccess
    {

    }

    /// <summary>
    /// partial part for <see cref="RecurringDocumentDataAccess"/>
    /// </summary>
    public partial class RecurringDocumentDataAccess : DataAccess<RecurringDocument, string>, IRecurringDocumentDataAccess
    {
        public RecurringDocumentDataAccess(
            ApplicationDbContext context,
            ILoggerFactory loggerFactory)
            : base(context, loggerFactory)
        {
        }

        protected override IQueryable<RecurringDocument> SetPagedResultFilterOptions<IFilter>
            (IQueryable<RecurringDocument> query, IFilter filterOption)
        {
            if (filterOption is RecurringFilterOptions options)
            {
                if (!(options.Status is null) && options.Status.Any())
                    query = query.Where(e => options.Status.Contains(e.Status));
            }

            return query;
        }

        protected override IQueryable<RecurringDocument> SetDefaultIncludsForListRetrieve(IQueryable<RecurringDocument> query)
            => query.Include(e => e.MaintenanceContract);

        protected override IQueryable<RecurringDocument> SetDefaultIncludsForSingleRetrieve(IQueryable<RecurringDocument> query)
            => query.Include(e => e.MaintenanceContract);
    }
}
