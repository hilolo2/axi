﻿namespace Axiobat.Persistence.DataContext.EntitiesConfigurations
{
    using App.Common;
    using Domain.Entities;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;
    using System.Collections.Generic;

    public class MaintenanceOperationSheetEntityConfiguration : IEntityTypeConfiguration<MaintenanceOperationSheet>
    {
        public void Configure(EntityTypeBuilder<MaintenanceOperationSheet> builder)
        {
            // the table name
            builder.ToTable("MaintenanceOperationSheets");

            builder.HasQueryFilter(e => !e.IsDeleted);

            // properties
            builder.Property(e => e.Reference)
               .HasMaxLength(50);

            builder.Property(e => e.Purpose)
                .HasMaxLength(500);

            builder.Property(e => e.Status)
                .HasMaxLength(50);

            builder.Property(e => e.Report)
                .HasColumnType("LONGTEXT");

            // indexes
            builder.HasIndex(e => e.Reference);

            // converters
            builder.Property(e => e.ClientSignature)
                 .HasConversion(
                     entity => entity.ToJson(false, false),
                     json => json.FromJson<Signature>())
                 .HasColumnType("LONGTEXT");

            builder.Property(e => e.TechnicianSignature)
                 .HasConversion(
                     entity => entity.ToJson(false, false),
                     json => json.FromJson<Signature>())
                 .HasColumnType("LONGTEXT");

            builder.Property(e => e.ChangesHistory)
                 .HasConversion(
                     entity => entity.ToJson(false, false),
                     json => json.FromJson<List<ChangesHistory>>())
                 .HasColumnType("LONGTEXT");

            builder.Property(e => e.UnDoneMotif)
                 .HasConversion(
                     entity => entity.ToJson(false, false),
                     json => json.FromJson<ICollection<MaintenanceOperationSheetMotif>>()
                        ?? new HashSet<MaintenanceOperationSheetMotif>())
                 .HasColumnType("LONGTEXT");

            builder.Property(e => e.Emails)
                 .HasConversion(
                     entity => entity.ToJson(false, false),
                     json => json.FromJson<ICollection<DocumentEmail>>())
                 .HasColumnType("LONGTEXT");

            builder.Property(e => e.Memos)
                 .HasConversion(
                     entity => entity.ToJson(false, false),
                     json => json.FromJson<ICollection<Memo>>())
                 .HasColumnType("LONGTEXT");

            builder.Property(e => e.AddressIntervention)
                 .HasConversion(
                     entity => entity.ToJson(false, false),
                     json => json.FromJson<Address>())
                 .HasColumnType("LONGTEXT");

            builder.Property(e => e.EquipmentDetails)
                 .HasConversion(
                     entity => entity.ToJson(false, false),
                     json => json.FromJson<ICollection<MaintenanceContractEquipmentDetails>>())
                 .HasColumnType("LONGTEXT");

            builder.Property(e => e.Observations)
                 .HasConversion(
                     entity => entity.ToJson(false, false),
                     json => json.FromJson<ICollection<TechnicianObservation>>())
                 .HasColumnType("LONGTEXT");

            builder.Property(e => e.OrderDetails)
                 .HasConversion(
                     entity => entity.ToJson(false, false),
                     json => json.FromJson<OrderDetails>())
                 .HasColumnType("LONGTEXT");

            builder.Property(e => e.Client)
                 .HasConversion(
                     entity => entity.ToJson(false, false),
                     json => json.FromJson<Client>())
                 .HasColumnType("LONGTEXT");

            // relationships
            builder.HasOne<Client>()
                .WithMany(e => e.MaintenanceOperationSheets)
                .HasForeignKey(e => e.ClientId)
                .IsRequired(false)
                .OnDelete(DeleteBehavior.SetNull);

            builder.HasOne(e => e.Technician)
               .WithMany(e => e.MaintenanceOperationSheets)
               .HasForeignKey(e => e.TechnicianId)
               .IsRequired(false)
               .OnDelete(DeleteBehavior.SetNull);

            builder.HasOne(e => e.MaintenanceVisit)
                .WithOne(e => e.MaintenanceOperationSheet)
                .HasForeignKey<MaintenanceOperationSheet>(e => e.MaintenanceVisitId)
                .IsRequired(false)
                .OnDelete(DeleteBehavior.Cascade);

            builder.HasOne(e => e.MaintenanceContract)
                .WithMany(e => e.MaintenanceOperationsSheets)
                .HasForeignKey(e => e.MaintenanceContractId)
                .IsRequired(false)
                .OnDelete(DeleteBehavior.Cascade);

        }
    }
}
