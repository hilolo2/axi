﻿namespace Axiobat.Persistence.DataContext.EntitiesConfigurations
{
    using Domain.Entities;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    /// <summary>
    /// the <see cref="Country"/> entity configuration
    /// </summary>
    internal class CountryEntityConfigurations : IEntityTypeConfiguration<Country>
    {
        public void Configure(EntityTypeBuilder<Country> builder)
        {
            builder.ToTable("Countries");

            // properties configuration
            builder.HasKey(e => e.CountryCode);

            builder.Property(e => e.CountryCode)
                .HasMaxLength(5);

            builder.Property(e => e.Name)
                .HasMaxLength(50);

            builder.Property(e => e.LanguageCode)
                .HasMaxLength(10);

            // index configuration
            builder.HasIndex(e => e.Name)
                .IsUnique();
        }
    }
}