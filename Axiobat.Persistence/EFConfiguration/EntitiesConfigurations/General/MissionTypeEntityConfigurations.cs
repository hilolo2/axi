﻿namespace Axiobat.Persistence.DataContext.EntitiesConfigurations
{
    using Domain.Entities;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    /// <summary>
    /// the <see cref="MissionType"/> entity configuration
    /// </summary>
    internal class MissionTypeEntityConfigurations : IEntityTypeConfiguration<MissionType>
    {
        public void Configure(EntityTypeBuilder<MissionType> builder)
        {
            builder.ToTable("MissionsType");
        }
    }
}