﻿namespace Axiobat.Persistence.DataContext.EntitiesConfigurations
{
    using Domain.Entities;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    /// <summary>
    /// the entity <see cref="GlobalHistory"/> database Configuration
    /// </summary>
    internal class GlobalHistoryEntityConfigurations : IEntityTypeConfiguration<GlobalHistory>
    {
        public void Configure(EntityTypeBuilder<GlobalHistory> builder)
        {
            builder.ToTable("GlobalHistory");

            // relationships configurations
            builder.HasOne(e => e.User)
                .WithMany()
                .HasForeignKey(e => e.UserId)
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
