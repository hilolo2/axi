﻿namespace Axiobat.Persistence.DataContext.EntitiesConfigurations
{
    using Domain.Entities;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    internal class InvoicesPaymentsEntityConfiguration : IEntityTypeConfiguration<Invoices_Payments>
    {
        public void Configure(EntityTypeBuilder<Invoices_Payments> builder)
        {
            // table name
            builder.ToTable("Invoices_Payments");

            // primary key
            builder.HasKey(e => new { e.InvoiceId, e.PaymentId });

            // configuration
            builder.HasOne(e => e.Invoice)
                .WithMany(e => e.Payments)
                .HasForeignKey(e => e.InvoiceId)
                .IsRequired(true)
                .OnDelete(DeleteBehavior.Cascade);

            builder.HasOne(e => e.Payment)
                .WithMany(e => e.Invoices)
                .HasForeignKey(e => e.PaymentId)
                .IsRequired(true)
                .OnDelete(DeleteBehavior.Cascade);
        }
    }

    internal class ExpensesPaymentsEntityConfiguration : IEntityTypeConfiguration<Expenses_Payments>
    {
        public void Configure(EntityTypeBuilder<Expenses_Payments> builder)
        {
            // table name
            builder.ToTable("Expenses_Payments");

            // primary key
            builder.HasKey(e => new { e.ExpenseId, e.PaymentId });

            // configuration
            builder.HasOne(e => e.Expense)
                .WithMany(e => e.Payments)
                .HasForeignKey(e => e.ExpenseId)
                .IsRequired(true)
                .OnDelete(DeleteBehavior.Cascade);

            builder.HasOne(e => e.Payment)
                .WithMany(e => e.Expenses)
                .HasForeignKey(e => e.PaymentId)
                .IsRequired(true)
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
