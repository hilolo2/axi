﻿namespace Axiobat.Persistence.DataContext.EntitiesConfigurations
{
    using Axiobat.Domain.Entities;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    internal class OperationSheets_TechniciansEntityConfiguration : IEntityTypeConfiguration<OperationSheets_Technicians>
    {
        public void Configure(EntityTypeBuilder<OperationSheets_Technicians> builder)
        {
            // table name
            builder.ToTable("OperationSheets_Technicians");

            // key
            builder.HasKey(e => new { e.TechnicianId, e.OperationSheetId });

            // relationships
            builder.HasOne(e => e.OperationSheet)
                .WithMany(e => e.Technicians)
                .HasForeignKey(e => e.OperationSheetId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Cascade);

            builder.HasOne(e => e.Technician)
                .WithMany(e => e.OperationSheets)
                .HasForeignKey(e => e.TechnicianId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
