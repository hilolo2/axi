﻿namespace Axiobat.Persistence.DataContext.EntitiesConfigurations
{
    using Domain.Entities;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    internal class ClassificationEntityConfiguration : IEntityTypeConfiguration<Classification>
    {
        public void Configure(EntityTypeBuilder<Classification> builder)
        {
            // table name
            builder.ToTable("Classifications");

            builder.Property(e => e.Description)
                .HasMaxLength(500);

            // relationships
            builder.HasMany(e => e.SubClassification)
                .WithOne(e => e.Parent)
                .HasForeignKey(e => e.ParentId)
                .IsRequired(false)
                .OnDelete(DeleteBehavior.Cascade);

            builder.HasOne(e => e.ChartAccountItem)
                .WithMany(e => e.Classifications)
                .HasForeignKey(e => e.ChartAccountItemId)
                .IsRequired(false)
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}