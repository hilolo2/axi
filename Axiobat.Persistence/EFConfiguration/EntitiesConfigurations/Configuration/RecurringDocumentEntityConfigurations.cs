﻿namespace Axiobat.Persistence.DataContext.EntitiesConfigurations
{
    using App.Common;
    using Domain.Entities;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;
    using Newtonsoft.Json.Linq;

    internal class RecurringDocumentEntityConfigurations : IEntityTypeConfiguration<RecurringDocument>
    {
        public void Configure(EntityTypeBuilder<RecurringDocument> builder)
        {
            builder.ToTable("RecurringDocuments");

            builder.Property(e => e.DocType)
                .HasColumnName("DocumentType");

            builder.Property(e => e.PeriodicityOptions)
                .HasConversion(
                    entity => entity.ToJson(false, false),
                    json => json.FromJson<PeriodicityOptions>())
                .HasColumnType("LONGTEXT");

            builder.Property(e => e.Document)
                .HasConversion(
                    entity => entity.ToJson(false, false),
                    json => json.FromJson<JObject>())
                .HasColumnType("LONGTEXT");

            builder.Property(e => e.Error)
                .HasConversion(
                    entity => entity.ToJson(false, false),
                    json => json.FromJson<JObject>())
                .HasColumnType("LONGTEXT");
        }
    }
}
