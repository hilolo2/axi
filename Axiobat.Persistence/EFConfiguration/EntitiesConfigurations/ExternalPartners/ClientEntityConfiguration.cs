﻿namespace Axiobat.Persistence.DataContext.EntitiesConfigurations
{
    using App.Common;
    using Axiobat.Domain.Constants;
    using Domain.Entities;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;
    using System.Collections.Generic;

    public class ClientEntityConfiguration : ExternalPartnerEntityConfiguration<Client>, IEntityTypeConfiguration<Client>
    {
        public override void Configure(EntityTypeBuilder<Client> builder)
        {
            base.Configure(builder);

            // the entity table name
            builder.ToTable("Clients");

            // properties configurations
            builder.Property(e => e.Code)
                .HasMaxLength(50);

            builder.Property(e => e.Type)
                .HasDefaultValue(ClientType.Particular);

            // conversions
            builder.Property(e => e.Addresses)
                 .HasConversion(
                     entity => entity.ToJson(false, false),
                     json => json.FromJson<ICollection<Address>>())
                 .HasColumnType("LONGTEXT");

            // conversions
            builder.Property(e => e.Materials)
                 .HasConversion(
                     entity => entity.ToJson(false, false),
                     json => json.FromJson<ICollection<Material>>())
                 .HasColumnType("LONGTEXT");
        }
    }
}
