﻿namespace Axiobat.Persistence.DataContext.EntitiesConfigurations
{
    using App.Common;
    using Domain.Entities;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;
    using System.Collections.Generic;

    internal class PaymentEntityConfiguration : IEntityTypeConfiguration<Payment>
    {
        public void Configure(EntityTypeBuilder<Payment> builder)
        {
            // table name
            builder.ToTable("Payment");

            // property
            builder.Property(e => e.Description)
                .HasMaxLength(500);

            builder.Ignore(e => e.Amount);

            builder.HasQueryFilter(e => !e.IsDeleted);

            //builder.Property(e => e.Amount)
            //    .ValueGeneratedNever();

            // conversions
            builder.Property(e => e.ChangesHistory)
                 .HasConversion(
                     entity => entity.ToJson(false, false),
                     json => json.FromJson<List<ChangesHistory>>())
                 .HasColumnType("LONGTEXT");

            // relationships
            builder.HasOne(e => e.Account)
                .WithMany(e => e.Payments)
                .HasForeignKey(e => e.AccountId)
                .IsRequired(false)
                .OnDelete(DeleteBehavior.SetNull);

            builder.HasOne(e => e.PaymentMethod)
                .WithMany(e => e.Payments)
                .HasForeignKey(e => e.PaymentMethodId)
                .IsRequired(false)
                .OnDelete(DeleteBehavior.SetNull);

            builder.HasOne(e => e.CreditNote)
                .WithMany(e => e.Payments)
                .HasForeignKey(e => e.CreditNoteId)
                .IsRequired(false)
                .OnDelete(DeleteBehavior.SetNull);
        }
    }
}
