﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Axiobat.Persistence.Migrations
{
    public partial class AddRelationBetweenExpenseAndClassification : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Expenses_ChartAccountCategories_ExpenseCategoryId",
                table: "Expenses");

            migrationBuilder.DropForeignKey(
                name: "FK_Products_ChartAccountCategories_CategoryId",
                table: "Products");

            migrationBuilder.DropIndex(
                name: "IX_Products_CategoryId",
                table: "Products");

            migrationBuilder.DropIndex(
                name: "IX_Expenses_ExpenseCategoryId",
                table: "Expenses");

            migrationBuilder.DropColumn(
                name: "CategoryId",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "ExpenseCategoryId",
                table: "Expenses");

            migrationBuilder.AddColumn<int>(
                name: "ClassificationId",
                table: "Products",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "ClassificationId",
                table: "Expenses",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.Sql("UPDATE Expenses SET ClassificationId = 11");
            migrationBuilder.Sql("UPDATE Products SET ClassificationId = 47");

            migrationBuilder.CreateIndex(
                name: "IX_Products_ClassificationId",
                table: "Products",
                column: "ClassificationId");

            migrationBuilder.CreateIndex(
                name: "IX_Expenses_ClassificationId",
                table: "Expenses",
                column: "ClassificationId");

            migrationBuilder.AddForeignKey(
                name: "FK_Expenses_Classifications_ClassificationId",
                table: "Expenses",
                column: "ClassificationId",
                principalTable: "Classifications",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            //testcomptable@gmail.com
            migrationBuilder.AddForeignKey(
                name: "FK_Products_Classifications_ClassificationId",
                table: "Products",
                column: "ClassificationId",
                principalTable: "Classifications",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Expenses_Classifications_ClassificationId",
                table: "Expenses");

            migrationBuilder.DropForeignKey(
                name: "FK_Products_Classifications_ClassificationId",
                table: "Products");

            migrationBuilder.DropIndex(
                name: "IX_Products_ClassificationId",
                table: "Products");

            migrationBuilder.DropIndex(
                name: "IX_Expenses_ClassificationId",
                table: "Expenses");

            migrationBuilder.DropColumn(
                name: "ClassificationId",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "ClassificationId",
                table: "Expenses");

            migrationBuilder.AddColumn<string>(
                name: "CategoryId",
                table: "Products",
                maxLength: 256,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ExpenseCategoryId",
                table: "Expenses",
                maxLength: 256,
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Products_CategoryId",
                table: "Products",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_Expenses_ExpenseCategoryId",
                table: "Expenses",
                column: "ExpenseCategoryId");

            migrationBuilder.AddForeignKey(
                name: "FK_Expenses_ChartAccountCategories_ExpenseCategoryId",
                table: "Expenses",
                column: "ExpenseCategoryId",
                principalTable: "ChartAccountCategories",
                principalColumn: "Id",
                onDelete: ReferentialAction.SetNull);

            migrationBuilder.AddForeignKey(
                name: "FK_Products_ChartAccountCategories_CategoryId",
                table: "Products",
                column: "CategoryId",
                principalTable: "ChartAccountCategories",
                principalColumn: "Id",
                onDelete: ReferentialAction.SetNull);
        }
    }
}
