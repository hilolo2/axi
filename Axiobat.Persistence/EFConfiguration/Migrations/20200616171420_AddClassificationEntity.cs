﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Axiobat.Persistence.Migrations
{
    public partial class AddClassificationEntity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Label",
                table: "ChartAccountCategories",
                maxLength: 256,
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 100,
                oldNullable: true);

            migrationBuilder.CreateTable(
                name: "Classifications",
                columns: table => new
                {
                    CreatedOn = table.Column<DateTimeOffset>(nullable: false, defaultValueSql: "CURRENT_TIMESTAMP"),
                    LastModifiedOn = table.Column<DateTimeOffset>(nullable: true, defaultValueSql: "CURRENT_TIMESTAMP"),
                    SearchTerms = table.Column<string>(maxLength: 500, nullable: true),
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Type = table.Column<int>(nullable: false),
                    Label = table.Column<string>(maxLength: 256, nullable: true),
                    Description = table.Column<string>(maxLength: 500, nullable: true),
                    ChartAccountItemId = table.Column<string>(maxLength: 256, nullable: true),
                    ParentId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Classifications", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Classifications_ChartAccountCategories_ChartAccountItemId",
                        column: x => x.ChartAccountItemId,
                        principalTable: "ChartAccountCategories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Classifications_Classifications_ParentId",
                        column: x => x.ParentId,
                        principalTable: "Classifications",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            //migrationBuilder.CreateTable(
            //    name: "PublishingContract",
            //    columns: table => new
            //    {
            //        CreatedOn = table.Column<DateTimeOffset>(nullable: false, defaultValueSql: "CURRENT_TIMESTAMP"),
            //        LastModifiedOn = table.Column<DateTimeOffset>(nullable: true, defaultValueSql: "CURRENT_TIMESTAMP"),
            //        SearchTerms = table.Column<string>(maxLength: 500, nullable: true),
            //        Id = table.Column<int>(nullable: false)
            //            .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
            //        Title = table.Column<string>(maxLength: 256, nullable: true),
            //        FileName = table.Column<string>(maxLength: 256, nullable: true),
            //        OrginalFileName = table.Column<string>(maxLength: 256, nullable: true),
            //        UserId = table.Column<Guid>(nullable: true),
            //        Tags = table.Column<string>(type: "LONGTEXT", nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_PublishingContract", x => x.Id);
            //        table.ForeignKey(
            //            name: "FK_PublishingContract_Users_UserId",
            //            column: x => x.UserId,
            //            principalTable: "Users",
            //            principalColumn: "Id",
            //            onDelete: ReferentialAction.Cascade);
            //    });

            migrationBuilder.CreateIndex(
                name: "IX_Classifications_ChartAccountItemId",
                table: "Classifications",
                column: "ChartAccountItemId");

            migrationBuilder.CreateIndex(
                name: "IX_Classifications_ParentId",
                table: "Classifications",
                column: "ParentId");

            migrationBuilder.CreateIndex(
                name: "IX_Classifications_SearchTerms",
                table: "Classifications",
                column: "SearchTerms");

            //migrationBuilder.CreateIndex(
            //    name: "IX_PublishingContract_SearchTerms",
            //    table: "PublishingContract",
            //    column: "SearchTerms");

            //migrationBuilder.CreateIndex(
            //    name: "IX_PublishingContract_UserId",
            //    table: "PublishingContract",
            //    column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Classifications");

            //migrationBuilder.DropTable(
            //    name: "PublishingContract");

            migrationBuilder.AlterColumn<string>(
                name: "Label",
                table: "ChartAccountCategories",
                maxLength: 100,
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 256,
                oldNullable: true);
        }
    }
}
