﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Axiobat.Persistence.Migrations
{
    public partial class AddExpirationAlertPeriodTypeToMaintenanceContractEntity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ExpirationAlertPeriodType",
                table: "MaintenanceContracts",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ExpirationAlertPeriodType",
                table: "MaintenanceContracts");
        }
    }
}
