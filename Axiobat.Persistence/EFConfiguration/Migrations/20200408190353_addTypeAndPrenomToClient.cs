﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Axiobat.Persistence.Migrations
{
    public partial class addTypeAndPrenomToClient : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Prenom",
                table: "Clients",
                maxLength: 256,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Type",
                table: "Clients",
                maxLength: 256,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Prenom",
                table: "Clients");

            migrationBuilder.DropColumn(
                name: "Type",
                table: "Clients");
        }
    }
}
