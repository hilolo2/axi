﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Axiobat.Persistence.Migrations
{
    public partial class AddMaintenanceVisitRelationWithClient : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ClientId",
                table: "MaintenanceVisits",
                maxLength: 256,
                nullable: false,
                defaultValue: "");

            migrationBuilder.CreateIndex(
                name: "IX_MaintenanceVisits_ClientId",
                table: "MaintenanceVisits",
                column: "ClientId");

            migrationBuilder.AddForeignKey(
                name: "FK_MaintenanceVisits_Clients_ClientId",
                table: "MaintenanceVisits",
                column: "ClientId",
                principalTable: "Clients",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MaintenanceVisits_Clients_ClientId",
                table: "MaintenanceVisits");

            migrationBuilder.DropIndex(
                name: "IX_MaintenanceVisits_ClientId",
                table: "MaintenanceVisits");

            migrationBuilder.DropColumn(
                name: "ClientId",
                table: "MaintenanceVisits");
        }
    }
}
