﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Axiobat.Persistence.Migrations
{
    public partial class AddMaintenanceModuleEntities : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "MaintenanceVisits",
                columns: table => new
                {
                    CreatedOn = table.Column<DateTimeOffset>(nullable: false, defaultValueSql: "CURRENT_TIMESTAMP"),
                    LastModifiedOn = table.Column<DateTimeOffset>(nullable: true, defaultValueSql: "CURRENT_TIMESTAMP"),
                    SearchTerms = table.Column<string>(maxLength: 500, nullable: true),
                    Id = table.Column<string>(maxLength: 256, nullable: false),
                    Status = table.Column<string>(maxLength: 256, nullable: true),
                    Date = table.Column<DateTime>(nullable: false),
                    MaintenanceContractId = table.Column<string>(maxLength: 256, nullable: false),
                    EquipmentDetails = table.Column<string>(type: "LONGTEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MaintenanceVisits", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MaintenanceVisits_MaintenanceContracts_MaintenanceContractId",
                        column: x => x.MaintenanceContractId,
                        principalTable: "MaintenanceContracts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "MaintenanceOperationSheets",
                columns: table => new
                {
                    CreatedOn = table.Column<DateTimeOffset>(nullable: false, defaultValueSql: "CURRENT_TIMESTAMP"),
                    LastModifiedOn = table.Column<DateTimeOffset>(nullable: true, defaultValueSql: "CURRENT_TIMESTAMP"),
                    SearchTerms = table.Column<string>(maxLength: 500, nullable: true),
                    Id = table.Column<string>(maxLength: 256, nullable: false),
                    Reference = table.Column<string>(maxLength: 50, nullable: true),
                    Status = table.Column<string>(maxLength: 50, nullable: true),
                    StartDate = table.Column<DateTime>(nullable: false),
                    EndDate = table.Column<DateTime>(nullable: false),
                    Report = table.Column<string>(type: "LONGTEXT", maxLength: 256, nullable: true),
                    Purpose = table.Column<string>(maxLength: 500, nullable: true),
                    VisitsCount = table.Column<int>(nullable: false),
                    TotalBasketConsumption = table.Column<int>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    AddressIntervention = table.Column<string>(type: "LONGTEXT", nullable: true),
                    ClientSignature = table.Column<string>(type: "LONGTEXT", nullable: true),
                    TechnicianSignature = table.Column<string>(type: "LONGTEXT", nullable: true),
                    ClientId = table.Column<string>(maxLength: 256, nullable: true),
                    TechnicianId = table.Column<Guid>(nullable: true),
                    MaintenanceContractId = table.Column<string>(maxLength: 256, nullable: false),
                    MaintenanceVisitId = table.Column<string>(maxLength: 256, nullable: false),
                    Memos = table.Column<string>(type: "LONGTEXT", nullable: true),
                    Emails = table.Column<string>(type: "LONGTEXT", nullable: true),
                    ChangesHistory = table.Column<string>(type: "LONGTEXT", nullable: true),
                    EquipmentDetails = table.Column<string>(type: "LONGTEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MaintenanceOperationSheets", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MaintenanceOperationSheets_Clients_ClientId",
                        column: x => x.ClientId,
                        principalTable: "Clients",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.SetNull);
                    table.ForeignKey(
                        name: "FK_MaintenanceOperationSheets_MaintenanceContracts_MaintenanceC~",
                        column: x => x.MaintenanceContractId,
                        principalTable: "MaintenanceContracts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MaintenanceOperationSheets_MaintenanceVisits_MaintenanceVisi~",
                        column: x => x.MaintenanceVisitId,
                        principalTable: "MaintenanceVisits",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MaintenanceOperationSheets_Users_TechnicianId",
                        column: x => x.TechnicianId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.SetNull);
                });

            migrationBuilder.CreateIndex(
                name: "IX_MaintenanceOperationSheets_ClientId",
                table: "MaintenanceOperationSheets",
                column: "ClientId");

            migrationBuilder.CreateIndex(
                name: "IX_MaintenanceOperationSheets_MaintenanceContractId",
                table: "MaintenanceOperationSheets",
                column: "MaintenanceContractId");

            migrationBuilder.CreateIndex(
                name: "IX_MaintenanceOperationSheets_MaintenanceVisitId",
                table: "MaintenanceOperationSheets",
                column: "MaintenanceVisitId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_MaintenanceOperationSheets_SearchTerms",
                table: "MaintenanceOperationSheets",
                column: "SearchTerms");

            migrationBuilder.CreateIndex(
                name: "IX_MaintenanceOperationSheets_TechnicianId",
                table: "MaintenanceOperationSheets",
                column: "TechnicianId");

            migrationBuilder.CreateIndex(
                name: "IX_MaintenanceVisits_MaintenanceContractId",
                table: "MaintenanceVisits",
                column: "MaintenanceContractId");

            migrationBuilder.CreateIndex(
                name: "IX_MaintenanceVisits_SearchTerms",
                table: "MaintenanceVisits",
                column: "SearchTerms");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MaintenanceOperationSheets");

            migrationBuilder.DropTable(
                name: "MaintenanceVisits");
        }
    }
}
