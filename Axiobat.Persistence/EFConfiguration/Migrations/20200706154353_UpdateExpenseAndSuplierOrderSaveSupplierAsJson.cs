﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Axiobat.Persistence.Migrations
{
    public partial class UpdateExpenseAndSuplierOrderSaveSupplierAsJson : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Supplier",
                table: "SuppliersOrders",
                type: "LONGTEXT",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "SupplierId",
                table: "Expenses",
                maxLength: 256,
                nullable: false,
                oldClrType: typeof(string),
                oldMaxLength: 256,
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Supplier",
                table: "Expenses",
                type: "LONGTEXT",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Supplier",
                table: "SuppliersOrders");

            migrationBuilder.DropColumn(
                name: "Supplier",
                table: "Expenses");

            migrationBuilder.AlterColumn<string>(
                name: "SupplierId",
                table: "Expenses",
                maxLength: 256,
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 256);
        }
    }
}
