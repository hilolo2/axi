﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Axiobat.Persistence.Migrations
{
    public partial class InitialSetupFirstMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ApplicationConfigurations",
                columns: table => new
                {
                    ConfigurationType = table.Column<string>(maxLength: 256, nullable: false),
                    Value = table.Column<string>(type: "LONGTEXT", maxLength: 256, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ApplicationConfigurations", x => x.ConfigurationType);
                });

            migrationBuilder.CreateTable(
                name: "ChartAccountCategories",
                columns: table => new
                {
                    CreatedOn = table.Column<DateTimeOffset>(nullable: false, defaultValueSql: "CURRENT_TIMESTAMP"),
                    LastModifiedOn = table.Column<DateTimeOffset>(nullable: true, defaultValueSql: "CURRENT_TIMESTAMP"),
                    SearchTerms = table.Column<string>(maxLength: 500, nullable: true),
                    Id = table.Column<string>(maxLength: 256, nullable: false),
                    Type = table.Column<int>(nullable: false),
                    CategoryType = table.Column<int>(nullable: false),
                    Label = table.Column<string>(maxLength: 100, nullable: true),
                    Code = table.Column<string>(maxLength: 15, nullable: true),
                    Description = table.Column<string>(maxLength: 500, nullable: true),
                    VATValue = table.Column<int>(nullable: false),
                    ParentId = table.Column<string>(maxLength: 256, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ChartAccountCategories", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ChartAccountCategories_ChartAccountCategories_ParentId",
                        column: x => x.ParentId,
                        principalTable: "ChartAccountCategories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Countries",
                columns: table => new
                {
                    CountryCode = table.Column<string>(maxLength: 5, nullable: false),
                    Name = table.Column<string>(maxLength: 50, nullable: true),
                    LanguageCode = table.Column<string>(maxLength: 10, nullable: true),
                    IsDefault = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Countries", x => x.CountryCode);
                });

            migrationBuilder.CreateTable(
                name: "FinancialAccounts",
                columns: table => new
                {
                    CreatedOn = table.Column<DateTimeOffset>(nullable: false, defaultValueSql: "CURRENT_TIMESTAMP"),
                    LastModifiedOn = table.Column<DateTimeOffset>(nullable: true, defaultValueSql: "CURRENT_TIMESTAMP"),
                    SearchTerms = table.Column<string>(maxLength: 500, nullable: true),
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Type = table.Column<int>(nullable: false),
                    Label = table.Column<string>(maxLength: 256, nullable: true),
                    Code = table.Column<string>(maxLength: 256, nullable: true),
                    IsDefault = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FinancialAccounts", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Groups",
                columns: table => new
                {
                    CreatedOn = table.Column<DateTimeOffset>(nullable: false, defaultValueSql: "CURRENT_TIMESTAMP"),
                    LastModifiedOn = table.Column<DateTimeOffset>(nullable: true, defaultValueSql: "CURRENT_TIMESTAMP"),
                    SearchTerms = table.Column<string>(maxLength: 500, nullable: true),
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(maxLength: 256, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Groups", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Labels",
                columns: table => new
                {
                    CreatedOn = table.Column<DateTimeOffset>(nullable: false, defaultValueSql: "CURRENT_TIMESTAMP"),
                    LastModifiedOn = table.Column<DateTimeOffset>(nullable: true, defaultValueSql: "CURRENT_TIMESTAMP"),
                    SearchTerms = table.Column<string>(maxLength: 500, nullable: true),
                    Id = table.Column<string>(maxLength: 256, nullable: false),
                    Value = table.Column<string>(maxLength: 256, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Labels", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Lots",
                columns: table => new
                {
                    CreatedOn = table.Column<DateTimeOffset>(nullable: false, defaultValueSql: "CURRENT_TIMESTAMP"),
                    LastModifiedOn = table.Column<DateTimeOffset>(nullable: true, defaultValueSql: "CURRENT_TIMESTAMP"),
                    SearchTerms = table.Column<string>(maxLength: 500, nullable: true),
                    Id = table.Column<string>(maxLength: 256, nullable: false),
                    Name = table.Column<string>(maxLength: 256, nullable: true),
                    Description = table.Column<string>(maxLength: 500, nullable: true),
                    ChangesHistory = table.Column<string>(type: "LONGTEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Lots", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PaymentMethod",
                columns: table => new
                {
                    CreatedOn = table.Column<DateTimeOffset>(nullable: false, defaultValueSql: "CURRENT_TIMESTAMP"),
                    LastModifiedOn = table.Column<DateTimeOffset>(nullable: true, defaultValueSql: "CURRENT_TIMESTAMP"),
                    SearchTerms = table.Column<string>(maxLength: 500, nullable: true),
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Value = table.Column<string>(maxLength: 256, nullable: true),
                    IsDefault = table.Column<bool>(nullable: false),
                    Type = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PaymentMethod", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Roles",
                columns: table => new
                {
                    CreatedOn = table.Column<DateTimeOffset>(nullable: false, defaultValueSql: "CURRENT_TIMESTAMP"),
                    LastModifiedOn = table.Column<DateTimeOffset>(nullable: true, defaultValueSql: "CURRENT_TIMESTAMP"),
                    SearchTerms = table.Column<string>(maxLength: 500, nullable: true),
                    Id = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(maxLength: 250, nullable: false),
                    NormalizedName = table.Column<string>(maxLength: 256, nullable: true),
                    ConcurrencyStamp = table.Column<string>(maxLength: 256, nullable: true),
                    Type = table.Column<int>(nullable: false),
                    Permissions = table.Column<string>(type: "LONGTEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Roles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Suppliers",
                columns: table => new
                {
                    CreatedOn = table.Column<DateTimeOffset>(nullable: false, defaultValueSql: "CURRENT_TIMESTAMP"),
                    LastModifiedOn = table.Column<DateTimeOffset>(nullable: true, defaultValueSql: "CURRENT_TIMESTAMP"),
                    SearchTerms = table.Column<string>(maxLength: 500, nullable: true),
                    Id = table.Column<string>(maxLength: 256, nullable: false),
                    Reference = table.Column<string>(maxLength: 50, nullable: true),
                    Name = table.Column<string>(maxLength: 256, nullable: true),
                    PhoneNumber = table.Column<string>(maxLength: 20, nullable: true),
                    LandLine = table.Column<string>(maxLength: 20, nullable: true),
                    Email = table.Column<string>(maxLength: 256, nullable: true),
                    Website = table.Column<string>(maxLength: 256, nullable: true),
                    Siret = table.Column<string>(maxLength: 25, nullable: true),
                    IntraCommunityVAT = table.Column<string>(maxLength: 256, nullable: true),
                    AccountingCode = table.Column<string>(maxLength: 20, nullable: true),
                    ContactInformations = table.Column<string>(type: "LONGTEXT", nullable: true),
                    ChangesHistory = table.Column<string>(type: "LONGTEXT", nullable: true),
                    Memos = table.Column<string>(type: "LONGTEXT", nullable: true),
                    Address = table.Column<string>(type: "LONGTEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Suppliers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Taxes",
                columns: table => new
                {
                    CreatedOn = table.Column<DateTimeOffset>(nullable: false, defaultValueSql: "CURRENT_TIMESTAMP"),
                    LastModifiedOn = table.Column<DateTimeOffset>(nullable: true, defaultValueSql: "CURRENT_TIMESTAMP"),
                    SearchTerms = table.Column<string>(maxLength: 500, nullable: true),
                    Id = table.Column<string>(maxLength: 256, nullable: false),
                    Name = table.Column<string>(maxLength: 256, nullable: true),
                    Value = table.Column<float>(nullable: false),
                    AccountingCode = table.Column<string>(maxLength: 256, nullable: true),
                    IsDefault = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Taxes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Units",
                columns: table => new
                {
                    CreatedOn = table.Column<DateTimeOffset>(nullable: false, defaultValueSql: "CURRENT_TIMESTAMP"),
                    LastModifiedOn = table.Column<DateTimeOffset>(nullable: true, defaultValueSql: "CURRENT_TIMESTAMP"),
                    SearchTerms = table.Column<string>(maxLength: 500, nullable: true),
                    Id = table.Column<string>(maxLength: 256, nullable: false),
                    Acronym = table.Column<string>(maxLength: 256, nullable: true),
                    Name = table.Column<string>(maxLength: 256, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Units", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "WorkshopDocumentsRubrics",
                columns: table => new
                {
                    CreatedOn = table.Column<DateTimeOffset>(nullable: false, defaultValueSql: "CURRENT_TIMESTAMP"),
                    LastModifiedOn = table.Column<DateTimeOffset>(nullable: true, defaultValueSql: "CURRENT_TIMESTAMP"),
                    SearchTerms = table.Column<string>(maxLength: 500, nullable: true),
                    Id = table.Column<string>(maxLength: 256, nullable: false),
                    Value = table.Column<string>(maxLength: 256, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WorkshopDocumentsRubrics", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "WorkshopDocumentTypes",
                columns: table => new
                {
                    CreatedOn = table.Column<DateTimeOffset>(nullable: false, defaultValueSql: "CURRENT_TIMESTAMP"),
                    LastModifiedOn = table.Column<DateTimeOffset>(nullable: true, defaultValueSql: "CURRENT_TIMESTAMP"),
                    SearchTerms = table.Column<string>(maxLength: 500, nullable: true),
                    Id = table.Column<string>(maxLength: 256, nullable: false),
                    Value = table.Column<string>(maxLength: 256, nullable: true),
                    IsDefault = table.Column<bool>(nullable: false),
                    AffectedStatus = table.Column<string>(maxLength: 256, nullable: true),
                    NewStatus = table.Column<string>(maxLength: 256, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WorkshopDocumentTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Products",
                columns: table => new
                {
                    CreatedOn = table.Column<DateTimeOffset>(nullable: false, defaultValueSql: "CURRENT_TIMESTAMP"),
                    LastModifiedOn = table.Column<DateTimeOffset>(nullable: true, defaultValueSql: "CURRENT_TIMESTAMP"),
                    SearchTerms = table.Column<string>(maxLength: 500, nullable: true),
                    Id = table.Column<string>(maxLength: 256, nullable: false),
                    Name = table.Column<string>(maxLength: 256, nullable: true),
                    Description = table.Column<string>(maxLength: 256, nullable: true),
                    ChangesHistory = table.Column<string>(type: "LONGTEXT", nullable: true),
                    Reference = table.Column<string>(maxLength: 256, nullable: true),
                    Designation = table.Column<string>(maxLength: 256, nullable: true),
                    TotalHours = table.Column<int>(nullable: false),
                    MaterialCost = table.Column<float>(nullable: false),
                    HourlyCost = table.Column<float>(nullable: false),
                    VAT = table.Column<float>(nullable: false),
                    Unite = table.Column<string>(maxLength: 256, nullable: true),
                    CategoryId = table.Column<string>(maxLength: 256, nullable: true),
                    Memo = table.Column<string>(type: "LONGTEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Products", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Products_ChartAccountCategories_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "ChartAccountCategories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.SetNull);
                });

            migrationBuilder.CreateTable(
                name: "Clients",
                columns: table => new
                {
                    CreatedOn = table.Column<DateTimeOffset>(nullable: false, defaultValueSql: "CURRENT_TIMESTAMP"),
                    LastModifiedOn = table.Column<DateTimeOffset>(nullable: true, defaultValueSql: "CURRENT_TIMESTAMP"),
                    SearchTerms = table.Column<string>(maxLength: 500, nullable: true),
                    Id = table.Column<string>(maxLength: 256, nullable: false),
                    Reference = table.Column<string>(maxLength: 50, nullable: true),
                    Name = table.Column<string>(maxLength: 256, nullable: true),
                    PhoneNumber = table.Column<string>(maxLength: 20, nullable: true),
                    LandLine = table.Column<string>(maxLength: 20, nullable: true),
                    Email = table.Column<string>(maxLength: 256, nullable: true),
                    Website = table.Column<string>(maxLength: 256, nullable: true),
                    Siret = table.Column<string>(maxLength: 25, nullable: true),
                    IntraCommunityVAT = table.Column<string>(maxLength: 256, nullable: true),
                    AccountingCode = table.Column<string>(maxLength: 20, nullable: true),
                    ContactInformations = table.Column<string>(type: "LONGTEXT", nullable: true),
                    ChangesHistory = table.Column<string>(type: "LONGTEXT", nullable: true),
                    Memos = table.Column<string>(type: "LONGTEXT", nullable: true),
                    Code = table.Column<string>(maxLength: 50, nullable: true),
                    GroupeId = table.Column<int>(nullable: true),
                    Addresses = table.Column<string>(type: "LONGTEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Clients", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Clients_Groups_GroupeId",
                        column: x => x.GroupeId,
                        principalTable: "Groups",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.SetNull);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    CreatedOn = table.Column<DateTimeOffset>(nullable: false, defaultValueSql: "CURRENT_TIMESTAMP"),
                    LastModifiedOn = table.Column<DateTimeOffset>(nullable: true, defaultValueSql: "CURRENT_TIMESTAMP"),
                    SearchTerms = table.Column<string>(maxLength: 500, nullable: true),
                    Id = table.Column<Guid>(nullable: false),
                    FirstName = table.Column<string>(maxLength: 256, nullable: true),
                    LastName = table.Column<string>(maxLength: 256, nullable: true),
                    RegistrationNumber = table.Column<string>(maxLength: 50, nullable: true),
                    UserName = table.Column<string>(maxLength: 50, nullable: false),
                    Email = table.Column<string>(maxLength: 50, nullable: true),
                    PasswordHash = table.Column<string>(maxLength: 256, nullable: true),
                    PhoneNumber = table.Column<string>(maxLength: 256, nullable: true),
                    Statut = table.Column<int>(nullable: false),
                    LastLogin = table.Column<DateTime>(nullable: true),
                    ChangesHistory = table.Column<string>(type: "LONGTEXT", nullable: true),
                    RoleId = table.Column<Guid>(nullable: false),
                    NormalizedUserName = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedEmail = table.Column<string>(maxLength: 256, nullable: true),
                    EmailConfirmed = table.Column<bool>(nullable: false),
                    PhoneNumberConfirmed = table.Column<bool>(nullable: false),
                    TwoFactorEnabled = table.Column<bool>(nullable: false),
                    LockoutEnabled = table.Column<bool>(nullable: false),
                    SecurityStamp = table.Column<string>(maxLength: 256, nullable: true),
                    ConcurrencyStamp = table.Column<string>(maxLength: 256, nullable: true),
                    LockoutEnd = table.Column<DateTimeOffset>(nullable: true),
                    AccessFailedCount = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Users_Roles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "Roles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Lots_Products",
                columns: table => new
                {
                    ProductId = table.Column<string>(maxLength: 256, nullable: false),
                    LotId = table.Column<string>(maxLength: 256, nullable: false),
                    Quantity = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Lots_Products", x => new { x.LotId, x.ProductId });
                    table.ForeignKey(
                        name: "FK_Lots_Products_Lots_LotId",
                        column: x => x.LotId,
                        principalTable: "Lots",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Lots_Products_Products_ProductId",
                        column: x => x.ProductId,
                        principalTable: "Products",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Products_Labels",
                columns: table => new
                {
                    ProductId = table.Column<string>(maxLength: 256, nullable: false),
                    LabelId = table.Column<string>(maxLength: 256, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Products_Labels", x => new { x.LabelId, x.ProductId });
                    table.ForeignKey(
                        name: "FK_Products_Labels_Labels_LabelId",
                        column: x => x.LabelId,
                        principalTable: "Labels",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Products_Labels_Products_ProductId",
                        column: x => x.ProductId,
                        principalTable: "Products",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Products_Suppliers",
                columns: table => new
                {
                    ProductId = table.Column<string>(maxLength: 256, nullable: false),
                    SupplierId = table.Column<string>(maxLength: 256, nullable: false),
                    Price = table.Column<float>(nullable: false),
                    IsDefault = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Products_Suppliers", x => new { x.ProductId, x.SupplierId });
                    table.ForeignKey(
                        name: "FK_Products_Suppliers_Products_ProductId",
                        column: x => x.ProductId,
                        principalTable: "Products",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Products_Suppliers_Suppliers_SupplierId",
                        column: x => x.SupplierId,
                        principalTable: "Suppliers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ConstructionWorkshops",
                columns: table => new
                {
                    CreatedOn = table.Column<DateTimeOffset>(nullable: false, defaultValueSql: "CURRENT_TIMESTAMP"),
                    LastModifiedOn = table.Column<DateTimeOffset>(nullable: true, defaultValueSql: "CURRENT_TIMESTAMP"),
                    SearchTerms = table.Column<string>(maxLength: 500, nullable: true),
                    Id = table.Column<string>(maxLength: 256, nullable: false),
                    Name = table.Column<string>(maxLength: 100, nullable: true),
                    Description = table.Column<string>(maxLength: 500, nullable: true),
                    Comment = table.Column<string>(maxLength: 100, nullable: true),
                    Status = table.Column<string>(maxLength: 256, nullable: true),
                    TotalHours = table.Column<int>(nullable: false),
                    Amount = table.Column<float>(nullable: false),
                    ProgressRate = table.Column<int>(nullable: false),
                    ClientId = table.Column<string>(maxLength: 256, nullable: true),
                    ChangesHistory = table.Column<string>(type: "LONGTEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ConstructionWorkshops", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ConstructionWorkshops_Clients_ClientId",
                        column: x => x.ClientId,
                        principalTable: "Clients",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.SetNull);
                });

            migrationBuilder.CreateTable(
                name: "AccountingPeriods",
                columns: table => new
                {
                    CreatedOn = table.Column<DateTimeOffset>(nullable: false, defaultValueSql: "CURRENT_TIMESTAMP"),
                    LastModifiedOn = table.Column<DateTimeOffset>(nullable: true, defaultValueSql: "CURRENT_TIMESTAMP"),
                    SearchTerms = table.Column<string>(maxLength: 500, nullable: true),
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    StartingDate = table.Column<DateTime>(nullable: false),
                    EndingDate = table.Column<DateTime>(nullable: true),
                    Period = table.Column<int>(nullable: false),
                    UserId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AccountingPeriods", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AccountingPeriods_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.SetNull);
                });

            migrationBuilder.CreateTable(
                name: "GlobalHistory",
                columns: table => new
                {
                    CreatedOn = table.Column<DateTimeOffset>(nullable: false, defaultValueSql: "CURRENT_TIMESTAMP"),
                    LastModifiedOn = table.Column<DateTimeOffset>(nullable: true, defaultValueSql: "CURRENT_TIMESTAMP"),
                    SearchTerms = table.Column<string>(maxLength: 500, nullable: true),
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    UserId = table.Column<Guid>(nullable: false),
                    ActionDate = table.Column<DateTime>(nullable: false),
                    DocumentType = table.Column<int>(nullable: false),
                    DocumentId = table.Column<string>(maxLength: 256, nullable: true),
                    Reference = table.Column<string>(maxLength: 256, nullable: true),
                    Action = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GlobalHistory", x => x.Id);
                    table.ForeignKey(
                        name: "FK_GlobalHistory_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UserLogins",
                columns: table => new
                {
                    LoginProvider = table.Column<string>(maxLength: 256, nullable: false),
                    ProviderKey = table.Column<string>(maxLength: 256, nullable: false),
                    ProviderDisplayName = table.Column<string>(maxLength: 256, nullable: true),
                    UserId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserLogins", x => new { x.LoginProvider, x.ProviderKey });
                    table.ForeignKey(
                        name: "FK_UserLogins_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UserTokens",
                columns: table => new
                {
                    UserId = table.Column<Guid>(nullable: false),
                    LoginProvider = table.Column<string>(maxLength: 256, nullable: false),
                    Name = table.Column<string>(maxLength: 256, nullable: false),
                    Value = table.Column<string>(maxLength: 256, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserTokens", x => new { x.UserId, x.LoginProvider, x.Name });
                    table.ForeignKey(
                        name: "FK_UserTokens_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Expenses",
                columns: table => new
                {
                    CreatedOn = table.Column<DateTimeOffset>(nullable: false, defaultValueSql: "CURRENT_TIMESTAMP"),
                    LastModifiedOn = table.Column<DateTimeOffset>(nullable: true, defaultValueSql: "CURRENT_TIMESTAMP"),
                    SearchTerms = table.Column<string>(maxLength: 500, nullable: true),
                    Id = table.Column<string>(maxLength: 256, nullable: false),
                    Reference = table.Column<string>(maxLength: 50, nullable: true),
                    Status = table.Column<string>(maxLength: 50, nullable: true),
                    Note = table.Column<string>(maxLength: 500, nullable: true),
                    Purpose = table.Column<string>(maxLength: 500, nullable: true),
                    PaymentCondition = table.Column<string>(maxLength: 1000, nullable: true),
                    WorkshopId = table.Column<string>(maxLength: 256, nullable: true),
                    ChangesHistory = table.Column<string>(type: "LONGTEXT", nullable: true),
                    DueDate = table.Column<DateTime>(nullable: false),
                    CreationDate = table.Column<DateTime>(nullable: false),
                    CancellationDocument = table.Column<string>(type: "LONGTEXT", nullable: true),
                    Closed = table.Column<bool>(nullable: false),
                    ExpenseCategoryId = table.Column<string>(maxLength: 256, nullable: true),
                    OrderDetails = table.Column<string>(type: "LONGTEXT", nullable: true),
                    SupplierId = table.Column<string>(maxLength: 256, nullable: true),
                    Memos = table.Column<string>(type: "LONGTEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Expenses", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Expenses_ChartAccountCategories_ExpenseCategoryId",
                        column: x => x.ExpenseCategoryId,
                        principalTable: "ChartAccountCategories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.SetNull);
                    table.ForeignKey(
                        name: "FK_Expenses_Suppliers_SupplierId",
                        column: x => x.SupplierId,
                        principalTable: "Suppliers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Expenses_ConstructionWorkshops_WorkshopId",
                        column: x => x.WorkshopId,
                        principalTable: "ConstructionWorkshops",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.SetNull);
                });

            migrationBuilder.CreateTable(
                name: "Quotes",
                columns: table => new
                {
                    CreatedOn = table.Column<DateTimeOffset>(nullable: false, defaultValueSql: "CURRENT_TIMESTAMP"),
                    LastModifiedOn = table.Column<DateTimeOffset>(nullable: true, defaultValueSql: "CURRENT_TIMESTAMP"),
                    SearchTerms = table.Column<string>(maxLength: 500, nullable: true),
                    Id = table.Column<string>(maxLength: 256, nullable: false),
                    Reference = table.Column<string>(maxLength: 50, nullable: true),
                    Status = table.Column<string>(maxLength: 50, nullable: true),
                    Note = table.Column<string>(maxLength: 500, nullable: true),
                    Purpose = table.Column<string>(maxLength: 500, nullable: true),
                    PaymentCondition = table.Column<string>(maxLength: 1000, nullable: true),
                    WorkshopId = table.Column<string>(maxLength: 256, nullable: true),
                    ChangesHistory = table.Column<string>(type: "LONGTEXT", nullable: true),
                    LotDetails = table.Column<bool>(nullable: false),
                    AddressIntervention = table.Column<string>(type: "LONGTEXT", nullable: true),
                    OrderDetails = table.Column<string>(type: "LONGTEXT", nullable: true),
                    Client = table.Column<string>(type: "LONGTEXT", nullable: true),
                    ClientId = table.Column<string>(maxLength: 256, nullable: true),
                    Emails = table.Column<string>(type: "LONGTEXT", nullable: true),
                    Situations = table.Column<string>(type: "LONGTEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Quotes", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Quotes_Clients_ClientId",
                        column: x => x.ClientId,
                        principalTable: "Clients",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.SetNull);
                    table.ForeignKey(
                        name: "FK_Quotes_ConstructionWorkshops_WorkshopId",
                        column: x => x.WorkshopId,
                        principalTable: "ConstructionWorkshops",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.SetNull);
                });

            migrationBuilder.CreateTable(
                name: "WorkshopDocuments",
                columns: table => new
                {
                    CreatedOn = table.Column<DateTimeOffset>(nullable: false, defaultValueSql: "CURRENT_TIMESTAMP"),
                    LastModifiedOn = table.Column<DateTimeOffset>(nullable: true, defaultValueSql: "CURRENT_TIMESTAMP"),
                    SearchTerms = table.Column<string>(maxLength: 500, nullable: true),
                    Id = table.Column<string>(maxLength: 256, nullable: false),
                    Comment = table.Column<string>(maxLength: 500, nullable: true),
                    Designation = table.Column<string>(maxLength: 256, nullable: true),
                    RubricId = table.Column<string>(maxLength: 256, nullable: false),
                    WorkshopId = table.Column<string>(maxLength: 256, nullable: false),
                    User = table.Column<string>(type: "LONGTEXT", nullable: true),
                    Attachments = table.Column<string>(type: "LONGTEXT", nullable: true),
                    ChangesHistory = table.Column<string>(type: "LONGTEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WorkshopDocuments", x => x.Id);
                    table.ForeignKey(
                        name: "FK_WorkshopDocuments_WorkshopDocumentsRubrics_RubricId",
                        column: x => x.RubricId,
                        principalTable: "WorkshopDocumentsRubrics",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_WorkshopDocuments_ConstructionWorkshops_WorkshopId",
                        column: x => x.WorkshopId,
                        principalTable: "ConstructionWorkshops",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Invoices",
                columns: table => new
                {
                    CreatedOn = table.Column<DateTimeOffset>(nullable: false, defaultValueSql: "CURRENT_TIMESTAMP"),
                    LastModifiedOn = table.Column<DateTimeOffset>(nullable: true, defaultValueSql: "CURRENT_TIMESTAMP"),
                    SearchTerms = table.Column<string>(maxLength: 500, nullable: true),
                    Id = table.Column<string>(maxLength: 256, nullable: false),
                    Reference = table.Column<string>(maxLength: 50, nullable: true),
                    Status = table.Column<string>(maxLength: 50, nullable: true),
                    Note = table.Column<string>(maxLength: 500, nullable: true),
                    Purpose = table.Column<string>(maxLength: 500, nullable: true),
                    PaymentCondition = table.Column<string>(maxLength: 1000, nullable: true),
                    WorkshopId = table.Column<string>(maxLength: 256, nullable: true),
                    ChangesHistory = table.Column<string>(type: "LONGTEXT", nullable: true),
                    TypeInvoice = table.Column<int>(nullable: false),
                    Situation = table.Column<float>(nullable: false),
                    DueDate = table.Column<DateTime>(nullable: false),
                    CreationDate = table.Column<DateTime>(nullable: false),
                    OrderDetails = table.Column<string>(type: "LONGTEXT", nullable: true),
                    LotDetails = table.Column<bool>(nullable: false),
                    QuoteId = table.Column<string>(maxLength: 256, nullable: true),
                    Closed = table.Column<bool>(nullable: false),
                    Client = table.Column<string>(type: "LONGTEXT", nullable: true),
                    ClientId = table.Column<string>(maxLength: 256, nullable: true),
                    Memos = table.Column<string>(type: "LONGTEXT", nullable: true),
                    Emails = table.Column<string>(type: "LONGTEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Invoices", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Invoices_Clients_ClientId",
                        column: x => x.ClientId,
                        principalTable: "Clients",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.SetNull);
                    table.ForeignKey(
                        name: "FK_Invoices_Quotes_QuoteId",
                        column: x => x.QuoteId,
                        principalTable: "Quotes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.SetNull);
                    table.ForeignKey(
                        name: "FK_Invoices_ConstructionWorkshops_WorkshopId",
                        column: x => x.WorkshopId,
                        principalTable: "ConstructionWorkshops",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.SetNull);
                });

            migrationBuilder.CreateTable(
                name: "SuppliersOrders",
                columns: table => new
                {
                    CreatedOn = table.Column<DateTimeOffset>(nullable: false, defaultValueSql: "CURRENT_TIMESTAMP"),
                    LastModifiedOn = table.Column<DateTimeOffset>(nullable: true, defaultValueSql: "CURRENT_TIMESTAMP"),
                    SearchTerms = table.Column<string>(maxLength: 500, nullable: true),
                    Id = table.Column<string>(maxLength: 256, nullable: false),
                    Reference = table.Column<string>(maxLength: 50, nullable: true),
                    Status = table.Column<string>(maxLength: 50, nullable: true),
                    Note = table.Column<string>(maxLength: 500, nullable: true),
                    Purpose = table.Column<string>(maxLength: 500, nullable: true),
                    PaymentCondition = table.Column<string>(maxLength: 1000, nullable: true),
                    WorkshopId = table.Column<string>(maxLength: 256, nullable: true),
                    ChangesHistory = table.Column<string>(type: "LONGTEXT", nullable: true),
                    DueDate = table.Column<DateTime>(nullable: false),
                    CreationDate = table.Column<DateTime>(nullable: false),
                    SupplierId = table.Column<string>(maxLength: 256, nullable: false),
                    QuoteId = table.Column<string>(maxLength: 256, nullable: true),
                    OrderDetails = table.Column<string>(type: "LONGTEXT", nullable: true),
                    Memos = table.Column<string>(type: "LONGTEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SuppliersOrders", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SuppliersOrders_Quotes_QuoteId",
                        column: x => x.QuoteId,
                        principalTable: "Quotes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.SetNull);
                    table.ForeignKey(
                        name: "FK_SuppliersOrders_Suppliers_SupplierId",
                        column: x => x.SupplierId,
                        principalTable: "Suppliers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SuppliersOrders_ConstructionWorkshops_WorkshopId",
                        column: x => x.WorkshopId,
                        principalTable: "ConstructionWorkshops",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.SetNull);
                });

            migrationBuilder.CreateTable(
                name: "WorkshopDocuments_Types",
                columns: table => new
                {
                    TypeId = table.Column<string>(maxLength: 256, nullable: false),
                    DocumentId = table.Column<string>(maxLength: 256, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WorkshopDocuments_Types", x => new { x.TypeId, x.DocumentId });
                    table.ForeignKey(
                        name: "FK_WorkshopDocuments_Types_WorkshopDocuments_DocumentId",
                        column: x => x.DocumentId,
                        principalTable: "WorkshopDocuments",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_WorkshopDocuments_Types_WorkshopDocumentTypes_TypeId",
                        column: x => x.TypeId,
                        principalTable: "WorkshopDocumentTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CreditNotes",
                columns: table => new
                {
                    CreatedOn = table.Column<DateTimeOffset>(nullable: false, defaultValueSql: "CURRENT_TIMESTAMP"),
                    LastModifiedOn = table.Column<DateTimeOffset>(nullable: true, defaultValueSql: "CURRENT_TIMESTAMP"),
                    SearchTerms = table.Column<string>(maxLength: 500, nullable: true),
                    Id = table.Column<string>(maxLength: 256, nullable: false),
                    Reference = table.Column<string>(maxLength: 50, nullable: true),
                    Status = table.Column<string>(maxLength: 50, nullable: true),
                    Note = table.Column<string>(maxLength: 500, nullable: true),
                    Purpose = table.Column<string>(maxLength: 500, nullable: true),
                    PaymentCondition = table.Column<string>(maxLength: 1000, nullable: true),
                    WorkshopId = table.Column<string>(maxLength: 256, nullable: true),
                    ChangesHistory = table.Column<string>(type: "LONGTEXT", nullable: true),
                    DueDate = table.Column<DateTime>(nullable: false),
                    CreationDate = table.Column<DateTime>(nullable: false),
                    Client = table.Column<string>(type: "LONGTEXT", nullable: true),
                    Closed = table.Column<bool>(nullable: false),
                    ClientId = table.Column<string>(maxLength: 256, nullable: true),
                    InvoiceId = table.Column<string>(maxLength: 256, nullable: true),
                    OrderDetails = table.Column<string>(type: "LONGTEXT", nullable: true),
                    Memos = table.Column<string>(type: "LONGTEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CreditNotes", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CreditNotes_Clients_ClientId",
                        column: x => x.ClientId,
                        principalTable: "Clients",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.SetNull);
                    table.ForeignKey(
                        name: "FK_CreditNotes_Invoices_InvoiceId",
                        column: x => x.InvoiceId,
                        principalTable: "Invoices",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.SetNull);
                    table.ForeignKey(
                        name: "FK_CreditNotes_ConstructionWorkshops_WorkshopId",
                        column: x => x.WorkshopId,
                        principalTable: "ConstructionWorkshops",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "OperationSheets",
                columns: table => new
                {
                    CreatedOn = table.Column<DateTimeOffset>(nullable: false, defaultValueSql: "CURRENT_TIMESTAMP"),
                    LastModifiedOn = table.Column<DateTimeOffset>(nullable: true, defaultValueSql: "CURRENT_TIMESTAMP"),
                    SearchTerms = table.Column<string>(maxLength: 500, nullable: true),
                    Id = table.Column<string>(maxLength: 256, nullable: false),
                    Reference = table.Column<string>(maxLength: 50, nullable: true),
                    Status = table.Column<string>(maxLength: 50, nullable: true),
                    OrderDetails = table.Column<string>(type: "LONGTEXT", nullable: true),
                    AddressIntervention = table.Column<string>(type: "LONGTEXT", nullable: true),
                    StartDate = table.Column<DateTime>(nullable: false),
                    EndDate = table.Column<DateTime>(nullable: false),
                    Report = table.Column<string>(type: "LONGTEXT", maxLength: 256, nullable: true),
                    Purpose = table.Column<string>(maxLength: 500, nullable: true),
                    VisitsCount = table.Column<int>(nullable: false),
                    TotalBasketConsumption = table.Column<int>(nullable: false),
                    GoogleCalendarId = table.Column<string>(maxLength: 500, nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    ClientSignature = table.Column<string>(type: "LONGTEXT", nullable: true),
                    TechnicianSignature = table.Column<string>(type: "LONGTEXT", nullable: true),
                    QuoteId = table.Column<string>(maxLength: 256, nullable: true),
                    InvoiceId = table.Column<string>(maxLength: 256, nullable: true),
                    WorkshopId = table.Column<string>(maxLength: 256, nullable: true),
                    ClientId = table.Column<string>(maxLength: 256, nullable: true),
                    Emails = table.Column<string>(type: "LONGTEXT", nullable: true),
                    Memos = table.Column<string>(type: "LONGTEXT", nullable: true),
                    ChangesHistory = table.Column<string>(type: "LONGTEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OperationSheets", x => x.Id);
                    table.ForeignKey(
                        name: "FK_OperationSheets_Clients_ClientId",
                        column: x => x.ClientId,
                        principalTable: "Clients",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.SetNull);
                    table.ForeignKey(
                        name: "FK_OperationSheets_Invoices_InvoiceId",
                        column: x => x.InvoiceId,
                        principalTable: "Invoices",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.SetNull);
                    table.ForeignKey(
                        name: "FK_OperationSheets_Quotes_QuoteId",
                        column: x => x.QuoteId,
                        principalTable: "Quotes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.SetNull);
                    table.ForeignKey(
                        name: "FK_OperationSheets_ConstructionWorkshops_WorkshopId",
                        column: x => x.WorkshopId,
                        principalTable: "ConstructionWorkshops",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.SetNull);
                });

            migrationBuilder.CreateTable(
                name: "SupplierOrders_Expenses",
                columns: table => new
                {
                    ExpenseId = table.Column<string>(maxLength: 256, nullable: false),
                    SupplierOrderId = table.Column<string>(maxLength: 256, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SupplierOrders_Expenses", x => new { x.SupplierOrderId, x.ExpenseId });
                    table.ForeignKey(
                        name: "FK_SupplierOrders_Expenses_Expenses_ExpenseId",
                        column: x => x.ExpenseId,
                        principalTable: "Expenses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SupplierOrders_Expenses_SuppliersOrders_SupplierOrderId",
                        column: x => x.SupplierOrderId,
                        principalTable: "SuppliersOrders",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Payment",
                columns: table => new
                {
                    CreatedOn = table.Column<DateTimeOffset>(nullable: false, defaultValueSql: "CURRENT_TIMESTAMP"),
                    LastModifiedOn = table.Column<DateTimeOffset>(nullable: true, defaultValueSql: "CURRENT_TIMESTAMP"),
                    SearchTerms = table.Column<string>(maxLength: 500, nullable: true),
                    Id = table.Column<string>(maxLength: 256, nullable: false),
                    Description = table.Column<string>(maxLength: 500, nullable: true),
                    Operation = table.Column<int>(nullable: false),
                    Type = table.Column<int>(nullable: false),
                    DatePayment = table.Column<DateTime>(nullable: false),
                    Closed = table.Column<bool>(nullable: false),
                    AccountId = table.Column<int>(nullable: true),
                    PaymentMethodId = table.Column<int>(nullable: true),
                    CreditNoteId = table.Column<string>(maxLength: 256, nullable: true),
                    ChangesHistory = table.Column<string>(type: "LONGTEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Payment", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Payment_FinancialAccounts_AccountId",
                        column: x => x.AccountId,
                        principalTable: "FinancialAccounts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.SetNull);
                    table.ForeignKey(
                        name: "FK_Payment_PaymentMethod_AccountId",
                        column: x => x.AccountId,
                        principalTable: "PaymentMethod",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.SetNull);
                    table.ForeignKey(
                        name: "FK_Payment_CreditNotes_CreditNoteId",
                        column: x => x.CreditNoteId,
                        principalTable: "CreditNotes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.SetNull);
                });

            migrationBuilder.CreateTable(
                name: "OperationSheets_Technicians",
                columns: table => new
                {
                    OperationSheetId = table.Column<string>(maxLength: 256, nullable: false),
                    TechnicianId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OperationSheets_Technicians", x => new { x.TechnicianId, x.OperationSheetId });
                    table.ForeignKey(
                        name: "FK_OperationSheets_Technicians_OperationSheets_OperationSheetId",
                        column: x => x.OperationSheetId,
                        principalTable: "OperationSheets",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_OperationSheets_Technicians_Users_TechnicianId",
                        column: x => x.TechnicianId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Expenses_Payments",
                columns: table => new
                {
                    ExpenseId = table.Column<string>(maxLength: 256, nullable: false),
                    PaymentId = table.Column<string>(maxLength: 256, nullable: false),
                    Amount = table.Column<float>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Expenses_Payments", x => new { x.ExpenseId, x.PaymentId });
                    table.ForeignKey(
                        name: "FK_Expenses_Payments_Expenses_ExpenseId",
                        column: x => x.ExpenseId,
                        principalTable: "Expenses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Expenses_Payments_Payment_PaymentId",
                        column: x => x.PaymentId,
                        principalTable: "Payment",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Invoices_Payments",
                columns: table => new
                {
                    InvoiceId = table.Column<string>(maxLength: 256, nullable: false),
                    PaymentId = table.Column<string>(maxLength: 256, nullable: false),
                    Amount = table.Column<float>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Invoices_Payments", x => new { x.InvoiceId, x.PaymentId });
                    table.ForeignKey(
                        name: "FK_Invoices_Payments_Invoices_InvoiceId",
                        column: x => x.InvoiceId,
                        principalTable: "Invoices",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Invoices_Payments_Payment_PaymentId",
                        column: x => x.PaymentId,
                        principalTable: "Payment",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AccountingPeriods_SearchTerms",
                table: "AccountingPeriods",
                column: "SearchTerms");

            migrationBuilder.CreateIndex(
                name: "IX_AccountingPeriods_UserId",
                table: "AccountingPeriods",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_ChartAccountCategories_ParentId",
                table: "ChartAccountCategories",
                column: "ParentId");

            migrationBuilder.CreateIndex(
                name: "IX_ChartAccountCategories_SearchTerms",
                table: "ChartAccountCategories",
                column: "SearchTerms");

            migrationBuilder.CreateIndex(
                name: "IX_Clients_GroupeId",
                table: "Clients",
                column: "GroupeId");

            migrationBuilder.CreateIndex(
                name: "IX_Clients_SearchTerms",
                table: "Clients",
                column: "SearchTerms");

            migrationBuilder.CreateIndex(
                name: "IX_ConstructionWorkshops_ClientId",
                table: "ConstructionWorkshops",
                column: "ClientId");

            migrationBuilder.CreateIndex(
                name: "IX_ConstructionWorkshops_SearchTerms",
                table: "ConstructionWorkshops",
                column: "SearchTerms");

            migrationBuilder.CreateIndex(
                name: "IX_Countries_Name",
                table: "Countries",
                column: "Name",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_CreditNotes_ClientId",
                table: "CreditNotes",
                column: "ClientId");

            migrationBuilder.CreateIndex(
                name: "IX_CreditNotes_InvoiceId",
                table: "CreditNotes",
                column: "InvoiceId");

            migrationBuilder.CreateIndex(
                name: "IX_CreditNotes_Reference",
                table: "CreditNotes",
                column: "Reference",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_CreditNotes_SearchTerms",
                table: "CreditNotes",
                column: "SearchTerms");

            migrationBuilder.CreateIndex(
                name: "IX_CreditNotes_WorkshopId",
                table: "CreditNotes",
                column: "WorkshopId");

            migrationBuilder.CreateIndex(
                name: "IX_Expenses_ExpenseCategoryId",
                table: "Expenses",
                column: "ExpenseCategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_Expenses_Reference",
                table: "Expenses",
                column: "Reference",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Expenses_SearchTerms",
                table: "Expenses",
                column: "SearchTerms");

            migrationBuilder.CreateIndex(
                name: "IX_Expenses_SupplierId",
                table: "Expenses",
                column: "SupplierId");

            migrationBuilder.CreateIndex(
                name: "IX_Expenses_WorkshopId",
                table: "Expenses",
                column: "WorkshopId");

            migrationBuilder.CreateIndex(
                name: "IX_Expenses_Payments_PaymentId",
                table: "Expenses_Payments",
                column: "PaymentId");

            migrationBuilder.CreateIndex(
                name: "IX_FinancialAccounts_SearchTerms",
                table: "FinancialAccounts",
                column: "SearchTerms");

            migrationBuilder.CreateIndex(
                name: "IX_GlobalHistory_SearchTerms",
                table: "GlobalHistory",
                column: "SearchTerms");

            migrationBuilder.CreateIndex(
                name: "IX_GlobalHistory_UserId",
                table: "GlobalHistory",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Groups_SearchTerms",
                table: "Groups",
                column: "SearchTerms");

            migrationBuilder.CreateIndex(
                name: "IX_Invoices_ClientId",
                table: "Invoices",
                column: "ClientId");

            migrationBuilder.CreateIndex(
                name: "IX_Invoices_QuoteId",
                table: "Invoices",
                column: "QuoteId");

            migrationBuilder.CreateIndex(
                name: "IX_Invoices_Reference",
                table: "Invoices",
                column: "Reference",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Invoices_SearchTerms",
                table: "Invoices",
                column: "SearchTerms");

            migrationBuilder.CreateIndex(
                name: "IX_Invoices_WorkshopId",
                table: "Invoices",
                column: "WorkshopId");

            migrationBuilder.CreateIndex(
                name: "IX_Invoices_Payments_PaymentId",
                table: "Invoices_Payments",
                column: "PaymentId");

            migrationBuilder.CreateIndex(
                name: "IX_Labels_SearchTerms",
                table: "Labels",
                column: "SearchTerms");

            migrationBuilder.CreateIndex(
                name: "IX_Lots_SearchTerms",
                table: "Lots",
                column: "SearchTerms");

            migrationBuilder.CreateIndex(
                name: "IX_Lots_Products_ProductId",
                table: "Lots_Products",
                column: "ProductId");

            migrationBuilder.CreateIndex(
                name: "IX_OperationSheets_ClientId",
                table: "OperationSheets",
                column: "ClientId");

            migrationBuilder.CreateIndex(
                name: "IX_OperationSheets_InvoiceId",
                table: "OperationSheets",
                column: "InvoiceId");

            migrationBuilder.CreateIndex(
                name: "IX_OperationSheets_QuoteId",
                table: "OperationSheets",
                column: "QuoteId");

            migrationBuilder.CreateIndex(
                name: "IX_OperationSheets_SearchTerms",
                table: "OperationSheets",
                column: "SearchTerms");

            migrationBuilder.CreateIndex(
                name: "IX_OperationSheets_WorkshopId",
                table: "OperationSheets",
                column: "WorkshopId");

            migrationBuilder.CreateIndex(
                name: "IX_OperationSheets_Technicians_OperationSheetId",
                table: "OperationSheets_Technicians",
                column: "OperationSheetId");

            migrationBuilder.CreateIndex(
                name: "IX_Payment_AccountId",
                table: "Payment",
                column: "AccountId");

            migrationBuilder.CreateIndex(
                name: "IX_Payment_CreditNoteId",
                table: "Payment",
                column: "CreditNoteId");

            migrationBuilder.CreateIndex(
                name: "IX_Payment_SearchTerms",
                table: "Payment",
                column: "SearchTerms");

            migrationBuilder.CreateIndex(
                name: "IX_PaymentMethod_SearchTerms",
                table: "PaymentMethod",
                column: "SearchTerms");

            migrationBuilder.CreateIndex(
                name: "IX_Products_CategoryId",
                table: "Products",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_Products_Reference",
                table: "Products",
                column: "Reference");

            migrationBuilder.CreateIndex(
                name: "IX_Products_SearchTerms",
                table: "Products",
                column: "SearchTerms");

            migrationBuilder.CreateIndex(
                name: "IX_Products_Labels_ProductId",
                table: "Products_Labels",
                column: "ProductId");

            migrationBuilder.CreateIndex(
                name: "IX_Products_Suppliers_SupplierId",
                table: "Products_Suppliers",
                column: "SupplierId");

            migrationBuilder.CreateIndex(
                name: "IX_Quotes_ClientId",
                table: "Quotes",
                column: "ClientId");

            migrationBuilder.CreateIndex(
                name: "IX_Quotes_Reference",
                table: "Quotes",
                column: "Reference",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Quotes_SearchTerms",
                table: "Quotes",
                column: "SearchTerms");

            migrationBuilder.CreateIndex(
                name: "IX_Quotes_WorkshopId",
                table: "Quotes",
                column: "WorkshopId");

            migrationBuilder.CreateIndex(
                name: "IX_Roles_Name",
                table: "Roles",
                column: "Name");

            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                table: "Roles",
                column: "NormalizedName");

            migrationBuilder.CreateIndex(
                name: "IX_Roles_SearchTerms",
                table: "Roles",
                column: "SearchTerms");

            migrationBuilder.CreateIndex(
                name: "IX_SupplierOrders_Expenses_ExpenseId",
                table: "SupplierOrders_Expenses",
                column: "ExpenseId");

            migrationBuilder.CreateIndex(
                name: "IX_Suppliers_SearchTerms",
                table: "Suppliers",
                column: "SearchTerms");

            migrationBuilder.CreateIndex(
                name: "IX_SuppliersOrders_QuoteId",
                table: "SuppliersOrders",
                column: "QuoteId");

            migrationBuilder.CreateIndex(
                name: "IX_SuppliersOrders_Reference",
                table: "SuppliersOrders",
                column: "Reference",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_SuppliersOrders_SearchTerms",
                table: "SuppliersOrders",
                column: "SearchTerms");

            migrationBuilder.CreateIndex(
                name: "IX_SuppliersOrders_SupplierId",
                table: "SuppliersOrders",
                column: "SupplierId");

            migrationBuilder.CreateIndex(
                name: "IX_SuppliersOrders_WorkshopId",
                table: "SuppliersOrders",
                column: "WorkshopId");

            migrationBuilder.CreateIndex(
                name: "IX_Taxes_SearchTerms",
                table: "Taxes",
                column: "SearchTerms");

            migrationBuilder.CreateIndex(
                name: "IX_Units_SearchTerms",
                table: "Units",
                column: "SearchTerms");

            migrationBuilder.CreateIndex(
                name: "IX_UserLogins_UserId",
                table: "UserLogins",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Users_Email",
                table: "Users",
                column: "Email",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "EmailIndex",
                table: "Users",
                column: "NormalizedEmail",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "UserNameIndex",
                table: "Users",
                column: "NormalizedUserName",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Users_RoleId",
                table: "Users",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "IX_Users_SearchTerms",
                table: "Users",
                column: "SearchTerms");

            migrationBuilder.CreateIndex(
                name: "IX_Users_UserName",
                table: "Users",
                column: "UserName",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_WorkshopDocuments_RubricId",
                table: "WorkshopDocuments",
                column: "RubricId");

            migrationBuilder.CreateIndex(
                name: "IX_WorkshopDocuments_SearchTerms",
                table: "WorkshopDocuments",
                column: "SearchTerms");

            migrationBuilder.CreateIndex(
                name: "IX_WorkshopDocuments_WorkshopId",
                table: "WorkshopDocuments",
                column: "WorkshopId");

            migrationBuilder.CreateIndex(
                name: "IX_WorkshopDocuments_Types_DocumentId",
                table: "WorkshopDocuments_Types",
                column: "DocumentId");

            migrationBuilder.CreateIndex(
                name: "IX_WorkshopDocumentsRubrics_SearchTerms",
                table: "WorkshopDocumentsRubrics",
                column: "SearchTerms");

            migrationBuilder.CreateIndex(
                name: "IX_WorkshopDocumentTypes_SearchTerms",
                table: "WorkshopDocumentTypes",
                column: "SearchTerms");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AccountingPeriods");

            migrationBuilder.DropTable(
                name: "ApplicationConfigurations");

            migrationBuilder.DropTable(
                name: "Countries");

            migrationBuilder.DropTable(
                name: "Expenses_Payments");

            migrationBuilder.DropTable(
                name: "GlobalHistory");

            migrationBuilder.DropTable(
                name: "Invoices_Payments");

            migrationBuilder.DropTable(
                name: "Lots_Products");

            migrationBuilder.DropTable(
                name: "OperationSheets_Technicians");

            migrationBuilder.DropTable(
                name: "Products_Labels");

            migrationBuilder.DropTable(
                name: "Products_Suppliers");

            migrationBuilder.DropTable(
                name: "SupplierOrders_Expenses");

            migrationBuilder.DropTable(
                name: "Taxes");

            migrationBuilder.DropTable(
                name: "Units");

            migrationBuilder.DropTable(
                name: "UserLogins");

            migrationBuilder.DropTable(
                name: "UserTokens");

            migrationBuilder.DropTable(
                name: "WorkshopDocuments_Types");

            migrationBuilder.DropTable(
                name: "Payment");

            migrationBuilder.DropTable(
                name: "Lots");

            migrationBuilder.DropTable(
                name: "OperationSheets");

            migrationBuilder.DropTable(
                name: "Labels");

            migrationBuilder.DropTable(
                name: "Products");

            migrationBuilder.DropTable(
                name: "Expenses");

            migrationBuilder.DropTable(
                name: "SuppliersOrders");

            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.DropTable(
                name: "WorkshopDocuments");

            migrationBuilder.DropTable(
                name: "WorkshopDocumentTypes");

            migrationBuilder.DropTable(
                name: "FinancialAccounts");

            migrationBuilder.DropTable(
                name: "PaymentMethod");

            migrationBuilder.DropTable(
                name: "CreditNotes");

            migrationBuilder.DropTable(
                name: "ChartAccountCategories");

            migrationBuilder.DropTable(
                name: "Suppliers");

            migrationBuilder.DropTable(
                name: "Roles");

            migrationBuilder.DropTable(
                name: "WorkshopDocumentsRubrics");

            migrationBuilder.DropTable(
                name: "Invoices");

            migrationBuilder.DropTable(
                name: "Quotes");

            migrationBuilder.DropTable(
                name: "ConstructionWorkshops");

            migrationBuilder.DropTable(
                name: "Clients");

            migrationBuilder.DropTable(
                name: "Groups");
        }
    }
}
