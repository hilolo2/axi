﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Axiobat.Persistence.Migrations
{
    public partial class RemoveUniqueConstraintOnReferenceIndex : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_SuppliersOrders_Reference",
                table: "SuppliersOrders");

            migrationBuilder.DropIndex(
                name: "IX_Quotes_Reference",
                table: "Quotes");

            migrationBuilder.DropIndex(
                name: "IX_MaintenanceContracts_Reference",
                table: "MaintenanceContracts");

            migrationBuilder.DropIndex(
                name: "IX_Invoices_Reference",
                table: "Invoices");

            migrationBuilder.DropIndex(
                name: "IX_Expenses_Reference",
                table: "Expenses");

            migrationBuilder.DropIndex(
                name: "IX_CreditNotes_Reference",
                table: "CreditNotes");

            migrationBuilder.AlterColumn<string>(
                name: "Type",
                table: "Clients",
                maxLength: 256,
                nullable: true,
                defaultValue: "particular",
                oldClrType: typeof(string),
                oldMaxLength: 256,
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_SuppliersOrders_Reference",
                table: "SuppliersOrders",
                column: "Reference");

            migrationBuilder.CreateIndex(
                name: "IX_Suppliers_Reference",
                table: "Suppliers",
                column: "Reference");

            migrationBuilder.CreateIndex(
                name: "IX_Quotes_Reference",
                table: "Quotes",
                column: "Reference");

            migrationBuilder.CreateIndex(
                name: "IX_OperationSheets_Reference",
                table: "OperationSheets",
                column: "Reference");

            migrationBuilder.CreateIndex(
                name: "IX_MaintenanceOperationSheets_Reference",
                table: "MaintenanceOperationSheets",
                column: "Reference");

            migrationBuilder.CreateIndex(
                name: "IX_MaintenanceContracts_Reference",
                table: "MaintenanceContracts",
                column: "Reference");

            migrationBuilder.CreateIndex(
                name: "IX_Invoices_Reference",
                table: "Invoices",
                column: "Reference");

            migrationBuilder.CreateIndex(
                name: "IX_Expenses_Reference",
                table: "Expenses",
                column: "Reference");

            migrationBuilder.CreateIndex(
                name: "IX_CreditNotes_Reference",
                table: "CreditNotes",
                column: "Reference");

            migrationBuilder.CreateIndex(
                name: "IX_Clients_Reference",
                table: "Clients",
                column: "Reference");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_SuppliersOrders_Reference",
                table: "SuppliersOrders");

            migrationBuilder.DropIndex(
                name: "IX_Suppliers_Reference",
                table: "Suppliers");

            migrationBuilder.DropIndex(
                name: "IX_Quotes_Reference",
                table: "Quotes");

            migrationBuilder.DropIndex(
                name: "IX_OperationSheets_Reference",
                table: "OperationSheets");

            migrationBuilder.DropIndex(
                name: "IX_MaintenanceOperationSheets_Reference",
                table: "MaintenanceOperationSheets");

            migrationBuilder.DropIndex(
                name: "IX_MaintenanceContracts_Reference",
                table: "MaintenanceContracts");

            migrationBuilder.DropIndex(
                name: "IX_Invoices_Reference",
                table: "Invoices");

            migrationBuilder.DropIndex(
                name: "IX_Expenses_Reference",
                table: "Expenses");

            migrationBuilder.DropIndex(
                name: "IX_CreditNotes_Reference",
                table: "CreditNotes");

            migrationBuilder.DropIndex(
                name: "IX_Clients_Reference",
                table: "Clients");

            migrationBuilder.AlterColumn<string>(
                name: "Type",
                table: "Clients",
                maxLength: 256,
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 256,
                oldNullable: true,
                oldDefaultValue: "particular");

            migrationBuilder.CreateIndex(
                name: "IX_SuppliersOrders_Reference",
                table: "SuppliersOrders",
                column: "Reference",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Quotes_Reference",
                table: "Quotes",
                column: "Reference",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_MaintenanceContracts_Reference",
                table: "MaintenanceContracts",
                column: "Reference",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Invoices_Reference",
                table: "Invoices",
                column: "Reference",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Expenses_Reference",
                table: "Expenses",
                column: "Reference",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_CreditNotes_Reference",
                table: "CreditNotes",
                column: "Reference",
                unique: true);
        }
    }
}
