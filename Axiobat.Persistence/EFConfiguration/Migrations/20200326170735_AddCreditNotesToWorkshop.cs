﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Axiobat.Persistence.Migrations
{
    public partial class AddCreditNotesToWorkshop : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CreditNotes_ConstructionWorkshops_WorkshopId",
                table: "CreditNotes");

            migrationBuilder.AddForeignKey(
                name: "FK_CreditNotes_ConstructionWorkshops_WorkshopId",
                table: "CreditNotes",
                column: "WorkshopId",
                principalTable: "ConstructionWorkshops",
                principalColumn: "Id",
                onDelete: ReferentialAction.SetNull);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CreditNotes_ConstructionWorkshops_WorkshopId",
                table: "CreditNotes");

            migrationBuilder.AddForeignKey(
                name: "FK_CreditNotes_ConstructionWorkshops_WorkshopId",
                table: "CreditNotes",
                column: "WorkshopId",
                principalTable: "ConstructionWorkshops",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
