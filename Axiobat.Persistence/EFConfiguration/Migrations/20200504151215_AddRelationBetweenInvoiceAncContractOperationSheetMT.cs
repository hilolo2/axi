﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Axiobat.Persistence.Migrations
{
    public partial class AddRelationBetweenInvoiceAncContractOperationSheetMT : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ContractId",
                table: "Invoices",
                maxLength: 256,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "OperationSheetMaintenanceId",
                table: "Invoices",
                maxLength: 256,
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Invoices_ContractId",
                table: "Invoices",
                column: "ContractId");

            migrationBuilder.CreateIndex(
                name: "IX_Invoices_OperationSheetMaintenanceId",
                table: "Invoices",
                column: "OperationSheetMaintenanceId",
                unique: false);

            migrationBuilder.AddForeignKey(
                name: "FK_Invoices_MaintenanceContracts_ContractId",
                table: "Invoices",
                column: "ContractId",
                principalTable: "MaintenanceContracts",
                principalColumn: "Id",
                onDelete: ReferentialAction.SetNull);

            migrationBuilder.AddForeignKey(
                name: "FK_Invoices_MaintenanceOperationSheets_OperationSheetMaintenanc~",
                table: "Invoices",
                column: "OperationSheetMaintenanceId",
                principalTable: "MaintenanceOperationSheets",
                principalColumn: "Id",
                onDelete: ReferentialAction.SetNull);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Invoices_MaintenanceContracts_ContractId",
                table: "Invoices");

            migrationBuilder.DropForeignKey(
                name: "FK_Invoices_MaintenanceOperationSheets_OperationSheetMaintenanc~",
                table: "Invoices");

            migrationBuilder.DropIndex(
                name: "IX_Invoices_ContractId",
                table: "Invoices");

            migrationBuilder.DropIndex(
                name: "IX_Invoices_OperationSheetMaintenanceId",
                table: "Invoices");

            migrationBuilder.DropColumn(
                name: "ContractId",
                table: "Invoices");

            migrationBuilder.DropColumn(
                name: "OperationSheetMaintenanceId",
                table: "Invoices");
        }
    }
}
