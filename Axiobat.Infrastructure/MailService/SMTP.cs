﻿namespace Axiobat.Infrastructure.MailService
{
    using Domain.Entities;

    /// <summary>
    /// a class that holds information about SMTP configuration
    /// </summary>
    internal partial class SMTP
    {
        /// <summary>
        /// user name
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// server
        /// </summary>
        public string Server { get; set; }

        /// <summary>
        /// password
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// port
        /// </summary>
        public string Port { get; set; }

        /// <summary>
        /// SSL
        /// </summary>
        public bool SSL { get; set; }

        public static implicit operator SMTP(MessagingSettings messagerie) => new SMTP()
        {
            UserName = messagerie.UserName,
            Password = messagerie.Password,
            Port = messagerie.Port,
            Server = messagerie.Server,
            SSL = messagerie.SSL,
        };

        public static implicit operator MessagingSettings(SMTP smtp) => new MessagingSettings()
        {
            UserName = smtp.UserName,
            Password = smtp.Password,
            Port = smtp.Port,
            Server = smtp.Server,
            SSL = smtp.SSL,
        };
    }
}
