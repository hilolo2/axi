﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

namespace Axiobat.Infrastructure.FileServices.Helpers
{
    class ConvertBase64ToImage
    {
        public string Replace(string ToReplace)
        {
            //string content = ToReplace.Replace("data:image/png;base64,", String.Empty);
            // content = ToReplace.Replace("data:image/jpeg;base64,", String.Empty);
            //content = ToReplace.Replace("data:image/jpg;base64,", String.Empty);
            //content = ToReplace.Replace("data:image/PNG;base64,", String.Empty);
           // var strImage = ToReplace.replace(/^ data:image\/[a - z] +; base64,/, "");
           // var solution = string.split("base64,")[1];
         var content =    Regex.Replace(ToReplace, @"^.+?(;base64),", string.Empty);

            //if (!content.EndsWith("="))
            //    content += "=";
            return content;
        }
        public bool IsBase64(string base64String)
        {
            if (string.IsNullOrEmpty(base64String) || base64String.Length % 4 != 0
               || base64String.Contains(" ") || base64String.Contains("\t") || base64String.Contains("\r") || base64String.Contains("\n"))
                return false;

            try
            {
                Convert.FromBase64String(base64String);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public System.Drawing.Image ByteArrayToImage(byte[] byteArrayIn)
        {
            using (MemoryStream ms = new MemoryStream(byteArrayIn))
            {
                return System.Drawing.Image.FromStream(ms);
            }
        }
    }
}
