﻿namespace Axiobat.Infrastructure.FileServices.Helpers
{
    using iTextSharp.text;

    /// <summary>
    /// the list of fonts
    /// </summary>
    internal static partial class PDFFonts
    {
        public static Font H7 = FontFactory.GetFont(FontFactory.COURIER, 6);
        public static Font H9 = FontFactory.GetFont(FontFactory.COURIER_BOLD, 12, BaseColor.Black);
        public static Font H8 = FontFactory.GetFont("Arial", 10, Font.NORMAL, new BaseColor(161, 165, 169));
        public static Font H11 = FontFactory.GetFont("Arial", 9, Font.NORMAL, new BaseColor(161, 165, 169));
        public static Font H10 = FontFactory.GetFont("Arial", 9, Font.NORMAL, BaseColor.Black);
        public static Font H10B = FontFactory.GetFont("Arial", 9, Font.BOLD, BaseColor.Black);
        public static Font H12 = FontFactory.GetFont("Arial", 9, Font.ITALIC, BaseColor.Black);
        public static Font H12Bold = FontFactory.GetFont("Arial", 8, Font.BOLDITALIC, BaseColor.Black);
        public static Font H10Bold = FontFactory.GetFont("Arial", 16, Font.BOLD, BaseColor.Black);
        public static Font H10Bold2 = FontFactory.GetFont("Arial", 10, Font.BOLD, BaseColor.Black);
        public static Font H13 = FontFactory.GetFont("Arial", 14, Font.NORMAL, new BaseColor(161, 165, 169));
        public static Font H14 = FontFactory.GetFont("Arial", 7, Font.NORMAL, BaseColor.Black);
        public static Font H15 = FontFactory.GetFont("Arial", 8, Font.NORMAL, BaseColor.Black);
        public static Font H16 = FontFactory.GetFont("Arial", 8, Font.NORMAL, BaseColor.Red);
        public static Font H17 = FontFactory.GetFont("Arial", 8, Font.NORMAL, BaseColor.Green);
        public static Font H15Bold = FontFactory.GetFont("Arial", 8, Font.BOLD, BaseColor.Black);
        public static Font H11G = FontFactory.GetFont("Arial", 8, Font.NORMAL, new BaseColor(161, 165, 169));
        public static Font HItalic = FontFactory.GetFont("Arial", 8, Font.ITALIC, BaseColor.Black);
    }

    /// <summary>
    /// list of colors
    /// </summary>
    internal static partial class PDFColors
    {
        public static BaseColor TableHeader = new BaseColor(199, 233, 250);
        public static BaseColor BorderBotom = new BaseColor(224, 224, 224);
        public static BaseColor BorderTop = new BaseColor(195, 224, 134);
    }
}
