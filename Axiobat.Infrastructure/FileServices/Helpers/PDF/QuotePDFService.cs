﻿namespace Axiobat.Infrastructure.FileServices.Helpers
{
    using App.Common;
    using Axiobat.Domain.Entities.Configuration;
    using Domain.Entities;
    using iTextSharp.text;
    using iTextSharp.text.html.simpleparser;
    using iTextSharp.text.pdf;
    using Newtonsoft.Json.Linq;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public partial class QuotePDFService
    {
        /// <summary>
        /// generate a PDF for the quote
        /// </summary>
        /// <param name="quote">the quote document instant</param>
        /// <returns>the PDF document as Byte[]</returns>
        internal byte[] Generate()
        {
            using (var memoryStream = new MemoryStream())
            {
                // create document and PDF writer
                var document = new iTextSharp.text.Document(PageSize.A4, 30, 30, 5, 70);
                var writer = PdfWriter.GetInstance(document, memoryStream);
                writer.PageEvent = new Footer(_pdfOptions);

                // open the document
                document.Open();

                // add document header section
                document.Add(CreateHeaderSection(_configuration, "Devis", _quote.Reference, _quote.Client.Reference,_quote.Client.FullName, _quote.CreatedOn.DateTime, _quote.DueDate, _pdfOptions));

                // add document Address informations section
                document.Add(CreateDocumentAddressSection(_quote.AddressIntervention, _quote.Client.BillingAddress));

                // Add order details section
                CreateOrderDetailsSection(document, _quote.Purpose, _quote.OrderDetails , _quote.LotDetails);

                // add Tax details Section
                document.Add(CreateTaxDetailsSection(_quote.PaymentCondition.IsValid() ? _quote.PaymentCondition : _configuration.PaymentCondition,
                    _quote.OrderDetails.GetTaxDetails(), _quote.OrderDetails, calculHtItem));                

                // add Payment Condition section
               // document.Add(CreatePaymentConditionSection(_quote.PaymentCondition.IsValid() ? _quote.PaymentCondition : _configuration.PaymentCondition));

                // add Page footer section
                document.Add(CreateFooterSection(_quote.Note.IsValid() ? _quote.Note : _configuration.Note));


                document.Add(CreateSignature(_quote));

                // close document
                document.Close();

                // export the file as byte array
                return memoryStream.ToArray();
            }
        }
    }

    public partial class QuotePDFService
    {
        private readonly Quote _quote;
        private readonly PdfOptions _pdfOptions;
        private readonly DocumentConfiguration _configuration;
        private float calculHtItem ;

        public QuotePDFService(Quote quote, DocumentConfiguration configuration, PdfOptions pdfOptions)
        {
            _quote = quote;
            _pdfOptions = pdfOptions;
            _configuration = configuration;
            calculHtItem = 0;
        }

        private IElement CreateTaxDetailsSection(string paymentCondition, IEnumerable<TaxDetails> taxDetails, WorkshopOrderDetails orderDetails, float calculHtItem)
        {
            PdfPTable tabtva = new PdfPTable(3) { SpacingBefore = 5f, WidthPercentage = 100 };

            tabtva.DefaultCell.Border = Rectangle.NO_BORDER;
            tabtva.SetWidths(new float[] { 350f, 70f, 300f });

            PdfPTable tab_calculeMultiTva = new PdfPTable(1);
            tab_calculeMultiTva.DefaultCell.Border = Rectangle.NO_BORDER;
            tab_calculeMultiTva.WidthPercentage = 100;
            PdfPTable eclatementTVA = CreatePaymentConditionSection(paymentCondition);

            //eclatementTVA.AddCell(new PdfPCell(new Paragraph(new Chunk("TVA", PDFFonts.H15))) { BorderWidth = 0, Padding = 5f, BackgroundColor = PDFColors.TableHeader, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 2 });
            //eclatementTVA.AddCell(new PdfPCell(new Paragraph(new Chunk("Base H.T", PDFFonts.H15))) { BorderWidth = 0, Padding = 5f, BackgroundColor = PDFColors.TableHeader, HorizontalAlignment = Element.ALIGN_RIGHT, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 2 });
            //eclatementTVA.AddCell(new PdfPCell(new Paragraph(new Chunk("Montant", PDFFonts.H15))) { BorderWidth = 0, Padding = 5f, BackgroundColor = PDFColors.TableHeader, HorizontalAlignment = Element.ALIGN_RIGHT, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 2 });

            //foreach (var tax in taxDetails)
            //{
            //    eclatementTVA.AddCell(new PdfPCell(new Paragraph(new Chunk("" + string.Format("{0:0.00}%", tax.Tax), PDFFonts.H15))) { BorderWidth = 0, Padding = 5f, BorderColorBottom = PDFColors.BorderBotom, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 2 });
            //    eclatementTVA.AddCell(new PdfPCell(new Paragraph(new Chunk(string.Format("{0:0.00}", tax.TotalHT) + " €", PDFFonts.H15))) { BorderWidth = 0, Padding = 5f, BorderColorBottom = PDFColors.BorderBotom, HorizontalAlignment = Element.ALIGN_RIGHT, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 2 });
            //    eclatementTVA.AddCell(new PdfPCell(new Paragraph(new Chunk(string.Format("{0:0.00}", tax.TotalTax) + " €", PDFFonts.H15))) { BorderWidth = 0, Padding = 5f, BorderColorBottom = PDFColors.BorderBotom, HorizontalAlignment = Element.ALIGN_RIGHT, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 2 });
            //}

            tab_calculeMultiTva.AddCell(new PdfPCell(eclatementTVA) { BorderWidth = 0});

            tab_calculeMultiTva.AddCell(new PdfPCell() { FixedHeight = 5f, BorderWidth = 0 });
            tabtva.AddCell(new PdfPCell(tab_calculeMultiTva) { BorderWidth = 0 });
            // space between tabs
            tabtva.AddCell(new PdfPCell() { BorderWidth = 0 });
            //
            PdfPTable condition = new PdfPTable(2);

            condition.DefaultCell.Border = Rectangle.NO_BORDER;
            condition.SetWidths(new float[] { 350f, 150f });
            var MontantHt = orderDetails.TotalHT * (1 + (orderDetails.Proportion / 100));
            var totalHtSansRemise = orderDetails.GetTotalHT();

            if ((orderDetails.GlobalDiscount.Value != 0))
            {
                var MontantSousHt = totalHtSansRemise / (1 + (orderDetails.Proportion / 100));
                condition.AddCell(new PdfPCell(new Paragraph(new Chunk("\nSous Total HT ", PDFFonts.H15))) { Padding = 5f, HorizontalAlignment = Element.ALIGN_LEFT, BorderWidthLeft = 0, BorderWidth = 0, BorderWidthBottom = 0.75f, BorderColorBottom = PDFColors.BorderBotom, VerticalAlignment = Element.ALIGN_BOTTOM });
                condition.AddCell(new PdfPCell(new Paragraph(new Chunk(string.Format("{0:0.00}", MontantSousHt) + " €", PDFFonts.H15))) { Padding = 5f, HorizontalAlignment = Element.ALIGN_RIGHT, BorderWidth = 0, BorderWidthBottom = 0.75f, BorderColorBottom = PDFColors.BorderBotom, VerticalAlignment = Element.ALIGN_BOTTOM });
                
                var symbol = orderDetails.GlobalDiscount.Type == Domain.Enums.TypeValue.Percentage ? "%" : "€";
                var txtMsg = "\nRemise globale " + (symbol == "%" ? "("+orderDetails.GlobalDiscount.Value.ToString()+symbol+")" : "");
                condition.AddCell(new PdfPCell(new Paragraph(new Chunk(txtMsg, PDFFonts.H15))) { Padding = 5f, HorizontalAlignment = Element.ALIGN_LEFT, BorderWidthLeft = 0, BorderWidth = 0, BorderWidthBottom = 0.75f, BorderColorBottom = PDFColors.BorderBotom, VerticalAlignment = Element.ALIGN_BOTTOM });
                condition.AddCell(new PdfPCell(new Paragraph(new Chunk(string.Format("{0:0.00}", orderDetails.GlobalDiscount.Calculate(totalHtSansRemise)) + " €", PDFFonts.H15))) { Padding = 5f, HorizontalAlignment = Element.ALIGN_RIGHT, BorderWidth = 0, BorderWidthBottom = 0.75f, BorderColorBottom = PDFColors.BorderBotom, VerticalAlignment = Element.ALIGN_BOTTOM });
                
            }

            condition.AddCell(new PdfPCell(new Paragraph(new Chunk("Montant HT", PDFFonts.H15)))
            { Padding = 5f, HorizontalAlignment = Element.ALIGN_LEFT, BorderWidthLeft = 0, BorderWidth = 0, BorderWidthBottom = 0.75f, BorderColorBottom = PDFColors.BorderBotom, VerticalAlignment = Element.ALIGN_BOTTOM });
            condition.AddCell(new PdfPCell(new Paragraph(new Chunk(string.Format("{0:0.00}", MontantHt) + " €", PDFFonts.H15)))
            { Padding = 5f, HorizontalAlignment = Element.ALIGN_RIGHT, BorderWidth = 0, BorderWidthBottom = 0.75f, BorderColorBottom = PDFColors.BorderBotom, VerticalAlignment = Element.ALIGN_BOTTOM });

            // double Total_ht = devis.TotalHt * (1 + (devis.Prorata / 100));
           

            if (orderDetails.Proportion != 0)
            {
                condition.AddCell(new PdfPCell(new Paragraph(new Chunk("\nMontant HT (prorata" + orderDetails.Proportion + "% inclus ):", PDFFonts.H15))) { Padding = 3f, HorizontalAlignment = Element.ALIGN_LEFT, BorderWidthLeft = 0, BorderWidth = 0, BorderWidthBottom = 0.75f, BorderColorBottom = PDFColors.BorderBotom, VerticalAlignment = Element.ALIGN_BOTTOM });
                condition.AddCell(new PdfPCell(new Paragraph(new Chunk(string.Format("{0:0.00}", MontantHt) + " €", PDFFonts.H15))) { Padding = 3f, HorizontalAlignment = Element.ALIGN_RIGHT, BorderWidth = 0, BorderWidthBottom = 0.75f, BorderColorBottom = PDFColors.BorderBotom, VerticalAlignment = Element.ALIGN_BOTTOM });
            }

           

            if (orderDetails.Proportion != 0)
            {
                condition.AddCell(new PdfPCell(new Paragraph(new Chunk("\nPart Prorata ", PDFFonts.H15))) { Padding = 5f, BorderWidthLeft = 0, BorderWidth = 0, BorderWidthBottom = 0.75f, BorderColorBottom = PDFColors.BorderBotom, VerticalAlignment = Element.ALIGN_BOTTOM });
                condition.AddCell(new PdfPCell(new Paragraph(new Chunk(string.Format("{0:0.00}", MontantHt - orderDetails.TotalHT) + " €", PDFFonts.H15))) { Padding = 5f, HorizontalAlignment = Element.ALIGN_RIGHT, BorderWidth = 0, BorderWidthBottom = 0.75f, BorderColorBottom = PDFColors.BorderBotom, VerticalAlignment = Element.ALIGN_BOTTOM });
            }

            foreach (var item in taxDetails)
            {
                condition.AddCell(new PdfPCell(new Paragraph(new Chunk("\nT.V.A " + string.Format("{0:0.00}%", item.Tax), PDFFonts.H15))) { Padding = 5f, BorderWidthLeft = 0, BorderWidth = 0, BorderWidthBottom = 0.75f, BorderColorBottom = PDFColors.BorderBotom, VerticalAlignment = Element.ALIGN_BOTTOM });
                condition.AddCell(new PdfPCell(new Paragraph(new Chunk(string.Format("{0:0.00}", item.TotalTax) + " €", PDFFonts.H15))) { Padding = 5f, HorizontalAlignment = Element.ALIGN_RIGHT, BorderWidth = 0, BorderWidthBottom = 0.75f, BorderColorBottom = PDFColors.BorderBotom, VerticalAlignment = Element.ALIGN_BOTTOM });
            }

            double MontanthtMontantht = (orderDetails.TotalHT * (orderDetails.PUC / 100));
            Phrase phrasep = new Phrase
            {
                new Chunk("\n PARTICIPATION PUC" + orderDetails.PUC + " % calculé sur", PDFFonts.H15),
                new Chunk("\n H.T(Prorata non compris) (H.T -" + orderDetails.PUC + "% * " + orderDetails.PUC / 100 + ")", PDFFonts.H15)
            };

            if (orderDetails.PUC != 0)
            {
                condition.AddCell(new PdfPCell(phrasep) { Padding = 3f, BorderWidthLeft = 0, BorderWidth = 0, BorderWidthBottom = 0.75f, BorderColorBottom = PDFColors.BorderBotom, VerticalAlignment = Element.ALIGN_BOTTOM });
                condition.AddCell(new PdfPCell(new Paragraph(new Chunk(string.Format("{0:0.00}", MontanthtMontantht) + " €", PDFFonts.H15))) { Padding = 5f, HorizontalAlignment = Element.ALIGN_RIGHT, BorderWidth = 0, BorderWidthBottom = 0.75f, BorderColorBottom = PDFColors.BorderBotom, VerticalAlignment = Element.ALIGN_BOTTOM });
            }

            condition.SpacingAfter = 10;
            condition.AddCell(new PdfPCell(new Paragraph(new Chunk("Total TTC", PDFFonts.H15Bold)))
            { Padding = 5f, BorderWidthLeft = 0, BorderWidth = 0, BorderColorBottom = PDFColors.BorderBotom, VerticalAlignment = Element.ALIGN_CENTER, PaddingTop = 12f });
            condition.AddCell(new PdfPCell(new Paragraph(new Chunk(string.Format("{0:0.00}",
                orderDetails.TotalTTC) + " €", PDFFonts.H15Bold))) { Padding = 3f, HorizontalAlignment = Element.ALIGN_RIGHT, BorderWidthLeft = 0,
                BorderWidth = 0, BorderColorBottom = PDFColors.BorderBotom, VerticalAlignment = Element.ALIGN_CENTER, PaddingTop = 12f });

            tabtva.AddCell(new PdfPCell(condition) { BorderWidth = 0, });
            return tabtva;
        }

        private void CreateOrderDetailsSection(iTextSharp.text.Document PDFDocument, string purpose, OrderDetails orderDetails , bool LotDetails)
        {
            PdfPTable prestations = new PdfPTable(1)
            {
                SpacingBefore = 10f,
                WidthPercentage = 100
            };
            prestations.DefaultCell.Border = Rectangle.NO_BORDER;
            prestations.AddCell(new PdfPCell() { FixedHeight = 10f, BorderWidth = 0 });

            PdfPTable articlestop = new PdfPTable(1)
            {
                WidthPercentage = 100
            };
            articlestop.DefaultCell.Border = Rectangle.NO_BORDER;
            if (purpose.IsValid())
            {

                Phrase phraseObjet = new Phrase();
                PdfPTable clientInfo = new PdfPTable(1);

                //   doc.Add(p);
                phraseObjet.Add(new Chunk("Objet : ", PDFFonts.H10B));
                phraseObjet.Add(new Chunk(purpose, PDFFonts.H15));

                articlestop.AddCell(new PdfPCell(new Paragraph(phraseObjet)) { BorderWidth = 0.75f, BorderColor = PDFColors.BorderBotom, Padding = 5f, BackgroundColor = PDFColors.BorderBotom, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_LEFT, Colspan = 2 });
                prestations.AddCell(new PdfPCell(articlestop) { BorderWidth = 0, PaddingBottom = 10 });
                PDFDocument.Add(prestations);
            }

            PdfPTable articlestab = new PdfPTable(12)
            { SpacingBefore = 10f, WidthPercentage = 100 };
            articlestab.DefaultCell.Border = Rectangle.NO_BORDER;

            // Header table            
            articlestab.AddCell(new PdfPCell(new Paragraph(new Chunk("Désignation", PDFFonts.H10B))) { FixedHeight = 20f, BorderWidth = 0, BackgroundColor = PDFColors.TableHeader, Padding = 5f, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 7 });            
            articlestab.AddCell(new PdfPCell(new Paragraph(new Chunk("Quantite", PDFFonts.H10B))) { FixedHeight = 20f, BorderWidth = 0, BackgroundColor = PDFColors.TableHeader, Padding = 5f, HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 1 });
            articlestab.AddCell(new PdfPCell(new Paragraph(new Chunk("Prix U.", PDFFonts.H10B))) { FixedHeight = 20f, BorderWidth = 0, BackgroundColor = PDFColors.TableHeader, Padding = 5f, HorizontalAlignment = Element.ALIGN_RIGHT, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 2 });
            articlestab.AddCell(new PdfPCell(new Paragraph(new Chunk("Prix HT", PDFFonts.H10B))) { FixedHeight = 20f, BorderWidth = 0, BackgroundColor = PDFColors.TableHeader, Padding = 5f, HorizontalAlignment = Element.ALIGN_RIGHT, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 2 });

            
            foreach (var productDetails in orderDetails.LineItems)
            {
                if (productDetails.Type == Domain.Enums.ProductType.Product)
                {
                    var product = (productDetails.Product as JObject).ToObject<MinimalProductDetails>();
                    PdfPTable ArticleDeisgnation = new PdfPTable(1);
                    string designation = product.Reference + "-" + product.Designation == "" ? product.Name : product.Designation;
                    ArticleDeisgnation.AddCell(new PdfPCell(new Paragraph(new Chunk(designation, PDFFonts.H15))) { BorderWidth = 0 });
                    ArticleDeisgnation.AddCell(new PdfPCell(new Paragraph(new Chunk(product.Description, PDFFonts.HItalic))) { BorderWidth = 0, PaddingLeft= 10f });
                    articlestab.AddCell(new PdfPCell(ArticleDeisgnation) { BorderWidthBottom = 0.3f, BorderWidthLeft = 0, BorderWidth = 0, Padding = 5f, BorderColorBottom = PDFColors.BorderBotom, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 7 });
                    articlestab.AddCell(new PdfPCell(new Paragraph(new Chunk(string.Format("{0}", productDetails.Quantity), PDFFonts.H15))) { BorderWidthBottom = 0.3f, BorderWidthLeft = 0, BorderWidth = 0, Padding = 5f, BorderColorBottom = PDFColors.BorderBotom, HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 1 });
                    articlestab.AddCell(new PdfPCell(new Paragraph(new Chunk(string.Format("{0:0.00}", product.TotalHT) + " €", PDFFonts.H15))) { BorderWidthBottom = 0.3f, BorderWidthLeft = 0, BorderWidth = 0, Padding = 5f, BorderColorBottom = PDFColors.BorderBotom, HorizontalAlignment = Element.ALIGN_RIGHT, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 2 });
                    articlestab.AddCell(new PdfPCell(new Paragraph(new Chunk(string.Format("{0:0.00}", product.TotalHT * productDetails.Quantity) + " €", PDFFonts.H15))) { BorderWidthBottom = 0.3f, BorderWidthLeft = 0, BorderWidth = 0, Padding = 5f, BorderColorBottom = PDFColors.BorderBotom, HorizontalAlignment = Element.ALIGN_RIGHT, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 2 });
                }

                if (productDetails.Type == Domain.Enums.ProductType.Lot && LotDetails)
                {
                    var Lot = (productDetails.Product as JObject).ToObject<MinimalLotDetails>();
                    articlestab.AddCell(new PdfPCell(new Paragraph(new Chunk(Lot.Name, PDFFonts.H15))) { BorderWidthBottom = 0.3f, BorderWidthLeft = 0, BorderWidth = 0, Padding = 5f, BackgroundColor = PDFColors.BorderBotom, BorderColorBottom = PDFColors.BorderBotom, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 3 });
                    articlestab.AddCell(new PdfPCell(new Paragraph(new Chunk("", PDFFonts.H15))) { BorderWidthBottom = 0.3f, BorderWidthLeft = 0, BorderWidth = 0, Padding = 5f, BackgroundColor = PDFColors.BorderBotom, BorderColorBottom = PDFColors.BorderBotom, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 3 });
                    articlestab.AddCell(new PdfPCell(new Paragraph(new Chunk("", PDFFonts.H15))) { BorderWidthBottom = 0.3f, BorderWidthLeft = 0, BorderWidth = 0, Padding = 5f, BackgroundColor = PDFColors.BorderBotom, BorderColorBottom = PDFColors.BorderBotom, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 2 });
                    articlestab.AddCell(new PdfPCell(new Paragraph(new Chunk("", PDFFonts.H15))) { BorderWidthBottom = 0.3f, BorderWidthLeft = 0, BorderWidth = 0, Padding = 5f, BackgroundColor = PDFColors.BorderBotom, BorderColorBottom = PDFColors.BorderBotom, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 1 });
                    articlestab.AddCell(new PdfPCell(new Paragraph(new Chunk(" Sous Total HT: " + string.Format("{0:0.00}", Lot.TotalHT), PDFFonts.HItalic))) { BorderWidthBottom = 0.3f, BorderWidthLeft = 0, BorderWidth = 0, Padding = 5f, BackgroundColor = PDFColors.BorderBotom, BorderColorBottom = PDFColors.BorderBotom, HorizontalAlignment = Element.ALIGN_RIGHT, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 3 });

                    foreach (var lotProductDetails in Lot.Products)
                    {
                        calculHtItem += lotProductDetails.ProductDetails.TotalHT;
                        PdfPTable ArticleDeisgnation = new PdfPTable(1);
                        string designation = lotProductDetails.ProductDetails.Reference + "-" + lotProductDetails.ProductDetails.Designation == "" ? lotProductDetails.ProductDetails.Name : lotProductDetails.ProductDetails.Designation;
                        ArticleDeisgnation.AddCell(new PdfPCell(new Paragraph(new Chunk("     " + designation, PDFFonts.H15))) { BorderWidth = 0 });
                        ArticleDeisgnation.AddCell(new PdfPCell(new Paragraph(new Chunk(lotProductDetails.ProductDetails.Description, PDFFonts.HItalic))) { BorderWidth = 0, PaddingLeft = 10f });
                        articlestab.AddCell(new PdfPCell(ArticleDeisgnation) { BorderWidthBottom = 0.3f, BorderWidthLeft = 0, BorderWidth = 0, Padding = 5f, BorderColorBottom = PDFColors.BorderBotom, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 7 });
                        articlestab.AddCell(new PdfPCell(new Paragraph(new Chunk(string.Format("{0}", lotProductDetails.Quantity), PDFFonts.H15))) { BorderWidthBottom = 0.3f, BorderWidthLeft = 0, BorderWidth = 0, Padding = 5f, BorderColorBottom = PDFColors.BorderBotom, HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 1 });
                        articlestab.AddCell(new PdfPCell(new Paragraph(new Chunk(string.Format("{0:0.00}", lotProductDetails.ProductDetails.TotalHT) + " €", PDFFonts.H15))) { BorderWidthBottom = 0.3f, BorderWidthLeft = 0, BorderWidth = 0, Padding = 5f, BorderColorBottom = PDFColors.BorderBotom, HorizontalAlignment = Element.ALIGN_RIGHT, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 2 });
                        articlestab.AddCell(new PdfPCell(new Paragraph(new Chunk(string.Format("{0:0.00}", lotProductDetails.ProductDetails.TotalHT * lotProductDetails.Quantity) + " €", PDFFonts.H15))) { BorderWidthBottom = 0.3f, BorderWidthLeft = 0, BorderWidth = 0, Padding = 5f, BorderColorBottom = PDFColors.BorderBotom, HorizontalAlignment = Element.ALIGN_RIGHT, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 2 });
                    }
                }

                if (productDetails.Type == Domain.Enums.ProductType.Lot && !LotDetails)
                {
                    var Lot = (productDetails.Product as JObject).ToObject<MinimalLotDetails>();
                    calculHtItem += Lot.TotalHT;
                    articlestab.AddCell(new PdfPCell(new Paragraph(new Chunk(Lot.Name, PDFFonts.H15))) { BorderWidthBottom = 0.3f, BorderWidthLeft = 0, BorderWidth = 0, Padding = 3f, BackgroundColor = PDFColors.BorderBotom, BorderColorBottom = PDFColors.BorderBotom, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 3 });
                    articlestab.AddCell(new PdfPCell(new Paragraph(new Chunk("", PDFFonts.H15))) { BorderWidthBottom = 0.3f, BorderWidthLeft = 0, BorderWidth = 0, Padding = 3f, BackgroundColor = PDFColors.BorderBotom, BorderColorBottom = PDFColors.BorderBotom, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 3 });
                    articlestab.AddCell(new PdfPCell(new Paragraph(new Chunk("", PDFFonts.H15))) { BorderWidthBottom = 0.3f, BorderWidthLeft = 0, BorderWidth = 0, Padding = 3f, BackgroundColor = PDFColors.BorderBotom, BorderColorBottom = PDFColors.BorderBotom, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 2 });
                    articlestab.AddCell(new PdfPCell(new Paragraph(new Chunk("", PDFFonts.H15))) { BorderWidthBottom = 0.3f, BorderWidthLeft = 0, BorderWidth = 0, Padding = 3f, BackgroundColor = PDFColors.BorderBotom, BorderColorBottom = PDFColors.BorderBotom, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 1 });
                    articlestab.AddCell(new PdfPCell(new Paragraph(new Chunk(" Sous Total HT: " + string.Format("{0:0.00}", Lot.TotalHT), PDFFonts.HItalic))) { BorderWidthBottom = 0.3f, BorderWidthLeft = 0, BorderWidth = 0, Padding = 3f, BackgroundColor = PDFColors.BorderBotom, BorderColorBottom = PDFColors.BorderBotom, HorizontalAlignment = Element.ALIGN_RIGHT, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 3 });
                }

               PDFDocument.Add(articlestab);
                articlestab.DeleteBodyRows();
            }
            if (orderDetails.LineItems.Count < 4)
            {
                for (int i = 0; i < 3; i++)
                {
                    articlestab.AddCell(new PdfPCell(new Paragraph(new Chunk("", PDFFonts.H15)))
                    {
                        BorderWidthBottom = 0,
                        BorderWidthLeft = 0,
                        BorderWidth = 0,
                        Padding = 5f,
                        HorizontalAlignment = Element.ALIGN_LEFT,
                        VerticalAlignment = Element.ALIGN_MIDDLE,
                        Colspan = 12
                    });

                    PDFDocument.Add(articlestab);
                    articlestab.DeleteBodyRows();
                }
            }
            else
            {
                articlestab.AddCell(new PdfPCell(new Paragraph(new Chunk("", PDFFonts.H15)))
                {
                    BorderWidthBottom = 0,
                    BorderWidthLeft = 0,
                    BorderWidth = 0,
                    Padding = 5f,
                    HorizontalAlignment = Element.ALIGN_LEFT,
                    VerticalAlignment = Element.ALIGN_MIDDLE,
                    Colspan = 12
                });

                PDFDocument.Add(articlestab);
                articlestab.DeleteBodyRows();
            }

            prestations.DeleteBodyRows();
        }

        private PdfPTable CreatePaymentConditionSection(string paymentCondition)
        {
            PdfPTable conditiontab = new PdfPTable(1)
            {
                SpacingBefore = 10f,
                WidthPercentage = 100,
                SpacingAfter = 10f,
            };
            if(paymentCondition != "")
            {
                conditiontab.AddCell(new PdfPCell(new Paragraph(new Chunk("Conditions de réglement :", PDFFonts.H10B)))
                { BorderWidth = 0, HorizontalAlignment = Element.ALIGN_LEFT, PaddingLeft = 0 });
            }
            Phrase phrase = new Phrase
            {
                CreateHTMLParagraph(paymentCondition)
            };
            phrase.Font = PDFFonts.H10B;

          
                       
            Paragraph paragraph = new Paragraph(phrase);
            paragraph.Font = PDFFonts.H10B;

            conditiontab.AddCell(new PdfPCell(paragraph) { BorderWidth = 0, HorizontalAlignment = Element.ALIGN_LEFT, PaddingLeft = 0 });
            return conditiontab;
        }

        private static IElement CreateFooterSection(string documentNote)
        {
            PdfPTable piedtab = new PdfPTable(1)
            {
                SpacingBefore = 5f,
                WidthPercentage = 100,
                SpacingAfter = 5f
            };
            if (documentNote != "")
            {
                piedtab.AddCell(new PdfPCell(new Paragraph(new Chunk("Notes :", PDFFonts.H10B)))
                { BorderWidth = 0, HorizontalAlignment = Element.ALIGN_LEFT, PaddingLeft = 0 });
            }

            
            Paragraph paragraph = new Paragraph(CreateHTMLParagraph(documentNote));
            paragraph.Font = PDFFonts.H15;

            piedtab.AddCell(new PdfPCell(paragraph) { BorderWidth = 0, HorizontalAlignment = Element.ALIGN_LEFT, PaddingLeft = 0 });

            //PdfPTable pdfTab = new PdfPTable(1);
            //pdfTab.AddCell(new PdfPCell(CreateHTMLParagraph(documentNote)) { BorderWidth = 0, HorizontalAlignment = Element.ALIGN_LEFT });
            //piedtab.AddCell(new PdfPCell(pdfTab) { BorderWidth = 0 });

            return piedtab;
        }

        private static IElement CreateDocumentAddressSection(Address AddressIntervention, Address defaultClientAddress)
        {
            PdfPTable table_header = new PdfPTable(2)
            {
                WidthPercentage = 100
            };

            // Left header
            PdfPTable leftRow = new PdfPTable(1);
            leftRow.DefaultCell.Border = Rectangle.NO_BORDER;


            leftRow.AddCell(new Paragraph("ADRESSE INTERVENTION : ", PDFFonts.H10B));
            leftRow.AddCell(new PdfPCell(new Paragraph(new Chunk(AddressIntervention != null ? AddressIntervention.Street  : " ", PDFFonts.H15))) {Padding= 1f, BorderWidth = 0, HorizontalAlignment = Element.ALIGN_LEFT });
            leftRow.AddCell(new PdfPCell(new Paragraph(new Chunk(AddressIntervention != null ? AddressIntervention.Complement : " ", PDFFonts.H15))) { Padding = 1f, BorderWidth = 0, HorizontalAlignment = Element.ALIGN_LEFT });
            leftRow.AddCell(new PdfPCell(new Paragraph(new Chunk($"{(AddressIntervention != null ? AddressIntervention.PostalCode : "")} {(AddressIntervention != null ? AddressIntervention.City : "")}", PDFFonts.H15))) { Padding = 1f, BorderWidth = 0, HorizontalAlignment = Element.ALIGN_LEFT });

            table_header.AddCell(new PdfPCell(leftRow) { BorderWidth = 0, PaddingTop = 10 });

            PdfPTable rightRow = new PdfPTable(1)
            {
                WidthPercentage = 80
            };

            rightRow.DefaultCell.Border = Rectangle.NO_BORDER;
            rightRow.AddCell(new PdfPCell(new Paragraph(new Chunk("ADRESSE FACTURATION:", PDFFonts.H10B))) { BorderWidth = 0 });

            rightRow.AddCell(new PdfPCell(new Paragraph(new Chunk(defaultClientAddress.Street, PDFFonts.H15))) { Padding = 1f, BorderWidth = 0 });
            rightRow.AddCell(new PdfPCell(new Paragraph(new Chunk(defaultClientAddress.Complement, PDFFonts.H15))) { Padding = 1f, BorderWidth = 0 });
            rightRow.AddCell(new PdfPCell(new Paragraph(new Chunk($"{defaultClientAddress.PostalCode} {defaultClientAddress.City}", PDFFonts.H15))) { Padding = 1f, BorderWidth = 0 });


            table_header.AddCell(new PdfPCell(rightRow) { BorderWidth = 0, PaddingTop = 10 });
            return table_header;
        }

        private static IElement CreateSignature(Quote quote)
        {

            PdfPTable signaturevisa = new PdfPTable(1) { SpacingBefore = 5f, WidthPercentage = 100 };
            signaturevisa.SpacingAfter = 5f;
            signaturevisa.DefaultCell.Border = Rectangle.NO_BORDER;
            PdfPTable visa = new PdfPTable(1) { WidthPercentage = 100 };
            visa.DefaultCell.Border = Rectangle.NO_BORDER;

            //// signature.SetWidths(new float[] { 350f, 70f, 550f });
            //visa.AddCell(new PdfPCell(new Paragraph(new Chunk("Visa", PDFFonts.H10)))
            //{ PaddingLeft = 50f, FixedHeight = 20f, BorderWidth = 0, BackgroundColor = PDFColors.TableHeader,
            //    HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_MIDDLE });
            //signaturevisa.AddCell(new PdfPCell(visa) { BorderWidth = 0, PaddingTop = 10 });

            PdfPTable signature = new PdfPTable(2) { WidthPercentage = 100 };
            signature.DefaultCell.Border = Rectangle.NO_BORDER;

            PdfPTable signature_technicien = new PdfPTable(1);
            signature_technicien.DefaultCell.Border = Rectangle.NO_BORDER;
            signature_technicien.WidthPercentage = 100;

            PdfPTable signature_client = new PdfPTable(1) { SpacingAfter = 1f }; ;

            // SignatureModel signetureClient= JsonConvert.DeserializeObject<SignatureModel>(ficheIntervention.SignatureClient);

            //Signature client


            Phrase LuApprouve = new Phrase();
            Phrase phrasesignatureClientsignature = new Phrase();

            Signature SignatureClient = null;

            if (quote.ClientSignature != null)
            {
                SignatureClient = quote.ClientSignature;

            }


            PdfPTable signatureClientTable = new PdfPTable(1);

            if (SignatureClient != null)
            {
                phrasesignatureClientsignature.Add(new Chunk("Signature client ", PDFFonts.H12Bold));
                if (!string.IsNullOrEmpty(SignatureClient.ImageContent))
                {
                    ConvertBase64ToImage convert = new ConvertBase64ToImage();
                    string content = convert.Replace(SignatureClient.ImageContent);
                    if (convert.IsBase64(content))
                    {
                        byte[] imageBytes = Convert.FromBase64String(content);
                        System.Drawing.Image result = convert.ByteArrayToImage(imageBytes);
                        Image Signatu = Image.GetInstance(result, null, false);
                        Signatu.ScaleAbsolute(160f, 160f);

                        signatureClientTable.AddCell(new PdfPCell(Signatu) { BorderWidth = Rectangle.NO_BORDER });
                        LuApprouve.Add(new Chunk("Lu et approuvé le " + SignatureClient.SignatureDate.ToString("dd/MM/yyyy"), PDFFonts.H12Bold));
                    }
                    else
                        signatureClientTable.AddCell(new PdfPCell(new Phrase(" ")) { BorderWidth = Rectangle.NO_BORDER });
                }
            }
            else
            {
                signatureClientTable.AddCell(new PdfPCell(new Phrase(" ")) { BorderWidth = Rectangle.NO_BORDER });
            }

            signature_client.AddCell(new PdfPCell(phrasesignatureClientsignature) { BorderWidth = 0, PaddingTop = 0, Padding = 5f, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_LEFT });
            signature_client.AddCell(new PdfPCell(signatureClientTable) { BorderWidth = 0, PaddingTop = 0, Padding = 5f, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_LEFT });
            signature_client.AddCell(new PdfPCell(LuApprouve) { BorderWidth = 0, PaddingTop = 0, Padding = 5f, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_LEFT });

            //signature_client.AddCell(new PdfPCell(signatureClientTable) { BorderWidth = 0.75f, PaddingTop = 0, Padding = 5f, BorderColor = PDFColors.BorderTop, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_LEFT });

            signature.AddCell(new PdfPCell(signature_client) { BorderWidth = 0, PaddingTop = 0 });

            signature.AddCell(new PdfPCell() { BorderWidth = 0 });
            signaturevisa.AddCell(new PdfPCell(signature) { BorderWidth = 0, PaddingTop = 5 });

            return signaturevisa;

        }
    

        private static IElement CreateHeaderSection(DocumentConfiguration configuration, string documentType, string documentReference, string clientReference,string nomClient, DateTime creationDate, DateTime dueDate, PdfOptions pdfOptions)
        {
           
            PdfPTable table_header = new PdfPTable(2)
            {
                WidthPercentage = 100
            };

            // Left header
            PdfPTable leftRow = new PdfPTable(1);
            leftRow.DefaultCell.Border = Rectangle.NO_BORDER;
            
            // logo
            ConvertBase64ToImage convert = new ConvertBase64ToImage();
            if (pdfOptions.Images.Logo != null && pdfOptions.Images.Logo != "")
            {
                string content = convert.Replace(pdfOptions.Images.Logo);


                var logo = Image.GetInstance(FileHelper.GetByteArray(content));

                logo.ScaleAbsolute(130f, 50f);
                leftRow.AddCell(new PdfPCell(logo) { FixedHeight = 50f, BorderWidth = 0, PaddingTop = 10, PaddingLeft = 0, HorizontalAlignment = Element.ALIGN_LEFT });

            }
            leftRow.AddCell(new PdfPCell() { FixedHeight = 7f, BorderWidth = 0 });
            PdfPTable leftRowseco = new PdfPTable(1);
            //info societe
            var Email = pdfOptions.Header.Email;
            var Address = pdfOptions.Header.Address;

            leftRow.AddCell(new PdfPCell(new Paragraph(new Chunk((pdfOptions.Header.CompanyName == null) ? "" : pdfOptions.Header.CompanyName, PDFFonts.H10B))) { BorderWidth = 0, HorizontalAlignment = Element.ALIGN_LEFT, PaddingRight = 80f, VerticalAlignment = Element.ALIGN_MIDDLE });


            if (Address != null)
            {
                leftRow.AddCell(new PdfPCell(new Paragraph((Address.Street == null) ? "" : Address.Street, PDFFonts.H10B)) { BorderWidth = 0, HorizontalAlignment = Element.ALIGN_LEFT, PaddingRight = 80f, VerticalAlignment = Element.ALIGN_MIDDLE });
                var ville = (Address.City == null) ? "" : Address.City;
                var PostalCode = (Address.PostalCode == null) ? "" : Address.PostalCode;

                leftRow.AddCell(new PdfPCell(new Paragraph(PostalCode + " " + ville, PDFFonts.H10B)) { BorderWidth = 0, HorizontalAlignment = Element.ALIGN_LEFT, PaddingRight = 80f, VerticalAlignment = Element.ALIGN_MIDDLE });

            }
            leftRow.AddCell(new PdfPCell(new Paragraph(new Chunk(string.IsNullOrEmpty(pdfOptions.Header.PhoneNumber) ? "" : "Tél: " + pdfOptions.Header.PhoneNumber, PDFFonts.H10B))) { BorderWidth = 0, HorizontalAlignment = Element.ALIGN_LEFT, PaddingRight = 80f, VerticalAlignment = Element.ALIGN_MIDDLE });
            leftRow.AddCell(new PdfPCell(new Paragraph(new Chunk(string.IsNullOrEmpty(pdfOptions.Header.Email) ? "" : "Email: " + pdfOptions.Header.Email, PDFFonts.H10B))) { BorderWidth = 0, HorizontalAlignment = Element.ALIGN_LEFT, PaddingRight = 80f, VerticalAlignment = Element.ALIGN_MIDDLE });


            leftRow.AddCell(new PdfPCell(leftRowseco) { BorderWidth = 0 });

            leftRow.AddCell(new PdfPCell() { FixedHeight = 5f, BorderWidth = 0 });
            table_header.AddCell(new PdfPCell(leftRow) { BorderWidth = 0, PaddingTop = 10 });

            // Right table
            PdfPTable rightRow = new PdfPTable(1)
            {
                WidthPercentage = 90,
            };

            rightRow.DefaultCell.Border = Rectangle.NO_BORDER;

            rightRow.AddCell(new PdfPCell() { FixedHeight = 30f, BorderWidth = 0 });

            PdfPTable infoDevis = new PdfPTable(2);
           
            //Reference
            infoDevis.AddCell(new PdfPCell(new Paragraph(new Chunk($"{documentType} {documentReference} ", PDFFonts.H10Bold2)))
            { BorderWidth = 0, Padding = 4f, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_MIDDLE , Colspan = 2});
           

            //Date Creation/echance
            Phrase phraseDateCreation = new Phrase();
            Phrase phraseDateExpiration = new Phrase();
            phraseDateCreation.Add(new Chunk($"Date création : \n{creationDate.ToString("dd/MM/yyyy")}", PDFFonts.H10Bold2));
            //phraseDateCreation.Add(new Chunk(creationDate.ToString("dd/MM/yyyy"), PDFFonts.H10));
            phraseDateExpiration.Add(new Chunk($"Date échéance :\n{dueDate.ToString("dd/MM/yyyy")}", PDFFonts.H10Bold2));
            //phraseDateExpiration.Add(new Chunk(dueDate.ToString("dd/MM/yyyy"), PDFFonts.H10));
            infoDevis.AddCell(new PdfPCell(phraseDateCreation) { BorderWidth = 0, Padding = 4f, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_LEFT });
            infoDevis.AddCell(new PdfPCell(phraseDateExpiration) { BorderWidth = 0, Padding = 4f, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_LEFT });

            // Client
            infoDevis.AddCell(new PdfPCell(new Paragraph(new Chunk($"Client : {nomClient}  ", PDFFonts.H10Bold2)))
            { BorderWidth = 0, Padding = 4f, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan=2 });
           
          

            PdfPTable infoClient = new PdfPTable(6);
            infoClient.AddCell(new PdfPCell(new Paragraph(new Chunk($"{documentType} n°  ", PDFFonts.H10B))) { BorderWidth = 0.75f, BorderColor = PDFColors.BorderBotom, Padding = 5f, BackgroundColor = PDFColors.TableHeader, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 2 });
            infoClient.AddCell(new PdfPCell(new Paragraph(new Chunk("Date", PDFFonts.H10B))) { BorderWidth = 0.75f, BorderColor = PDFColors.BorderBotom, Padding = 5f, BackgroundColor = PDFColors.TableHeader, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 2 });
            infoClient.AddCell(new PdfPCell(new Paragraph(new Chunk("Client", PDFFonts.H10B))) { BorderWidth = 0.75f, BorderColor = PDFColors.BorderBotom, Padding = 5f, BackgroundColor = PDFColors.TableHeader, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 2 });

            infoClient.AddCell(new PdfPCell(new Paragraph(new Chunk(documentReference, PDFFonts.H15))) { BorderWidth = 0.75f, BorderColor = PDFColors.BorderBotom, Padding = 5f, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 2 });

            //Phrase phraseDateCreation = new Phrase();
            //Phrase phraseDateExpiration = new Phrase();
            //PdfPTable tabDateDocument = new PdfPTable(1);

            ////   doc.Add(p);
            //phraseDateCreation.Add(new Chunk("Date création : ", PDFFonts.H10B));
            //phraseDateCreation.Add(new Chunk(creationDate.ToString("dd/MM/yyyy"), PDFFonts.H15));
            //phraseDateExpiration.Add(new Chunk("Date échéance : ", PDFFonts.H10B));
            //phraseDateExpiration.Add(new Chunk(dueDate.ToString("dd/MM/yyyy"), PDFFonts.H15));
            //tabDateDocument.AddCell(new PdfPCell(phraseDateCreation) { BorderWidth = 0, Padding = 5f, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_LEFT });
            //tabDateDocument.AddCell(new PdfPCell(phraseDateExpiration) { BorderWidth = 0, Padding = 5f, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_LEFT });
            //infoClient.AddCell(new PdfPCell(tabDateDocument) { BorderWidth = 0.75f, BorderColor = PDFColors.BorderBotom, Padding = 5f, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 2 });


            //infoClient.AddCell(new PdfPCell(new Paragraph(new Chunk(clientReference, PDFFonts.H15))) { BorderWidth = 0.75f, BorderColor = PDFColors.BorderBotom, Padding = 5f, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 2 });
            Phrase phraseClientnom = new Phrase();
            Phrase phraseclientReference = new Phrase();
            PdfPTable clientInfo = new PdfPTable(1);

            //   doc.Add(p);
            phraseClientnom.Add(new Chunk("Nom : ", PDFFonts.H10B));
            phraseClientnom.Add(new Chunk((nomClient == null) ? " " : nomClient, PDFFonts.H15));
            phraseclientReference.Add(new Chunk("Réference : ", PDFFonts.H10B));
            phraseclientReference.Add(new Chunk((clientReference == null) ? " " : clientReference, PDFFonts.H15));
            //clientInfo.AddCell(new PdfPCell(phraseClientnom) { BorderWidth = 0, Padding = 5f, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_LEFT });
            //clientInfo.AddCell(new PdfPCell(phraseclientReference) { BorderWidth = 0, Padding = 5f, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_LEFT });


            clientInfo.AddCell(new PdfPCell(phraseClientnom) { BorderWidth = 0, Padding = 5f, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_LEFT });
            clientInfo.AddCell(new PdfPCell(phraseclientReference) { BorderWidth = 0, Padding = 5f, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_LEFT });

            infoClient.AddCell(new PdfPCell(clientInfo) { BorderWidth = 0.75f, BorderColor = PDFColors.BorderBotom, Padding = 5f, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 2 });

            //rightRow.AddCell(new PdfPCell(infoClient) { BorderWidth = 0, PaddingTop = 0 });
            rightRow.AddCell(new PdfPCell(infoDevis) { BorderWidth = 0, PaddingTop = 0 });


            rightRow.AddCell(new PdfPCell() { FixedHeight = 5f, BorderWidth = 0 });
            table_header.AddCell(new PdfPCell(rightRow) { BorderWidth = 0, PaddingTop = 0 });

            return table_header;
        }

        private static Paragraph CreateHTMLParagraph(string text)
        {
            Paragraph p = new Paragraph();
            using (StringReader sr = new StringReader(text))
            {
                foreach (IElement e in HtmlWorker.ParseToList(sr, null))
                {
                    p.Add(e);
                }
            }
            return p;
        }
        public partial class Footer : PdfPageEventHelper
        {
            private PdfOptions _pdfOptions;

            public Footer(PdfOptions pdfOptions)
            {
                _pdfOptions = pdfOptions;
            }

            public override void OnEndPage(PdfWriter wri, iTextSharp.text.Document doc)
            {

                PdfTemplate templateNumPage;

                templateNumPage = wri.DirectContent.CreateTemplate(30, 60);


                base.OnEndPage(wri, doc);
                BaseColor borderTopColor = new BaseColor(224, 224, 224);


                Font fontH13 = FontFactory.GetFont("Arial", 14, Font.NORMAL, new BaseColor(161, 165, 169));
                Font fontH14 = FontFactory.GetFont("Arial", 7, Font.NORMAL, BaseColor.Black);
                int courantPageNumber = wri.CurrentPageNumber;
                String pageText = "Page " + courantPageNumber.ToString();
                PdfPTable footerTab = new PdfPTable(3);
                PdfPTable pdfTab = new PdfPTable(1);
                pdfTab.HorizontalAlignment = Element.ALIGN_BOTTOM;
                pdfTab.TotalWidth = 550;
                pdfTab.AddCell(new PdfPCell() { BorderWidth = 0, FixedHeight = 5f });

                PdfPTable footerContenu = new PdfPTable(2);
                footerContenu.SetWidths(new float[] { 65f, 35f });
                footerContenu.AddCell(new PdfPCell(new Paragraph()) { BorderWidth = 0, PaddingLeft = 15, PaddingTop = 0, HorizontalAlignment = Element.ALIGN_LEFT });
                footerContenu.AddCell(new PdfPCell(new Paragraph(pageText, fontH14)) { BorderWidth = 0, HorizontalAlignment = Element.ALIGN_RIGHT, VerticalAlignment = Element.ALIGN_BOTTOM, PaddingRight = 15 });

                pdfTab.AddCell(new PdfPCell(footerContenu) { BorderWidth = 0 });
                pdfTab.AddCell(new PdfPCell() { FixedHeight = 5f, BorderWidth = 0 });
                PdfPTable footerPied = new PdfPTable(1);
                footerPied.AddCell(new PdfPCell() { FixedHeight = 1f, Padding = 1f, BorderWidthBottom = 0, BorderWidthTop = 0.5f, BorderWidth = 0, BorderColorTop = borderTopColor, PaddingTop = 2 });

                pdfTab.AddCell(new PdfPCell(footerPied) { BorderWidth = 0, PaddingLeft = 40, PaddingRight = 40 });

                pdfTab.AddCell(new PdfPCell(new Paragraph((_pdfOptions.Footer.Line1 == null) ? "" : _pdfOptions.Footer.Line1, PDFFonts.H15)) { BorderWidth = 0, HorizontalAlignment = Element.ALIGN_CENTER });
                pdfTab.AddCell(new PdfPCell(new Paragraph((_pdfOptions.Footer.Line2 == null) ? "" : _pdfOptions.Footer.Line2, PDFFonts.H15)) { BorderWidth = 0, HorizontalAlignment = Element.ALIGN_CENTER });
                pdfTab.AddCell(new PdfPCell(new Paragraph((_pdfOptions.Footer.Line3 == null) ? "" : _pdfOptions.Footer.Line3, PDFFonts.H15)) { BorderWidth = 0, HorizontalAlignment = Element.ALIGN_CENTER });

                pdfTab.WriteSelectedRows(0, -2, 18, 70, wri.DirectContent);


                footerTab.AddCell(new PdfPCell(pdfTab) { BorderWidth = 0 });
            }
        }
    }

}
