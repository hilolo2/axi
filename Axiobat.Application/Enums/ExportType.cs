﻿namespace Axiobat.Application.Enums
{
    /// <summary>
    /// the data export type
    /// </summary>
    public enum ExportType
    {
        /// <summary>
        /// export the data as Excel
        /// </summary>
        Excel,

        /// <summary>
        /// export the data as CSV
        /// </summary>
        CSV,

        /// <summary>
        /// export the data as XML
        /// </summary>
        XML,

        /// <summary>
        /// export data as a pdf
        /// </summary>
        PDF,
    }
}
