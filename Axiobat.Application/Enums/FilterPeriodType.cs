﻿namespace Axiobat.Application.Enums
{
    /// <summary>
    /// the date filter period type
    /// </summary>
    public enum FilterPeriodType
    {
        /// <summary>
        /// no date filter is applied
        /// </summary>
        None = 0,

        /// <summary>
        /// the period is defined as a date interval
        /// </summary>
        Interval,

        /// <summary>
        /// tack the current month as an interval
        /// </summary>
        CurrentMonth,

        /// <summary>
        /// tack the last month as an interval
        /// </summary>
        LastMonth,

        /// <summary>
        /// tack the last three month as an interval
        /// </summary>
        LastThreeMonths,

        /// <summary>
        /// tack the last six month as an interval
        /// </summary>
        LastSixMonths,

        /// <summary>
        /// tack the current year as an interval
        /// </summary>
        CurrentYear,

        /// <summary>
        /// tack the last year as an interval
        /// </summary>
        LastYear,

        /// <summary>
        /// tack the current accounting period as an interval
        /// </summary>
        CurrentAccountingPeriod,
    }
}
