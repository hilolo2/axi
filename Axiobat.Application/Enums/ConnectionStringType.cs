﻿namespace Axiobat.Application.Enums
{
    /// <summary>
    /// the type of the connection string
    /// </summary>
    public enum ConnectionStringType
    {
        ApplicationDb,
    }
}
