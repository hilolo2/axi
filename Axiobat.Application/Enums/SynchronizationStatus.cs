﻿namespace Axiobat.Application.Enums
{
    /// <summary>
    /// type of the Synchronization
    /// </summary>
    public enum SynchronizationStatus
    {
        /// <summary>
        /// the object has been added
        /// </summary>
        Added,

        /// <summary>
        /// the object has been updated
        /// </summary>
        Updated
    }
}
