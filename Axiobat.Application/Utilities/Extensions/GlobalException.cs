﻿namespace Axiobat
{
    using System.Linq;
    using System.Text.RegularExpressions;

    public static class GlobalException
    {
        public static string[] FindAllMatches(this string value, string pattern)
            => Regex.Matches(value, pattern)
                    .Cast<Match>()
                    .Select(m => m.Value)
                    .Distinct()
                    .ToArray();
    }
}
