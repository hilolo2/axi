﻿namespace Axiobat.Application.Data
{
    using Axiobat.Application.Models;
    using Domain.Entities;
    using System.Threading.Tasks;

    /// <summary>
    /// the data access for <see cref="ConstructionWorkshop"/>
    /// </summary>
    public interface IConstructionWorkshopDataAccess : IDataAccess<ConstructionWorkshop, string>
    {
        /// <summary>
        /// get the client of the workshop
        /// </summary>
        /// <param name="workshopId">the id of the workshop</param>
        /// <returns>the client instant</returns>
        Task<Client> GetWorkshopClientAsync(string workshopId);

        /// <summary>
        /// get the workshop with the given id for financial summary report
        /// </summary>
        /// <param name="constructionWorkshopId">the id of the construction workshop</param>
        /// <returns>the workshop model</returns>
        Task<ConstructionWorkshop> GetByIdForFinancialSummaryAsync(string constructionWorkshopId);

        /// <summary>
        /// retrieve the workshop documents counts informations
        /// </summary>
        /// <param name="constructionWorkshopId">the construction workshop id</param>
        /// <returns>the workshop documents count</returns>
        Task<WorkshopDocumentsCount> GetWorkshopDocumentsCountAsync(string constructionWorkshopId);
        Task<int> GetQuotesDocumentsCountAsync(string workshopId);
        Task<int> GetInvoiceDocumentsCountAsync(string workshopId);
        Task<int> GetSupplierOrderDocumentsCountAsync(string workshopId);
    }
}
