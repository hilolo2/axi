﻿namespace Axiobat.Application.Data
{
    using Domain.Entities;

    /// <summary>
    /// the dataAccess for <see cref="CreditNote"/>
    /// </summary>
    public interface ICreditNoteDataAccess : IDocumentDataAccess<CreditNote>
    {

    }
}
