﻿namespace Axiobat.Application.Data
{
    using Application.Models;
    using Domain.Entities;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    /// <summary>
    /// the analytics data access
    /// </summary>
    public interface IAnalyticsDataAccess
    {
        Task<IEnumerable<ConstructionWorkshop>> GetWorkshopForAnalyticsAsync(WorkshopAnalyticsFilterOtions filterModel);
        Task<IEnumerable<Invoice>> GetInvoicesForAnalyticsAsync(ExternalPartnerFilterOptions filterModel);
        Task<IEnumerable<CreditNote>> GetCreditNotesForAnalyticsAsync(ExternalPartnerFilterOptions filterModel);
        Task<IEnumerable<Expense>> GetExpensesForAnalyticsAsync(ExternalPartnerFilterOptions filterModel);
        Task<IEnumerable<Expense>> GetExpensesForPurchaseAnalyticsAsync(TransactionAnalyticsFilterOptions filterModel);
        Task<IEnumerable<Quote>> getQuotesForAnalyticsAsync(TransactionAnalyticsFilterOptions filterModel);

        Task<IEnumerable<MaintenanceContract>> GetMaintenanceContractsForAnalyticsAsync(MaintenanceAnalyticsFilterOptions filter);
    }
}
