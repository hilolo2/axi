﻿namespace Axiobat.Application.Data
{
    using Domain.Entities;
    using Microsoft.AspNetCore.Identity;
    using System;
    using System.Threading.Tasks;

    /// <summary>
    /// an interface that defines the data access layer for the <see cref="Role"/> entity
    /// </summary>
    public interface IRoleDataAccess : IDataAccess<Role, Guid>, IRoleStore<Role>
    {
        /// <summary>
        /// get the role of the user
        /// </summary>
        /// <param name="userId">the id of the user</param>
        /// <returns>user role</returns>
        Task<Role> GetUserRoleAsync(Guid userId);
    }
}
