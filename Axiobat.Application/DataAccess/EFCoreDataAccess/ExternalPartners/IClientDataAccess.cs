﻿namespace Axiobat.Application.Data
{
    using Domain.Entities;

    /// <summary>
    /// the data access for <see cref="Client"/> entity
    /// </summary>
    public interface IClientDataAccess : IExternalPartnersDataAccess<Client>
    {

    }
}
