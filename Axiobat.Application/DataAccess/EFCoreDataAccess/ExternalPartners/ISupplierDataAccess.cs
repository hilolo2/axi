﻿namespace Axiobat.Application.Data
{
    using Domain.Entities;
    using System.Threading.Tasks;

    /// <summary>
    /// /the data access for <see cref="Supplier"/> entity
    /// </summary>
    public interface ISupplierDataAccess : IExternalPartnersDataAccess<Supplier>
    {

    }
}
