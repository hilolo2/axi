﻿namespace Axiobat.Application.Models
{
    using Domain.Entities;
    using System;

    /// <summary>
    /// this class is used to describe the email confirmation requirements
    /// </summary>
    [ModelFor(typeof(User))]
    public class UserConfirmEmailModel
    {
        /// <summary>
        /// the id of the user
        /// </summary>
        public Guid UserId { get; set; }

        /// <summary>
        /// the email confirmation token
        /// </summary>
        public string Token { get; set; }
    }
}
