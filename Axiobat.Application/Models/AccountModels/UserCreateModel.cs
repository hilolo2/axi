﻿namespace Axiobat.Application.Models
{
    using Domain.Entities;
    using Domain.Enums;
    using System;

    /// <summary>
    /// a class that describe the User Creation Requirement
    /// </summary>
    [ModelFor(typeof(User))]
    public class UserCreateModel
    {
        /// <summary>
        /// Last name of the user
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// first name of the user
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// the registration number of the user
        /// </summary>
        public string Reference { get; set; }

        /// <summary>
        /// the password for the user
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// the user UserName used for login, if is null the email value will be taken
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// the status of the user : Active or Not
        /// </summary>
        public Status Statut { get; set; }

        /// <summary>
        /// the email of the user
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets a telephone number for the user.
        /// </summary>
        public string PhoneNumber { get; set; }

        /// <summary>
        /// the id of the role of this user
        /// </summary>
        public Guid RoleId { get; set; }
    }
}
