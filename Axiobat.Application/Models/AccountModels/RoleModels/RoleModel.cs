﻿namespace Axiobat.Application.Models
{
    using Domain.Enums;
    using Domain.Entities;
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// a class that defines a model for <see cref="Role"/>
    /// </summary>
    [ModelFor(typeof(Role))]
    public class RoleModel : IModel<Role, Guid>
    {
        /// <summary>
        /// default constructor
        /// </summary>
        public RoleModel()
        {
            Permissions = new HashSet<PermissionModel>();
        }

        /// <summary>
        /// the id of the role
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// the name/label of the role
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// type of the role
        /// </summary>
        public RoleType Type { get; set; }

        /// <summary>
        /// list of rights
        /// </summary>
        public IEnumerable<PermissionModel> Permissions { get; set; }
    }
}
