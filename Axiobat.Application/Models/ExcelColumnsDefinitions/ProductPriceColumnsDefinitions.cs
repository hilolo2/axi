﻿namespace Axiobat.Application.Models.DataImport
{
    public class ProductPriceColumnsDefinitions
    {
        public int ProductReference { get; set; }
        public int SupplierReference { get; set; }
        public int Price { get; set; }
        public int IsDefault { get; set; }
    }
}