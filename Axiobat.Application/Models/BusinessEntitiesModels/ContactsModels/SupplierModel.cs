﻿namespace Axiobat.Application.Models
{
    using Domain.Entities;

    /// <summary>
    /// the <see cref="Supplier"/> Model
    /// </summary>
    [ModelFor(typeof(Supplier))]
    public partial class SupplierModel
    {
        /// <summary>
        /// the address of the supplier
        /// </summary>
        /// <remarks>
        /// this is a JSON Object
        /// </remarks>
        public Address Address { get; set; }
    }

    /// <summary>
    /// the partial part for <see cref="SupplierModel"/>
    /// </summary>
    public partial class SupplierModel : ExternalPartnerModel<Supplier>
    {

    }
}
