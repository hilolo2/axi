﻿namespace Axiobat.Application.Models
{
    using Axiobat.Domain.Constants;
    using Domain.Entities;
    using System.Collections.Generic;

    /// <summary>
    /// the creation/update model used for setting the client information
    /// </summary>
    [ModelFor(typeof(Client))]
    public partial class ClientPutModel : ExternalPartnerPutModel<Client>
    {
        /// <summary>
        /// the client code
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// the id of the groupe that this client belongs to it
        /// </summary>
        public int? GroupeId { get; set; }

        /// <summary>
        /// the client prenom
        /// </summary>
        public string Prenom { get; set; }

        /// <summary>
        /// the client type
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// list of Addresses of the client
        /// </summary>
        public ICollection<Address> Addresses { get; set; }

        /// <summary>
        /// list of Materials of the client
        /// </summary>
        public ICollection<Material> Materials { get; set; }


        public override void Update(Client entity)
        {
            base.Update(entity);
            entity.Code = Code;
            entity.GroupeId = GroupeId;
            entity.Addresses = Addresses;
            entity.Materials = Materials;
            entity.Type = Type;

            if (Type == ClientType.Professional)
            {
                entity.LastName = "";
            }
        }
    }
}
