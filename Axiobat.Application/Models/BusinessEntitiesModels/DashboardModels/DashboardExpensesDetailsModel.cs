﻿namespace Axiobat.Application.Models
{
    using System.Collections.Generic;

    /// <summary>
    /// this class defines the model of the dashboard expenses details
    /// </summary>
    public class DashboardExpensesDetailsModel
    {
        public DashboardExpensesDetailsModel()
        {
            CurrentPeriod = new LinkedList<PerMonthTotalDetails>();
            PreCurrentPeriod = new LinkedList<PerMonthTotalDetails>();
        }

        /// <summary>
        /// the total Per Month for the current period
        /// </summary>
        public ICollection<PerMonthTotalDetails> CurrentPeriod { get; set; }

        /// <summary>
        /// the total Per Month for the PreCurrent period
        /// </summary>
        public ICollection<PerMonthTotalDetails> PreCurrentPeriod { get; set; }
    }
}
