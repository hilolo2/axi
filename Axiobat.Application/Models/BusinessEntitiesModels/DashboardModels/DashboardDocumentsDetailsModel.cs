﻿namespace Axiobat.Application.Models
{
    /// <summary>
    /// this class defines the model of the dashboard document details
    /// </summary>
    public class DashboardDocumentsDetailsModel
    {
        /// <summary>
        /// the quotes details
        /// </summary>
        public QuoteAnalyticsResult QuotesDetails { get; }

        /// <summary>
        /// the invoice details
        /// </summary>
        public DocumentAnalyticsResult InvoicesDetails { get; }

        /// <summary>
        /// the workshop details
        /// </summary>
        public WorkshopDocumentAnalyticsResult WorkshopDetails { get; }

        /// <summary>
        /// the contract details
        /// </summary>
        public DocumentAnalyticsResult ContractDetails { get; }

        /// <summary>
        /// the operation sheets details
        /// </summary>
        public DocumentAnalyticsResult OperationSheetDetails { get; }

        public DashboardDocumentsDetailsModel(
            QuoteAnalyticsResult quotesDetails,
            DocumentAnalyticsResult invoicesDetails,
            WorkshopDocumentAnalyticsResult workshopDetails,
            DocumentAnalyticsResult contractDetails,
            DocumentAnalyticsResult operationSheetDetails)
        {
            QuotesDetails = quotesDetails;
            InvoicesDetails = invoicesDetails;
            WorkshopDetails = workshopDetails;
            ContractDetails = contractDetails;
            OperationSheetDetails = operationSheetDetails;
        }
    }

    public class WorkshopDocumentAnalyticsResult : DocumentAnalyticsResult
    {
        public WorkshopAnalytics MarginDetails { get; set; }
    }
}
