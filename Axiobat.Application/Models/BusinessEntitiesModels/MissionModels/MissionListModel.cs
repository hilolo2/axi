﻿namespace Axiobat.Application.Models
{
    using Domain.Entities;
    using System;

    [ModelFor(typeof(Mission))]
    public class MissionListModel : IModel<Mission>
    {
        /// <summary>
        ///  Id list of the  technician service
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// the kind of the mission one of <see cref="Constants.MissionKind"/>
        /// </summary>
        public string MissionKind { get; set; }

        /// <summary>
        /// the type of the mission
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// the title of the of the task
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Object of the task
        public string Object { get; set; }

        /// <summary>
        /// Type the status of the task
        /// </summary>
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// the duration of the mission, saved as HH:MM
        /// </summary>
        public string Duration { get; set; }

        /// <summary>
        /// the Technician worked all day
        /// </summary>
        public bool AllDayLong { get; set; }

        /// <summary>
        /// the starting date of the task
        /// </summary>
        public DateTime StartingDate { get; set; }

        /// <summary>
        /// the end date of the task
        /// </summary>
        public DateTime? EndingDate { get; set; }

        /// <summary>
        /// name of the client
        /// </summary>
        public string ClientName { get; set; }

        /// <summary>
        /// name of the Technician
        /// </summary>
        public string TechnicianName { get; set; }

        /// <summary>
        /// the workshop associated with this mission
        /// </summary>
        public ConstructionWorkshopMinimalModel Workshop { get; set; }
    }
}
