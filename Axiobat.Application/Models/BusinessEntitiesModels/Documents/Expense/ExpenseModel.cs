﻿namespace Axiobat.Application.Models
{
    using Domain.Entities;
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// the model for <see cref="Expense"/>
    /// </summary>
    [ModelFor(typeof(Expense))]
    public partial class ExpenseModel : DocumentModel<Expense>
    {
        /// <summary>
        /// create an instant of <see cref="ExpenseModel"/>
        /// </summary>
        public ExpenseModel()
        {
            Memos = new HashSet<Memo>();
        }

        /// <summary>
        /// the date the expense should be payed
        /// </summary>
        public DateTime DueDate { get; set; }

        /// <summary>
        /// Expense creation date
        /// </summary>
        public DateTime CreationDate { get; set; }

        /// <summary>
        /// a cancellation document in case this expense has been canceled
        /// </summary>
        public Memo CancellationDocument { get; set; }

        /// <summary>
        /// the order details
        /// </summary>
        public OrderDetails OrderDetails { get; set; }

        /// <summary>
        /// the Classification associated with the expense
        /// </summary>
        public ClassificationModel Category { get; set; }

        /// <summary>
        /// the supplier associated with this Expense
        /// </summary>
        public SupplierDocuement Supplier { get; set; }

        /// <summary>
        /// the Data sheets associated with the product
        /// </summary>
        public ICollection<Memo> Memos { get; set; }

        /// <summary>
        /// list of payments associated with the expense
        /// </summary>
        public ICollection<PaymentModel> Payments { get; set; }

        /// <summary>
        /// list of attachments
        /// </summary>
        public ICollection<Attachment> Attachments { get; set; }

        /// <summary>
        /// a boolean check if you can cancel this invoice
        /// </summary>
        public bool CanCancel { get; set; }

        /// <summary>
        /// a boolean check if you can delete this invoice
        /// </summary>
        public bool CanDelete { get; set; }
        /// <summary>
        /// a boolean check if you can delete this invoice
        /// </summary>
        public bool CanUpdate { get; set; }
    }
}
