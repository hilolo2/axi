﻿namespace Axiobat.Application.Models
{
    using Domain.Enums;
    using Domain.Entities;
    using System;

    /// <summary>
    /// a minimal document information
    /// </summary>
    [ModelFor(typeof(Document))]
    public partial class MinimalDocumentModel
    {
        /// <summary>
        /// the type of the document
        /// </summary>
        public DocumentType DocumentType { get; set; }

        /// <summary>
        /// the id of the entity
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// the document reference
        /// </summary>
        public string Reference { get; set; }

        /// <summary>
        /// status of the quote
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// the total HT
        /// </summary>
        public float TotalHT { get; set; }

        /// <summary>
        /// the total TTC
        /// </summary>
        public float TotalTTC { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime DueDate { get; set; }
    }
}
