﻿namespace Axiobat.Application.Models
{
    using Domain.Entities;

    /// <summary>
    /// the update/create model
    /// </summary>
    /// <typeparam name="TEntity">the entity type</typeparam>
    [ModelFor(typeof(Document))]
    public partial class DocumentPutModel<TEntity>
    {
        /// <summary>
        /// the document reference
        /// </summary>
        public string Reference { get; set; }

        /// <summary>
        /// status of the quote
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// a note about the quote
        /// </summary>
        public string Note { get; set; }

        /// <summary>
        /// the purpose of the quote
        /// </summary>
        public string Purpose { get; set; }

        /// <summary>
        /// the conditions of the payment
        /// </summary>
        public string PaymentCondition { get; set; }

        /// <summary>
        /// the id of the workshop
        /// </summary>
        public string WorkshopId { get; set; }
    }

    /// <summary>
    /// partial part for <see cref="DocumentPutModel{TEntity}"/>
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    public partial class DocumentPutModel<TEntity> : IUpdateModel<TEntity>
        where TEntity : Document
    {
        /// <summary>
        /// update the entity form is model
        /// </summary>
        /// <param name="entity">the entity instant</param>
        public virtual void Update(TEntity entity)
        {
            entity.Note = Note;
            entity.Status = Status;
            entity.Purpose = Purpose;
            entity.Reference = Reference;
            entity.WorkshopId = WorkshopId;
            entity.PaymentCondition = PaymentCondition;
        }
    }
}
