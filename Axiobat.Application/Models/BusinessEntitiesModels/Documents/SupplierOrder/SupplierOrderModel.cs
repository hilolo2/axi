﻿namespace Axiobat.Application.Models
{
    using Domain.Entities;
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// the model for <see cref="SupplierOrder"/>
    /// </summary>
    [ModelFor(typeof(SupplierOrder))]
    public partial class SupplierOrderModel : DocumentModel<SupplierOrder>
    {
        /// <summary>
        /// the date the Supplier Order should be payed or has been payed
        /// </summary>
        public DateTime DueDate { get; set; }

        /// <summary>
        /// Supplier Order creation date
        /// </summary>
        public DateTime CreationDate { get; set; }

        /// <summary>
        /// the supplier associated with this supplier order
        /// </summary>
        public SupplierDocuement Supplier { get; set; }

        /// <summary>
        /// the devis order details
        /// </summary>
        public OrderDetails OrderDetails { get; set; }

        /// <summary>
        /// list of memos associated with the invoice
        /// </summary>
        public ICollection<Memo> Memos { get; set; }

        /// <summary>
        /// list of expenses
        /// </summary>
        public ICollection<ExpenseModel> Expenses { get; set; }
    }
}
