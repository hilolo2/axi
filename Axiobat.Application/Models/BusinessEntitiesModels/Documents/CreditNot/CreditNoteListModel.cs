﻿namespace Axiobat.Application.Models
{
    using Domain.Entities;
    using System;

    /// <summary>
    /// the list model for <see cref="CreditNote"/>
    /// </summary>
    [ModelFor(typeof(CreditNote))]
    public partial class CreditNoteListModel
    {
        /// <summary>
        /// the id of the entity
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// the document reference
        /// </summary>
        public string Reference { get; set; }

        /// <summary>
        /// status of the CreditNote
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// the client associated with this CreditNote
        /// </summary>
        public string Client { get; set; }

        /// <summary>
        /// the date the CreditNote should be payed or has been payed
        /// </summary>
        public DateTime DueDate { get; set; }

        /// <summary>
        /// Credit Note creation date
        /// </summary>
        public DateTime CreationDate { get; set; }

        /// <summary>
        /// the name of the workshop this credit note is associated with it
        /// </summary>
        public string Workshop { get; set; }

        /// <summary>
        /// the total HT of the Credit Note
        /// </summary>
        public float TotalHT { get; set; }

        /// <summary>
        /// the total TTC of the Credit Note
        /// </summary>
        public float TotalTTC { get; set; }
    }
}
