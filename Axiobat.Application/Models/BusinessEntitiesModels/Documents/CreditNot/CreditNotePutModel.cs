﻿namespace Axiobat.Application.Models
{
    using App.Common;
    using Domain.Entities;
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// the model for <see cref="Quote"/>
    /// </summary>
    [ModelFor(typeof(CreditNote))]
    public partial class CreditNotePutModel : DocumentPutModel<CreditNote>
    {
        /// <summary>
        /// the date the invoice should be payed or has been payed
        /// </summary>
        public DateTime DueDate { get; set; }

        /// <summary>
        /// Credit Note creation date
        /// </summary>
        public DateTime CreationDate { get; set; }

        /// <summary>
        /// the id of the client
        /// </summary>
        [PropertyName("Client")]
        [PropertyValue("Client.FullName")]
        public string ClientId { get; set; }

        /// <summary>
        /// the client
        /// </summary>
        public ClientDocument Client { get; set; }

        /// <summary>
        /// the invoice order details
        /// </summary>
        public WorkshopOrderDetails OrderDetails { get; set; }
    }

    /// <summary>
    /// partial part for <see cref="CreditNotePutModel"/>
    /// </summary>
    public partial class CreditNotePutModel : DocumentPutModel<CreditNote>
    {
        /// <summary>
        /// update the entity from the current model
        /// </summary>
        /// <param name="entity">the entity instant</param>
        public override void Update(CreditNote entity)
        {
            base.Update(entity);
            entity.DueDate = DueDate;
            entity.ClientId = ClientId;
            entity.Client = Client;
            entity.OrderDetails = OrderDetails;
            entity.Workshop = null;
        }
    }
}
