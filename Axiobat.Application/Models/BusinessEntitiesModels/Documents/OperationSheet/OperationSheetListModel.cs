﻿namespace Axiobat.Application.Models
{
    using Axiobat.Domain.Enums;
    using Domain.Entities;
    using System;
    using System.Collections.Generic;

    public partial class CombinedOperationSheetListModel
    {
        /// <summary>
        /// the id of the entity
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// the document reference
        /// </summary>
        public string Reference { get; set; }

        /// <summary>
        /// the starting date of the operation
        /// </summary>
        public DateTime StartDate { get; set; }

        /// <summary>
        /// the ending date of the operation
        /// </summary>
        public DateTime EndDate { get; set; }

        /// <summary>
        /// the Statut of the Operation Sheet
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// the type of the Maintenance OperationSheet
        /// </summary>
        public MaintenanceOperationSheetType Type { get; set; }

        /// <summary>
        /// a boolean check if you can delete this operation Sheet
        /// </summary>
        public bool CanDelete { get; set; }

        /// <summary>
        /// a boolean check if you can delete this operation Sheet
        /// </summary>
        public bool CanUpdate { get; set; }

        public string Workshop { get; set; }
        public object Client { get; set; }

    }

    /// <summary>
    /// list model for <see cref="OperationSheet"/>
    /// </summary>
    [ModelFor(typeof(OperationSheet))]
    public partial class OperationSheetListModel
    {
        /// <summary>
        /// the id of the entity
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// the document reference
        /// </summary>
        public string Reference { get; set; }

        /// <summary>
        /// the starting date of the operation
        /// </summary>
        public DateTime StartDate { get; set; }

        /// <summary>
        /// the ending date of the operation
        /// </summary>
        public DateTime EndDate { get; set; }

        /// <summary>
        /// the workshop associated with this operation sheet
        /// </summary>
        public string Workshop { get; set; }

        /// <summaryglo
        /// the client associated with this operation sheet
        /// </summaryGlo
        public string Client { get; set; }

        /// <summary>
        /// the purpose of the Operation Sheet
        /// </summary>
        public string Purpose { get; set; }

        /// <summary>
        /// the Statut of the Operation Sheet
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// the Intervention Address
        /// </summary>
        public Address AddressIntervention { get; set; }

        /// <summary>
        /// list of Technicians associated with this operation sheet
        /// </summary>
        public ICollection<UserMinimalModel> Technicians { get; set; }

        public OrderDetails OrderDetails { get; set; }

        public ICollection<Address>  Adresses { get; set; }

        public string ClientId { get; set; }

        public string WorkshopId { get; set; }

        public Boolean QuoteEtablir { get; set; }

        /// <summary>
        /// a boolean check if you can delete this operation Sheet
        /// </summary>
        public bool CanDelete { get; set; }

        /// <summary>
        /// a boolean check if you can delete this operation Sheet
        /// </summary>
        public bool CanUpdate { get; set; }

        public string ClientCity { get; set; }

        /// <summary>
        /// Smile
        /// </summary>
        public SmileSatisfication SmileSatisfication { get; set; }
    }
}
