﻿namespace Axiobat.Application.Models
{
    using Domain.Entities;
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// the model for <see cref="Quote"/>
    /// </summary>
    [ModelFor(typeof(Quote))]
    public partial class QuoteModel : DocumentModel<Quote>
    {
        /// <summary>
        /// Quote creation date
        /// </summary>
        public DateTime CreationDate { get; set; }

        /// <summary>
        /// Quote due date
        /// </summary>
        public DateTime DueDate { get; set; }

        /// <summary>
        /// whether to include the Lot details or not
        /// </summary>
        public bool LotDetails { get; set; }

        /// <summary>
        /// the Intervention Address
        /// </summary>
        public Address AddressIntervention { get; set; }

        /// <summary>
        /// the devis order details
        /// </summary>
        public WorkshopOrderDetails OrderDetails { get; set; }

        /// <summary>
        /// the client associated with the invoice
        /// </summary>
        public ClientDocument Client { get; set; }

        /// <summary>
        /// list of emails that has been sent for this documents
        /// </summary>
        public ICollection<DocumentEmail> Emails { get; set; }

        /// <summary>
        /// the list of Situations
        /// </summary>
        public ICollection<QuoteSituations> Situations { get; set; }

        /// <summary>
        /// the list of Situations
        /// </summary>
        public Signature ClientSignature { get; set; }

        /// <summary>
        /// list of memos associated with this entity
        /// </summary>
        public ICollection<Memo> Memos { get; set; }

        /// <summary>
        /// a boolean check if you can generate an Invoice for this quote
        /// </summary>
        public bool CanGenerateInvoice { get; set; }

        /// <summary>
        /// a boolean check if you can generate an DownPayment Invoice for this quote
        /// </summary>
        public bool CanGenerateDownPaymentInvoice { get; set; }

        /// <summary>
        /// a boolean check if you can generate a Situation Invoice for this quote
        /// </summary>
        public bool CanGenerateSituationInvoice { get; set; }

        /// <summary>
        /// a boolean check if you can cancel this quote
        /// </summary>
        public bool CanCancel { get; set; }

        /// <summary>
        /// a boolean check if you can delete this quote
        /// </summary>
        public bool CanDelete { get; set; }
        /// <summary>
        /// a boolean check if you can UPDATE this quote
        /// </summary>
        public bool CanUpdate { get; set; }
        /// <summary>
        /// a boolean check if you can GENERATE this SupplierOrder
        /// </summary>

        public bool CanGenerateSupplierOrder { get; set; }


        public bool CanGenerateOperationSheet { get; set; }

        
    }

    /// <summary>
    /// partial part for <see cref="QuoteModel"/>
    /// </summary>
    public partial class QuoteModel : DocumentModel<Quote>
    {
        /// <summary>
        /// create an instant of <see cref="QuoteModel"/>
        /// </summary>
        public QuoteModel() : base()
        {
            Emails = new HashSet<DocumentEmail>();
            Situations = new HashSet<QuoteSituations>();
        }
    }
}
