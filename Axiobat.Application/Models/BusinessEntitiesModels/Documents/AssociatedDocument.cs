﻿namespace Axiobat.Application.Models
{
    using Domain.Enums;
    using System;

    /// <summary>
    /// this class defines the associated document
    /// </summary>
    public partial class AssociatedDocument
    {
        /// <summary>
        /// the id of the document
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// the document reference
        /// </summary>
        public string Reference { get; set; }

        /// <summary>
        /// the status of the document
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// the type of the document
        /// </summary>
        public DocumentType DocumentType { get; set; }

        /// <summary>
        /// the invoice type
        /// </summary>
        public InvoiceType TypeInvoice { get; set; }

        /// <summary>
        /// the creation date of the document
        /// </summary>
        public DateTime CreationDate { get; set; }
    }
}
