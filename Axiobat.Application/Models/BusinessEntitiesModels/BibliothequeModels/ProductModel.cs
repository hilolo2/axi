﻿namespace Axiobat.Application.Models
{
    using Domain.Entities;
    using System.Collections.Generic;

    [ModelFor(typeof(Product))]
    public partial class ProductModel : ProductsBaseModel<Product>
    {
        /// <summary>
        /// the product reference, should be unique
        /// </summary>
        public string Reference { get; set; }

        /// <summary>
        /// the total estimated hours for the product
        /// </summary>
        public float TotalHours { get; set; }

        /// <summary>
        /// the estimated material cost that the product will consume
        /// </summary>
        public float MaterialCost { get; set; }

        /// <summary>
        /// the cost of selling the product, the price is Hourly based.
        /// </summary>
        public float HourlyCost { get; set; }

        /// <summary>
        /// the total price of the product, HT
        /// </summary>
        public float TotalHT { get; set; }

        /// <summary>
        /// the total price of the product, TAX
        /// </summary>
        public float TaxValue { get; set; }

        /// <summary>
        /// the total price of the product, TTC
        /// </summary>
        public float TotalTTC { get; set; }

        /// <summary>
        /// the Value added tax
        /// </summary>
        public float VAT { get; set; }

        /// <summary>
        /// the product unite
        /// </summary>
        public string Unite { get; set; }

        /// <summary>
        /// the product category type
        /// </summary>
        public ProductCategoryTypeModel ProductCategoryType { get; set; }

        /// <summary>
        /// the product Classification
        /// </summary>
        public ClassificationModel Category { get; set; }

        /// <summary>
        /// the Data sheets associated with the product
        /// </summary>
        public ICollection<Memo> Memo { get; set; }

        /// <summary>
        /// list of labels
        /// </summary>
        public ICollection<LabelModel> Labels { get; set; }

        /// <summary>
        /// list of the product suppliers
        /// </summary>
        public ICollection<ProductSupplierInfo> ProductSuppliers { get; set; }
    }
}
