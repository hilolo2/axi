﻿namespace Axiobat.Application.Models
{
    using Domain.Entities;
    using System.Collections.Generic;

    /// <summary>
    /// the model for <see cref="ProductsBase"/>
    /// </summary>
    [ModelFor(typeof(ProductsBase))]
    public class ProductsBaseModel<TEntity> : IModel<TEntity>
        where TEntity : ProductsBase
    {
        /// <summary>
        /// the id of the product
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// the Designation
        /// </summary>
        public string Designation { get; set; }

        /// <summary>
        /// description
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// the changes history
        /// </summary>
        public ICollection<ChangesHistory> ChangesHistory { get; set; }
    }
}
