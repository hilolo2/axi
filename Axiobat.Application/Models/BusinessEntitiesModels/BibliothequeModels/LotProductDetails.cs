﻿namespace Axiobat.Application.Models
{
    /// <summary>
    /// the lot product relationship details
    /// </summary>
    public class LotProductDetails
    {
        /// <summary>
        /// the id of the product
        /// </summary>
        public string ProductId { get; set; }

        /// <summary>
        /// the quantity of the products in the Lot
        /// </summary>
        public int Quantity { get; set; }

        /// <summary>
        /// the full details of the product
        /// </summary>
        public ProductModel ProductDetails { get; set; }
    }
}
