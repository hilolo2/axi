﻿namespace Axiobat.Application.Models
{
    using Domain.Entities;

    /// <summary>
    /// create model for <see cref="MaintenanceOperationSheet"/>
    /// </summary>
    [ModelFor(typeof(MaintenanceOperationSheet))]
    public partial class MaintenanceOperationSheetCreateModel : MaintenanceOperationSheetBaseCreateModel
    {
        /// <summary>
        /// the client associated with this operation sheet
        /// </summary>
        public ClientDocument Client { get; set; }

        /// <summary>
        /// the year of maintenance visit
        /// </summary>
        public int Year { get; set; }

        /// <summary>
        /// the month of maintenance visit
        /// </summary>
        public int Month { get; set; }

        /// <summary>
        /// the id of the MaintenanceO contract
        /// </summary>
        public string MaintenanceContractId { get; set; }
    }
}
