﻿namespace Axiobat.Application.Models
{
    using Domain.Enums;
    using Domain.Entities;
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// the create and update model for <see cref="MaintenanceContract"/>
    /// </summary>
    [ModelFor(typeof(MaintenanceContract))]
    public partial class MaintenanceContractPutModel
    {
        /// <summary>
        /// the reference for the entity
        /// </summary>
        public string Reference { get; set; }

        /// <summary>
        /// status of the contract
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// the site of the maintenance
        /// </summary>
        public Address Site { get; set; }

        /// <summary>
        /// the starting date of the operation
        /// </summary>
        public DateTime StartDate { get; set; }

        /// <summary>
        /// the ending date of the operation
        /// </summary>
        public DateTime EndDate { get; set; }

        /// <summary>
        /// whether to enable the Enable Auto Renewal or not
        /// </summary>
        public bool EnableAutoRenewal { get; set; }

        /// <summary>
        /// whether the expiration alert has been enabled or not
        /// </summary>
        public bool ExpirationAlertEnabled { get; set; }

        /// <summary>
        /// the expiration alert period
        /// </summary>
        public int ExpirationAlertPeriod { get; set; }

        /// <summary>
        /// the id of the Recurring associated with this contract
        /// </summary>
        public string RecurringDocumentId { get; set; }

        /// <summary>
        /// the type of the expiration alert period
        /// </summary>
        public PeriodType ExpirationAlertPeriodType { get; set; }

        /// <summary>
        /// the id of the client
        /// </summary>
        public string ClientId { get; set; }

        /// <summary>
        ///  the client
        /// </summary>
        public Client Client { get; set; }

        /// <summary>
        /// list of PublishingContract associated with this contract
        /// </summary>
        public ICollection<PublishingContractMinimal> PublishingContracts { get; set; }

        /// <summary>
        /// list of attachments
        /// </summary>
        public ICollection<AttachmentModel> Attachments { get; set; }

        /// <summary>
        /// list of Equipment Maintenance being maintained by this contract argument
        /// </summary>
        public ICollection<MaintenanceContractEquipmentDetails> EquipmentMaintenance { get; set; }

        /// <summary>
        /// the devis order details
        /// </summary>
        public WorkshopOrderDetails OrderDetails { get; set; }
    }

    /// <summary>
    /// partial part for <see cref="MaintenanceContractPutModel"/>
    /// </summary>
    public partial class MaintenanceContractPutModel : IUpdateModel<MaintenanceContract>
    {
        /// <summary>
        /// create an instant of <see cref="MaintenanceContractPutModel"/>
        /// </summary>
        public MaintenanceContractPutModel()
        {
            Attachments = new HashSet<AttachmentModel>();
            PublishingContracts = new HashSet<PublishingContractMinimal>();
            EquipmentMaintenance = new HashSet<MaintenanceContractEquipmentDetails>();
        }

        /// <summary>
        /// update the entity from the current model
        /// </summary>
        /// <param name="entity">the entity to be updated</param>
        public void Update(MaintenanceContract entity)
        {
            entity.Site = Site;
            entity.Status = Status;
            entity.EndDate = EndDate;
            entity.StartDate = StartDate;
            entity.Reference = Reference;
            entity.ClientId = ClientId;
            entity.Client = Client;
            entity.OrderDetails = OrderDetails;
            entity.EnableAutoRenewal = EnableAutoRenewal;
            entity.RecurringDocumentId = RecurringDocumentId;
            entity.EquipmentMaintenance = EquipmentMaintenance;
            entity.ExpirationAlertEnabled = ExpirationAlertEnabled;
            entity.ExpirationAlertPeriod = ExpirationAlertPeriod * (int)ExpirationAlertPeriodType;
        }
    }
}
