﻿namespace Axiobat.Application.Models
{
    using Domain.Entities;
    using System;

    /// <summary>
    /// the list model for <see cref="MaintenanceContract"/>
    /// </summary>
    [ModelFor(typeof(MaintenanceContract))]
    public partial class MaintenanceContractListModel
    {
        /// <summary>
        /// the id of the model
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// the id of the reference
        /// </summary>
        public string  Reference { get; set; }

        /// <summary>
        /// name of the client
        /// </summary>
        public ClientModel Client { get; set; }

        /// <summary>
        /// whether the expiration alert has been enabled or not
        /// </summary>
        [Newtonsoft.Json.JsonProperty("ExpirationAlertEnabled")]
        public bool Alerted { get; set; }

        /// <summary>
        /// status of the contract
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// the starting date of the operation
        /// </summary>
        public DateTime StartDate { get; set; }

        /// <summary>
        /// the ending date of the operation
        /// </summary>
        public DateTime EndDate { get; set; }
    }

    ///// <summary>
    ///// the partial part for <see cref="MaintenanceContractListModel"/>
    ///// </summary>
    //public partial class MaintenanceContractListModel : IModel<MaintenanceContract>
    //{
    //    /// <summary>
    //    /// create an instant of <see cref="MaintenanceContractListModel"/>
    //    /// </summary>
    //    public MaintenanceContractListModel()
    //    {

    //    }
    //}
}
