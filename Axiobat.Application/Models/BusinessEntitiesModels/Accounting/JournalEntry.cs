﻿namespace Axiobat.Application.Models
{
    using Enums;
    using Domain.Enums;
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// the class used for presenting the journal
    /// </summary>
    public partial class JournalEntry
    {
        /// <summary>
        /// create an instant of <see cref="JournalEntry"/>
        /// </summary>
        public JournalEntry()
        {
            Credits = new List<JournalEntry>();
        }

        /// <summary>
        /// the accounting code
        /// </summary>
        public string JournalCode { get; set; }

        /// <summary>
        /// the date of the entry
        /// </summary>
        public DateTime EntryDate { get; set; }

        /// <summary>
        /// the chart account value
        /// </summary>
        public string ChartAccount { get; set; }

        /// <summary>
        /// the part number
        /// </summary>
        public string PieceNumber { get; set; }

        /// <summary>
        /// the id of the external partner
        /// </summary>
        public string ExternalPartner { get; set; }

        /// <summary>
        /// the journal entry type
        /// </summary>
        public JournalEntryType EntryType { get; set; }

        /// <summary>
        /// the type of the document
        /// </summary>
        public DocumentType DocumentType { get; set; }

        /// <summary>
        /// the amount associated with this journal entry
        /// </summary>
        public float Amount { get; set; }

        /// <summary>
        /// a list that represent the Credit part of this entry
        /// </summary>
        public List<JournalEntry> Credits { get; set; }
    }

    /// <summary>
    /// the cash JournalModel
    /// </summary>
    public partial class CashJournalModel : JournalEntry
    {
        /// <summary>
        /// the type of the payment
        /// </summary>
        public FinancialAccountsType PaymentType { get; set; }

        /// <summary>
        /// the Tiers
        /// </summary>
        public string Tiers { get; set; }
    }
}
