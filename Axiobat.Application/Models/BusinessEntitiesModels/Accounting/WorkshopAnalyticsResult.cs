﻿namespace Axiobat.Application.Models
{
    using Domain.Entities;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// this class defines the model for the workshop analytics result
    /// </summary>
    public class WorkshopAnalyticsResult
    {
        public IEnumerable<WorkshopByStatus> WorkshopsByStatus { get; set; }

        public IEnumerable<MinimalWorkshopInfo> TopWorkshopsByTurnover { get; set; }

        public IEnumerable<MinimalWorkshopInfo> TopWorkshopsByWorkingHours { get; set; }

        public IEnumerable<OperationSheetsByStatus> OperationSheetsByStatus { get; set; }

        public WorkshopAnalytics MarginDetails { get; set; }
    }

    public class WorkshopByStatus
    {
        public WorkshopByStatus()
        {
            Workshops = new HashSet<MinimalWorkshopInfo>();
        }

        public string Status { get; set; }

        public IEnumerable<MinimalWorkshopInfo> Workshops { get; set; }

        public float TotalTurnover => Workshops.Sum(e => e.Turnover);

        public double TotalWorkingHours => Workshops.Sum(e => e.WorkingHours);
    }

    public class OperationSheetsByStatus
    {
        public string Status { get; set; }

        public IEnumerable<OperationSheetsMinimalInfo> operationSheets { get; set; }
    }

    public class WorkshopAnalytics
    {
        public float WokshopMargin => WorkforceMargin - Subcontracting;

        public float Subcontracting { get; set; }

        public float WorkforceMargin { get; set; }
    }
}
