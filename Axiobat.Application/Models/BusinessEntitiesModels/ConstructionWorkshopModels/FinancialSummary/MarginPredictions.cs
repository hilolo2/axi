﻿namespace Axiobat.Application.Models
{
    /// <summary>
    /// the margin Predictions
    /// </summary>
    public partial class MarginPredictions
    {
        public MarginPredictions(float total, float percent)
        {
            Total = total;
            Percent = percent;
        }

        /// <summary>
        /// the total of the Margin
        /// </summary>
        public float Total { get; }

        /// <summary>
        /// the margin percent
        /// </summary>
        public float Percent { get; }

        /// <summary>
        /// the total Holdback
        /// </summary>
        public float TotalHoldback { get; set; }

        /// <summary>
        /// margin on material
        /// </summary>
        public float MaterialMargin { get; set; }

        /// <summary>
        /// margin on Workforce
        /// </summary>
        public float WorkforceMargin { get; set; }

        /// <summary>
        /// the holdback Percent
        /// </summary>
        public float PercentHoldback => TotalHoldback / Total * 100;

        /// <summary>
        /// the Material Percent
        /// </summary>
        public float PercentMaterial => MaterialMargin / Total * 100;

        /// <summary>
        /// the Workforce Percent
        /// </summary>
        public float PercentWorkforce => WorkforceMargin / Total * 100;
    }
}
