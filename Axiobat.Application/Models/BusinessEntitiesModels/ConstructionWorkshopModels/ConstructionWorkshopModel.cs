﻿namespace Axiobat.Application.Models
{
    using Domain.Entities;
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// model for <see cref="ConstructionWorkshop"/>
    /// </summary>
    [ModelFor(typeof(ConstructionWorkshop))]
    public partial class ConstructionWorkshopModel
    {
        /// <summary>
        /// the id of the entity
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// name of the construction workshop
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// a description of the construction workshop
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// a Comment about the construction workshop
        /// </summary>
        public string Comment { get; set; }

        /// <summary>
        /// the date the workshop has been created
        /// </summary>
        public DateTimeOffset CreatedOn { get; set; }

        /// <summary>
        /// the construction workshop status
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// the total hours spent on this workshop
        /// </summary>
        public int TotalHours { get; set; }

        /// <summary>
        /// the total amount spent on the workshop
        /// </summary>
        public float Amount { get; set; }

        /// <summary>
        /// the Progress Rate on the workshop
        /// </summary>
        public int ProgressRate { get; set; }

        /// <summary>
        /// the Progress Rate on the Turnover
        /// </summary>
        public float Turnover { get; set; }

        /// <summary>
        /// list of history changes on the entity
        /// </summary>
        public ICollection<ChangesHistory> ChangesHistory { get; set; }

        /// <summary>
        /// the client that owns this workshop
        /// </summary>
        public ClientDocument Client { get; set; }
    }
}
