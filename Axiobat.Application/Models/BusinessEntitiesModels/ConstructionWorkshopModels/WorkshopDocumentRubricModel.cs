﻿namespace Axiobat.Application.Models
{
    using Axiobat.Domain.Entities;
    using Axiobat.Domain.Enums;

    /// <summary>
    /// the model for <see cref="WorkshopDocumentRubric"/>
    /// </summary>
    [ModelFor(typeof(WorkshopDocumentRubric))]
    public class WorkshopDocumentRubricModel : IModel<WorkshopDocumentRubric, string>, IUpdateModel<WorkshopDocumentRubric>
    {
        /// <summary>
        /// the model id
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// the value of the rubric
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// the type of the rubric
        /// </summary>
        public DocumentRubricType Type { get; set; }

        /// <summary>
        /// the id of the workshop that owns this rubric
        /// </summary>
        public string WorkshopId { get; set; }

        /// <summary>
        /// the total count of documents associated with this Rubric
        /// </summary>
        public int TotalDocuments { get; set; }

        /// <summary>
        /// count of associated commercial document
        /// </summary>
        public int AssociatedDocumentsCount { get; set; }

        /// <summary>
        /// update the entity from the current value
        /// </summary>
        /// <param name="entity">the entity to update</param>
        public void Update(WorkshopDocumentRubric entity)
        {
            entity.Value = Value;
            entity.WorkshopId = WorkshopId;
        }
    }
}
