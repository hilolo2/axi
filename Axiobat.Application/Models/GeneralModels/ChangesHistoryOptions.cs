﻿namespace Axiobat.Application.Models
{
    using Domain.Enums;
    using Domain.Entities;
    using System.Collections.Generic;

    /// <summary>
    /// the options for inserting a new ChangesHistory instant to a <see cref="IRecordable"/> entity
    /// </summary>
    [ModelFor(typeof(ChangesHistory))]
    public partial class ChangesHistoryOptions
    {
        /// <summary>
        /// create an instant of <see cref="ChangedField"/>
        /// </summary>
        public ChangesHistoryOptions()
        {
            Fields = new List<ChangedField>();
        }

        /// <summary>
        /// the type of the action performed, example : Added / Modified / Deleted
        /// </summary>
        public ChangesHistoryType Action { get; set; }

        /// <summary>
        /// information
        /// </summary>
        public string Information { get; set; }

        /// <summary>
        /// list of Fields that has been changed
        /// </summary>
        public ICollection<ChangedField> Fields { get; set; }
    }
}
