﻿namespace Axiobat.Application.Models
{
    using Axiobat.Domain.Enums;
    using System.Collections.Generic;

    /// <summary>
    /// retrieve user by role type
    /// </summary>
    public partial class UserRetrievealByRoleType
    {
        /// <summary>
        /// create an instant of <see cref="UserRetrievealByRoleType"/>
        /// </summary>
        public UserRetrievealByRoleType() : base()
        {
            RoleTypes = new HashSet<RoleType>();
        }

        /// <summary>
        /// the roles type to retrieve the users with it
        /// </summary>
        public IEnumerable<RoleType> RoleTypes { get; set; }
    }
}
