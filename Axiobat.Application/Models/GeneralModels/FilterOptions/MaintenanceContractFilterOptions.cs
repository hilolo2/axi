﻿namespace Axiobat.Application.Models
{
    using System.Collections.Generic;

    /// <summary>
    /// the documents Filter Options
    /// </summary>
    public partial class MaintenanceContractFilterOptions : ExternalPartnerFilterOptions
    {
        /// <summary>
        /// create an instant of <see cref="MaintenanceContractFilterOptions"/>
        /// </summary>
        public MaintenanceContractFilterOptions() : base()
        {
            Status = new HashSet<string>();
        }

        /// <summary>
        /// the status of the document
        /// </summary>
        public IEnumerable<string> Status { get; set; }

        /// <summary>
        /// get the Contract in the alerted state
        /// </summary>
        public bool Alerted { get; set; }
    }
}
