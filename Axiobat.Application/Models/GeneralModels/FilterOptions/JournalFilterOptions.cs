﻿namespace Axiobat.Application.Models
{
    using Enums;
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// the filter options of the journal
    /// </summary>
    public partial class JournalFilterOptions : DateRangeFilterOptions
    {
        /// <summary>
        /// the journal type to be retrieved
        /// </summary>
        public JournalType JournalType { get; set; }
    }

    /// <summary>
    /// the analytics filter options
    /// </summary>
    public partial class WorkshopAnalyticsFilterOtions : DateRangeFilterOptions
    {
        /// <summary>
        /// create an instant of <see cref="WorkshopAnalyticsFilterOtions"/>
        /// </summary>
        public WorkshopAnalyticsFilterOtions() : base()
        {
            WorkshopId = new HashSet<string>();
            ClientId = new HashSet<string>();
            GroupeId = new HashSet<int>();
        }

        /// <summary>
        /// the id of the workshops
        /// </summary>
        public IEnumerable<string> WorkshopId { get; set; }

        /// <summary>
        /// the id of the clients
        /// </summary>
        public IEnumerable<string> ClientId { get; set; }

        /// <summary>
        /// the id of the groups
        /// </summary>
        public IEnumerable<int> GroupeId { get; set; }
    }

    /// <summary>
    /// the purchase analytics filter options
    /// </summary>
    public partial class TransactionAnalyticsFilterOptions : ExternalPartnerFilterOptions
    {
        /// <summary>
        /// create an instant of <see cref="TransactionAnalyticsFilterOptions"/>
        /// </summary>
        public TransactionAnalyticsFilterOptions() : base()
        {
            ProdcutId = new HashSet<string>();
        }

        /// <summary>
        /// list of products ids to filter with it
        /// </summary>
        public IEnumerable<string> ProdcutId { get; set; }

        /// <summary>
        /// the id of the product to get the purchase details for it
        /// </summary>
        public string PurchaseAnalyticsFor { get; set; }
    }

    /// <summary>
    /// the filter options for the Maintenance Analytics
    /// </summary>
    public partial class MaintenanceAnalyticsFilterOptions : DateRangeFilterOptions
    {
        /// <summary>
        /// create an instant of <see cref="MaintenanceAnalyticsFilterOptions"/>
        /// </summary>
        public MaintenanceAnalyticsFilterOptions()
        {
            Technicians = new HashSet<Guid>();
            ClientId = new HashSet<string>();
            GroupeId = new HashSet<int>();
        }

        /// <summary>
        /// the list of Technicians ids
        /// </summary>
        public IEnumerable<Guid> Technicians { get; set; }

        /// <summary>
        /// the id of the clients
        /// </summary>
        public IEnumerable<string> ClientId { get; set; }

        /// <summary>
        /// the id of the groups
        /// </summary>
        public IEnumerable<int> GroupeId { get; set; }

        /// <summary>
        /// the number of the months the contracts will expire in
        /// </summary>
        public int? ExpireIn { get; set; }
    }
}
