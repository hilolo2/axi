﻿namespace Axiobat.Application.Models
{
    using System.Collections.Generic;

    /// <summary>
    /// the documents Filter Options
    /// </summary>
    public partial class DocumentFilterOptions : DateRangeFilterOptions
    {
        /// <summary>
        /// create an instant of <see cref="DocumentFilterOptions"/>
        /// </summary>
        public DocumentFilterOptions() : base()
        {
            Status = new HashSet<string>();
        }

        /// <summary>
        /// the id of the External Partner, client or supplier
        /// </summary>
        public string ExternalPartnerId { get; set; }

        /// <summary>
        /// the id of the workshop
        /// </summary>
        public string WorkshopId { get; set; }

        /// <summary>
        /// the status of the document
        /// </summary>
        public IEnumerable<string> Status { get; set; }
    }

    /// <summary>
    /// the <see cref="Domain.Entities.RecurringDocument"/> Filter Options
    /// </summary>
    public partial class RecurringFilterOptions : FilterOptions
    {
        /// <summary>
        /// the status of the document
        /// </summary>
        public IEnumerable<string> Status { get; set; }
    }
}
