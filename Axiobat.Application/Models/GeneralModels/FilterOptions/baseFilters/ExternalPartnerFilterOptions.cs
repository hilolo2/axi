﻿namespace Axiobat.Application.Models
{
    using System.Collections.Generic;

    /// <summary>
    /// a filter option with a list of partners to filter with it
    /// </summary>
    public partial class ExternalPartnerFilterOptions : DateRangeFilterOptions
    {
        /// <summary>
        /// create an instant of <see cref="ExternalPartnerFilterOptions"/>
        /// </summary>
        public ExternalPartnerFilterOptions() : base()
        {
            ExternalPartnerId = new HashSet<string>();
        }

        /// <summary>
        /// the id of the External Partner, client or supplier
        /// </summary>
        public IEnumerable<string> ExternalPartnerId { get; set; }
    }
}
