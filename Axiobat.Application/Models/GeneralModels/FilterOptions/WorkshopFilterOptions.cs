﻿namespace Axiobat.Application.Models
{
    using System.Collections.Generic;

    /// <summary>
    /// the workshop filter options
    /// </summary>
    public class WorkshopFilterOptions : FilterOptions
    {
        /// <summary>
        /// create an instant of <see cref="WorkshopFilterOptions"/>
        /// </summary>
        public WorkshopFilterOptions() : base()
        {
            Status = new HashSet<string>();
        }

        /// <summary>
        /// the id of the client
        /// </summary>
        public string ClientId { get; set; }

        /// <summary>
        /// the workshop status
        /// </summary>
        public ICollection<string> Status { get; set; }
    }
}
