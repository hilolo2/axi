﻿namespace Axiobat.Application.Models
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// the operation sheet filter options
    /// </summary>
    public partial class OperationSheetFilterOptions : DocumentFilterOptions
    {
        /// <summary>
        /// create an instant of <see cref="OperationSheetFilterOptions"/>
        /// </summary>
        public OperationSheetFilterOptions() : base ()
        {
            TechniciansId = new HashSet<Guid>();
        }

        /// <summary>
        /// the id of the Technician
        /// </summary>
        public IEnumerable<Guid> TechniciansId { get; set; }
    }
}
