﻿namespace Axiobat.Application.Models
{
    using Domain.Entities;

    /// <summary>
    /// a model for <see cref="Tax"/>
    /// </summary>
    [ModelFor(typeof(Tax))]
    public partial class TaxModel : IUpdateModel<Tax>, IModel<Tax, string>
    {
        /// <summary>
        /// the id of the Tax
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// name of the Tax
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// the value of the Tax
        /// </summary>
        public float Value { get; set; }

        /// <summary>
        /// the accounting code of the category
        /// </summary>
        public string AccountingCode { get; set; }

        /// <summary>
        /// if this instant is the default one
        /// </summary>
        public bool IsDefault { get; set; }
    }

    /// <summary>
    /// partial part for <see cref="TaxModel"/>
    /// </summary>
    public partial class TaxModel
    {
        /// <summary>
        /// update the entity for the current model
        /// </summary>
        /// <param name="entity">the entity to be updated</param>
        public void Update(Tax entity)
        {
            entity.Name = Name;
            entity.Value = Value;
            entity.IsDefault = IsDefault;
            entity.AccountingCode = AccountingCode;
        }

        /// <summary>
        /// the string representation of the object
        /// </summary>
        /// <returns></returns>
        public override string ToString()
            => $"name: {Name}, value {Value}%";
    }
}
