﻿namespace Axiobat.Application.Models
{
    using App.Common;
    using Domain.Entities;
    using Domain.Enums;

    /// <summary>
    /// the put model for <see cref="ChartAccountItem"/>
    /// </summary>
    [ModelFor(typeof(ChartAccountItem))]
    public partial class ChartAccountItemPutModel : IUpdateModel<ChartAccountItem>
    {
        /// <summary>
        /// the id of the category to be updated
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// the accounting code associated with this category
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// the label of the category
        /// </summary>
        public string Label { get; set; }

        /// <summary>
        /// the value of the tax, if this category is of type <see cref="ChartAccountType.VAT"/>
        /// </summary>
        public float? VATValue { get; set; }

        /// <summary>
        /// the parent category
        /// </summary>
        public string ParentId { get; set; }

        public void Update(ChartAccountItem entity)
        {
            entity.Code = Code;
            entity.VATValue = VATValue ?? 0;

            if (Label.IsValid())
                entity.Label = Label;

            if (ParentId.IsValid())
                entity.ParentId = ParentId;
        }
    }
}
