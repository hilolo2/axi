﻿namespace Axiobat.Application.Models
{
    using App.Common;
    using Domain.Enums;
    using Enums;
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// this class defines the data export options
    /// </summary>
    public partial class DataExportOptions
    {
        /// <summary>
        /// define the type of the export
        /// </summary>
        public ExportType ExportType { get; set; } = ExportType.Excel;

        /// <summary>
        /// the document to export the data for it
        /// </summary>
        public DocumentType Document { get; set; }
    }

    /// <summary>
    /// partial part for <see cref="DataExportOptions"/>
    /// </summary>
    public partial class DataExportOptions
    {
        /// <summary>
        /// return string reApplication of the object
        /// </summary>
        /// <returns>the string value</returns>
        public override string ToString()
            => $"export type: {ExportType}, document: {Document}";

        /// <summary>
        /// get the mimeType associated with the selected export file type
        /// </summary>
        /// <returns>the mime type</returns>
        public string GetFileMimetype()
        {
            switch (ExportType)
            {
                case ExportType.CSV:
                    return FileHelper.GetExtensionMimeType(".csv");
                case ExportType.XML:
                    return FileHelper.GetExtensionMimeType(".xml");
                case ExportType.Excel:
                default:
                    return FileHelper.GetExtensionMimeType(".xls");
            }
        }

        /// <summary>
        /// generate a file name based on the client id and file type and add a timestamps to it
        /// </summary>
        /// <returns>generated file name</returns>
        public string GenerateFileName(string clientId)
        {
            var file = $"{clientId}_{Document}_".AppendTimeStamp();

            switch (ExportType)
            {
                case ExportType.CSV:
                    file += ".csv";
                    break;
                case ExportType.XML:
                    file += ".xml";
                    break;
                case ExportType.Excel:
                default:
                    file += ".xls";
                    break;
            }

            return file;
        }
    }

    public partial class ExportByPeriodByte
    {
        /// <summary>
        /// define the type of the export
        /// </summary>
        public string idClient { get; set; }

        public TypeExportInvoice type { get; set; }

        /// <summary>
        /// the start date
        /// </summary>
        public DateTime? startDate { get; set; }

        /// <summary>
        /// the start date
        /// </summary>
        public DateTime? endDate { get; set; }


        public bool exportFcature { get; set; }
    }

    public partial class ExportByte
    {
       public List<byte[]> InvoiceCredit { get; set; }

       public List<byte[]> Documents { get; set; }
    }
}
