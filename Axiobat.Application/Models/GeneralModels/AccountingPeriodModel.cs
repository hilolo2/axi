﻿namespace Axiobat.Application.Models
{
    using Domain.Entities;
    using System;

    /// <summary>
    /// model for <see cref="AccountingPeriod"/>
    /// </summary>
    [ModelFor(typeof(AccountingPeriod))]
    public partial class AccountingPeriodModel
    {
        /// <summary>
        /// the id of the model entity
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// the starting date of the accounting period
        /// </summary>
        public DateTime StartingDate { get; set; }

        /// <summary>
        /// the ending date of the accounting period
        /// </summary>
        public DateTime? EndingDate { get; set; }

        /// <summary>
        /// the accounting period
        /// </summary>
        public int Period { get; set; }
    }

    /// <summary>
    /// partial part for <see cref="AccountingPeriodModel"/>
    /// </summary>
    public partial class AccountingPeriodModel : IModel<AccountingPeriod, int>, IUpdateModel<AccountingPeriod>
    {
        /// <summary>
        /// update the entity from the current model
        /// </summary>
        /// <param name="entity">the entity instant</param>
        public void Update(AccountingPeriod entity)
        {
            entity.StartingDate = StartingDate;
            entity.EndingDate = EndingDate;
            entity.Period = Period;
        }

        /// <summary>
        /// get the string representation of the entity
        /// </summary>
        /// <returns>the string value</returns>
        public override string ToString()
            => $"id: {Id}, start at: {StartingDate.ToShortDateString()}, last for {Period}" + (EndingDate.HasValue ? $" ended at {EndingDate?.ToShortDateString()}" : "");
    }
}
