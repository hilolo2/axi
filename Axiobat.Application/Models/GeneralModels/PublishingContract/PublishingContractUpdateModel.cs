﻿namespace Axiobat.Application.Models
{
    using Domain.Entities;

    public class PublishingContractUpdateModel : PublishingContractCreateModel , IUpdateModel<PublishingContract>
    {
        public void Update(PublishingContract contract)
        {
            contract.Title = Title;
            contract.Tags = Tags;
            contract.OrginalFileName = OrginalFileName;
            contract.FileName = FileName;
        }
    }
}
