﻿namespace Axiobat.Application.Models
{
    using System.Collections.Generic;

    /// <summary>
    /// model used for downloading a pushed contract
    /// </summary>
    public class PublishingContractDownloadRequest
    {
        /// <summary>
        /// the list tags to replace
        /// </summary>
        public Dictionary<string, string> Tags { get; set; }
    }

    public class PublishingContractDownloadModel
    {
        public byte[] Data { get; set; }
        public string ContentType { get; set; }
        public string Filename { get; set; }
    }
}
