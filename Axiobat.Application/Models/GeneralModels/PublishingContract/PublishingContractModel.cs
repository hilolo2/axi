﻿namespace Axiobat.Application.Models
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// a class define publishing contract model
    /// </summary>
    public class PublishingContractModel
    {
        /// <summary>
        /// the id publishing contract
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// the name of contract
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// the tags of contract
        /// </summary>
        public IDictionary<string, string> Tags { get; set; }

        /// <summary>
        /// the name of file
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// the original file name
        /// </summary>
        public string OrginalFileName { get; set; }

        /// <summary>
        /// the id of associate who associate with this entity
        /// </summary>
        public Guid? UserId { get; set; }

        /// <summary>
        /// the date of create
        /// </summary>
        public DateTimeOffset CreatedOn { get; set; }
    }
}
