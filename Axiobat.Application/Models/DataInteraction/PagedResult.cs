﻿namespace Axiobat.Application.Models
{
    using System.Collections.Generic;

    /// <summary>
    /// the paged result implementation class
    /// </summary>
    /// <typeparam name="TResult">the type of the data to be returned</typeparam>
    [System.Diagnostics.DebuggerStepThrough]
    public class PagedResult<TResult> : ListResult<TResult>
    {
        /// <summary>
        /// the index of the current selected page
        /// </summary>
        public int CurrentPage { get; set; }

        /// <summary>
        /// the total count of all pages
        /// </summary>
        public int PagesCount => RowsCount <= 0 ? 1 : (int)System.Math.Ceiling((double)RowsCount / PageSize);

        /// <summary>
        /// the skip value
        /// </summary>
        [Newtonsoft.Json.JsonIgnore]
        public int Skip => CalculateSkip(CurrentPage, PageSize);

        /// <summary>
        /// the page size, the desired number of items per page
        /// </summary>
        public int PageSize { get; set; }

        /// <summary>
        /// the total count of all items
        /// </summary>
        public int RowsCount { get; set; }

        /// <summary>
        /// create an instant of <see cref="PagedResult{TResult}"/> form the given result
        /// </summary>
        /// <param name="result">the result to clone</param>
        /// <returns>an instant on <see cref="PagedResult{TResult}"/></returns>
        public static new PagedResult<TResult> Clone(Result result)
            => new PagedResult<TResult>()
            {
                Status = result.Status,
                Error = result.Error,
                Message = result.Message,
                MessageCode = result.MessageCode,
                CurrentPage = 1,
                PageSize = 10,
                RowsCount = 0,
                Value = default,
            };

        /// <summary>
        /// this method is used to calculate the skip and page size
        /// </summary>
        /// <returns>skip and page size</returns>
        public static (int skip, int pageCount) CalculateSkipAndPageSize(int ItemsCount, int page, int pageSize)
        {
            var pageCount = (int)System.Math.Ceiling((double)ItemsCount / pageSize);
            var skip = (page - 1) * pageSize;

            return (skip, pageCount);
        }

        /// <summary>
        /// this method is used to calculate the skip value
        /// </summary>
        /// <returns>skip value</returns>
        public static int CalculateSkip(int page, int pageSize)
            => (page - 1) * pageSize;

        #region Operators overrides

        public static implicit operator List<TResult>(PagedResult<TResult> result)
            => result is null || !result.HasValue ? (default) : result.Value as List<TResult>;

        #endregion
    }
}
