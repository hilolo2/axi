﻿namespace Axiobat.Application.Models
{
    using App.Common;
    using Application.Enums;
    using Domain.Interfaces;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;

    /// <summary>
    /// an implementation of <see cref="DataRequestBuilder"/>
    /// </summary>
    /// <typeparam name="TEntity">the entity we building the data request for</typeparam>
    [System.Diagnostics.DebuggerStepThrough]
    public class DataRequestBuilder<TEntity>
        where TEntity : class, IEntity
    {
        protected string SearchQuery;
        protected Expression<Func<TEntity, bool>> Predicate;
        protected Expression<Func<TEntity, object>> OrderByKeySelector;
        protected Expression<Func<TEntity, object>> OrderByDescKeySelector;
        protected ICollection<Expression<Func<TEntity, object>>> includeDefinitions;
        protected bool WithDefaultIncludes = true;
        protected int? Limit = null;
        protected int? Skip = null;

        /// <summary>
        /// the default constructor
        /// </summary>
        public DataRequestBuilder()
        {
            includeDefinitions = new HashSet<Expression<Func<TEntity, object>>>();
        }

        /// <summary>
        /// true to use the default includes configured in the target entity dataAccess, defaulted to true
        /// </summary>
        /// <param name="value">true to use the default includes false if not</param>
        /// <returns>the builder</returns>
        public DataRequestBuilder<TEntity> AddDefaultIncludes(bool value)
        {
            WithDefaultIncludes = value;
            return this;
        }

        /// <summary>
        /// add the search query
        /// </summary>
        /// <param name="query">the search query</param>
        /// <returns>the builder</returns>
        public DataRequestBuilder<TEntity> AddQuery(string query)
        {
            SearchQuery = query;
            return this;
        }

        /// <summary>
        /// specify the entities to be included
        /// </summary>
        /// <param name="includes">the include expression</param>
        /// <returns>the builder</returns>
        public DataRequestBuilder<TEntity> AddInclude(Expression<Func<TEntity, object>> include)
        {
            includeDefinitions.Add(include);
            return this;
        }

        /// <summary>
        /// add the predicate query
        /// </summary>
        /// <param name="predicate">the predicate value</param>
        /// <returns>the builder</returns>
        public DataRequestBuilder<TEntity> AddPredicate(Expression<Func<TEntity, bool>> predicate)
        {
            Predicate = predicate;
            return this;
        }

        /// <summary>
        /// add the orderBy query
        /// </summary>
        /// <param name="orderByKeySelector">orderBy selector</param>
        /// <returns>the builder</returns>
        public DataRequestBuilder<TEntity> AddOrderBy(SortDirection orderBy, Expression<Func<TEntity, object>> orderByKeySelector)
        {
            switch (orderBy)
            {
                case SortDirection.Descending:
                    OrderByDescKeySelector = orderByKeySelector;
                    break;
                case SortDirection.Ascending:
                    OrderByKeySelector = orderByKeySelector;
                    break;
            }

            return this;
        }

        /// <summary>
        /// add the orderBy query
        /// </summary>
        /// <param name="orderByKeySelector">orderBy selector</param>
        /// <returns>the builder</returns>
        public DataRequestBuilder<TEntity> AddSkip(int skip)
        {
            Skip = skip;
            return this;
        }

        /// <summary>
        /// add the orderBy query
        /// </summary>
        /// <param name="orderByKeySelector">orderBy selector</param>
        /// <returns>the builder</returns>
        public DataRequestBuilder<TEntity> AddLimit(int limit)
        {
            Limit = limit;
            return this;
        }

        /// <summary>
        /// build and return the DataRequest instant
        /// </summary>
        /// <returns>DataRequest instant</returns>
        public virtual DataRequest<TEntity> Build()
        {
            var dataRequestInstant = ReflectionHelper
                .CreateInstantOf<DataRequest<TEntity>>(Array.Empty<Type>(), null);

            dataRequestInstant.Predicate = Predicate;
            dataRequestInstant.OrderByKeySelector = OrderByKeySelector;
            dataRequestInstant.IncludeDefinitions = includeDefinitions.ToArray();
            dataRequestInstant.OrderByDescKeySelector = OrderByDescKeySelector;
            dataRequestInstant.WithDefaultIncludes = WithDefaultIncludes;
            dataRequestInstant.SearchQuery = SearchQuery;
            dataRequestInstant.Limit = Limit;
            dataRequestInstant.Skip = Skip;

            EmptyFields();
            return dataRequestInstant;
        }

        /// <summary>
        /// clear the values of the fields
        /// </summary>
        protected virtual void EmptyFields()
        {
            SearchQuery = null;
            Predicate = null;
            OrderByKeySelector = null;
            includeDefinitions.Clear();
            OrderByDescKeySelector = null;
        }
    }
}
