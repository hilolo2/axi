﻿namespace Axiobat.Application.Exceptions
{
    using Domain.Exceptions;
    using Microsoft.Extensions.Logging;
    using Models;
    using System;

    /// <summary>
    /// the base exception class for all application public logic Exception
    /// </summary>
    [Serializable]
    public class AxiobatInternalException : AxiobatException
    {
        /// <summary>
        /// the log event associated with this exception
        /// </summary>
        [NonSerialized]
        protected EventId _logEventId;

        /// <summary>
        /// the result instant associated with the exception
        /// </summary>
        public Result Result { get; }

        /// <summary>
        /// Initializes a new instance of the System.Exception class with a specified error message, and the message code set to <see cref="CodeMessage.publicError"/>
        /// message.</summary>
        /// <param name="message">The message that describes the error.</param>
        public AxiobatInternalException(string message)
            : this(message, Axiobat.MessageCode.InternalError) { }

        /// <summary>
        /// Initializes a new instance of the System.Exception class with a specified error
        /// message.</summary>
        /// <param name="message">The message that describes the error.</param>
        /// <param name="codeMessage">The <see cref="CodeMessage"/> associated with this exception.</param>
        public AxiobatInternalException(string message, MessageCode codeMessage)
            : base(message)
        {
            Result = Result.Failed(message, codeMessage);
            _logEventId = LogEvent.AxiobatpublicException;
        }

        /// <summary>
        /// log the exception details
        /// </summary>
        /// <param name="logger">the logger instant</param>
        public virtual void Log(ILogger logger)
            => logger.LogError(_logEventId, this, "an public error has occurred, code: {code}", (int)Result.MessageCode);
    }
}
