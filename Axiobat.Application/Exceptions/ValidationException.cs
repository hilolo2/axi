﻿namespace Axiobat.Application.Exceptions
{
    using Domain.Exceptions;
    using Models;
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// the base exception class for all validation Exception
    /// </summary>
    [Serializable]
    public class ValidationException : AxiobatException
    {
        /// <summary>
        /// the result instant associated with the exception
        /// </summary>
        public Result Result { get; }

        /// <summary>
        /// the validation info if any
        /// </summary>
        public object ValidationInfo { get; }

        /// <summary>
        /// Initializes a new instance of the System.Exception class with a specified error
        /// message.</summary>
        /// <param name="message">The message that describes the error.</param>
        /// <param name="messageCode">The <see cref="MessageCode"/> associated with this exception.</param>
        public ValidationException(string message, MessageCode messageCode)
            : this(message, messageCode, null) { }

        /// <summary>
        /// Initializes a new instance of the System.Exception class with a specified error
        /// message.</summary>
        /// <param name="message">The message that describes the error.</param>
        /// <param name="messageCode">The <see cref="MessageCode"/> associated with this exception.</param>
        public ValidationException(string message, MessageCode messageCode, object validationInfo)
            : base(message, messageCode)
        {
            Result = Result.Failed(message, messageCode);
            ValidationInfo = validationInfo;
        }

        protected ValidationException(SerializationInfo serializationInfo, StreamingContext streamingContext)
            : base(serializationInfo, streamingContext)
        {

        }
    }
}
