﻿namespace Axiobat.Application.Exceptions
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// this exception will be throw when given invalid model type
    /// </summary>
    [Serializable]
    public class ModelTypeIsNotValidException : ValidationException
    {
        public readonly string _propretyName;
        public readonly Type _expectedType;

        /// <summary>
        /// Initializes a new instance of the System.Exception class with a specified error
        /// message.</summary>
        /// <param name="propretyName">the name of the property of the model.</param>
        /// <param name="expectedType">The expected type that the function expecting</param>
        public ModelTypeIsNotValidException(string propretyName, Type expectedType)
            : base($"the given type for {propretyName} is not valid, expecting {expectedType.ToString()}", Axiobat.MessageCode.InvalidData)
        {
            _propretyName = propretyName;
            _expectedType = expectedType;
        }

        /// <summary>
        /// create an instant of <see cref="ModelTypeIsNotValidException"/>
        /// </summary>
        /// <param name="serializationInfo">the <see cref="SerializationInfo"/> instant</param>
        /// <param name="streamingContext">the <see cref="StreamingContext"/> instant</param>
        protected ModelTypeIsNotValidException(SerializationInfo serializationInfo, StreamingContext streamingContext)
            : base (serializationInfo, streamingContext) { }
    }
}
