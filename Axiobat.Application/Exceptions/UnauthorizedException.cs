﻿namespace Axiobat.Application.Exceptions
{
    using System;

    /// <summary>
    /// the validation exception associated with the unauthorized cases
    /// </summary>
    [Serializable]
    public class UnauthorizedException : ValidationException
    {
        private static readonly string _message = "you're not authorized to perform this action";

        /// <summary>
        /// Initializes a new instance of the System.Exception class with a specified error
        /// message.
        /// </summary>
        /// <param name="message">The message that describes the error.</param>
        public UnauthorizedException() : base(_message, Axiobat.MessageCode.Unauthorized) { }

        /// <summary>
        /// Initializes a new instance of the System.Exception class with a specified error
        /// message.
        /// </summary>
        /// <param name="message">The message that describes the error.</param>
        public UnauthorizedException(string message) : base(message, Axiobat.MessageCode.Unauthorized) { }

        /// <summary>
        /// Initializes a new instance of the System.Exception class with a specified error
        /// message.
        /// </summary>
        /// <param name="message">The message that describes the error.</param>
        /// <param name="messageCode">The message code that describes the error.</param>
        public UnauthorizedException(string message, MessageCode messageCode) : base(message, messageCode) { }

        /// <summary>
        /// Initializes a new instance of the System.Exception class with serialized data.
        /// </summary>
        /// <param name="serializationInfo">The System.Runtime.Serialization.SerializationInfo that holds the serialized object data about the exception being thrown.</param>
        /// <param name="streamingContext">The System.Runtime.Serialization.StreamingContext that contains contextual information about the source or destination.</param>
        protected UnauthorizedException(System.Runtime.Serialization.SerializationInfo serializationInfo, System.Runtime.Serialization.StreamingContext streamingContext)
            : base(_message, Axiobat.MessageCode.NotFound)
        {
            throw new NotImplementedException();
        }
    }
}
