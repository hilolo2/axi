﻿namespace Axiobat.Application.Services.Localization
{
    /// <summary>
    /// the translation service
    /// </summary>
    public interface ITranslationService
    {
        /// <summary>
        /// retrieve the translation version of the text if not found the text it self will be returned
        /// </summary>
        /// <param name="text">the text to translate</param>
        /// <param name="args">the list of arguments</param>
        /// <returns>the translation version of the text</returns>
        string Get(string text, params object[] args);
    }
}
