﻿namespace Axiobat.Application.Services.Configuration
{
    using Application.Models;
    using Domain.Entities;
    using Domain.Enums;
    using System.Threading.Tasks;

    /// <summary>
    /// service for managing classification
    /// </summary>
    public interface IClassificationService
    {
        Task PutAsync(ClassificationPutModel[] model);

        Task<ListResult<TOut>> GetAllAsync<TOut>();

        Task<Result<TOut>> GetByIdAsync<TOut>(int categoryId);

        Task<TOut[]> GetCategoriesByTypeAsync<TOut>(ClassificationType categoryType);

        Task<Result> DeleteAsync(int categoryId);

        Task<Classification> GetDefaultClassificationAsync();
    }
}
