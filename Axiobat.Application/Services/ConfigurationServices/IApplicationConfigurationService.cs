﻿namespace Axiobat.Application.Services.Configuration
{
    using Models;
    using Domain.Enums;
    using System.Threading.Tasks;
    using Domain.Entities;

    /// <summary>
    /// service for <see cref="Setting"/>
    /// </summary>
    public interface IApplicationConfigurationService : ISynchronize
    {
        /// <summary>
        /// get the email template id
        /// </summary>
        /// <param name="emailTemplateName">the name of the template, one of <see cref="EmailTemplates"/> values</param>
        /// <returns>the id of the email template</returns>
        Task<string> GetEmailTemplateIdAsync(string emailTemplateName);

        /// <summary>
        /// get the email address
        /// </summary>
        /// <param name="emailId">the id of the email to retrieve, one of <see cref="Emails"/></param>
        /// <returns>the email address</returns>
        Task<string> GetEmailAsync(string emailId);

        /// <summary>
        /// retrieve the document configuration of the given document type
        /// </summary>
        /// <param name="documentType">the type of the document to retrieve the configuration for it</param>
        /// <returns>the configuration of  the document</returns>
        Task<DocumentConfiguration> GetDocumentConfigAsync(DocumentType documentType);

        /// <summary>
        /// get the setting of the given type
        /// </summary>
        /// <param name="settingType">the type of the setting to be retrieved, must be one of <see cref="ApplicationSettings"/> values</param>
        /// <returns>the setting value as a string</returns>
        Task<string> GetAsync(string settingType);

        /// <summary>
        /// update the value of the configuration based on the given configuration type
        /// </summary>
        /// <param name="configurationType">the configuration type</param>
        /// <param name="value">the updated value of the configuration</param>
        Task UpdateAsync(string configurationType, string value);

        /// <summary>
        /// get the setting of the given type, and deserialize the setting value to the given type
        /// </summary>
        /// <typeparam name="TOut">the type of the settings</typeparam>
        /// <param name="settingType">the type of the setting to be retrieved, must be one of <see cref="ApplicationSettings"/> values</param>
        /// <returns>the setting value as an object of the given type</returns>
        Task<TOut> GetAsync<TOut>(string settingType);

        /// <summary>
        /// use this method to increment the reference counter by one
        /// </summary>
        /// <typeparam name="TEntity">the type of the entity to increment the reference for it</typeparam>
        /// <returns>returns>
        Task IncrementReferenceAsync<TEntity>();

        /// <summary>
        /// use this method to increment the reference counter by one
        /// </summary>
        /// <param name="docType">the document to increment the counter for it</param>
        /// <returns>the operation result</returns>
        Task IncrementReferenceAsync(DocumentType docType);

        /// <summary>
        /// generate a reference of the given doc type form the numerotation document
        /// </summary>
        /// <param name="docType">the type of the document to generate the reference for it</param>
        /// <returns>the reference value</returns>
        Task<string> GenerateRefereceAsync(DocumentType docType);

        /// <summary>
        /// retrieve list of entities paginated
        /// </summary>
        /// <param name="filter">the filter options</param>
        /// <returns>the list of entities paginated</returns>
        Task<PagedResult<TModel>> GetAllAsync<TEntity, TModel>(FilterOptions filter) where TEntity : Entity;

        /// <summary>
        /// get list of all entities
        /// </summary>
        /// <typeparam name="TEntity">the entity type</typeparam>
        /// <typeparam name="TModel">the model type</typeparam>
        /// <returns>the list result</returns>
        Task<ListResult<TModel>> GetAllAsync<TEntity, TModel>() where TEntity : Entity;

        /// <summary>
        /// retrieve list of all taxes
        /// </summary>
        /// <returns>the list of taxes</returns>
        Task<Result<TModel>> GetByIdAsync<TKey, TEntity, TModel>(TKey entityId) where TEntity : Entity<TKey>;

        /// <summary>
        /// retrieve
        /// </summary>
        /// <returns></returns>
        Task<ListResult<Country>> GetAllCountriesAsync();

        /// <summary>
        /// update the entity if exist, if not exist the entity will be added
        /// </summary>
        /// <typeparam name="TModel">the model type</typeparam>
        /// <typeparam name="TEntity">the entity type</typeparam>
        /// <typeparam name="TKey">type of the entity key</typeparam>
        /// <param name="model">the model instant</param>
        /// <returns>the operation result</returns>
        Task<Result<TModel>> PutAsync<TModel, TEntity, TKey>(TModel model)
            where TModel : IModel<TEntity, TKey>, IUpdateModel<TEntity>
            where TEntity : Entity<TKey>;

        /// <summary>
        /// get the country with the given code
        /// </summary>
        /// <param name="countryCode">the country code</param>
        /// <returns>the country</returns>
        Task<Result<Country>> GetCountryByCodeAsync(string countryCode);

        /// <summary>
        /// delete the entity with the given id
        /// </summary>
        /// <typeparam name="TEntity">the id of the entity to be deleted</typeparam>
        /// <param name="labelId">the label id</param>
        /// <returns>the operation result</returns>
        Task<Result> DeleteAsync<TEntity, TKey>(TKey entityId) where TEntity : Entity<TKey>;

        /// <summary>
        /// get the document type that has the <see cref="WorkshopDocumentType.AffectedStatus"/> and <see cref="WorkshopDocumentType.NewStatus"/> populated
        /// </summary>
        /// <returns>the <see cref="WorkshopDocumentType"/> instant</returns>
        Task<WorkshopDocumentType> GetSpecialDocumentTypeAsync();

        /// <summary>
        /// get the payment method that defines the credit note
        /// </summary>
        /// <returns>the payment method model</returns>
        Task<Result<PaymentMethodModel>> GetCreditNotePaymentMethodAsyn();

        /// <summary>
        /// get list of turnovers goals
        /// </summary>
        /// <returns>the list of result</returns>
        Task<ListResult<TurnoverGoalModel>> GetTurnoverGoalsAsync();
    }
}
