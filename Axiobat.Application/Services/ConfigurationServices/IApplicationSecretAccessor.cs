﻿namespace Axiobat.Application.Services.Configuration
{
    using Enums;
    using Models;
    using System.Threading.Tasks;

    /// <summary>
    /// the application secrets accessor service
    /// </summary>
    public interface IApplicationSecretAccessor
    {
        /// <summary>
        /// get the connection string based on the given type
        /// </summary>
        /// <param name="type">the type of the connection string</param>
        /// <returns>the connection string value</returns>
        Task<string> GetAppDbConnectionStringAsync(ConnectionStringType type);

        /// <summary>
        /// get the authentication SignInKey
        /// </summary>
        /// <returns>the SignInKey value</returns>
        Task<string> GetSignInKeyAsync();

        /// <summary>
        /// get the Google Calendar Options
        /// </summary>
        /// <returns>the <see cref="GoogleCalendarOptions"/> instant</returns>
        Task<GoogleCalendarOptions> GetGoogleCalendarOptions();
    }
}
