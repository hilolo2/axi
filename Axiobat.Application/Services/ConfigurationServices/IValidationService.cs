﻿namespace Axiobat.Application.Services.Configuration
{
    using Models.ValidationModels;
    using Domain.Entities;
    using Domain.Enums;
    using System.Threading.Tasks;

    /// <summary>
    /// a global validation service
    /// </summary>
    public interface IValidationService
    {
        /// <summary>
        /// validate if the workshop has any document associated with it
        /// </summary>
        /// <param name="workshop">the workshop instant</param>
        /// <param name="documentTypes">the documents types to validate</param>
        Task WorkshopHasNoDocumentAsync(ConstructionWorkshop workshop, params DocumentType[] documentTypes);

        /// <summary>
        /// check if the given user any documents associated with them
        /// </summary>
        /// <param name="user">the user instant</param>
        /// <returns>the check</returns>
        Task<DeleteUserDocumentValidation> HasDocumentsAsync(User user);

        /// <summary>
        /// check if the given supplier any documents associated with them
        /// </summary>
        /// <param name="supplier">the supplier instant</param>
        /// <returns>the check</returns>
        Task<DeleteUserDocumentValidation> HasDocumentsAsync(Supplier supplier);

        /// <summary>
        /// validate if the given invoice can be canceled
        /// </summary>
        /// <param name="invoice">the invoice to be canceled</param>
        /// <exception cref="Exceptions.ValidationException">if invoice is of type <see cref="InvoiceType.Situation"/> or <see cref="InvoiceType.Situation"/> and has no quote associated with it, or the quote is closed</exception>
        Task CanCancelInvoiceAsync(Invoice invoice);
    }
}