﻿namespace Axiobat.Application.Services.Configuration
{
    using Domain.Entities;
    using Domain.Enums;
    using Domain.Interfaces;
    using Models;
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    /// <summary>
    /// the service for <see cref="GlobalHistory"/>
    /// </summary>
    public interface IHistoryService
    {
        /// <summary>
        /// add a new global history record with the given type
        /// </summary>
        /// <typeparam name="TEntity">the type of the entity being manipulated</typeparam>
        /// <param name="actionType">the type of the action</param>
        Task AddAsync<TEntity>(ChangesHistoryType actionType);

        /// <summary>
        /// add a new global history record with the given type
        /// </summary>
        /// <typeparam name="TEntity">the type of the entity being manipulated</typeparam>
        /// <param name="actionType">the type of the action</param>
        /// <param name="companyId">the id of the company</param>
        /// <param name="userId">the id of the user</param>
        Task AddAsync<TEntity>(ChangesHistoryType actionType, Guid userId);

        /// <summary>
        /// recored a new changes history to the given entity
        /// </summary>
        /// <typeparam name="TEntity">the entity type, must implement <see cref="IRecordable"/></typeparam>
        /// <param name="entity">the entity instant</param>
        /// <param name="ChangesHistory">the changes history information</param>
        void Recored<TEntity>(TEntity entity, Action<ChangesHistoryOptions> ChangesHistory) where TEntity : IRecordable;

        /// <summary>
        /// recored a changes history
        /// </summary>
        /// <param name="entity">the entity implementing <see cref="IRecordable"/></param>
        /// <param name="actionType">the action type</param>
        /// <param name="information">the information</param>
        /// <param name="fields">the entity champs changes</param>
        void Recored<TEntity>(TEntity entity, ChangesHistoryType actionType, string information = null, ICollection<ChangedField> fields = null) where TEntity : IRecordable;

        /// <summary>
        /// recored a changes history
        /// </summary>
        /// <param name="history">the entity history collection</param>
        /// <param name="actionType">the action type</param>
        /// <param name="fields">the entity champs changes</param>
        void Recored(List<ChangesHistory> history, ChangesHistoryType actionType, string information = null, ICollection<ChangedField> fields = null);

        /// <summary>
        /// recored a changes history
        /// </summary>
        /// <param name="history">the entity history collection</param>
        /// <param name="actionType">the action type</param>
        /// <param name="user">the user how made the change</param>
        /// <param name="fields">the entity champs changes</param>
        void Recored(List<ChangesHistory> history, ChangesHistoryType actionType, MinimalUser user, string information = null, ICollection<ChangedField> fields = null);

        /// <summary>
        /// detect the changes and add them to the history of the associate how is updated
        /// </summary>
        /// <param name="model">the update model, with all new values</param>
        /// <param name="entity">the associate in database</param>
        void RecoredUpdate<TEntity, TModel>(TEntity entity, TModel model) where TEntity : IRecordable;

        /// <summary>
        /// a method to detect the fields that has been changed between the current and the updated value
        /// </summary>
        /// <typeparam name="T">the type of the value to compare</typeparam>
        /// <param name="current">the current value or the original value</param>
        /// <param name="updated">the updated or the new value</param>
        /// <returns>the list of changes</returns>
        List<ChangedField> GetChangedFields<T>(T current, T updated);
    }
}
