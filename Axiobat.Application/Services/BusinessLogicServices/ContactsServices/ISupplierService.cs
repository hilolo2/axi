﻿namespace Axiobat.Application.Services.Contacts
{
    using Models;
    using Domain.Entities;
    using System.Threading.Tasks;

    /// <summary>
    /// service for <see cref="Client"/>
    /// </summary>
    public interface ISupplierService : IExternalPartnersService<Supplier>
    {
        /// <summary>
        /// retrieve the list of products of the supplier
        /// </summary>
        /// <typeparam name="TOut"></typeparam>
        /// <param name="supplierId">the id of the supplier</param>
        /// <returns>the list of the out put result</returns>
        Task<ListResult<TOut>> GetSupplierProductsAsync<TOut>(string supplierId);

        /// <summary>
        /// get the list of suppliers
        /// </summary>
        /// <returns>the list of suppliers</returns>
        Task<ExternalPartnerMinimalInfo[]> GetSuppliersForImportAsync();
    }
}
