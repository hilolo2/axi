﻿namespace Axiobat.Application.Services.Contacts
{
    using Configuration;
    using Domain.Entities;
    using FileService;

    /// <summary>
    /// the base service for contact entities, <see cref="Client"/> and <see cref="Supplier"/>
    /// </summary>
    public interface IExternalPartnersService<TEntity>
        : IDataService<TEntity, string>,
          IMemoService,
          IReferenceService,
          ISynchronize
        where TEntity : ExternalPartner
    {

    }
}
