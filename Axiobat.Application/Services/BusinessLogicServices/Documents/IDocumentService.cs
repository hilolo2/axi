﻿namespace Axiobat.Application.Services.Documents
{
    using Configuration;
    using Domain.Entities;
    using System.Threading.Tasks;

    /// <summary>
    /// the service for <see cref="TEntity"/>
    /// </summary>
    /// <typeparam name="TEntity">the type of the entity</typeparam>
    public interface IDocumentService<TEntity> : IDataService<TEntity, string>, IReferenceService, ISynchronize
        where TEntity : Entity<string>
    {
        /// <summary>
        /// get the count of documents the given workshop owns
        /// </summary>
        /// <param name="workshopId">the id of the workshop</param>
        /// <returns>the count of documents</returns>
        Task<int> GetWorkShopDocumentsCountAsync(string workshopId);
    }
}
