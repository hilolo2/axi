﻿namespace Axiobat.Application.Services.Documents
{
    using Domain.Entities;
    using FileService;
    using Models;
    using System.Threading.Tasks;

    /// <summary>
    /// the service for <see cref="SupplierOrder"/>
    /// </summary>
    public interface ISupplierOrderService : IDocumentService<SupplierOrder>, IMemoService
    {
        /// <summary>
        /// generate the Supplier Orders from the given quote
        /// </summary>
        /// <param name="document">the Quote document instant</param>
        /// <returns>the operation result</returns>
        Task<ListResult<MinimalDocumentModel>> GenerateFromQuoteAsync(Quote document);

        /// <summary>
        /// update the status of the given supplier orders
        /// </summary>
        /// <param name="status">the status to assign</param>
        /// <param name="supplierOrdersIds">the list of supplier orders to be updated</param>
        /// <returns>the operation result</returns>
        Task UpdateStatusAsync(string status, params string[] supplierOrdersIds);

        /// <summary>
        /// get the workshop supplier orders with the given id
        /// </summary>
        /// <param name="workshopId">the id of the workshop</param>
        /// <param name="supplierId">the id of the supplier</param>
        /// <param name="status">the documents status</param>
        /// <returns>the list of the workshops</returns>
        Task<ListResult<SupplierOrderModel>> GetWorkshopSupplierOrdersBySupplierIdAsync(string workshopId, string supplierId, params string[] status);
    }
}
