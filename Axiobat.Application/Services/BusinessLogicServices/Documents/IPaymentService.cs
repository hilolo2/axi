﻿namespace Axiobat.Application.Services.Documents
{
    using Application.Models;
    using Domain.Entities;
    using System.Threading.Tasks;

    /// <summary>
    /// the service for <see cref="Payment"/>
    /// </summary>
    public interface IPaymentService : IDataService<Payment, string>
    {
        /// <summary>
        /// get the payment total balance
        /// </summary>
        /// <returns>the payment balance</returns>
        Task<Result<PaymentBalance>> GetPaymentsBalanceAsync();

        /// <summary>
        /// create a transfer operation between two accounts
        /// </summary>
        /// <param name="model">the model user for creating the transfer operation</param>
        /// <returns>the operation result</returns>
        Task<Result> CreateTransferOperationAsync(PaymentCreatetransferModel model);
    }
}
