﻿namespace Axiobat.Application.Services.Documents
{
    using Models;
    using System.Threading.Tasks;

    /// <summary>
    /// a service for duplicating the documents
    /// </summary>
    public interface IDuplicableService
    {
        /// <summary>
        /// duplicate the document with the given id
        /// </summary>
        /// <typeparam name="TOut">the output type</typeparam>
        /// <param name="documentId"></param>
        /// <returns>the desired output</returns>
        Task<Result<TOut>> DuplicateAsync<TOut>(string documentId);
    }
}
