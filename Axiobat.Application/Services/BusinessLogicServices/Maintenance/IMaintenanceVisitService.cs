﻿namespace Axiobat.Application.Services.Maintenance
{
    using Models;
    using Domain.Entities;
    using System.Threading.Tasks;

    /// <summary>
    /// the MaintenanceVisit service
    /// </summary>
    public interface IMaintenanceVisitService : IDataService<MaintenanceVisit, string>
    {
        /// <summary>
        /// get the list of all contract maintenance visits
        /// </summary>
        /// <typeparam name="Tout">the type of the output</typeparam>
        /// <param name="contractId">the id of the contract</param>
        /// <returns>list of contract maintenance visits</returns>
        Task<ListResult<MaintenanceVisitModel>> GetAllContractMaintenanceVisitAsync(string contractId);

        /// <summary>
        /// create a maintenance contract that represent the maintenance operation for the given year and month
        /// </summary>
        /// <typeparam name="TOut">the type of the output</typeparam>
        /// <param name="maintenanceContractId">the id of the contract</param>
        /// <param name="year">the year</param>
        /// <param name="month">the month</param>
        /// <returns>the operation result</returns>
        Task<TOut> CreateMaintenanceVisitAsync<TOut>(string maintenanceContractId, int year, int month, ClientDocument client = null);

        /// <summary>
        /// get the MaintenanceVisit for the given contract in the given year and month
        /// </summary>
        /// <typeparam name="TOut">the type of the output</typeparam>
        /// <param name="maintenanceContractId">the id of the contract</param>
        /// <param name="year">the year</param>
        /// <param name="month">the month</param>
        /// <returns>the desired output</returns>
        Task<Result<MaintenanceVisitModel>> GetMaintenanceVisitAsync(MaintenanceVisitRetrieveOptions options);

        /// <summary>
        /// update status of the maintenance visit with the given id
        /// </summary>
        /// <param name="maintenanceVisitId">the id of the maintenance visit</param>
        /// <param name="status">the status to set the maintenance visit to it</param>
        Task UpdateStatusAsync(string maintenanceVisitId, string status);
    }
}
