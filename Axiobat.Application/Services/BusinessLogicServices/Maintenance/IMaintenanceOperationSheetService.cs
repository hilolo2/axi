﻿namespace Axiobat.Application.Services.Maintenance
{
    using FileService;
    using Configuration;
    using Domain.Entities;
    using Models;
    using System.Threading.Tasks;

    /// <summary>
    /// the MaintenanceOperationSheet service
    /// </summary>
    public interface IMaintenanceOperationSheetService : IDataService<MaintenanceOperationSheet, string>, IReferenceService, IMemoService, ISynchronize
    {
        /// <summary>
        /// retrieve the operation sheet with the given reference
        /// </summary>
        /// <typeparam name="TOut">the output type</typeparam>
        /// <param name="operationSheetReference">the operation sheet reference</param>
        /// <returns>the operation sheet</returns>
        Task<Result<TOut>> GetByReferenceAsync<TOut>(string operationSheetReference);

        /// <summary>
        /// send the operationSheet with the given id with an email using the given options
        /// </summary>
        /// <param name="operationSheetId">the id of the document</param>
        /// <param name="emailOptions">the email options</param>
        /// <returns>the operation result</returns>
        Task<Result> SendAsync(string operationSheetId, SendEmailOptions emailOptions);

        /// <summary>
        /// update the status of the operation Sheet
        /// </summary>
        /// <param name="operationSheetId">the id of the operation Sheet to update</param>
        /// <param name="model">the model used for updating the status</param>
        /// <returns>the operation result</returns>
        Task<Result> UpdateStatusAsync(string operationSheetId, DocumentUpdateStatusModel model);

        /// <summary>
        /// update the status of the operation Sheet
        /// </summary>
        /// <param name="operationSheetId">the id of the operation Sheet to update</param>
        /// <param name="model">the model used for updating the status</param>
        /// <returns>the operation result</returns>
        Task<Result> UpdateSignatureAsync(string operationSheetId, DocumentUpdateSignatureModel model);
        Task<Result> SetStatusToUndoneAsync(string operationSheetId, MaintenanceOperationSheetMotif model);
    }
}
