﻿namespace Axiobat.Application.Services.Documents
{
    using Models;
    using System.Threading.Tasks;

    /// <summary>
    /// this interface that defines the email document
    /// </summary>
    public interface IEmailDocument<TKey>
    {
        /// <summary>
        /// email the document using the given option
        /// </summary>
        /// <param name="documentId">the id the document</param>
        /// <param name="emailOptions">the email options</param>
        /// <returns>the operation result</returns>
        Task<Result> SendAsync(TKey documentId, SendEmailOptions emailOptions);
    }
}
