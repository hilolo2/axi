﻿namespace Axiobat.Application.Services.FileService
{
    using Application.Enums;
    using Domain.Entities;
    using Domain.Interfaces;
    using Models;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    /// <summary>
    /// the base interface for all File Services
    /// </summary>
    public interface IFileService
    {
        /// <summary>
        /// save the list of files
        /// </summary>
        /// <param name="filesModels">list of files to save</param>
        /// <returns>the operation result</returns>
        Task<Result> SaveAsync(List<FileModel> filesModels);

        /// <summary>
        /// save the given file
        /// </summary>
        /// <param name="model">the file to be saved</param>
        /// <returns>the operation result</returns>
        Task<Result> SaveAsync(FileModel model);

        /// <summary>
        /// save the list of the attachment, and return list of the saved attachments
        /// </summary>
        /// <param name="attachments">list of attachment models to be saved</param>
        /// <returns>list of saved attachments</returns>
        Task<List<Attachment>> SaveAsync(params AttachmentModel[] attachments);

        /// <summary>
        /// delete the list of attachment
        /// </summary>
        /// <param name="attachments">the list of attachment to be deleted</param>
        /// <returns>the count the attachment deleted successful</returns>
        Task<int> DeleteAsync(params Attachment[] attachments);

        /// <summary>
        /// delete the file with the given name
        /// </summary>
        /// <param name="fileId">name of the file to delete</param>
        /// <returns>the operation result</returns>
        Task<Result> DeleteAsync(string fileId);

        /// <summary>
        /// get the file with the given name
        /// </summary>
        /// <param name="fileId">name of the file to retrieve</param>
        /// <returns>the file</returns>
        Task<Result<string>> GetAsync(string fileId);

        /// <summary>
        /// this is used to generate Excel file for the given model list
        /// </summary>
        /// <typeparam name="TModel">the type of model to export</typeparam>
        /// <typeparam name="TExportOptions">the type of data export options</typeparam>
        /// <param name="data">the list of entities to export</param>
        /// <param name="exportOptions">the data export options</param>
        /// <returns>the excel file as byte array</returns>
        byte[] GenerateFile<TModel, TExportOptions>(IEnumerable<TModel> data, TExportOptions exportOptions)
            where TExportOptions : DataExportOptions
            where TModel : IEntity;

        /// <summary>
        /// this is used to generate Excel file for the given model list
        /// </summary>
        /// <typeparam name="TModel">the type of model to export</typeparam>
        /// <typeparam name="TExportOptions">the type of data export options</typeparam>
        /// <param name="data">the entity to export</param>
        /// <param name="exportOptions">the data export options</param>
        /// <returns>the excel file as byte array</returns>
        byte[] GenerateFile<TModel, TExportOptions>(TModel data, TExportOptions exportOptions)
            where TExportOptions : DataExportOptions
            where TModel : IEntity;

        /// <summary>
        /// export the journal data as a byte array
        /// </summary>
        /// <param name="journalData">the journal data</param>
        /// <param name="journalType">the type of the journal</param>
        /// <returns>the file as a byte array</returns>
        byte[] ExportJournal(IEnumerable<JournalEntry> journalData, JournalType journalType);

        byte[] ExportContrat(MaintenanceContract contratData);

        byte[] generateInvoicebyFilter(ExportByPeriodByte model, IEnumerable<DocumentSharedModel> documents);

        byte[] generateInvoicebyFilterClient(IEnumerable<Invoice> documents);

        byte[] generateCreditNotebyFilterClient(IEnumerable<CreditNote> documents);
    }
}
