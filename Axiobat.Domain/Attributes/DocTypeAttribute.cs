﻿namespace Axiobat
{
    using Domain.Enums;

    /// <summary>
    /// use this attribute to define the docType value of a class
    /// </summary>
    [System.AttributeUsage(System.AttributeTargets.Class, Inherited = true, AllowMultiple = false)]
    public sealed class DocTypeAttribute : System.Attribute
    {
        /// <summary>
        /// Add A docType to the given class
        /// </summary>
        /// <param name="docType">the docType</param>
        public DocTypeAttribute(DocumentType docType)
        {
            DocType = docType;
        }

        /// <summary>
        /// the docType
        /// </summary>
        public DocumentType DocType { get; }
    }
}
