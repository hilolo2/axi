﻿namespace Axiobat.Domain.Entities
{
    /// <summary>
    /// the default value configuration
    /// </summary>
    public partial class DefaultValue
    {
        /// <summary>
        /// the selling price
        /// </summary>
        public float SalesPrice { get; set; }

        /// <summary>
        /// the baying price
        /// </summary>
        public float BuyingPrice { get; set; }

        /// <summary>
        /// the cart cost
        /// </summary>
        [Newtonsoft.Json.JsonProperty("cartCost")]
        public float BasketCost { get; set; }

        /// <summary>
        /// the displacement cost
        /// </summary>
        public float DisplacementCost { get; set; }

        /// <summary>
        /// the starting hour
        /// </summary>
        public string StartingHour { get; set; }

        /// <summary>
        /// the ending hour
        /// </summary>
        public string EndingHour { get; set; }
        
        /// <summary>
        /// the default VAT
        /// </summary>
        public string DefaultVAT { get; set; }
    }
}
