﻿namespace Axiobat.Domain.Entities
{
    using Enums;
    using System.Collections.Generic;

    /// <summary>
    /// this class defines a classification
    /// </summary>
    [DocType(DocumentType.Classification)]
    public partial class Classification : Entity<int>
    {
        /// <summary>
        /// the type of the document
        /// </summary>
        public DocumentType DocumentType => DocumentType.Classification;

        /// <summary>
        /// the type of the classification
        /// </summary>
        public ClassificationType Type { get; set; }

        /// <summary>
        /// the label of the category
        /// </summary>
        public string Label { get; set; }

        /// <summary>
        /// a description of the category
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// the id of the chart of account associated with this classification
        /// </summary>
        public string ChartAccountItemId { get; set; }

        /// <summary>
        /// the id of the parent classification
        /// </summary>
        public int? ParentId { get; set; }

        /// <summary>
        /// the chart of account associated with this classification
        /// </summary>
        public ChartAccountItem ChartAccountItem { get; set; }

        /// <summary>
        /// parent classification
        /// </summary>
        public Classification Parent { get; set; }

        /// <summary>
        /// list of sub classification associated with this classification
        /// </summary>
        public ICollection<Classification> SubClassification { get; set; }

        /// <summary>
        /// the list of products associated with this category
        /// </summary>
        public ICollection<Product> Products { get; set; }
    }

    /// <summary>
    /// partial part for <see cref="Classification"/>
    /// </summary>
    public partial class Classification : Entity<int>, System.IEquatable<Classification>
    {
        /// <summary>
        /// the id of the classification for Assets Purchases
        /// </summary>
        public const int AssetsPurchases = 7;

        public Classification()
        {
            Products = new HashSet<Product>();
            SubClassification = new HashSet<Classification>();
        }

        /// <summary>
        /// build the search terms of the entity
        /// </summary>
        public override void BuildSearchTerms()
            => SearchTerms = $"{Label}";

        /// <inheritdoc/>
        public override string ToString()
            => $"{Label}";

        /// <summary>
        /// check if the given object equals the c
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public bool Equals(Classification other)
            => !(other is null) &&
                    other.Id == other.Id &&
                    other.Label == Label &&
                    other.ParentId == ParentId;

        /// <summary>
        /// check if the given object equals the c
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            if (obj is null) return false;
            if (obj.GetType() != typeof(Classification)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return Equals(obj as Classification);
        }

        /// <summary>
        /// get the hash code value of the entity
        /// </summary>
        /// <returns>the has value</returns>
        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = -1880813789;
                hashCode = hashCode * -1521134295 + EqualityComparer<int>.Default.GetHashCode(Id);
                hashCode = hashCode * -1521134295 + Type.GetHashCode();
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Label);
                hashCode = hashCode * -1521134295 + EqualityComparer<int?>.Default.GetHashCode(ParentId);
                return hashCode;
            }
        }

        public static bool operator ==(Classification left, Classification right)
            => EqualityComparer<Classification>.Default.Equals(left, right);

        public static bool operator !=(Classification left, Classification right)
            => !(left == right);
    }
}
