﻿namespace Axiobat.Domain.Entities
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// a minimal version of the <see cref="PublishingContract"/> entity
    /// </summary>
    public partial class PublishingContractMinimal
    {
        public PublishingContractMinimal()
        {
            Tags = new Dictionary<string, string>();
        }
        /// <summary>
        /// the id of the publishing contract
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// the name of contract
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// the name of file
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// the original file name
        /// </summary>
        public string OrginalFileName { get; set; }

        /// <summary>
        /// the id of associate who associate with this entity
        /// </summary>
        public Guid? UserId { get; set; }

        /// <summary>
        /// list of tags associated with the PublishingContract
        /// </summary>
        public IDictionary<string, string> Tags { get; private set; }

        /// <summary>
        /// the implicit conversion between the <see cref="PublishingContract"/> and <see cref="PublishingContractMinimal"/>
        /// </summary>
        /// <param name="PublishingContract">the <see cref="PublishingContract"/> instant</param>
        public static implicit operator PublishingContractMinimal(PublishingContract publishingContract)
            => publishingContract is null ? null : new PublishingContractMinimal
            {
                Id = publishingContract.Id,
                Tags = publishingContract.Tags,
                Title = publishingContract.Title,
                UserId = publishingContract.UserId,
                FileName = publishingContract.FileName,
                OrginalFileName = publishingContract.OrginalFileName,
            };
    }
}
