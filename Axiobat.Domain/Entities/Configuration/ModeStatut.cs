﻿namespace Axiobat.Domain.Entities
{
    using Axiobat.Domain.Enums;
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// a class the defines the accounting periods
    /// </summary>
    public partial class ModeStatut
    {
        /// <summary>
        /// the starting date of the accounting period
        /// </summary>
        public TypeStatus type { get; set; }

        /// <summary>
        /// the ending date of the accounting period
        /// </summary>
        public string Observation { get; set; }


    }
}
