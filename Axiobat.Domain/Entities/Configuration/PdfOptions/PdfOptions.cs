﻿namespace Axiobat.Domain.Entities.Configuration
{
    /// <summary>
    /// the PDF options
    /// </summary>
    public partial class PdfOptions
    {
        /// <summary>
        /// the header of the pdf
        /// </summary>
        public PDFHeader Header { get; set; }

        /// <summary>
        /// footer of the PDF
        /// </summary>
        public PDFFooter Footer { get; set; }

        /// <summary>
        /// the images associated with the pdf
        /// </summary>
        public PDFImages Images { get; set; }
    }
}
