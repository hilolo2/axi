﻿namespace Axiobat.Domain.Entities.Configuration
{
    using Newtonsoft.Json;

    /// <summary>
    /// Pdf footer
    /// </summary>
    public partial class PDFFooter
    {
        [JsonProperty("line_1")]
        public string Line1 { get; set; }

        [JsonProperty("line_2")]
        public string Line2 { get; set; }

        [JsonProperty("line_3")]
        public string Line3 { get; set; }
    }
}
