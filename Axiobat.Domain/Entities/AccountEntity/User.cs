﻿namespace Axiobat.Domain.Entities
{
    using Domain.Enums;
    using Interfaces;
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// a class that describe a user
    /// </summary>
    [DocType(DocumentType.Agent)]
    public partial class User
    {
        /// <summary>
        /// Last name of the user
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// first name of the user
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// the registration number of the user
        /// </summary>
        public string Reference { get; set; }

        /// <summary>
        /// Gets or sets the user name for this user.
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// Gets or sets the email address for this user.
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets a salted and hashed representation of the password for this user.
        /// </summary>
        public string PasswordHash { get; set; }

        /// <summary>
        /// Gets or sets a telephone number for the user.
        /// </summary>
        public string PhoneNumber { get; set; }

        /// <summary>
        /// the status of the user : Active or Not
        /// </summary>
        public Status Statut { get; set; }

        /// <summary>
        /// date of the last login for the user
        /// </summary>
        public DateTime? LastLogin { get; set; }

        /// <summary>
        /// the id of the role owned by this user
        /// </summary>
        public Guid RoleId { get; set; }

        /// <summary>
        /// the entity modification history
        /// </summary>
        public List<ChangesHistory> ChangesHistory { get; set; }

        /// <summary>
        /// list of the operation sheets this user (Technician) has
        /// </summary>
        public ICollection<OperationSheets_Technicians> OperationSheets { get; set; }

        /// <summary>
        /// list of the operation sheets this user (Technician) has
        /// </summary>
        public ICollection<MaintenanceOperationSheet> MaintenanceOperationSheets { get; set; }

        /// <summary>
        /// list of the PublishingContract this user has published
        /// </summary>
        public ICollection<PublishingContract> PublishedContracts { get; set; }

        /// <summary>
        /// list of the tasks associated with this technician
        /// </summary
        public ICollection<Mission> Missions { get; set; }
    }

    /// <summary>
    /// this part holds all the identity related properties
    /// </summary>
    public partial class User : Entity<Guid>, IRecordable, IReferenceable<User>
    {
        /// <summary>
        /// this id of the demo user
        /// </summary>
        public readonly static Guid DemoUserId = new Guid("2FDFB38B-FDFE-4953-84A5-4B2B7229D01C");

        /// <summary>
        /// the id of the default super admin
        /// </summary>
        public readonly static Guid SuperAdmin = new Guid("DC079D59-044B-433E-8B26-685B0DB79376");

        /// <summary>
        /// Initializes a new instance of <see cref="IdentityUser"/>.
        /// </summary>
        /// <remarks>
        /// The Id property is initialized to form a new GUID string value.
        /// </remarks>
        public User()
        {
            Id = Guid.NewGuid();
            SecurityStamp = Guid.NewGuid().ToString();

            Missions = new HashSet<Mission>();
            ChangesHistory = new List<ChangesHistory>();
            OperationSheets = new HashSet<OperationSheets_Technicians>();
            MaintenanceOperationSheets = new HashSet<MaintenanceOperationSheet>();
        }

        /// <summary>
        /// Initializes a new instance of <see cref="IdentityUser"/>.
        /// </summary>
        /// <param name="userName">The user name.</param>
        /// <remarks>
        /// The Id property is initialized to form a new GUID string value.
        /// </remarks>
        public User(string userName) : this()
        {
            UserName = userName;
        }

        /// <summary>
        /// user full name
        /// </summary>
        public string FullName => $"{FirstName} {LastName}";

        /// <summary>
        /// Gets or sets the normalized user name for this user.
        /// </summary>
        public string NormalizedUserName { get; set; }

        /// <summary>
        /// Gets or sets the normalized email address for this user.
        /// </summary>
        public string NormalizedEmail { get; set; }

        /// <summary>
        /// Gets or sets a flag indicating if a user has confirmed their email address.
        /// </summary>
        /// <value>True if the email address has been confirmed, otherwise false.</value>
        public bool EmailConfirmed { get; set; }

        /// <summary>
        /// Gets or sets a flag indicating if a user has confirmed their telephone address.
        /// </summary>
        /// <value>True if the telephone number has been confirmed, otherwise false.</value>
        public bool PhoneNumberConfirmed { get; set; }

        /// <summary>
        /// Gets or sets a flag indicating if two factor authentication is enabled for this user.
        /// </summary>
        /// <value>True if 2fa is enabled, otherwise false.</value>
        public bool TwoFactorEnabled { get; set; }

        /// <summary>
        /// Gets or sets a flag indicating if the user could be locked out.
        /// </summary>
        /// <value>True if the user could be locked out, otherwise false.</value>
        public bool LockoutEnabled { get; set; }

        /// <summary>
        /// A random value that must change whenever a users credentials change (password changed, login removed)
        /// </summary>
        public string SecurityStamp { get; set; }

        /// <summary>
        /// A random value that must change whenever a user is persisted to the store
        /// </summary>
        public string ConcurrencyStamp { get; set; } = Guid.NewGuid().ToString();

        /// <summary>
        /// Gets or sets the date and time, in UTC, when any user lockout ends.
        /// </summary>
        /// <remarks>
        /// A value in the past means the user is not locked out.
        /// </remarks>
        public DateTimeOffset? LockoutEnd { get; set; }

        /// <summary>
        /// Gets or sets the number of failed login attempts for the current user.
        /// </summary>
        public int AccessFailedCount { get; set; }

        /// <summary>
        /// the Roles the user belongs to
        /// </summary>
        public Role Role { get; set; }

        /// <summary>
        /// build the search string for querying users
        /// </summary>
        public override void BuildSearchTerms()
            => SearchTerms = $"{UserName} {Email} {FirstName} {LastName}".ToLower();

        public bool CanIncrementOnCreate() => true;

        public bool CanIncrementOnUpdate(User oldState) => false;

        /// <summary>
        /// the string representation of the object
        /// </summary>
        /// <returns></returns>
        public override string ToString()
            => $"{UserName} : {FirstName} {LastName}";
    }
}
