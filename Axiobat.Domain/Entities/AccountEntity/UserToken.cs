﻿namespace Axiobat.Domain.Entities
{
    using System;

    /// <summary>
    /// Represents an authentication token for a user.
    /// </summary>
    public class UserToken
    {
        /// <summary>
        /// Gets or sets the primary key of the user that the token belongs to.
        /// </summary>
        public virtual Guid UserId { get; set; }

        /// <summary>
        /// Gets or sets the LoginProvider this token is from.
        /// </summary>
        public virtual string LoginProvider { get; set; }

        /// <summary>
        /// Gets or sets the name of the token.
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// Gets or sets the token value.
        /// </summary>
        public virtual string Value { get; set; }

        /// <summary>
        /// the user who own this Token
        /// </summary>
        public User User { get; set; }
    }
}
