﻿namespace Axiobat.Domain.Entities
{
    using Enums;

    /// <summary>
    /// this class defines a mission
    /// </summary>
    [DocType(DocumentType.Mission)]
    public class MissionType : Entity<int>
    {
        /// <summary>
        /// the mission Type
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// the kind of mission this type belongs to it
        /// </summary>
        public string MissionKind { get; set; }

        /// <summary>
        /// the build the search Terms of the entity
        /// </summary>
        public override void BuildSearchTerms()
            => SearchTerms = $"{Value}";
    }
}
