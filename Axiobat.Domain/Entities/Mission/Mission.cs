﻿namespace Axiobat.Domain.Entities
{
    using Enums;
    using Newtonsoft.Json.Linq;
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// this class defines a mission
    /// </summary>
    [DocType(DocumentType.Mission)]
    public partial class Mission
    {
        /// <summary>
        /// the type of the Document
        /// </summary>
        /// Mission
        public DocumentType DocumentType => DocumentType.Mission;

        /// <summary>
        /// the kind of the mission one of <see cref="Constants.MissionKind"/>
        /// </summary>
        public string MissionKind { get; set; }

        /// <summary>
        /// the type of the mission
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// the title of the of the task
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Object of the task
        /// </summary>
        public string Object { get; set; }

        /// <summary>
        /// the phone number of client
        /// </summary>
        public string PhoneNumber { get; set; }

        /// <summary>
        /// Type the status of the task
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// the Technician worked all day
        /// </summary>
        public bool AllDayLong { get; set; }

        /// <summary>
        /// the starting date of the task
        /// </summary>
        public DateTime StartingDate { get; set; }

        /// <summary>
        /// the end date of the task
        /// </summary>
        public DateTime? EndingDate { get; set; }

        /// <summary>
        /// the duration of the mission, saved as HH:MM
        /// </summary>
        public string Duration { get; set; }

        /// <summary>
        ///  the id of the Client that this ETask belongs to it
        /// </summary>
        public string ClientId { get; set; }

        /// <summary>
        /// the id of the technicien
        /// </summary>
        public Guid TechnicianId { get; set; }

        /// <summary>
        /// the id of the workshop associated with this mission
        /// </summary>
        public string WorkshopId { get; set; }

        /// <summary>
        /// the client associated with this task
        /// </summary>
        public Client Client { get; set; }

        /// <summary>
        /// the technician associated with the task
        /// </summary>
        public User Technician { get; set; }

        /// <summary>
        /// the workshop associated with this mission
        /// </summary>
        public ConstructionWorkshop Workshop { get; set; }

        /// <summary>
        /// list of Additional Information associated with this mission
        /// </summary>
        public ICollection<JObject> AdditionalInfo { get; set; }
    }

    /// <summary>
    /// the partial part for <see cref="Mission"/>
    /// </summary>
    public partial class Mission : Entity<string>
    {
        public Mission()
        {
            Id = GenerateId();
            AdditionalInfo = new HashSet<JObject>();
        }

        /// <summary>
        /// generate an id for the document
        /// </summary>
        /// <returns>the new generated id</returns>
        public string GenerateId() => EntityId.Generate(DocumentType);

        /// <summary>
        /// build the search terms of the entity
        /// </summary>
        public override void BuildSearchTerms()
              => SearchTerms = $"{Title} {Object}".ToLower();
    }
}
