﻿namespace Axiobat.Domain.Entities
{
    /// <summary>
    /// a class that defines an Material
    /// </summary>
    public partial class Material
    {
        /// <summary>
        /// the id
        /// </summary>
        public string id { get; set; }

        /// <summary>
        /// the brand
        /// </summary>
        public string Brand { get; set; }

        /// <summary>
        /// the model
        /// </summary>
        public string Model { get; set; }

        /// <summary>
        /// the serialNumber
        /// </summary>
        public string SerialNumber { get; set; }

        /// <summary>
        /// the designation
        /// </summary>
        public string Designation { get; set; }

  
    }

    /// <summary>
    /// partial part for <see cref="Address"/>
    /// </summary>
    public partial class Material
    {
        /// <summary>
        /// create an instant of <see cref="Address"/>
        /// </summary>
        public Material()
        {

        }

        public Material(string brand, string model, string serialNumber, string designation)
        {
            Brand = brand;
            Model = model;
            SerialNumber = serialNumber;
            Designation = designation;
        }
    }
}
