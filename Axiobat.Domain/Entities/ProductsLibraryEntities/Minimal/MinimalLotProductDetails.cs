﻿namespace Axiobat.Domain.Entities
{
    /// <summary>
    /// the minimal information about a <see cref="Lot"/> <see cref="ProductDetails"/> relationship
    /// </summary>
    public partial class MinimalLotProductDetails
    {
        /// <summary>
        /// the id of the product
        /// </summary>
        public string ProductId { get; set; }

        /// <summary>
        /// the id of the lot
        /// </summary>
        public string LotId { get; set; }

        /// <summary>
        /// the quantity of the products in the Lot
        /// </summary>
        public int Quantity { get; set; }

        /// <summary>
        /// the product associated with the lot
        /// </summary>
        public MinimalProductDetails ProductDetails { get; set; }
    }
}
