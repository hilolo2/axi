﻿namespace Axiobat.Domain.Entities
{
    /// <summary>
    /// this class holds the information about the "line" product type
    /// </summary>
    public partial class MinimalLineDetails
    {
        /// <summary>
        /// the id of the product
        /// </summary>
        public string Id { get; set; }
        /// <summary>
        /// the Designation
        /// </summary>
        public string Designation { get; set; }
        /// <summary>
        /// description
        /// </summary>
        public string Description { get; set; }
    }
}
