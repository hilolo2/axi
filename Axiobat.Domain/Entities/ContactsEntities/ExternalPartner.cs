﻿namespace Axiobat.Domain.Entities
{
    using Enums;
    using Domain.Interfaces;
    using System.Collections.Generic;
    using System;
    using System.Linq;

    /// <summary>
    /// the base class for the <see cref="Client"/> and <see cref="Supplier"/> entities
    /// </summary>
    public abstract partial class ExternalPartner
    {
        /// <summary>
        /// the type of the document
        /// </summary>
        public virtual DocumentType DocumentType => DocumentType.Undefined;

        /// <summary>
        /// the supplier reference, should be unique
        /// </summary>
        public string Reference { get; set; }

        /// <summary>
        /// the client full name
        /// </summary>
        public string FullName => $"{FirstName} {LastName}";

        /// <summary>
        /// first name of the client
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// last name of the client
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// phone number
        /// </summary>
        public string PhoneNumber { get; set; }

        /// <summary>
        /// the Fax
        /// </summary>
        public string LandLine { get; set; }

        /// <summary>
        /// the email of the client, should be in a proper email format but is not required
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// client website
        /// </summary>
        public string Website { get; set; }

        /// <summary>
        /// Directory Identification System institutions
        /// </summary>
        public string Siret { get; set; }

        /// <summary>
        /// Intra-community VAT means value added tax (VAT) applied to commercial transactions between different countries of the European Union.
        /// for more informations <see href="https://debitoor.fr/termes-comptables/tva-intracommunautaire">HERE</see>
        /// </summary>
        public string IntraCommunityVAT { get; set; }

        /// <summary>
        /// the accounting identifier of the client
        /// </summary>
        public string AccountingCode { get; set; }

        /// <summary>
        /// mark an entity as deleted
        /// </summary>
        public bool IsDeleted { get; set; }

        /// <summary>
        /// the list of contacts for this supplier
        /// </summary>
        /// <remarks>
        /// this is JSON Object
        /// </remarks>
        public ICollection<ContactInformation> ContactInformations { get; set; }

        /// <summary>
        /// the history of changes
        /// </summary>
        public List<ChangesHistory> ChangesHistory { get; set; }

        /// <summary>
        /// memos of the supplier
        /// </summary>
        public ICollection<Memo> Memos { get; set; }

        /// <summary>
        /// a note about the quote
        /// </summary>
        public string Note { get; set; }

        /// <summary>
        /// the conditions of the payment
        /// </summary>
        public string PaymentCondition { get; set; }
    }

    /// <summary>
    /// the partial part for <see cref="ContactBase{TEntity}"/>
    /// </summary>
    /// <typeparam name="TEntity">the type of the entity</typeparam>
    public abstract partial class ExternalPartner : Entity<string>, IRecordable, IDeletable
    {
        /// <summary>
        /// construct an instant of <see cref="ContactBase{TEntity}"/>
        /// </summary>
        protected ExternalPartner()
        {
            Id = EntityId.Generate(DocumentType);
            ContactInformations = new HashSet<ContactInformation>();
            ChangesHistory = new List<ChangesHistory>();
            Memos = new List<Memo>();
        }

        /// <summary>
        /// build the search term for the client
        /// </summary>
        public override void BuildSearchTerms()
            => SearchTerms = $"{Reference} {FullName}".ToLower();

        /// <summary>
        /// the string representation of the object
        /// </summary>
        /// <returns></returns>
        public override string ToString()
            => $"{FullName}, code: {AccountingCode}";

        public bool CanIncrementOnCreate() => true;

        public bool CanIncrementOnUpdate(ExternalPartner oldState) => false;
    }
}
