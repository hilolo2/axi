﻿namespace Axiobat.Domain.Entities
{
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// this class defines the client entity used in documents
    /// </summary>
    public partial class ClientDocument : ExternalPartnerDocument
    {
        /// <summary>
        /// the client code
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// the client type one of <see cref="Constants.ClientType"/>
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// list of Addresses of the client
        /// </summary>
        public ICollection<Address> Addresses { get; set; }

        /// <summary>
        /// the Billing Address
        /// </summary>
        public Address BillingAddress => DefaultAddress();

        /// <summary>
        /// get the default address of the client
        /// </summary>
        /// <returns>the client address</returns>
        private Address DefaultAddress()
            => Addresses is null
            ? null
            : Addresses.FirstOrDefault(e => e.IsDefault) ?? Addresses.FirstOrDefault();

        /// <summary>
        /// the implicit conversion between the <see cref="Client"/> and <see cref="ClientMinimalInfo"/>
        /// </summary>
        /// <param name="client">the <see cref="Client"/> instant</param>
        public static implicit operator ClientDocument(Client client)
            => client is null ? null : new ClientDocument
            {
                Id = client.Id,
                Code = client.Code,
                Type = client.Type,
                Note = client.Note,
                Siret = client.Siret,
                Email = client.Email,
                Website = client.Website,
                LandLine = client.LandLine,
                LastName = client.LastName,
                FirstName = client.FirstName,
                Addresses = client.Addresses,
                Reference = client.Reference,
                PhoneNumber = client.PhoneNumber,
                AccountingCode = client.AccountingCode,
                PaymentCondition = client.PaymentCondition,
                IntraCommunityVAT = client.IntraCommunityVAT,
            };
    }
}
