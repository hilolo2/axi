﻿namespace Axiobat.Domain.Entities
{
    /// <summary>
    /// this class defines the supplier entity used in documents
    /// </summary>
    public partial class SupplierDocuement : ExternalPartnerDocument
    {
        /// <summary>
        /// the address of the supplier
        /// </summary>
        public Address Address { get; set; }

        /// <summary>
        /// the implicit conversion between the <see cref="Client"/> and <see cref="ClientMinimalInfo"/>
        /// </summary>
        /// <param name="supplier">the <see cref="Client"/> instant</param>
        public static implicit operator SupplierDocuement(Supplier supplier)
            => supplier is null ? null : new SupplierDocuement
            {
                Id = supplier.Id,
                Note = supplier.Note,
                Email = supplier.Email,
                Siret = supplier.Siret,
                Address = supplier.Address,
                Website = supplier.Website,
                LandLine = supplier.LandLine,
                LastName = supplier.LastName,
                FirstName = supplier.FirstName,
                Reference = supplier.Reference,
                PhoneNumber = supplier.PhoneNumber,
                AccountingCode = supplier.AccountingCode,
                PaymentCondition = supplier.PaymentCondition,
                IntraCommunityVAT = supplier.IntraCommunityVAT,
            };
    }
}
