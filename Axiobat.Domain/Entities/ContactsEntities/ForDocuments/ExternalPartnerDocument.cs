﻿namespace Axiobat.Domain.Entities
{
    /// <summary>
    /// this class defines External Partner used in Documents
    /// </summary>
    public partial class ExternalPartnerDocument
    {
        /// <summary>
        /// the id of the client
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// the supplier reference, should be unique
        /// </summary>
        public string Reference { get; set; }

        /// <summary>
        /// the client full name
        /// </summary>
        [Newtonsoft.Json.JsonProperty("Name")]
        public string FullName => $"{FirstName} {LastName}";

        /// <summary>
        /// first name of the client
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// last name of the client
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// phone number
        /// </summary>
        public string PhoneNumber { get; set; }

        /// <summary>
        /// the Fax
        /// </summary>
        public string LandLine { get; set; }

        /// <summary>
        /// the email of the client, should be in a proper email format but is not required
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// client website
        /// </summary>
        public string Website { get; set; }

        /// <summary>
        /// Directory Identification System institutions
        /// </summary>
        public string Siret { get; set; }

        /// <summary>
        /// Intra-community VAT means value added tax (VAT) applied to commercial transactions between different countries of the European Union.
        /// for more informations <see href="https://debitoor.fr/termes-comptables/tva-intracommunautaire">HERE</see>
        /// </summary>
        public string IntraCommunityVAT { get; set; }

        /// <summary>
        /// the accounting identifier of the client
        /// </summary>
        public string AccountingCode { get; set; }

        /// <summary>
        /// a note about the quote
        /// </summary>
        public string Note { get; set; }

        /// <summary>
        /// the conditions of the payment
        /// </summary>
        public string PaymentCondition { get; set; }
    }
}
