﻿namespace Axiobat.Domain.Entities
{
    using Enums;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// this class represent an Expense
    /// </summary>
    [DocType(DocumentType.Expenses)]
    public partial class Expense
    {
        /// <summary>
        /// the type of the document
        /// </summary>
        public override DocumentType DocumentType => DocumentType.Expenses;

        /// <summary>
        /// the date the expense should be payed
        /// </summary>
        public DateTime DueDate { get; set; }

        /// <summary>
        /// Expense creation date
        /// </summary>
        public DateTime CreationDate { get; set; }

        /// <summary>
        /// a cancellation document in case this expense has been canceled
        /// </summary>
        public Memo CancellationDocument { get; set; }

        /// <summary>
        /// the order details
        /// </summary>
        public OrderDetails OrderDetails { get; set; }

        /// <summary>
        /// the id of the supplier
        /// </summary>
        public string SupplierId { get; set; }

        /// <summary>
        /// the supplier associated with this supplier order
        /// </summary>
        public SupplierDocuement Supplier { get; set; }

        /// <summary>
        /// list of supplier orders associated with this Expense
        /// </summary>
        public ICollection<SupplierOrders_Expenses> SupplierOrders { get; set; }

        /// <summary>
        /// the list of payments associated with the expense
        /// </summary>
        public ICollection<Expenses_Payments> Payments { get; set; }

        /// <summary>
        /// list of attachments
        /// </summary>
        public ICollection<Attachment> Attachments { get; set; }

        /// <summary>
        /// the Data sheets associated with the product
        /// </summary>
        public ICollection<Memo> Memos { get; set; }
    }

    /// <summary>
    /// partial part for <see cref="Expense"/>
    /// </summary>
    public partial class Expense : Document
    {
        /// <summary>
        /// create an instant of <see cref="Expense"/>
        /// </summary>
        public Expense() : base()
        {
            Memos = new HashSet<Memo>();
            Payments = new HashSet<Expenses_Payments>();
            Attachments = new HashSet<Attachment>();
            SupplierOrders = new HashSet<SupplierOrders_Expenses>();
        }

        /// <summary>
        /// get the total HT of the Invoice
        /// </summary>
        /// <returns>the total Ht</returns>
        public float GetTotalHT()
        {
            return OrderDetails.TotalHT;
        }

        /// <summary>
        /// get the total Tax of the Invoice
        /// </summary>
        /// <returns>the total Tax</returns>
        public float GetTotalTax()
        {
            return OrderDetails.GetTaxDetails().Sum(e => e.TotalTax);
        }

        /// <summary>
        /// get the total TTC of the Invoice
        /// </summary>
        /// <returns>the total TTC</returns>
        public float GetTotalTTC()
        {
            return OrderDetails.TotalTTC;
        }

        /// <summary>
        /// get the expense rest payment, for this function to work the payment list should be included
        /// </summary>
        /// <returns>the value of the rest to pay</returns>
        public double GetRestToPay()
            => OrderDetails.TotalTTC - GetTotalPaid();

        /// <summary>
        /// get the total amount of the payments, for this function to work the payment list should be included
        /// </summary>
        /// <returns>the total payments amount</returns>
        public double GetTotalPaid()
            => Payments.Sum(e => e.Amount);
    }
}
