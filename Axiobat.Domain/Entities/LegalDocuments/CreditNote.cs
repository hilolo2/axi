﻿namespace Axiobat.Domain.Entities
{
    using Interfaces;
    using Enums;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// this class represent a Credit Note
    /// </summary>
    [DocType(DocumentType.CreditNote)]
    public partial class CreditNote
    {
        /// <summary>
        /// the type of the document
        /// </summary>
        public override DocumentType DocumentType => DocumentType.CreditNote;

        /// <summary>
        /// the date the invoice should be payed or has been payed
        /// </summary>
        public DateTime DueDate { get; set; }

        /// <summary>
        /// Credit Note creation date
        /// </summary>
        public DateTime CreationDate { get; set; }

        /// <summary>
        /// the information of the client associated with this Quote
        /// </summary>
        public ClientDocument Client { get; set; }

        /// <summary>
        /// the id of the client
        /// </summary>
        public string ClientId { get; set; }

        /// <summary>
        /// the id of the invoice
        /// </summary>
        public string InvoiceId { get; set; }

        /// <summary>
        /// the invoices associated with this credit note
        /// </summary>
        public Invoice Invoice { get; set; }

        /// <summary>
        /// the invoice order details
        /// </summary>
        public WorkshopOrderDetails OrderDetails { get; set; }

        /// <summary>
        /// list of emails that has been sent for this documents
        /// </summary>
        public ICollection<DocumentEmail> Emails { get; set; }

        /// <summary>
        /// the list of payments associated with the credit note
        /// </summary>
        public ICollection<Payment> Payments { get; set; }

        /// <summary>
        /// list of memos associated with the invoice
        /// </summary>
        public ICollection<Memo> Memos { get; set; }
    }

    /// <summary>
    /// partial part for <see cref="CreditNote"/>
    /// </summary>
    public partial class CreditNote : Document, IReferenceable<CreditNote>
    {
        /// <summary>
        /// create an instant of <see cref="CreditNote"/>
        /// </summary>
        public CreditNote() : base()
        {
            Emails = new HashSet<DocumentEmail>();
            Payments = new HashSet<Payment>();
            Memos = new HashSet<Memo>();
        }

        /// <summary>
        /// check if we can increment the reference on the update
        /// </summary>
        /// <param name="oldState">the old state of the entity</param>
        /// <returns>true if we can, false if not</returns>
        public bool CanIncrementOnUpdate(CreditNote oldState) => base.CanIncrementOnUpdate(oldState);

        /// <summary>
        /// get the total HT of the Invoice
        /// </summary>
        /// <returns>the total Ht</returns>
        public float GetTotalHT()
        {
            return Math.Abs(OrderDetails.TotalHT) * -1;
        }

        /// <summary>
        /// get the total Tax of the Invoice
        /// </summary>
        /// <returns>the total Tax</returns>
        public float GetTotalTax()
        {
            return Math.Abs(OrderDetails.GetTaxDetails().Sum(e => e.TotalTax)) * -1;
        }

        /// <summary>
        /// get the total TTC of the Invoice
        /// </summary>
        /// <returns>the total TTC</returns>
        public float GetTotalTTC()
        {
            return Math.Abs(OrderDetails.TotalTTC) * -1;
        }
    }
}
