﻿namespace Axiobat.Domain.Entities
{
    /// <summary>
    /// the class that defines the Many to Many relation ship between
    /// <see cref="Expense"/> and <see cref="SupplierOrder"/>
    /// </summary>
    public class SupplierOrders_Expenses
    {
        /// <summary>
        /// the id of the expense
        /// </summary>
        public string ExpenseId { get; set; }

        /// <summary>
        /// the id of the supplier order
        /// </summary>
        public string SupplierOrderId { get; set; }

        /// <summary>
        /// the expense
        /// </summary>
        public Expense Expense { get; set; }

        /// <summary>
        /// the supplier order
        /// </summary>
        public SupplierOrder SupplierOrder { get; set; }
    }
}
