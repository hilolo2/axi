﻿namespace Axiobat.Domain.Entities
{
    using Axiobat.Domain.Enums;
    using System;

    /// <summary>
    /// a class that represent an Operation Sheet Signature
    /// </summary>
    public class Signature
    {
        /// <summary>
        /// the day the Signature has been made
        /// </summary>
        public DateTime SignatureDate { get; set; }

        /// <summary>
        /// the name of the person who made the Signature
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// the id of the person who made the Signature
        /// </summary>
        public string Id { get; set; }


        public SmileSatisfication SmileSatisfication { get; set; }

        /// <summary>
        /// a base64 string representation of the object
        /// </summary>
        public string ImageContent { get; set; }

        /// <summary>
        /// get the string representation of the object
        /// </summary>
        /// <returns>the string value</returns>
        public override string ToString()
            => $"name: {Name}, at: {SignatureDate.ToShortDateString()}";
    }
}
