﻿namespace Axiobat.Domain.Entities
{
    using Enums;
    using Interfaces;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// a class defines the Operation Sheet
    /// </summary>
    [DocType(DocumentType.OperationSheet)]
    public partial class OperationSheet
    {
        /// <summary>
        /// the type of the document
        /// </summary>
        public virtual DocumentType DocumentType => DocumentType.OperationSheet;

        /// <summary>
        /// the document reference
        /// </summary>
        public string Reference { get; set; }

        /// <summary>
        /// the status of the operation sheet, one of <see cref="Constants.OperationSheetStatus"/>
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// the order details associated with this operation sheet
        /// </summary>
        public OrderDetails OrderDetails { get; set; }

        /// <summary>
        /// the Intervention Address
        /// </summary>
        public Address AddressIntervention { get; set; }

        /// <summary>
        /// the starting date of the operation
        /// </summary>
        public DateTime StartDate { get; set; }

        /// <summary>
        /// the ending date of the operation
        /// </summary>
        public DateTime EndDate { get; set; }

        /// <summary>
        /// the Report associated with the operation sheet
        /// </summary>
        public string Report { get; set; }

        /// <summary>
        /// the purpose of the quote
        /// </summary>
        public string Purpose { get; set; }

        /// <summary>
        /// the count of the Visits the technician has made to the client
        /// </summary>
        public int VisitsCount { get; set; }

        /// <summary>
        /// the total Basket consumption the technician has made
        /// </summary>
        public int TotalBasketConsumption { get; set; }

        /// <summary>
        /// the id of the Google Calendar associated with this operation sheet
        /// </summary>
        public string GoogleCalendarId { get; set; }

        /// <summary>
        /// apply the logical delete of the entity
        /// </summary>
        public bool IsDeleted { get; set; }

        /// <summary>
        /// the Signature of the client
        /// </summary>
        public Signature ClientSignature { get; set; }

        /// <summary>
        /// the technicien signature
        /// </summary>
        public Signature TechnicianSignature { get; set; }

        /// <summary>
        /// the information of the client associated with this OperationSheet
        /// </summary>
        public ClientDocument Client { get; set; }

        /// <summary>
        /// the id of the client
        /// </summary>
        public string ClientId { get; set; }

        /// <summary>
        /// id of the Quote associated with this operation sheet
        /// </summary>
        public string QuoteId { get; set; }

        /// <summary>
        /// id of the Invoice associated with this operation sheet
        /// </summary>
        public string InvoiceId { get; set; }

        /// <summary>
        /// the id of the workshop associated with this operation sheet
        /// </summary>
        public string WorkshopId { get; set; }

        /// <summary>
        /// the Quote associated with this operation sheet
        /// </summary>
        public Quote Quote { get; set; }

        /// <summary>
        /// the invoice associated with this operation sheet
        /// </summary>
        public Invoice Invoice { get; set; }

        /// <summary>
        /// the construction workshop
        /// </summary>
        public ConstructionWorkshop Workshop { get; set; }

        /// <summary>
        /// list of Technicians associated with the operation sheet
        /// </summary>
        public ICollection<OperationSheets_Technicians> Technicians { get; set; }

        /// <summary>
        /// list of emails that has been sent for this documents
        /// </summary>
        public ICollection<DocumentEmail> Emails { get; set; }

        /// <summary>
        /// the Data sheets associated with the product
        /// </summary>
        public ICollection<Memo> Memos { get; set; }

        /// <summary>
        /// entity changes history
        /// </summary>
        public List<ChangesHistory> ChangesHistory { get; set; }

        /// <summary>
        /// entity changes mode statut
        /// </summary>
        public List<ModeStatut> ModeStatut { get; set; }
    }

    /// <summary>
    /// partial part for <see cref="OperationSheet"/>
    /// </summary>
    public partial class OperationSheet : Entity<string>, IReferenceable<OperationSheet>, IRecordable, IDeletable
    {
        /// <summary>
        /// create an instant of <see cref="OperationSheet"/>
        /// </summary>
        public OperationSheet()
        {
            Id = GenerateId();
            Memos = new HashSet<Memo>();
            Emails = new HashSet<DocumentEmail>();
            ChangesHistory = new List<ChangesHistory>();
            ModeStatut = new List<ModeStatut>();
        }

        /// <summary>
        /// build the search terms of the object
        /// </summary>
        public override void BuildSearchTerms()
            => SearchTerms = $"{Client.FullName} {Purpose} {Reference}".ToLower();

        /// <summary>
        /// generate an id for the document
        /// </summary>
        /// <returns>the new generated id</returns>
        public string GenerateId() => EntityId.Generate(DocumentType);

        public bool CanIncrementOnCreate() => true;

        public bool CanIncrementOnUpdate(OperationSheet oldState) => false;

        /// <summary>
        /// get the total HT of the Invoice
        /// </summary>
        /// <returns>the total Ht</returns>
        public float GetTotalHT()
        {
            return OrderDetails.TotalHT;
        }

        /// <summary>
        /// get the total Tax of the Invoice
        /// </summary>
        /// <returns>the total Tax</returns>
        public float GetTotalTax()
        {
            return OrderDetails.GetTaxDetails().Sum(e => e.TotalTax);
        }

        /// <summary>
        /// get the total TTC of the Invoice
        /// </summary>
        /// <returns>the total TTC</returns>
        public float GetTotalTTC()
        {
            return OrderDetails.TotalTTC;
        }
    }
}
