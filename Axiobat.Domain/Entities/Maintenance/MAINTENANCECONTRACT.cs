﻿namespace Axiobat.Domain.Entities
{
    using Interfaces;
    using Domain.Enums;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// this class defines the maintenance contract
    /// </summary>
    [DocType(DocumentType.MaintenanceContract)]
    public partial class MaintenanceContract
    {
        /// <summary>
        /// the type of the document
        /// </summary>
        public virtual DocumentType DocumentType => DocumentType.MaintenanceContract;

        /// <summary>
        /// if the entity has been deleted or not
        /// </summary>
        public bool IsDeleted { get; set; }

        /// <summary>
        /// the reference for the entity
        /// </summary>
        public string Reference { get; set; }

        /// <summary>
        /// status of the contract, one of the values of <see cref="Constants.MaintenanceContractStatus"/>
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// the site of the maintenance
        /// </summary>
        public Address Site { get; set; }

        /// <summary>
        /// whether to enable the Enable Auto Renewal or not
        /// </summary>
        public bool EnableAutoRenewal { get; set; }

        /// <summary>
        /// whether the expiration alert has been enabled or not
        /// </summary>
        public bool ExpirationAlertEnabled { get; set; }

        /// <summary>
        /// the expiration alert period, the value should be in days
        /// </summary>
        public int ExpirationAlertPeriod { get; set; }

        /// <summary>
        /// the type of the expiration alert period
        /// </summary>
        public PeriodType ExpirationAlertPeriodType { get; set; }

        /// <summary>
        /// the starting date of the operation
        /// </summary>
        public DateTime StartDate { get; set; }

        /// <summary>
        /// the ending date of the operation
        /// </summary>
        public DateTime EndDate { get; set; }

        /// <summary>
        /// the id of the Recurring associated with this contract
        /// </summary>
        public string RecurringDocumentId { get; set; }

        /// <summary>
        /// the Recurring Document
        /// </summary>
        public RecurringDocument RecurringDocument { get; set; }

        /// <summary>
        /// the id of the client associated with this contract
        /// </summary>
        public string ClientId { get; set; }

        /// <summary>
        /// the id of the client associated with this contract
        /// </summary>
        public ClientDocument Client { get; set; }

        /// <summary>
        /// list of memos associated with the invoice
        /// </summary>
        public ICollection<Memo> Memos { get; set; }

        /// <summary>
        /// list of attachments
        /// </summary>
        public ICollection<Attachment> Attachments { get; set; }

        /// <summary>
        /// entity changes history
        /// </summary>
        public List<ChangesHistory> ChangesHistory { get; set; }

        /// <summary>
        /// list of Equipment Maintenance being maintained by this contract argument
        /// </summary>
        public ICollection<MaintenanceContractEquipmentDetails> EquipmentMaintenance { get; set; }

        /// <summary>
        /// list of maintenance visits
        /// </summary>
        public ICollection<MaintenanceVisit> MaintenanceVisits { get; set; }

        /// <summary>
        /// list of maintenance operation sheets
        /// </summary>
        public ICollection<MaintenanceOperationSheet> MaintenanceOperationsSheets { get; set; }

        /// <summary>
        /// list of invoices associated with the Contract
        /// </summary>
        public ICollection<Invoice> Invoices { get; set; }

        /// <summary>
        /// list of PublishingContract associated with this contract
        /// </summary>
        public ICollection<PublishingContractMinimal> PublishingContracts { get; set; }

        /// <summary>
        /// the devis order details
        /// </summary>
        public WorkshopOrderDetails OrderDetails { get; set; }
    }

    /// <summary>
    /// partial part for <see cref="MaintenanceContract"/>
    /// </summary>
    public partial class MaintenanceContract : Entity<string>, IReferenceable<MaintenanceContract>, IRecordable, IDeletable
    {
        /// <summary>
        /// create an instant of <see cref="MaintenanceContract"/>
        /// </summary>
        public MaintenanceContract()
        {
            Id = GenerateId();
            Memos = new HashSet<Memo>();
            Invoices = new HashSet<Invoice>();
            Attachments = new HashSet<Attachment>();
            ChangesHistory = new List<ChangesHistory>();
            MaintenanceVisits = new List<MaintenanceVisit>();
            PublishingContracts = new HashSet<PublishingContractMinimal>();
            MaintenanceOperationsSheets = new HashSet<MaintenanceOperationSheet>();
            EquipmentMaintenance = new HashSet<MaintenanceContractEquipmentDetails>();

        }

        /// <summary>
        /// build search terms of the entity
        /// </summary>
        public override void BuildSearchTerms()
            => SearchTerms = $"{Reference} {Site}";

        /// <summary>
        /// generate an id for the document
        /// </summary>
        /// <returns>the new generated id</returns>
        public string GenerateId() => EntityId.Generate(DocumentType);

        /// <summary>
        /// get string representation of the object
        /// </summary>
        /// <returns>the string value</returns>
        public override string ToString()
            => $"contract Ref: {Reference}, status: {Status}, has {EquipmentMaintenance.Count} Equipment Maintenance, duration: ~{Math.Round((EndDate - StartDate).TotalDays, 0)} Days";

        /// <summary>
        /// get the list of the equipment maintenance of the given month
        /// </summary>
        /// <param name="month">the month of the operation</param>
        /// <returns>list of <see cref="MaintenanceContractEquipmentDetails"/></returns>
        public List<MaintenanceContractEquipmentDetails> GetEquipmentMaintenanceOf(int month)
            => EquipmentMaintenance
                .Where(e => e.HasOperationIn(month))
                .Select(e => e.GenerateFor(month))
                .ToList();

        /// <summary>
        /// get the list of the equipment maintenance of the given date month
        /// </summary>
        /// <param name="date">the date of the operation</param>
        /// <returns>list of <see cref="MaintenanceContractEquipmentDetails"/></returns>
        public List<MaintenanceContractEquipmentDetails> GetEquipmentMaintenanceOf(DateTime date)
            => GetEquipmentMaintenanceOf(date.Month);

        /// <summary>
        /// get the MaintenanceVisit in the given date
        /// </summary>
        /// <param name="date">the date to check for</param>
        /// <returns>the <see cref="MaintenanceVisit"/> instant if any</returns>
        public MaintenanceVisit GetMaintenanceVisits(DateTime date)
            => MaintenanceVisits.FirstOrDefault(e => e.Date.Month == date.Month && e.Date.Year == date.Year);

        public bool CanIncrementOnCreate() => true;

        public bool CanIncrementOnUpdate(MaintenanceContract oldState) => false;

        /// <summary>
        /// get the total HT of the Invoice
        /// </summary>
        /// <returns>the total Ht</returns>
        public float GetTotalHT()
        {
            return OrderDetails.TotalHT;
        }

        /// <summary>
        /// get the total Tax of the Invoice
        /// </summary>
        /// <returns>the total Tax</returns>
        public float GetTotalTax()
        {
            return OrderDetails.GetTaxDetails().Sum(e => e.TotalTax);
        }

        /// <summary>
        /// get the total TTC of the Invoice
        /// </summary>
        /// <returns>the total TTC</returns>
        public float GetTotalTTC()
        {
            return OrderDetails.TotalTTC;
        }
    }
}
