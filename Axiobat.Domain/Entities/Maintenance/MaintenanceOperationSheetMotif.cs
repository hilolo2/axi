﻿namespace Axiobat.Domain.Entities
{
    using System;

    /// <summary>
    /// this class defines the motif used to justify the status <see cref="Constants.MaintenanceOperationSheetStatus.UnDone"/>
    /// </summary>
    public partial class MaintenanceOperationSheetMotif
    {
        public MaintenanceOperationSheetMotif()
        {
            Id = Guid.NewGuid().ToString();
            Date = DateTime.Now;
        }

        /// <summary>
        /// the id of the motif
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// the description of the motif
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// the date
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// the user
        /// </summary>
        public MinimalUser User { get; set; }
    }
}
