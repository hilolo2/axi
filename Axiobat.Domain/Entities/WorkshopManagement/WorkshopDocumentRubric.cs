﻿namespace Axiobat.Domain.Entities
{
    using Domain.Enums;
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// this class defines the workshop document rubric
    /// </summary>
    public partial class WorkshopDocumentRubric : Entity<string>
    {
        /// <summary>
        /// the value of the rubric
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// the type of the rubric
        /// </summary>
        public DocumentRubricType Type { get; set; } = DocumentRubricType.Custom;

        /// <summary>
        /// the id of the workshop that owns this rubric
        /// </summary>
        public string WorkshopId { get; set; }

        /// <summary>
        /// the workshop that owns this rubric
        /// </summary>
        public ConstructionWorkshop Workshop { get; set; }

        /// <summary>
        /// list of documents associated with this rubric
        /// </summary>
        public ICollection<WorkshopDocument> Documents { get; set; }
    }

    /// <summary>
    /// partial part for <see cref="WorkshopDocumentRubric"/>
    /// </summary>
    public partial class WorkshopDocumentRubric
    {
        public static List<WorkshopDocumentRubric> Default => new List<WorkshopDocumentRubric>
        {
           
            new WorkshopDocumentRubric
            {
                Value = "Devis",
                SearchTerms = "Devis",
                Id = Guid.NewGuid().ToString(),
                Type = DocumentRubricType.Quote,
            },
              new WorkshopDocumentRubric
            {
                Value = "Fiche Intervention",
                SearchTerms = "Fiche Intervention",
                Id = Guid.NewGuid().ToString(),
                Type = DocumentRubricType.OperationSheet,
            },
            new WorkshopDocumentRubric
            {
                Value = "Facturation",
                SearchTerms = "Facturation",
                Id = Guid.NewGuid().ToString(),
                Type = DocumentRubricType.Billing,
            },
                  new WorkshopDocumentRubric
            {
                Value = "Avoir",
                SearchTerms = "Avoir",
                Id = Guid.NewGuid().ToString(),
                Type = DocumentRubricType.CreditNote,
            },

                   new WorkshopDocumentRubric
            {
                Value = "Commandes",
                SearchTerms = "Commandes",
                Id = Guid.NewGuid().ToString(),
                Type = DocumentRubricType.Orders,
            },
                    new WorkshopDocumentRubric
            {
                Value = "Dépanse",
                SearchTerms = "Dépanse",
                Id = Guid.NewGuid().ToString(),
                Type = DocumentRubricType.Expenses
            },
        };

        /// <summary>
        /// create an instant of <see cref="WorkshopDocumentRubric"/>
        /// </summary>
        public WorkshopDocumentRubric() : base()
        {
            Documents = new HashSet<WorkshopDocument>();
        }

        /// <summary>
        /// build the search terms of the object
        /// </summary>
        public override void BuildSearchTerms()
            => SearchTerms = $"{Value}";
    }
}
