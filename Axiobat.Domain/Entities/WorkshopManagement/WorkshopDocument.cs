﻿namespace Axiobat.Domain.Entities
{
    using App.Common;
    using Domain.Enums;
    using Interfaces;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// this class defines the documentations of a workshop
    /// </summary>
    public partial class WorkshopDocument
    {
        /// <summary>
        /// the id of the <see cref="WorkshopDocument"/> entity
        /// </summary>
        public override string Id
        {
            get => _id;
            set => _id = value.IsValid() ? value : Generator.GenerateRandomId();
        }

        /// <summary>
        /// the comment associated with the Workshop Documentation
        /// </summary>
        public string Comment { get; set; }

        /// <summary>
        /// the documentation designation
        /// </summary>
        public string Designation { get; set; }

        /// <summary>
        /// the id of the rubric this workshop is associated with it
        /// </summary>
        public string RubricId { get; set; }

        /// <summary>
        /// the id of the workshop
        /// </summary>
        public string WorkshopId { get; set; }

        /// <summary>
        /// the user that has added this memo
        /// </summary>
        public MinimalUser User { get; set; }

        /// <summary>
        /// the workshop that this document is associated with it
        /// </summary>
        public ConstructionWorkshop Workshop { get; set; }

        /// <summary>
        /// the rubric this workshop is associated with it
        /// </summary>
        public WorkshopDocumentRubric Rubric { get; set; }

        /// <summary>
        /// the of types this document has
        /// </summary>
        public ICollection<WorkshopDocumentsTypes> Types { get; set; }

        /// <summary>
        /// list of attachments associated with this documentation
        /// </summary>
        public ICollection<Attachment> Attachments { get; set; }

        /// <summary>
        /// the list of changes history
        /// </summary>
        public List<ChangesHistory> ChangesHistory { get; set; }
    }

    /// <summary>
    /// partial part for <see cref="WorkshopDocument"/>
    /// </summary>
    public partial class WorkshopDocument : Entity<string>, IRecordable
    {
        private string _id;

        /// <summary>
        /// create an instant of <see cref="WorkshopDocument"/>
        /// </summary>
        public WorkshopDocument()
        {
            Attachments = new HashSet<Attachment>();
            Types = new HashSet<WorkshopDocumentsTypes>();
            ChangesHistory = new List<ChangesHistory>();
        }

        /// <summary>
        /// create an instant of <see cref="WorkshopDocument"/>
        /// </summary>
        /// <param name="userId">the id of the user</param>
        /// <param name="workshopeId">the id of the workshop</param>
        public WorkshopDocument(string workshopeId) : this()
        {
            WorkshopId = workshopeId;
        }

        /// <summary>
        /// build the search terms of the object
        /// </summary>
        public override void BuildSearchTerms()
            => SearchTerms = $"{Comment} {Designation}";

        /// <summary>
        /// the string representation of the object
        /// </summary>
        /// <returns>the string value</returns>
        public override string ToString()
            => $"id: {Id}, Rubric Id: {RubricId}, with {Attachments.Count} attachments";

        /// <summary>
        /// get the last time the property with the given name has been updated in
        /// </summary>
        /// <param name="propertyName">the name of the property</param>
        /// <returns>the date</returns>
        public DateTime? LastTimeUpdated(string propertyName)
            => ChangesHistory
                .Where(e => e.Action == ChangesHistoryType.Updated && e.Fields.Any(f => f.PropName == propertyName))
                .OrderByDescending(e => e.DateAction)
                .FirstOrDefault()?.DateAction;
    }
}
