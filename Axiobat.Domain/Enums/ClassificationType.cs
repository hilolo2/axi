﻿namespace Axiobat.Domain.Enums
{
    /// <summary>
    /// type of the classification
    /// </summary>
    public enum ClassificationType
    {
        /// <summary>
        /// expense classification
        /// </summary>
        Expense = 0,

        /// <summary>
        /// sales classification
        /// </summary>
        Sales = 1,
    }
}
