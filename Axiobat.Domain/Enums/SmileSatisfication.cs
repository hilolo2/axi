﻿namespace Axiobat.Domain.Enums
{
    /// <summary>
    /// an enum that defines the type of the value, (None, Percentage, Amount)
    /// </summary>
    public enum SmileSatisfication
    {
        none = 0 , 
        frown = 1,
        meh = 2,
        smile = 3
    }
}
