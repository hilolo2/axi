﻿namespace Axiobat.Domain.Enums
{
    /// <summary>
    /// Email delivery status
    /// </summary>
    public enum EmailStatus
    {
        /// <summary>
        /// there is an error sending the email
        /// </summary>
        Error,

        /// <summary>
        /// email has been sent successfully
        /// </summary>
        Sent,

        /// <summary>
        /// the email sending is in progress
        /// </summary>
        InProgress,

        /// <summary>
        /// the email has been opened
        /// </summary>
        Opened
    }
}
