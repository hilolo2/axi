﻿namespace Axiobat.Domain.Enums
{
    /// <summary>
    /// the type of the chart account category
    /// </summary>
    public enum CategoryType
    {
        /// <summary>
        /// a custom category defined by the user
        /// </summary>
        Custom,

        /// <summary>
        /// the category defines the internal transfer
        /// </summary>
        InternalTransfers,

        /// <summary>
        /// the category is for client
        /// </summary>
        Client,

        /// <summary>
        /// the category is for supplier
        /// </summary>
        Supplier,

        /// <summary>
        /// the category is for CreditNote
        /// </summary>
        CreditNote,

        /// <summary>
        /// the category is for cash
        /// </summary>
        Cash,

        /// <summary>
        /// the category is for bank
        /// </summary>
        Bank,

        /// <summary>
        /// the category for Subcontracting
        /// </summary>
        SubContracting = 7,

        /// <summary>
        /// category for Purchases
        /// </summary>
        Purchases = 8,

        /// <summary>
        /// Workforce Purchase
        /// </summary>
        WorkforcePurchase = 9,

        /// <summary>
        /// category for Others
        /// </summary>
        Other = 11
    }
}
