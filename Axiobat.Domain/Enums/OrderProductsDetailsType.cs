﻿namespace Axiobat.Domain.Enums
{
    /// <summary>
    /// the type of the product details included in the order
    /// </summary>
    public enum OrderProductsDetailsType
    {
        /// <summary>
        /// the list of the product are selected and are included as a list
        /// </summary>
        List,

        /// <summary>
        /// the list of the product are selected as a file
        /// </summary>
        File
    }
}
