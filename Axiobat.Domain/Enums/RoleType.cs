﻿namespace Axiobat.Domain.Enums
{
    /// <summary>
    /// type of the role
    /// </summary>
    public enum RoleType
    {
        /// <summary>
        /// the company type is unspecified
        /// </summary>
        Unspecified = -1,

        /// <summary>
        /// a role defined by a company
        /// </summary>
        UserDefined = 0,

        /// <summary>
        /// the super admin role
        /// </summary>
        SuperAdminRole = 1,

        /// <summary>
        /// a role that defines a Technician
        /// </summary>
        Technician = 2,

        /// <summary>
        /// a role that defines a manager
        /// </summary>
        Manager = 3,

        /// <summary>
        /// a role that defines a client
        /// </summary>
        Client = 4,

        /// <summary>
        /// a role that defines a Maintenance Technician
        /// </summary>
        MaintenanceTechnician = 5,

        /// <summary>
        /// a role that defines a Workshop Technician
        /// </summary>
        WorkshopTechnician = 6,
    }
}
