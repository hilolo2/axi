﻿namespace Axiobat.Domain.Enums
{
    /// <summary>
    /// this enum defines all types of documents used in the application
    /// </summary>
    public enum DocumentType
    {
        /// <summary>
        /// the document don't have any document type associated with it
        /// </summary>
        Undefined = -1,

        /// <summary>
        /// client docType
        /// </summary>
        Client = 1,

        /// <summary>
        /// supplier docType
        /// </summary>
        Supplier = 2,

        /// <summary>
        /// products docType
        /// </summary>
        Agent = 3,

        /// <summary>
        /// Quote docType
        /// </summary>
        Quote = 4,

        /// <summary>
        /// the docType that defines the OperationSheet ("fiche_intervention")
        /// </summary>
        OperationSheet = 5,

        /// <summary>
        /// invoice docType
        /// </summary>
        Invoice = 6,

        /// <summary>
        /// the document type that defines CreditNote (Avoir)
        /// </summary>
        CreditNote = 7,

        /// <summary>
        /// Supplier Good Order
        /// </summary>
        SupplierOrder = 8,

        /// <summary>
        /// the docType that defines the Maintenance Operation Sheet ("fiche_intervention_maintenance")
        /// </summary>
        MaintenanceOperationSheet = 9,

        /// <summary>
        /// document type that defines the expenses
        /// </summary>
        Expenses = 10,

        /// <summary>
        /// document is a payment document
        /// </summary>
        Payment = 11,

        /// <summary>
        /// a configuration document
        /// </summary>
        Configuration = 12,

        /// <summary>
        /// document is a product
        /// </summary>
        Product = 13,

        /// <summary>
        /// document is a lot
        /// </summary>
        Lot = 14,

        /// <summary>
        /// document is a workshop
        /// </summary>
        Workshop = 15,

        /// <summary>
        /// document is an Equipment Maintenance
        /// </summary>
        EquipmentMaintenance = 16,

        /// <summary>
        /// document id a Maintenance Contract
        /// </summary>
        MaintenanceContract = 17,

        /// <summary>
        /// document id a maintenance visit
        /// </summary>
        MaintenanceVisit = 18,

        /// <summary>
        /// the docType for RecurringDocument entity
        /// </summary>
        RecurringDocument = 19,

        /// <summary>
        /// classification entity
        /// </summary>
        Classification = 20,

        /// <summary>
        /// Publishing Contract entity
        /// </summary>
        PublishingContract = 21,

        /// <summary>
        /// The doctype For Mission Entity
        /// </summary>
        Mission = 22
    }
}
