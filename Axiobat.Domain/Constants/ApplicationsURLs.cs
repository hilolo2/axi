﻿namespace Axiobat.Domain.Constants
{
    /// <summary>
    /// the applications URLs
    /// </summary>
    public static partial class ApplicationsURLs
    {
        /// <summary>
        /// the web application URLs
        /// </summary>
        public const string WebApp = "web_app";

        /// <summary>
        /// the URL where to send the user to when requesting a rest password
        /// example : https://Axiobat.fr/rest
        /// </summary>
        public const string RestPassword = "rest_password";

        /// <summary>
        /// the URL where to send user to confirm there email
        /// </summary>
        public const string EmailConfirmation = "email_confirmation";

        /// <summary>
        /// the URL where to send user to confirm there email
        /// </summary>
        public const string AccountantEmailConfirmation = "accountant_email_confirmation";

        /// <summary>
        /// the URL where to send the user being invited to the join the app
        /// </summary>
        public const string UserInvitaion = "user_invitaion";
    }
}
