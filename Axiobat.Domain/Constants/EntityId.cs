﻿namespace Axiobat
{
    using App.Common;
    using Domain.Entities;
    using Domain.Enums;
    using Domain.Exceptions;
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// this class defines the unique format of the generated id of the entities
    /// </summary>
    public partial struct EntityId
    {
        /// <summary>
        /// name of the database
        /// </summary>
        public string Constant { get; }

        /// <summary>
        /// the type of the document, this is set using <see cref="DocTypeAttribute"/>
        /// </summary>
        public DocumentType DocType { get; }

        /// <summary>
        /// the Timestamps
        /// </summary>
        public string Timestamps { get; }

        /// <summary>
        /// get the dateTime value of the Timestamps
        /// </summary>
        public DateTime Date => DateHelper.GetDateFromTimestamps(Timestamps);

        /// <summary>
        /// generate a new id
        /// </summary>
        /// <returns>the created couchDb id</returns>
        public static EntityId Generate(DocumentType docType)
            => new EntityId(docType);

        /// <summary>
        /// generate an id for the entity
        /// </summary>
        /// <typeparam name="TEntity">the type of the entity</typeparam>
        /// <returns>the generate </returns>
        public static EntityId Generate<TEntity>(DateTime timestamps) where TEntity : Entity
            => new EntityId(typeof(TEntity).DocType(), timestamps);

        public static EntityId Generate<TEntity>() where TEntity : Entity
            => Generate(typeof(TEntity).DocType());
    }

    /// <summary>
    /// partial part for <see cref="EntityId"/>
    /// </summary>
    public partial struct EntityId : IEquatable<EntityId>
    {
        /// <summary>
        ///  custom constant value that defines uniquely identifies the id
        /// </summary>
        private const string _constantValue = "chlubdjubs1hu";

        /// <summary>
        /// the regular expression of the id
        /// </summary>
        private const string _regexTemplate = @"^(\w+)::(\w+)::(\d+)$";

        /// <summary>
        /// get an empty value for the entity id
        /// </summary>
        public readonly static EntityId Empty = new EntityId("", DocumentType.Undefined, new DateTime(1999, 1, 1));

        /// <summary>
        /// create a new <see cref="EntityId"/> instant
        /// </summary>
        /// <param name="databaseName">name of the database</param>
        /// <param name="docType">the docType</param>
        public EntityId(DocumentType docType) : this()
        {
            DocType = docType;
            Constant = _constantValue;
            Timestamps = DateTime.Now.ToString(DateHelper.TimestampFormat);
        }

        /// <summary>
        /// create a new <see cref="EntityId"/> instant
        /// </summary>
        /// <param name="databaseName">name of the database</param>
        /// <param name="docType">the docType</param>
        /// <param name="timestamps">the timestamps, when this is id has been generated</param>
        public EntityId(DocumentType docType, string timestamps)
            : this(_constantValue, docType, timestamps) { }

        /// <summary>
        /// create a new <see cref="EntityId"/> instant
        /// </summary>
        /// <param name="databaseName">name of the database</param>
        /// <param name="docType">the docType</param>
        /// <param name="timestamps">the timestamps, when this is id has been generated</param>
        public EntityId(DocumentType docType, DateTime date)
            : this(_constantValue, docType, date) { }

        /// <summary>
        /// create a new <see cref="EntityId"/> instant
        /// </summary>
        /// <param name="databaseName">name of the database</param>
        /// <param name="docType">the docType</param>
        /// <param name="timestamps">the timestamps, when this is id has been generated</param>
        public EntityId(string constant, DocumentType docType) : this(docType)
        {
            Constant = constant;
            Timestamps = DateTime.Now.ToString(DateHelper.TimestampFormat);
        }

        /// <summary>
        /// create a new <see cref="EntityId"/> instant
        /// </summary>
        /// <param name="databaseName">name of the database</param>
        /// <param name="docType">the docType</param>
        /// <param name="timestamps">the timestamps, when this is id has been generated</param>
        public EntityId(string constant, DocumentType docType, string timestamps)
            : this(constant, docType)
        {
            Timestamps = timestamps;
        }

        /// <summary>
        /// create a new <see cref="EntityId"/> instant
        /// </summary>
        /// <param name="databaseName">name of the database</param>
        /// <param name="docType">the docType</param>
        /// <param name="timestamps">the timestamps, when this is id has been generated</param>
        public EntityId(string constant, DocumentType docType, DateTime date)
            : this(constant, docType)
        {
            Timestamps = date.ToString(DateHelper.TimestampFormat);
        }

        /// <summary>
        /// check if the given <see cref="EntityId"/> instant equals to this instant
        /// </summary>
        /// <param name="other">the <see cref="EntityId"/> to compare</param>
        /// <returns>true if equals, false if not</returns>
        public bool Equals(EntityId other)
            => other.Constant.Equals(Constant)
                && other.DocType.Equals(DocType)
                && other.Timestamps.Equals(Timestamps);

        /// <summary>
        /// check if this instant equals the given instant
        /// </summary>
        /// <param name="obj">the instant to validate</param>
        /// <returns>true if equals, false if not</returns>
        public override bool Equals(object obj)
        {
            if (obj is null) return false;
            if (obj.GetType() != typeof(EntityId)) return false;
            if (ReferenceEquals(obj, this)) return true;
            return Equals((EntityId)obj);
        }

        /// <summary>
        /// get the hashCode of this id
        /// </summary>
        /// <returns>the hashCode</returns>
        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = 13;
                hashCode = (hashCode * 397) ^ (Constant.IsValid() ? Constant.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (((int)DocType).GetHashCode());
                hashCode = (hashCode * 397) ^ (Timestamps.IsValid() ? Timestamps.GetHashCode() : 0);
                return hashCode;
            }
        }

        /// <summary>
        /// get the string value of the id
        /// </summary>
        /// <returns>the id value as a string</returns>
        public override string ToString()
            => $"{Constant}::{DocType}::{Timestamps}";

        /// <summary>
        /// check if this <see cref="EntityId"/> is valid, basically we check if all properties has values
        /// </summary>
        /// <returns>true if valid false if not</returns>
        public bool IsValid() => Constant.IsValid() && DocType != DocumentType.Undefined && Timestamps.IsValid();

        public bool IsEmpty() => this.Equals(Empty);

        /// <summary>
        /// get the entity id instant from the given id
        /// </summary>
        /// <param name="id">the id to create the entity id from it</param>
        /// <returns>the <see cref="EntityId"/> instant</returns>
        public static EntityId Get(string id, bool throwIfNotValid = true)
        {
            if (!id.IsValid())
                throw new AxiobatException("the given id is not in a valid format, must be as 'constant::docType::timestamp'", MessageCode.InvalidIdentifierFormat);

            var match = System.Text.RegularExpressions.Regex.Match(id, _regexTemplate);
            if (!match.Success || match.Groups.Count <= 3)
            {
                if (throwIfNotValid)
                    throw new AxiobatException("the given id is not in a valid format, must be as 'constant::docType::timestamp'", MessageCode.InvalidIdentifierFormat);

                return Empty;
            }

            DocumentType documentType = DocumentType.Undefined;
            if (Enum.TryParse(match.Groups[2].Value, out DocumentType result))
                documentType = result;

            return new EntityId(match.Groups[1].Value, documentType, match.Groups[3].Value);
        }

        /// <summary>
        /// get an instant of <see cref="EntityId"/> from a string
        /// </summary>
        /// <param name="id">the string id</param>
        public static explicit operator EntityId(string id) => Get(id);

        /// <summary>
        /// get the string representation of the <see cref="EntityId"/>
        /// </summary>
        /// <param name="EntityId">the <see cref="EntityId"/> instant</param>
        public static implicit operator string(EntityId EntityId) => EntityId.ToString();

        public static bool operator ==(EntityId left, EntityId right) => EqualityComparer<EntityId>.Default.Equals(left, right);
        public static bool operator !=(EntityId left, EntityId right) => !(left == right);
    }
}
