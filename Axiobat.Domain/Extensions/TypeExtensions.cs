﻿namespace Axiobat
{
    using App.Common;
    using Domain.Interfaces;
    using Domain.Enums;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// the <see cref="Type"/> extensions
    /// </summary>
    public static partial class TypeExtensions
    {
        /// <summary>
        /// extract the docType, this function will check if the given type has the <see cref="DocTypeAttribute"/>
        /// if does the attribute value will be returned otherwise the name of the class with first character lower case will be returned
        /// </summary>
        /// <param name="type">the type of the class to get the docType for it</param>
        /// <returns>the docType</returns>
        public static DocumentType DocType(this Type type)
        {
            var docType = type.GetAttribute<DocTypeAttribute>();
            if (docType is null)
                return DocumentType.Undefined;

            return docType.DocType;
        }

        /// <summary>
        /// get the last time the property with the given name has been updated in
        /// </summary>
        /// <param name="propertyName">the name of the property</param>
        /// <returns>the date</returns>
        public static DateTime? LastTimeUpdated(this IRecordable recordable, string propertyName)
            => recordable?.ChangesHistory?
                .Where(e => e.Action == ChangesHistoryType.Updated && e.Fields.Any(f => f.PropName == propertyName))
                .OrderByDescending(e => e.DateAction)
                .FirstOrDefault()?.DateAction;

        public static bool ListsEquals<T>(this IEnumerable<T> first, IEnumerable<T> secound)
        {
            if (first.Count() != secound.Count())
                return false;

            if (!first.Except(secound).Any())
                return false;

            return true;
        }
    }
}
