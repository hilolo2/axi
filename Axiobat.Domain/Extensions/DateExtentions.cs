﻿namespace Axiobat
{
    using System;

    public static class DateTimeExtensions
    {
        public static DateTime StartOfWeek(this DateTime date, DayOfWeek startOfWeek = DayOfWeek.Monday)
        {
            int difference = (7 + (date.DayOfWeek - startOfWeek)) % 7;
            return date.AddDays(-1 * difference).Date;
        }

        public static DateTime EndOfWeek(this DateTime date, DayOfWeek startOfWeek = DayOfWeek.Monday)
        {
            return date.StartOfWeek(startOfWeek).AddDays(6);
        }

        public static DateTime DayOfWeekDate(this DateTime date, DayOfWeek dayOfWeek, DayOfWeek startOfWeek = DayOfWeek.Monday)
        {
            var startOfWeekDate = date.StartOfWeek(startOfWeek);
            var daysToAdd = dayOfWeek - startOfWeek;
            if (dayOfWeek < startOfWeek)
                daysToAdd = 6;

            return startOfWeekDate.AddDays(daysToAdd);
        }

        public static DateTime NextWeek(this DateTime date, DayOfWeek startOfWeek = DayOfWeek.Monday)
        {
            return date.EndOfWeek().AddDays(1);
        }
    }
}
